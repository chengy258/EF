# RDD离散变量驱动问题
&emsp;

> **作者**: 邱紫烨（中山大学）
>
> **E-Mail:** <qiuzy@mail2.sysu.edu.cn>

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

&emsp;

## 1 背景介绍

谢谦等（2019）对目前学术界断点回归（regression-discontinuity designs)应用的最新进展做了详细的综述，但他们侧重于对五大期刊已经使用的RD方法进行介绍，对于还未出现的多配置变量RDD、分位数RDD、拐点回归设计、多断点ED、离散型配置变量RDD等新进展尚未详细设计。这篇文章参考2018年AER上的论文 *Inference in Regression Discontinuity Designs with a Discrete Running Variable* ，并对离散型变量配置RDD做简要的介绍。

## 2 RDD理论回顾以及存在问题

断点回归是一种随机试验，此种随机实验定义了这样一个特征，即接受处置(Treatment)的概率是一个或者几个变量的间断函数。Hahn et al(2001) 提出了断点回归的首要假设，如果变量d表示处置效应，x表示决定处置的关键变量，那么满足:

$$
d^+ \equiv lim_{x\rightarrow\ x^+_{0}}E[d_{i}| x_{i}=x]
$$

$$
d^- \equiv lim_{x\rightarrow\ x^-_{0}}E[d_{i}| x_{i}=x]
$$

$$
d^{+} \neq d^{-}
$$

在使用断点回归的情况下，存在一个变量，如果该变量大于一个临界值时，个体接受处置，而在该变量小于临界值时，个体不接受处置。一般而言，个体在接受处置的情况下，无法观测到其没有接受处置的情况。而在断点回归中，小于临界值的个体可以作为很好的控制组(Control Group)来反映个体没有接受处置时的情况，尤其是在变量连续的情况下，临界值附近样本的差值能够很好地反映出处置与否对某一经济变量的影响。

断点回归可以分为两类，一类拥有确定的临界值(Sharp RDD), 即在临界值一侧的所有观测点都接受了处置，反之，在临界值另一侧的所有观测点都没有接受处置。此时，接受处置的概率从临界值一侧的0跳转到另一侧的1。另一类的临界点是模糊的(Fuzzy RDD), 即在临界值附近，接受处置的概率是单调变化的。Hahn et al(2001) 在一定的假设下，证明了无论是哪一类型的断点回归，都可以利用临界值附近样本的系统性变化来研究处置与否和经济变量变动的因果关系，并提出了相应的估计方法，由此，断点回归在经济学中的应用逐渐普及开来。

估计ATE(Average Treatment Effect,平均处置效应)的经典方法是多项式回归，即通过选取一定的带宽，并对临界值两边窗口内的经济变量进行线性回归，根据临界值上经济变量值的跳跃得出平均处置效应。然而，由于真实的条件期望函数往往不是线性的，因而估计出的ATE往往是有偏误的。如果选取的带宽足够小，这种估计偏误在方差的影响下可以忽略不计，从而可以使用基于EHW异方差稳健标准误的置信区间进行因果推断。无论变量是连续还是离散的，这种方法都适用。然而，如果经济变量只取了个别数值，且在选取的临界值附近，变量之间的数值差距过大，那么研究者就不得不选择一个大的带宽以获得足够多的观察值，此时平均处置效应的估计偏误就难以忽略不计了(2008, LC from heron)。这种问题在断点实证检验的设计中广泛存在，例如学生的分数、学校的入学率、公司的员工数量以及个人出生年份等。现有研究主要通过对变量进行聚类来解决这个问题，即将值相同的变量聚类，并通过构造类稳健的标准误来估计处置效应的方差。

在离散型变量的情形中，这种聚类的方法可能无法解决ATE的估计偏误问题，下面将通过一个具体案例加以说明。

![Figure 1](https://s3.ax1x.com/2021/01/26/sOz9gO.md.png)

通过对一份2003年至2005年人口普查数据（共有170693个观察值）进行小样本随机抽样(样本量取100、500、2000、10000），Figure 1展示了年龄（横轴）与时薪取对数（纵轴）之间的关系，并以40岁作为临界值。可以发现，在临界值两端的时薪取值变没有明显的跳跃，说明平均处置效应不存在（为零）。
对每一个随机抽取的子样本，都按下式进行OLS估计（包括一次项回归和二次项回归）：
$$
log(WAGE_{i})= \alpha_{h} + \tau_{h} · \mathbb{I} \{AGE \geq 40 \}   + \sum_{j=1}^{p}\beta_{hj}^- · (AGE_{i}-40)^p + \sum_{j=1}^p\beta_{hj}^+ · \mathbb{I} \{ {AGE_{i} \geq 40} \} · (AGE_{i} - 40)^p + U_{i}
$$

利用上式，可以得出临界值上的处置效应，因为所估计的系数大小与所选取的带宽密切相关，所以每一个估计系数都用它所对应的带宽作为下标加以区分。因为我们拥有完整的样本，并能够利用全样本估计出真实的处置效应，从而将子样本的估计结果与全样本的估计结果进行比较。根据上图可以发现，临界值两端的经济变量并不是完全的线性形式，因此，OLS估计必然存在一定的偏误，而这种偏误可能会随着带宽的变动而发生改变（在Table 1 的Column 2中可以得到具体体现）。

为了进一步评估系数估计的准确性，文章还计算了每一个子样本估计得出的CRV和EHW标准误。由于模型存在估计偏误的问题，因此EHW标准误构造的置信区间只能在小于95%的概率下捕捉到这种零值效应，当方差足够大时，这种估计偏误可以忽略不计。此外，如果将变量聚类是合适的修正偏误的办法，那么CRV标准误应当大于EHW标准误，并且由CRV标准误构造的置信区间应该在大约95%的概率水平上捕捉到零值效应。

下表（Table 1）给出了估计的结果，在一次项回归中，保持样本量不变，系数估计存在向下偏误，且CRV标准误会随着带宽的增加而逐渐扩大。同时，尽管CRV标准误均大于EHW标准误，其所构造的置信区间在大样本情况下效果不佳。而在二次项回归中的情形中，保持样本量不变，无论选取的带宽大小如何，CRV标准误均较小，由其构造的置信区间都在95%的显著水平之下。

![Table 1](https://s3.ax1x.com/2021/01/26/sXSmw9.png)

当所估计的模型拥有较小的估计偏误时，标准误也往往较小，但会导致其构造的置信区间相应的缩小，当聚类数量较小时，这种问题尤为严重。在另一方面，当样本量的选取不确定时，往往存在比较严重的估计偏误现象，但此时的置信区间依然很狭窄。

## 3 数理证明
在清晰断点回归(Sharp RDD)中，我们从大样本中随机抽取N个观察值，让$Y_{i}(1)$ 和 $Y_{i}(0)$分别代表接受和没有接受处置的观察值因变量，并将$T_{i}\in\{0,1\}$作为观察值是否接受处置的指示变量。只有当变量达到经过正态化处理的临界值才会被标记为受到处置，即
$$
T_{i} =   \mathbb{I}\{ {X_{i}\geq0} \}
$$

若用$\mu(X_{i}) = E[Y_{i} | X_{i}$]表示期望值，那么可以由此得出平均处置效应(ATE, Average Treatment Effect)的表达式如下：
$$
\mu(X_{i}) = E(Y_{i}(1)-Y_{i}(0)|X_{i} = 0) = \lim_{x\downarrow0}\mu(x)-\lim_{x \uparrow0}\mu(x).
$$
用来估计ATE的方法主要是局部多项式回归，一般的步骤为，首先确定带宽$h > 0$和项数$p \geq 0$，其中，$p$一般取1或者2. 剔除带宽之外的所有样本，对$[-h,h]$内的样本进行$X_{i}$对$Y_{i}$的OLS回归，各个系数的数学表达式如下所示：
$$
\widehat{\tau} = e'_{1}\widehat{\theta},\ \theta= \widehat{Q}^{-1}\frac{1}{N_{h}}\sum_{i=1}^{N_{h}}M_{i}M_{i}', \ \ M_{i}=m(X_{i})  
$$

$$
m(x)= (\mathbb{I}\{ {x\geq0} \},\mathbb{I}\{ {x\geq0} \}x,...,\mathbb{I}\{ {x\geq0} \}x^p,1,x,...,x^p)'
$$

在系数估计的基础上，得出$\widehat{\tau}$的异方差稳健标准误$\widehat{\sigma}_{EHW}/\sqrt N_{h}$.为了解决离散型断点回归中EHW标准误可能存在的问题，引入了CRV标准误(2008,LC from heron),把拥有相同结果的经济变量归为同一类，具体表达式为

$$
\widehat{\sigma}_{CRV}/\sqrt N_{h}
$$

基于EHW标准误和CRV标准误分别构建出置信区间

$$
\widehat{\tau}\pm z_{1-\alpha/2} \times \widehat{\sigma}_{EHW}/\sqrt N_{h}, \ and  \ \ \  \ \  \widehat{\tau} \pm z_{1-\alpha/2} \times \widehat{\sigma}_{CRV}/\sqrt N_{h}
$$

那么，在离散型变量的情况下，基于EHW标准误构造的置信区间会产生什么问题呢？在有限样本中，$\widehat{\tau}$是$\tau_{h}$的渐近无偏估计：
$$
\tau_{h} =e_{1}'\theta_{h}, \ \ \theta_{h}= Q^{-1}E[M_{i}Y_{i}| |X_{i}|\leq h], \ \ Q = E[M_{i}M_{i}'| |X_{i}| \leq h]
$$

然而，$\widehat{\tau}$往往是$\tau$的有偏估计，估计偏误$\tau-\widehat{\tau}$的大小取决于p次多项式能够对于$\mu(x)$的拟合程度。在大样本情形下，EHW服从标准正态分布，即：
$$
\sqrt {N_{h}}(\widehat{\tau}-\tau_{h})/\widehat{\sigma}_{EHW}\longrightarrow^d\mathcal{N}(0,1).
$$

因此，基于EHW标准误构造的t统计量和置信区间能够用来估计$\tau_{h}$,如果估计偏误$\tau_{h}-\tau$能够依样本趋近于0，即
$$
\frac{\tau_{h}-\tau}{\widehat{\sigma}_{EHW}/ \sqrt {N_{h}}} \rightarrow ^P 0.
$$
成立，那么就能够用这个方法估计$\tau$，且无论在连续型还是离散型变量的情况下都适用。通过选取一个合适的带宽$h$使得上式成立的过程就是欠光滑过程(Undersmoothing).但是，如果临界值两端的变量值差距过大，为了拥有足够的样本，研究者不得不选取较大的带宽，使得最后的估计值出现偏误。

为了解决EHW标准误在离散型RDD中的估计偏误问题，LC引入了CRV标准误，并在随后被广泛使用(2008, LC from heron)。具体操作步骤如下：首先，令$\delta(x)= \mu(x)-m(x)'\theta_{h}$代表p次多项式对条件期望的估计偏误，并且令$\delta_{i}=\delta(X_{i})$.
接着，对于估计窗口内的变量，我们有：
$$
Y_{i}=M_{i}'\theta_{h}+u_{i}, \ \ u_{i}=\delta_{i}+\varepsilon_{i}
$$
其中，$\varepsilon_{i}=Y_{i}-\mu(X_{i})$真实值与期望值之差，且所有实现值相同的变量有相同的$\delta_{i}$。这样一来，$\delta_{i}$就变成了一个随机效应，而不是根据误差而发生改变的具体值，即

$$
\delta_{i}= \sum_{g=1}^{G_{h}}D_{g}\mathbb{I}\{ X_{i} = x_g \},
$$
其中，$D=(D_1,...,D_h)$是一列独立同分布的零均值随机向量。

## 4 置信区间的性质
通过讨论$\widehat{\sigma}_{EHW}^2$ 的性质，我们能够进一步讨论其对应的置信区间的性质。通过改变数据生成过程 $(DGPs,data \ generating \ process)$, 我们能够得到不同样本大小下$\mu(x)$的期望值。在$3.1$的假设下，存在常数$b$和$d_{1}$，使得当样本逐渐增加时，对每一个在窗口内的变量$x_{g}$,有
$$
\sqrt {N_{h}}(\tau_{h}-\tau) \rightarrow b \ \ and \ \ \sqrt {N_{h}}\delta(x_{g})\rightarrow d_{g}
$$

大样本情形下，$\sqrt {N_{h}}(\widehat{\tau}-\tau)$服从均值为零的正态分布，渐近方差为$\sigma_{\tau}^{2}=\sum_{g=1}^{G_{h}} \sigma_{g}^{2} \omega_{g}$，其中，$\omega_{g}=e_{1}^{\prime} Q^{-1} Q_{g} Q^{-1} e_{1}$。EHW方差估计值是$\sigma^2_{\tau}$的一致估计。

正如3.1中所讨论的，在$b$不为0的情况下，利用EHW标准误构造的置信区间往往会低估$\tau$，而利用CRV标准误$\widehat{\sigma}_{CRV} {/}\sqrt{N_{h}}$构造的置信区间也有相同的点估计，因此，为了让CRV置信区间有正确的覆盖范围，理论上CRV标准误平均值应该比EHW标准误平均值更大一些。

然而，事实往往并非如此：在正确识别的情况下，利用CRV标准误构造的置信区间往往会低估平均处置效应；而在错误识别的情况下，存在低估和高估两种可能。$\widehat{\sigma}_{CRV}^2$的渐近性质取决于估计矩内支持点数量是固定不变的还是会随着样本大小而发生改变。

首先，文章检验了在样本增加情况下保持估计窗口不变时$\widehat{\sigma}_{CRV}^2$的性质，并以此来模拟估计窗口$[-h,h]$内样本数量较少的情形。实证结果表明，$\widehat{\sigma}_{CRV}$并没有收敛到一个常数，而是收敛到一个非退化极限$\widehat{\sigma}_{CRV,\infty}^2$,这意味着即使在大样本情形下它依然是随机值。根据定理1，我们能够将$\widehat{\sigma}_{CRV,\infty}^2$与$\sigma^2_{\tau}$之间的期望差异分解为如下形式：
$$
\mathbb{E}\left(\widehat{\sigma}_{\mathrm{CRV}, \infty}^{2}\right)-\sigma_{\tau}^{2}=\sum_{g=1}^{G_{h}} d_{g}^{2} \pi_{g} \omega_{g}+\sum_{g=1}^{G_{h}} m\left(x_{g}\right)^{\prime} Q^{-1}\left(\sum_{j=1}^{G_{h}} \sigma_{j}^{2} Q_{j}-2 \sigma_{g}^{2} Q\right) Q^{-1} m\left(x_{g}\right) \cdot \pi_{g} \omega_{g}
$$
等式右边第一项是正值，大小取决于错误识别的程度，第二项与错误识别的程度无关，当将估计残差$\hat{\mu}_{i}$用真实残差值$\mu_{i}$替代时会使第二项变为0。第二项的符号往往难以判断，因此，需要假设条件方差服从渐近同分布，即$\sigma_{g}^{2}=\sigma^{2}$，从而使得$\sigma_{\tau}^{2}=\sigma^{2} \sum_{g=1}^{G_{h}} \omega_{g}$，将化简后的结果除以$\sigma_{\tau}^2$,可以得到期望差值的变化率：
$$
\frac{\mathbb{E}\left(\widehat{\sigma}_{\mathrm{CRV}, \infty}^{2}\right)-\sigma_{\tau}^{2}}{\sigma_{\tau}^{2}}=\frac{\sum_{g=1}^{G_{h}} d_{g}^{2} \pi_{g} \omega_{g}}{\sigma^{2} \sum_{g=1}^{G_{h}} \omega_{g}}-\frac{\sum_{g=1}^{G_{h}} m\left(x_{g}\right)^{\prime} Q^{-1} m\left(x_{g}\right) \pi_{g} \omega_{g}}{\sum_{g=1}^{G_{h}} \omega_{g}} \equiv T_{1}+T_{2}
$$
由于EHW方差估计量是$\sigma_{\tau}^2$的一致估计，根据上式，可以推断出$\widehat{\sigma}_{CRV}^2$的值大致是$\widehat{\sigma}_{EHW}^2$的$(1+T_{1}+T_{2})$倍。为了使CI置信区间能够很好地反映错误识别的程度，$(T_{1}+T_{2})$应该为正数，但事实往往并非如此。

首先，$T_{1}$项确实是正值，且会随着估计偏误程度的增大而增加。而$T_{2}$项在同方差假设下是负值，因为$m\left(x_{g}\right)^{\prime} Q^{-1} m\left(x_{g}\right)$和$\omega_{g}$都是正值，这一假设在轻微异方差的情况下同样成立。而$T_{2}$的大小只与连续变量的边缘分布有关，因此，如果误差项的方差$\sigma^2$非常大的话，$T_{2}$就会成为整个式子正负符号的决定项。此外，$T_{2}$还与最高次项系数$p$有关。根据$\sum_{g=1}^{G_{h}} m\left(x_{g}\right)^{\prime} Q^{-1} m\left(x_{g}\right) \pi_{g}=\operatorname{trace}\left(\sum_{g=1}^{G_{h}} Q_{g} Q^{-1}\right)=2(p+1)$可见，$T_{2}$项是对$2(p+1)$的再次加权平均，次项数$p$越高$T_{2}$也会随之增加，这也就意味着$p$的增加会增大$\widehat{\sigma}_{\mathrm{CRV}, \infty}^{2}$与$\sigma_{\tau}^{2}$之间的期望差异。最后，$T_{2}$与小样本估计下的衰减偏误有一致的趋势。尽管理论上这种估计偏误非常普遍，但它对于小样本估计有很大影响。

总之，理论结果显示，如果错误识别的程度和聚类个数都可以忽略不计的话，CRV标准误的值会比EHW标准误的值更小些。因此，使用连续变量聚类会加剧而不是解决估计偏误的问题，这种情况在正确识别的情形下尤为严重。

接着，文章考察了随着带宽$G_{h}$和样本量的增加，$\widehat{\sigma}_{CRV}^2$的性质会发生什么样的变化，并以此来模拟估计窗口$[-h,h]$内有大量样本点的情形。随着$G_{h}$和$N_{h}$的无限增大，$\widehat{\sigma}_{CRV}^2$会逐渐趋于0，由此我们得出：
$$
\widehat{\sigma}_{\mathrm{CRV}}^{2}-\sigma_{\tau}^{2}=\sum_{g=1}^{G_{h}} d_{g}^{2} \pi_{g} \omega_{g}+o_{P}(1)
$$
这意味着在样本量足够大的情况下，CRV标准误确实比EHW标准误更大些，此时对连续变量进行聚类有更好的估计效果。但这并不意味着CRV置信区间能够对所有的期望误差都有正确的估计，只是表明在估计窗口$[-h,h]$内存在这样一个期望函数集$\mathcal{M}_{\mathrm{CRV}}$能够得出一致的估计。但这一函数集包含的内容要远远不止多项式方程，所以这并不能成为使用CRV标准误的理由。事实上，任何使得CRV置信区间宽于EHW置信区间的函数形式（包括加入一个随机的常数项）都能带来更好的估计效果。所以，判断基于CRV标准误的估计结果是否稳健要看$\mathcal{M}_{\mathrm{CRV}}$中是否有清晰易解释的条件期望函数，以帮助对$\mu(x)$做出合理的假设。然而，$\mathcal{M}_{\mathrm{CRV}}$的范围受到$\omega_{g}$和$\pi_{g}$的影响，而这取决于连续变量的分布情况。因此，当断点附近的样本量不同时，就算是相同的断点设计可能也会得到不同的结果。

基于这些原因，当样本点数量足够多并且存在估计偏误的可能时，一个科学的对策是选择更小的带宽。此外，文章还提出了利用“真实置信区间”(Honest Confidence Interval)推断的方法，以获得良好的估计效果，下一部分将对此做进一步阐述。

## 5 真实置信区间(Honest Confidence Interval)

如果$\mu(x)$的条件期望函数在临界值两端能够随意变动的话，任何一条拟合的直线都能给出一个$\tau$的预测值，此时难以对ATE进行有效且正确的推断。因此，需要对$\mu(x)$作出进一步限制：假设$\mu \in \mathcal{M}$，其中$ \mathcal{M}$是一个函数集，并且构造出满足如下条件的置信区间：
$$
\liminf _{N \rightarrow \infty} \inf _{\mu \in \mathcal{M}} P_{\mu}\left(\tau \in C^{1-\alpha}\right) \geq 1-\alpha
$$
其中$P_{\mu}$表明置信区间的覆盖效果很大程度上受期望函数形式的影响，将其称作真实地反映了$ \mathcal{M}$的形式，一般来说，置信区间最好能够反映清晰易解释、有意义的函数形式。RDHonest是R中的一个包，能够构造出真实置信区间，并通过加入两条约束条件证明$\mu(x)$能用p次多项式进行良好的拟合。

首先对$\mu(x)$的二阶导形式进行限制，也就是对函数形式的平滑程度进行限制。首先假设临界值两端的$\mu(x)$二阶可微，且两端期望函数二阶导的差值为常数$K$。通过假设$K$趋近于0，可以将函数形式限定较为光滑的线性函数，而大的$K$值则代表着更不光滑的函数形式。只需要假设$\mu(x)$处处二阶可导，得到如下定义：
$$
\mathcal{M}_{\mathrm{H}}(K)=\left\{\mu:\left|\mu^{\prime}(a)-\mu^{\prime}(b)\right| \leq K|a-b| \text { for all } a, b \in \mathbb{R}_{-} \text {and all } a, b \in \mathbb{R}_{+}\right\}
$$

基于连续变量的真实置信区间更好构造，用$\tilde{\tau}_{h}=\mathbb{E}\left(\widehat{\tau} \mid X_{1}, \ldots, X_{N_{h}}\right)$指代处置效应估计系数的条件期望，$\widehat{\sigma}_{\mathrm{NN}}^{2} / N_{h}$指代$\widehat{\tau}$的条件方差$\mathbb{V}\left(\widehat{\tau} \mid X_{1}, \ldots, X_{N_{h}}\right)$的最近邻估计。则估计系数可以表达为如下形式：

$$
\widehat{\sigma}_{\mathrm{NN}}^{2}=e_{1}^{\prime} \widehat{Q}^{-1} \widehat{\Omega}_{\mathrm{NN}} \widehat{Q}^{-1} e_{1}, \quad \widehat{\Omega}_{\mathrm{NN}}=\frac{1}{N_{h}} \sum_{g=1}^{G_{h}} n_{g} \widehat{\sigma}_{g}^{2} m\left(x_{g}\right) m\left(x_{g}\right)^{\prime}
$$

其中，$n_{g}$是满足$X_{i}=x_{g}$的观察值的个数，$\widehat{\sigma}_{g}^{2}=\sum_{i: X_{i}=x_{g}}\left(Y_{i}-\bar{Y}_{g}\right)^{2} /\left(n_{g}-1\right)$是条件方差$\sigma_{g}^{2}=\mathbb{V}\left(Y_{i} \mid X_{i}=x_{g}\right)$的无偏估计，且$\bar{Y}_{g}=n_{g}^{-1} \sum_{i: X_{i}=x_{g}} Y_{i}$。由此，t统计量可以由下式得出：

$$\frac{\widehat{\tau}-\tau}{\widehat{\sigma}_{\mathrm{NN}} / \sqrt{N_{h}}}=\frac{\widehat{\tau}-\tilde{\tau}_{h}}{\widehat{\sigma}_{\mathrm{NN}} / \sqrt{N_{h}}}+\frac{\tilde{\tau}_{h}-\tau}{\widehat{\sigma}_{\mathrm{NN}} / \sqrt{N_{h}}}$$

中心极限定理使得上式第一部分在大样本情形下服从标准正态分布，而第二项拥有一个上界值$r_{\mathrm{sup}}=\frac{\sup _{\mu \in \mathcal{M}_{H}(K)}\left|\tilde{\tau}_{h}-\tau\right|}{\widehat{\sigma}_{\mathrm{NN}} / \sqrt{N_{h}}}$。
由于上界值是由$\mu(x)$决定的，当$x \geq 0$时为$-K x^{2}$，当$x<0$时为$K x^{2}$，可以将第二项的上界值表示为如下形式：

$$r_{\text {sup }}=-\frac{K}{2} \frac{\sum_{i=1}^{N_{h}} w\left(X_{i}\right) X_{i}^{2} \operatorname{sign}\left(X_{i}\right)}{\widehat{\sigma}_{\mathrm{NN}} / \sqrt{N_{h}}}, \quad w\left(X_{i}\right)=\frac{1}{N_{h}} \cdot e_{1}^{\prime} \widehat{Q}^{-1} M_{i}$$
这种对二阶导设定上界值的方式$(BDS,Bounding the Second Derivative)$方式可以构造出如下置信区间：
#### *Proposition 1*

令$CV_{1-{\alpha}}(r)$指代$\mid \mathcal{N}(r, 1) \mid$分布的$1-\alpha$分位数，那么所构造的置信区间：

$$C_{\mathrm{BSD}}^{1-\alpha}=\left(\hat{\tau} \pm \mathrm{cv}_{1-\alpha}\left(r_{\text {sup }}\right) \times \widehat{\sigma}_{\mathrm{NN}} / \sqrt{N_{h}}\right)$$

是反映$\mathcal{M}_{\mathrm{H}}(K)$的真实置信区间。这一置信区间适用于离散型和连续型两种情形，并且由于考虑进了有限样本情形下估计偏误问题，因此能够适用于不同的带宽。

第二是对临界值的设定误差进行限制，即所选择的临界值在给定带宽内能够得出拟合效果最好的模型，并且临界值的左极限$ \lim_{x\uparrow 0}\delta(x)$ 不能大于所有临界值以下的变量的预测误差，临界值的右极限同理。

#### *Proposition 2*

利用$C_{\mathrm{BME}}^{1-\alpha} \equiv\left(\min _{W \in \mathcal{W}} c_{L}^{1-\alpha / 2}(W), \max _{W \in W} c_{R}^{1-\alpha / 2}(W)\right)$能够构造出$\mathcal{M}_{\mathrm{BME}}(h)$的真实置信区间。

## 6 RDHonest命令介绍及R语言使用示例

RDHonest是R语言内置的包，能够通过参数设定估计出真实置信区间，下面将对它的具体用法做一个简单的说明。
### 6.1 RDHonest命令下载

``` R
install.packages("remotes") # if not installed
remotes::install_github("kolesarm/RDHonest")
help("RDHonest") #下载后可以进一步阅读帮助文档
``` 

### 6.2 语法结构与可选项
```R
RDHonest(
  formula, ## 拟合方程
  data,    ## 数据集
  subset,  ## 可选项，选择子数据集进行拟合
  weights, ## 可选项，各个观察变量的权重
  cutoff = 0,  ## 临界值
  M, ## 条件均值函数二阶导的界限
  kern = "triangular", ## 局部回归中使用的核函数形式，主要包括triangular, uniform和epanechnikov
  na.action,  ## 数据存在缺失值时的常规操作，通常使用na.omit进行清理
  opt.criterion,  ##带宽需要最大化的判断准则，包括MSE、FLCI和OCI
  bw.equal = TRUE, ## 逻辑值，表示临界值两端的带宽是否要相等
  h, ## 带宽的参数
  se.method = "nn",  ## 标准误的估计方法，如果这一项缺失，结果将不予报告
                     ## 常见的方法有nn（近邻法）、EHW、demeaned、plugin和supplied.var等
  alpha = 0.05,  ## 置信水平，1-alpha用以构造置信区间
  J = 3,  ## 使用nn法进行标准误估计时，近邻的数量
  sclass = "H",  ## 平滑类别，T for Taylor， H for Hölder 
  order = 1,  ## 局部回归的项数，1代表线性回归，2代表二次项回归
  se.initial = "EHW" ## 估计原始方差的方法，包括EHW、demeaned、Silverman、SilvermanNN和nn等
)
```

### 6.3 在清晰断点回归模型（Sharp RD）中的运用
Oreopoulos（2006）的文章考察了最小离校年龄的变动对学校参与率和工人收入的影响，这项变动分别于1947年在大不列颠(包括英格兰、苏格兰和威尔士)和1957年在北爱尔兰发生，数据样本为在1935年-1965年间年龄14岁的英国工人。Oreopoulos使用了清晰断点回归来估计最低离校年龄的提高对英国工人日后收入的影响，解释变量是工人到达14岁的年份，临界值为1947年，被解释变量为收入对数，作者基于此构建了如下识别方程：
$$
\log \left(\operatorname{EARN}_{i}\right)=\beta_{0}+\tau \cdot \mathbb{I}\left\{\operatorname{YEAR} 14_{i} \geq 1947\right\}+\sum_{k=1}^{4} \beta_{k} \cdot \operatorname{YEAR} 14_{i}^{k}+U_{i}
$$

下面将利用这一数据集和识别方程对RDHonest的使用做一个简要介绍。

首先，加载需要的包，并对数据进行预处理和可视化，由于数据集中解释变量为离散型，因此需要计算每个变量的均值，这一操作将通过设定 *avg = Inf* 来实现，并利用plot_RDscatter进行绘图，有时需要对点的大小进行调整。可视化结果如下：

![Figure 2](https://s3.ax1x.com/2021/01/26/sXSRkn.png)

具体代码如下：
```R
library("RDHonest")
## 数据预处理
#假设第一列是结果，第二列是变量
#Oreopoulos06的数据
# 对收入变量取对数
do <- RDData(data.frame(logearn = log(cghs$earnings),year14 = cghs$yearat14),
             cutoff = 1947)
## 可视化
## Figure 1 : Oreopoulos(2006) data
f2 <- plot_RDscatter(do,avg = Inf, xlab = "Year aged 14",
                     ylab = "Log earnings", propdotsize =  TRUE)
## 根据点的个数调整图像大小
f2 + ggplot2::scale_size_area(max_size = 4)
```
接着，使用包内置的RDHonest和RDHonestBME命令对真实置信区间进行估计，并对文章结果进行复现，此处以第(11)列为例，其他列的结果可以通过调整参数得到，具体调整方法见下代码。
```R
## Replicate Table2 Column(2)~(7),run local linear 
## 通过调整h的参数为Inf/6/3，调整order的参数为1/2/4，以得到(2)~(7)列的结果
RDHonestBME(log(earnings) ~ yearat14,cutoff = 1947, data = cghs, h = 3, order = 2)
## 结果与Table 2 Column (5) 相一致
Call:
RDHonestBME(formula = log(earnings) ~ yearat14, data = cghs,     cutoff = 1947, h = 3, order = 1)
Confidence intervals:
(-0.06965587, 0.2019889)
## Replicate Table2 Column(8)~(11), run local linear
## 调整M的参数为0.004/0.002/0.04/0.2以得到(8)~(11)列的结果，此处以第(11)列为例
RDHonest(log(earnings)~yearat14,cutoff = 1947, data = cghs,
         kern = "uniform", M = 0.2, opt.criterion = "FLCI",
         sclass = "H")
## 结果与Table 2 Column (11) 相一致
Call:
RDHonest(formula = log(earnings) ~ yearat14, data = cghs, cutoff = 1947, 
    M = 0.2, kern = "uniform", opt.criterion = "FLCI", sclass = "H")
Inference by se.method:
     Estimate Maximum Bias Std. Error
nn 0.07909463    0.2368293 0.06784089

Confidence intervals:
nn    (-0.269323, 0.4275122), (-0.269323, Inf), (-Inf, 0.4275122)
Bandwidth: 2
Number of effective observations: 2017.075 
## 选取triangular作为kernel往往会比选取uniform作为kernel得到更范围更窄的置信区间
RDHonest(log(earnings)~yearat14,cutoff = 1947, data = cghs,
         kern = "triangular", M = 0.2, opt.criterion = "FLCI",
         sclass = "H")
Call:
RDHonest(formula = log(earnings) ~ yearat14, data = cghs, cutoff = 1947, 
    M = 0.2, kern = "triangular", opt.criterion = "FLCI", sclass = "H")
Inference by se.method:
     Estimate Maximum Bias Std. Error
nn 0.08367602          0.2 0.06873519

Confidence intervals:
nn    (-0.2293833, 0.3967354), (-0.2293833, Inf), (-Inf, 0.3967354)

Bandwidth: 2
Number of effective observations: 1696.793 
```
有时，解释变量是作为一个个箱体进入解释方程的，可以通过确定箱体的数量，并对数据集进行处理，具体操作如下:
```R
## 加权回归
d <- cghs
## 生成一个新变量mod取值为0-19
d$mod <- seq_along(d$yearat14)%%20
## 让箱体作为组别和年份的交乘项
d$cell <- d$mod/100 + d$yearat14
## 计算出每个箱体内的平均值
dd <- data.frame()
for (j in unique(d$cell)){
  dd <- rbind(dd,data.frame(y=mean(log(d$earnings)[d$cell == j]),
                            x = mean(d$yearat14[d$cell == j]), weights = length(d$yearat14[d$cell == j])))
}
```
### 6.4 在模糊断点回归模型（Fuzzy RD）中的运用

在 Battistin et al. [2009] 的文章中，解释变量是达到法定退休年龄的时间，处置变量为是否退休的指示变量，临界值取0，并且删去了更好位于临界值上的样本。此时，与RDData命令类似，FRDData能够将数据转换为方便进行断点回归的形式：
```R
dr <- FRDData(cbind(logf=log(rcp[,6]),rcp[,c(3,2)]),cutoff = 0)
```
使用FRDHonest命令，能够构造出真实置信区间，带宽既可以自己设定，也可以让命令自行计算最优带宽。
```R
## 初始带宽,处置效应的初始估计值为0
r <- FRDHonest(log(cn)~retired | elig_year,data = rcp,kern = "triangular", M = c(0.001,0.002),opt.criterion = "MSE",
               sclass = "H",T0=0)
## 初始的处置效应和带宽的估计值如下
Call:
FRDHonest(formula = log(cn) ~ retired | elig_year, data = rcp, 
    M = c(0.001, 0.002), kern = "triangular", opt.criterion = "MSE",     sclass = "H", T0 = 0)
Inference by se.method:
      Estimate Maximum Bias Std. Error
nn -0.08669925   0.04751076 0.06665954

Confidence intervals:
nn    (-0.2445288, 0.07113025), (-0.2438552, Inf), (-Inf, 0.07045669)

Bandwidth: 10.19201
Number of effective observations: 1620.518 
## 自动计算最优带宽（需要提供初始带宽，即将T0=0更换为T0=r$estimate）
r <- FRDHonest(log(cn)~retired | elig_year,data = rcp,kern = "triangular", M = c(0.001,0.002),opt.criterion = "MSE",
               sclass = "H",T0=r$estimate)
## 最优带宽的计算结果如下
Call:
FRDHonest(formula = log(cn) ~ retired | elig_year, data = rcp, 
    M = c(0.001, 0.002), kern = "triangular", opt.criterion = "MSE",     sclass = "H", T0 = r$estimate)
Inference by se.method:
      Estimate Maximum Bias Std. Error
nn -0.09062174   0.04374351 0.07162249
Confidence intervals:
nn    (-0.2535519, 0.07230846), (-0.2521738, Inf), (-Inf, 0.07093029)
Bandwidth: 9.551739
Number of effective observations: 1485.949 
## 自动计算最优带宽（直接使用RDOptBW命令计算）
FRDOptBW(log(cn)~retired | elig_year,data = rcp,kern = "triangular", M = c(0.001,0.002),opt.criterion = "MSE",
         sclass = "H",T0=r$estimate)
## 可以发现两种方法得出的最优带宽是一致的
Call:
FRDOptBW(formula = log(cn) ~ retired | elig_year, data = rcp, 
    M = c(0.001, 0.002), kern = "triangular", opt.criterion = "MSE",     sclass = "H", T0 = r$estimate)
Bandwidth: 9.551739
```
## 7 总结
离散变量驱动的RDD在实证研究中十分常见，本文通过实例指出常用的由连续变量聚类生成的置信区间标准误存在准确度上的缺陷。针对这个问题，文章提出如下解决方案：首先，不应该对连续变量和离散变量做过于武断的区分。如果该离散变量在断点附近有大量的数据支撑的话，可以通过缩小带宽以达到降低估计偏误的目的，并使用EHW(Eicker-Huber-White)异方差稳健标准误进行因果推断。反之，如果离散变量在断点附近的数据量较少，样本方差不足以使得系数的估计偏差能够忽略不计，那么这个时候离散变量的断点回归估计就会产生问题。如果想要直接解决这种估计偏误问题，可以通过使用文章提供的两种“真实置信区间” (Honest CIs) 方法进行估计。

## 8 参考资料
- Kolesár, Michal, and Christoph Rothe. “Inference in Regression Discontinuity Designs with a Discrete Running Variable.” The American Economic Review, vol. 108, no. 8, 2018, pp. 2277–2304. [[PDF]](https://arxiv.org/pdf/1606.04086.pdf)，[[R-Data-Prog]](https://github.com/kolesarm/RDHonest)

- Lee D S . Randomized Experiments from Non-Random Selection in US House Elections[J]. Journal of Econometrics, 2008, 142(2):675-697.[[PDF]](http://www.princeton.edu/~davidlee/wp/RDrand.pdf)

- Oreopoulos, Philip. Estimating Average and Local Average Treatment Effects of Education when Compulsory Schooling Laws Really Matter.[J]. American Economic Review, 2006.[[PDF]](http://faculty.smu.edu/millimet/classes/eco7377/papers/oreopoulos.pdf)

- Battistin E , Brugiavini A , Weber R G . The Retirement Consumption Puzzle: Evidence from a Regression Discontinuity Approach[J]. American Economic Review, 2009, 99(5):2209-2226.[[PDF]](https://www.ifs.org.uk/wps/wp0805.pdf)

