**作者**：黄子瑜 (中山大学)    
**邮箱**：<huangzy86@mail2.sysu.edu.cn>  

**备注**：本文翻译自 Vincent Spruyt 的推文 [Feature extraction using PCA](https://www.visiondummy.com/2014/05/feature-extraction-using-pca/) 。特此致谢！

[TOC]

# 使用PCA提取特征

在本文中，我们将讨论主成分分析（PCA）的工作原理，以及它如何被用作分类问题的降维技术。

维数诅咒表明在高维空间中，分类器倾向于过度拟合训练数据。随之而来的问题是哪些特征应该保留，哪些应该从高维特征向量中删除。如果该特征向量的所有特征在统计上是独立的，则可以通过各种特征选择方法找到最小的判别特征，并直接从这个向量中消除最小的判别特征。但是在实践中，许多特征相互依赖或都依赖于底层未知变量。因此，一个特征可以由一个值表示多种类型信息的组合。除去这样的一个特征将删除更多的信息。接下来，我们将介绍使用PCA作为特征提取来解决这个问题，并从两个不同的角度介绍其内部运作。

## 一、PCA——一种去相关方法

通常，特征都是相关的。例如，我们想用一幅图中每个像素的红，绿和蓝分量进行分类（例如，检测狗与猫）。对红光最敏感的图像传感器也能捕捉一些蓝光和绿光。类似地，对蓝光和绿光最敏感传感器对红光也表现出一定程度的敏感度。其结果是，一个像素的R，G，B分量在统计上是相关的。因此，如果直接从特征向量中消除R分量也会删除一些有关G和B的信息。换句话说，消除特性之前，我们希望改变整个特征空间从而获得底层不相关分量。

下面我们考虑一个二维特征空间的例子。

通过两个一维高斯特征向量x1~N（0,1）和x2~N（0,1）的线性组合生成其原始数据： 
$$
x = x_2 + x_1\\
y = x_2 - x_1
$$

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-原始数据图.png)

[^图1]: 用颜色表示特征向量的二维相关数据

图1所示的特征x和y显然相关。实际上，它们的协方差矩阵是：
$$
\sum = \left[\begin{matrix}
16.87 & 14.94\\
14.94 & 17.27
\end{matrix}\right]
$$
Stata代码如下：

```
clear all
//生成白数据
set seed 123456  
set obs 1000
gen x1 = rnormal()
set seed 654321
set obs 1000
gen x2 = rnormal()
//生成原始数据
gen x = x2 + x1
gen y = x2 - x1
//求原始数据特征向量
pca x y
//定义原始数据特征向量矩阵
mat V = (-0.7071 , 0.7071 \ 0.7071 , 0.7071)
//求原始数据协方差矩阵
corr x y,cov
matrix rename r(C) cov1
//由原始数据构造已知协方差矩阵的数据（过程参考《协方差矩阵的几何解释》）
mat A = inv(cov1)*(16.87, 14.94 \ 14.94, 17.27)
mat list A
mata
A = (8.9708118 , 8.0116408 \ 7.2611513 , 8.3177799)
B = matpowersym(A,0.5)
B
end
//根据求出的旋转矩阵B求出图1所需数据
gen xn = 2.503596318 *x + 1.64402472*y
gen yn = 1.64402472*x + 2.369591235*y
//重命名，便于后续作图
drop x y
rename xn x
rename yn y
//求x,y的协方差矩阵cov2
corr x y,cov
matrix rename r(C) cov2
//求cov2的特征向量与特征值
mat symeigen H1 L1 =  cov2
mat list H1 //特征向量矩阵（每列表示一个特征向量）
H1[2,2]
           e1          e2
x   .71419036  -.69995152
y   .69995152   .71419036

mat list L1 //特征值
L1[1,2]
           e1         e2
r1  32.769239  1.3190747

//根据数据的特征向量与特征值设置向量坐标
gen a = -0.71419036*32.769239 in 1
replace a = 0.71419036*32.769239 in 2
gen vmax = -0.69995152*32.769239 in 1
replace vmax = 0.69995152*32.769239 in 2
gen b = -0.69995152*1.3190747 in 1
replace b = 0.69995152*1.3190747 in 2
gen vmin = 0.71419036*1.3190747 in 1
replace vmin = -0.71419036*1.3190747 in 2
//画矢量图
twoway(scatter y x, msize(0.5) xlabel(-15(5)15) ylabel(-15(5)15))(line vmax a, lwidth(thick) lcolor(green))(line vmin b , lwidth(thick) lcolor(pink))
```

由于特征x和y是一些底层未知成分x1和x2的线性组合，直接消除x或y会除去来自x1和x2的一些信息。相反，用协方差矩阵的特征向量来旋转数据，使我们能够直接恢复独立成分x1和x2。原始数据协方差矩阵的特征向量是（每列表示一个特征向量）：
$$
V = \left[\begin{matrix}
-0.7071 & 0.7071\\
0.7071 & 0.7071
\end{matrix}\right]
$$
由推文[A geometric interpretation of the covariance matrix](https://www.visiondummy.com/2014/04/geometric-interpretation-covariance-matrix/)可知，协方差矩阵可以被分解成不相关的白数据上的一系列旋转和缩放操作，其中，旋转矩阵用协方差的特征向量定义。因此，直观来看，图1中所示的数据D通过旋转每个数据点可以去相关，使得特征向量V成为新的基准轴：
$$
\begin{equation}
D' = VD\tag{1}
\end{equation}
$$
在此情况下V是旋转矩阵，对应于45°（cos（45°）=0.7071）的旋转。其次，将V看做一个线性变换矩阵得到一个新的坐标系，使得每个新特征x'和y'用原始特征x1和x2的线性组合来表示：
$$
\begin{equation}
x' = -0.7071x + 0.7071y = -0.7071(x_2 + x_1) + 0.7071(x_2 - x_1) = -1.4142x_1\tag{2}
\end{equation}
$$
$$
\begin{equation}
y' = 0.7071x + 0.7071y = 0.7071(x_2 + x_1) + 0.7071(x_2 - x_1) = 1.4142x_2\tag{3}
\end{equation}
$$

换言之，特征空间的去相关对应于数据中未知的、不相关的分量x’和y’的恢复（如果变换矩阵是不正交的，则缩放因子不能得知）。一旦这些成分被恢复，降低特征空间的维数就变得很容易了，只需要简单地删除x1或x2即可。

经过变换后，我们得到图2。其协方差矩阵现在是对角的，这意味着新的轴是不相关的：
$$
\sum' = \left[\begin{matrix}
1.06 & 0.0\\
0.0 & 16.0
\end{matrix}\right]
$$
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-变换后不相关特征图.png)

[^图2]: 用颜色表示特征向量的二维不相关数据

Stata代码如下：

```
//获得x'与y'
gen xp = -1.4142*x1
gen yp = 1.4142*x2
//其余过程与图1类似，此处不赘述，仅给出作矢量图的代码
twoway(scatter yp xp, msize(0.5) xlabel(-15(5)15) ylabel(-15(5)15))(line vpmax ap, lwidth(thick) lcolor(green))(line vpmin bp , lwidth(thick) lcolor(pink))
```

继续通过PCA进行降维，只需将数据投影到协方差矩阵的最大特征向量上。对于上面的例子，所得到的一维特征空间如图3所示。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-一维数据图.png)

[^图3]: 二维数据投影到其最大特征向量上

Stata代码如下：

```
gen z = 0
//注：由于stata中没有作一维图的函数，故对横轴标签采取以“ ”填充的方法获得一维效果图
twoway(scatter yp z, msize(0.5) xlabel(-15 " " -10 " " -5 " " 0 " " 5 " " 5 " " 10 " " 15 " ") ylabel(-15(5)15))(line vpmax ap, lcolor(pink))
```

在上述例子中，我们以一个二维问题开始。如果我们想降低维数，问题仍然是是否消除x’或y’。尽管这样的选择可能取决于许多因素，如分类问题中数据的可分性，但是PCA简单地假定最有趣的特征是具有最大方差的那一个。这种假设是基于信息论的角度，由于最大方差的维对应于最大熵的维，因此编码的信息最多,往往对应于定义数据的主要成分。最小的特征向量则通常简单地表示噪声分量。

上面的例子很容易推广到更高维的特征空间。例如，在三维情况下，我们既可以将数据投影到两个最大特征向量定义的平面来获得二维特征空间，也可以将其投影到最大特征向量来获得一维特征空间。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-原始数据图（不含向量坐标）.png)

Stata代码如下：

```
clear all
//生成随机数据
mat s = (2,2,2) //skew factor
set seed 1234
set obs 100000
gen x = rnormal() //生成服从标准正态分布的随机数
mkmat x in 1/334, matrix(x) //将变量x转化为334×1的矩阵
mat s1 = s[1,1]*x
mat s2 = s[1,2]*x
mat s3 = s[1,3]*x
//构建循环，生成334×3的随机数据矩阵
mat y1 = J(334,1,0)
forvalues i = 1(1)334{
scalar a1 = rnormal(s1[`i',1],1) + 3
mat y1[`i',1] = a1
}
mat y2 = J(334,1,0)
forvalues i = 1(1)334{
scalar a2 = rnormal(s2[`i',1],1) + 2
mat y2[`i',1] = a2
}
mat y3 = J(334,1,0)
forvalues i = 1(1)334{
scalar a3 = rnormal(s3[`i',1],1) + 1
mat y3[`i',1] = a3
}
mat data = [y1,y2,y3]
//将矩阵转为变量，以便后续绘图
svmat data, names(y)
//绘制原始数据图
scat3 y1 y2 y3, rotate(45) elevate(45) symbol(circle) spikes(lp(dot) lw(none)) mc(red) msize(0.5)
```

接下来我们用matlab作矢量图，通过计算特征向量来手动执行PCA。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-原始三维数据矢量图.png)

[^图4.1]: 原始三维数据矢量图

```matlab
clear all;
close all;
%构造随机数据
s = [2 2 2];  % 倾斜因子
x = randn(334,1);
y1 = normrnd(s(1).*x,1)+3;
y2 = normrnd(s(2).*x,1)+2;
y3 = normrnd(s(3).*x,1)+1;
data = [y1 y2 y3];
%得到数据平均值的坐标
avg = mean(data);
X0=avg(1);
Y0=avg(2);
Z0=avg(3);
%绘制原始数据矢量图
scatter3(data(:,1), data(:,2), data(:,3), 5, data(:,3), 'filled');
colormap(gray);
%计算特征向量与特征值
covariance = cov(data);
[eigenvec, eigenval ] = eig(covariance);
% 获得最大特征向量的坐标
largest_eigenvec = eigenvec(:, 3);
largest_eigenval = eigenval(3,3);
medium_eigenvec = eigenvec(:, 2);
medium_eigenval = eigenval(2,2);
smallest_eigenvec = eigenvec(:, 1);
smallest_eigenval = eigenval(1,1);
% 绘制特征向量
hold on;
quiver3(X0, Y0, Z0, largest_eigenvec(1)*sqrt(largest_eigenval), largest_eigenvec(2)*sqrt(largest_eigenval), largest_eigenvec(3)*sqrt(largest_eigenval), '-m', 'LineWidth',3);
quiver3(X0, Y0, Z0, medium_eigenvec(1)*sqrt(medium_eigenval), medium_eigenvec(2)*sqrt(medium_eigenval), medium_eigenvec(3)*sqrt(medium_eigenval), '-g', 'LineWidth',3);
quiver3(X0, Y0, Z0, smallest_eigenvec(1)*sqrt(smallest_eigenval), smallest_eigenvec(2)*sqrt(smallest_eigenval), smallest_eigenvec(3)*sqrt(smallest_eigenval), '-r', 'LineWidth',3);
hold on;
%设置轴坐标
hXLabel = xlabel('x');
hYLabel = ylabel('y');
hZLabel = zlabel('z');
xlim([-10,10]);
ylim([-10,10]);
zlim([-10,10]);
title('Original 3D data');
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-去相关数据三维图.png)

[^图4.2]: 去相关数据三维图

```matlab
%中心化数据
data = data-repmat(avg, size(data, 1), 1);
%标准化数据
stdev = sqrt(diag(covariance));
data = data./repmat(stdev', size(data, 1), 1);
%数据去相关
decorrelateddata = (data*eigenvec);
%绘制解相关数据图
figure;
scatter3(decorrelateddata(:,1), decorrelateddata(:,2), decorrelateddata(:,3), 5, decorrelateddata(:,3), 'filled');
colormap(gray);
%绘制特征向量(即轴(0,0,1),(0,1,0),(1,0,0)),中心化数据的均值在(0,0,0)
hold on;
quiver3(0, 0, 0, 0, 0, 1*sqrt(largest_eigenval), '-m', 'LineWidth',3);
quiver3(0, 0, 0, 0, 1*sqrt(medium_eigenval), 0, '-g', 'LineWidth',3);
quiver3(0, 0, 0, 1*sqrt(smallest_eigenval), 0, 0, '-r', 'LineWidth',3);
hold on;
%设置轴坐标
hXLabel = xlabel('x');
hYLabel = ylabel('y');
hZLabel = zlabel('z');
xlim([-5,5]);
ylim([-5,5]);
zlim([-5,5]);
title('Decorrelated 3D data');
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-将数据投射到两个最大的特征向量上的二维数据图.png)

[^图4.3]: 将数据投射到两个最大的特征向量上的二维数据图

```matlab
%将数据投射到两个最大的特征向量上
eigenvec_2d=eigenvec(:,2:3);
data_2d = data*eigenvec_2d;
%绘制二维数据图
figure;
scatter(data_2d(:,1), data_2d(:,2), 5, data(:,3), 'filled');
colormap(gray);
%绘制特征向量
hold on;
quiver(0, 0, 0*sqrt(largest_eigenval), 1*sqrt(largest_eigenval), '-m', 'LineWidth',3);
quiver(0, 0, 1*sqrt(medium_eigenval), 0*sqrt(medium_eigenval), '-g', 'LineWidth',3);
hold on;
%设置轴坐标
hXLabel = xlabel('x');
hYLabel = ylabel('y');
xlim([-5,5]);
ylim([-5,5]);
title('Projected 2D data');
grid on;
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-将数据投射到最大的特征向量上的一维数据图.png)

[^图4.4]: 将数据投射到最大的特征向量上的一维数据图

```matlab
%将数据投射到最大的特征向量上
eigenvec_1d=eigenvec(:,3);
data_1d = data*eigenvec_1d;
%绘制一维数据图
figure;
scatter(repmat(0, size(data_1d,1), 1), data_1d, 5, data(:,3), 'filled');
colormap(gray);
%绘制特征向量
hold on;
quiver(0, 0, 0*sqrt(largest_eigenval), 1*sqrt(largest_eigenval), '-m', 'LineWidth',3);
hold on;
%设置轴坐标
hXLabel = xlabel('x');
hYLabel = ylabel('y');
xlim([-5,5]);
ylim([-5,5]);
title('Projected 1D data');
grid on;
```

一般来说，主成分分析使我们获得原始N维数据的一个线性M维子空间，其中M小于等于N。此外，如果未知且不相关的成分满足高斯分布，则PCA实际上充当了独立成分分析的角色，因为不相关的高斯变量在统计上是独立的。但是，如果底层成分不是正态分布，PCA仅仅产生去相关的变量，这些变量不一定是独立的。在此情况下，非线性降维算法可能是一个更好的选择。



## 二、正交回归方法

在上述的讨论中，我们以获得独立成分（如果数据不是正态分布，至少是不相关成分）为目标来减少特征空间的维数。我们发现，这些所谓的“主成分”是由我们数据协方差矩阵的特征值分解获得的。然后将数据投影到最大特征向量从而降低维数。

现在，我们不考虑找不相关成分。相反，我们现在尝试通过找到原始特征空间上的一个线性子空间来降低维数，我们可以将我们的数据投影到这个子空间上使得映射误差最小化。在二维情况下，这意味着我们试图找到一个向量，将数据投影到这个向量上对应的映射误差小于将数据投影到任何其他可能的向量上得到的映射误差。接下来的问题是如何找到这个最优向量。

考虑下图所示例子。这里展示了三种不同的映射向量，以及所得到的一维数据。接下来，我们将讨论如何确定哪些映射向量使得映射误差最小。在寻找这个向量之前，我们必须先定义这个误差函数。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-映射图.png)

[^图5]: 通过投影到线性子空间上来降维的方法

一个众所周知的用一条线来拟合二维数据的方法是最小二乘回归法。给定自变量x和因变量y，最小二乘回归函数对应于直线f(x)=ax+b，使得残差的平方之和
$$
\sum_{i=0}^{N}[f(x_i)-y_i]^2
$$
最小化。换句话说，如果x为自变量，那么得到的回归函数f(x)是可以预测因变量y且使得误差平方最小的线性函数。由此产生的模型f(x)在上图中用蓝线画出，被最小化的误差示于下图。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-y回归.png)

[^图6]: 以x为自变量，y为因变量的线性回归，对应于垂直映射误差的最小化

在特征提取中，除了定义特征x为自变量，特征y为因变量外，我们还可以定义y为自变量，并找到一个线性函数f(y)来预测因变量x，使得
$$
\sum_{i=0}^{N}[f(y_i)-x_i]^2
$$
最小化。这对应于水平映射误差的最小化，并由此产生不同的线性模型，如下图所示。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-x_regression.png)

[^图7]: 以y为自变量，x为因变量的线性回归，对应于水平映射误差的最小化

 显然，自变量和因变量的选择改变了所得到的模型，使得普通最小二乘回归成为一个非对称回归。这样是因为最小二乘回归假定自变量是无噪声的，而因变量是有噪声的。然而，在分类的情况下，所有的特征通常都是有噪声的观测，即x和y都应被视为自变量。事实上，我们希望得到一个模型f(x,y)，它同时最小化水平的和垂直的映射误差。这相当于找到一个模型，使得正交映射误差最小化，如下图所示。
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-xy_regression.png)

[^图8]: 两个变量都是自变量的线性回归，对应于正交映射误差的最小化

由此产生的回归被称为总体最小二乘回归或正交回归，假定两个变量都是不完美的观测。有趣的是，现在得到的向量（表示最小化正交映射误差的映射方向）对应于数据的最大主成分：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-regressionline_eigenvector.png)

[^图9]: 将数据投影到其上可使其正交映射误差最小化的向量对应于该数据协方差矩阵的最大特征向量

换言之，如果我们想通过将原始数据投影到一个向量上从而使得映射误差的平方在所有方向上最小化来降低维数，那么我们可以简单地将数据投影到最大的特征向量上。这正是我们前一节所说的PCA，这样的映射也可以对特征空间去相关。



## 三、PCA实际应用：特征脸

上述实例出于可视化的目的限于两个或三个维度，当特征的数量和训练样本相比不可忽略时，降低维数通常会变得非常重要。举个例子，假设我们要执行面部识别，即基于带有标记的面部图像训练数据集来确定人的身份。一个办法是把图像上每个像素的亮度作为特征。如果输入图像的大小是32×32，这意味着该特征向量包含1024个特征值。判断新的图像通过计算这1024维矢量与我们训练数据集中特征向量之间的欧氏距离完成。拥有最小的欧式距离的图像正是我们寻找的那个人。

然而，如果我们只有几百训练样本，在1024维空间中运行会变得有问题。此外，欧氏距离在高维空间中行为比较奇怪（具体原因可参考[The Curse of Dimensionality in classification](https://www.visiondummy.com/2014/04/curse-dimensionality-affect-classification/)）。因此，我们可以利用PCA方法，通过计算1024维特征向量协方差矩阵的特征向量来降低特征空间的维数，然后映射每个特征向量到最大特征向量。

因为二维数据的特征向量是2维的，三维数据的特征向量是3维的，1024维数据的特征向量是1024维。换句话说，为了可视化，我们可以重塑每个1024维特征向量到一个32×32的图像。下图展示了由剑桥人脸数据集的特征分解获得的前四个特征向量：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-Eigenfaces.png)

[^图10]: 四个最大的特征向量重新塑造成图像，得到所谓的特征面

每个1024维的特征向量（构成特征脸）可以投影到N个最大的特征向量上，并可以表示为这些特征脸的线性组合。这些线性组合的权重决定了这个人的身份。因为最大特征向量表示数据中最大的方差，所以这些特征脸描述信息量最大的图像区域（眼睛，鼻子，嘴等）。通过只考虑前N（如N = 70）个特征向量，特征空间的维数大大降低了。

剩下的问题是现在应该使用多少个特征脸，或者在一般情况下，应保留多少个特征向量。去除过多的特征向量可能会删除特征空间里的重要信息，而去除太少的特征向量则会产生维数诅咒问题。遗憾的是，对这个问题到现在仍没有明确的回答。尽管交叉验证技术可用来获得超参数的估计值，但如何选择维数的最佳数目仍然是一个问题，目前主要凭借经验（即“试错法”）来解决。在消除特征向量时，检查原始数据的方差保留了多少（以百分比表示，用保留特征值的总和除以所有特征值的总和）是有用的。



## 四、PCA配方

基于前面的章节，我们现在可以列出应用PCA进行特征提取的简单配方： 

### （ 1）中心化数据

我们证明了协方差矩阵可以写为一系列线性操作（缩放和旋转）（具体证明过程参考[A geometric interpretation of the covariance matrix](https://www.visiondummy.com/2014/04/geometric-interpretation-covariance-matrix/)）。特征分解提取这些变换矩阵：特征向量表示旋转矩阵，而特征值代表缩放因子。但是，协方差矩阵不包含任何与数据变换相关的信息。事实上，为了表示这种数据变换，我们需要仿射变换而不是线性变换。

因此，应用PCA来旋转数据以获得不相关轴之前，任何现有的位移都需要用每个数据点减去数据的平均值。这相当于将数据居中，使得其平均值为零。

### （2）标准化数据 

协方差矩阵的特征向量指向数据最大方差的方向。然而，方差是一个绝对数，而不是相对数。这意味着，以厘米为单位测得的数据方差比以米为单位测得的相同数据的方差大得多。举个例子，一个特征表示对象的长度（以米为单位），而第二个特征表示对象的宽度（以厘米为单位）。如果数据没有被标准化，那么最大方差及最大特征向量将隐性地由第一个特征定义。

为了避免PCA的这种尺度依赖性，用每个特征除以它的标准差来标准化数据是有用的。在不同特征对应不同度量标准的情况下，这一点尤其重要。

### （3）计算特征分解 

由于数据将被投影到最大特征向量上来减少维数，所以我们需要进行特征分解。奇异值分解（SVD）是计算特征分解最常用的方法之一。

### （4）映射数据 

为了降低维数，数据被简单地映射到最大特征向量上。令V表示每列都包含最大特征向量的矩阵，令D表示每列包含不同观测值的原始数据。映射数据D’通过
$$
D' = V^TD
$$
得到。我们可以直接选择其余的维数即V的列，或者我们可以定义在消除特征向量时需要保留的原始数据方差的数量。如果只有N个特征向量被保留，且e1… eN表示相应的特征值，那么对原始d维数据进行投影后，保留的方差数量可以通过下式计算： 
$$
\begin{equation}
s=\frac{\sum_{i=0}^{N}e_i}{\sum_{j=0}^{d}e_j}\tag{4}
\end{equation}
$$


## 五、PCA陷阱

在上述的讨论中，我们已经做出了几个假设。在第一部分，我们讨论了PCA如何去相关。在讨论开始时，我们希望恢复未知的、底层的独立成分。然后，我们假设我们的数据满足高斯分布，这样的统计独立性等价于没有线性相关性。而PCA允许我们去相关，从而恢复高斯分布情况下的独立成分。然而，值得注意的是，去相关只对应于高斯情况下的统计独立性。考虑采样y=sin(x)半个周期所获得的数据： 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-y=sin(x)散点图.png)

[^图11]: 非相关数据只有在正态分布的情况下才统计上独立。在这个例子中，一个明显的非线性相关性即y=sin(x)仍然存在。

Stata代码如下：

```
clear all
//设置种子数与观察值
set seed 2022
set obs 50
//设置满足y=sin(x)的半个周期的数据集
gen x = runiform(0,_pi)
gen y = sin(x)
//画出y=sin(x)半个周期采样散点图
scatter y x, msize(1) xlabel(0(0.5)3.2) ylabel(0(0.1)1) title("y=sin(x)")
```

虽然上述数据显然是不相关的（平均而言，当x值增大时，y值增加的和它减少的一样多），因此对应于一个对角的协方差矩阵，但这两个变量之间仍然有明确的非线性关系。

一般情况下，PCA只是对数据去相关，但不会消除统计上的相关性。如果底层成分不服从高斯分布，那么ICA（独立成分分析）之类的技术会更好。另一方面，如果非线性显然存在，可以使用如非线性PCA等降维技术。然而，这些方法都很容易过拟合，因为更多的参数是基于相同数量的训练数据被估计出来的。

本文的第二个假设是，特征空间中的最大方差捕获了最具鉴别性的信息。因为最大方差方向编码的信息最多，这种假设很可能是真的。然而，在某些情况下，鉴别信息实际上位于最小方差的方向，这使得PCA大大损害分类性能。举个例子，考虑下图的两种情况，我们将二维特征空间降到一维表示： 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/黄子瑜-使用PCA提取数据-v1-猫狗图.png)

[^图12]: 在第一种情况下，PCA将数据变成线性不可分，损害了分类性能。当最具鉴别性的信息存在于较小的特征向量中时，就会发生这种情况。

如果最具鉴别性的信息包含在较小的特征向量中，应用PCA实际上可能恶化维数诅咒问题，因为现在需要基于更复杂的分类模型（例如非线性分类器）来分类低维问题。在这种情况下，其他降维的方法可能会更令人感兴趣，如线性判别分析（LDA），其试图找到能够一个映射向量，该向量最优地分离这两个类别。



## 六、总结

在这篇文章中，我们从两个方面讨论了PCA在特征提取和降维方面的优势。第一个方面说明了PCA如何让我们能够去除特征空间的相关性，而第二个3方面表明PCA实际上对应于正交回归。

此外，我们还简要介绍了众所周知的、以PCA的特征提取为基础的实例：特征脸。此外，我们还讨论了PCA一些重要的缺点。




## 七、参考文献与相关推文

[1]Vincent Spruyt，个人网站（visiondummy.com）推文，[Feature extraction using PCA](https://www.visiondummy.com/2014/05/feature-extraction-using-pca/) 

[2]Vincent Spruyt，个人网站（visiondummy.com）推文，[A geometric interpretation of the covariance matrix](https://www.visiondummy.com/2014/04/geometric-interpretation-covariance-matrix/)

[3]Vincent Spruyt，个人网站（visiondummy.com）推文，[What are eigenvectors and eigenvalues?](https://www.visiondummy.com/2014/03/eigenvalues-eigenvectors/)

[4]gwave，知乎，[特征向量与特征值的几何意义(2): 微分方程-相平面图](https://zhuanlan.zhihu.com/p/355088340)

