# 反向因果专题：来源、特征及解决方法

&emsp;

> **作者：** 苏志聪 (中山大学)  
> **邮箱：** <1214785239@qq.com> 

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

## 1. 简介
本篇推文介绍层次回归的理论基础及用来进行层次回归的简便命令：`nestreg` ，并结合具体子加以说明。

## 2. 理论基础

层次回归，也叫分层回归，就是对两个或多个回归模型进行比较。我们可以根据两个模型所解释的变异量的差异来比较两个模型，在其他条件一样的情况下，如果后一模型解释了更多的变异，则证明后一模型是更好的模型。

在对某个或某组预测变量进行评估时，我们可以用到层次回归。例如，我们想要检验变量x对模型的解释作用，我们可以先对剔除了变量x的模型进行回归，得出该模型的R2。然后我们再把变量x加入到模型中来进行回归，若得出的R2较上一个模型有显著的提升，则证明变量x对模型有显著的解释作用。

检验方程式如下：
$$F=((R_a^2-R_b^2)/M)/((1-R_b^2)/d_b)$$
其中，M是指模型b中的新增变量的个数；$R_a^2$是模型a(包含基本解释变量的模型)的R2；$R_b^2$是模型a(包含较多解释变量的模型)的R2;$d_b$是指模型b误差变异的自由度。
上述公式中的F通常被用来衡量新增变量对模型解释力的提升程度，F越大，说明新增变量对模型的解释作用越强。

## 3. `nestreg` 介绍

### 3.1 语法介绍

```stata
 nestreg [, options ] : command_name depvar (varlist) [(varlist) ...] [if] [in] [weight] [, command_options]
```
- `depvar` 被解释变量
- `varlist` 前一个表示需要基本的解释变量，后一个表示后续加入模型的解释变量

### 3.2 Option 介绍

```stata
Reporting
      waldtable      report Wald test results; the default
      lrtable        report likelihood-ratio test results
      quietly        suppress any output from command_name
      store(stub)    store nested estimation results in _est_stub#
```
- `wald` 报告wald检验的结果，当option为空时默认设置为进行wald检验
- `lr`   报告IR检验的结果
- `qui`  不输出结果
- `store(stub)` 将结果保存至_est_stub#

### 3.3 Stata实操

下面以一个具体的例子来说明如何使用nestreg命令进行层次回归。
建立以下模型：
$lnwage_i=\alpha_i+\beta_{1i}age+\beta_{2i}hours+\beta_{3i}ttl_exp+\beta_{4i}tenure+\beta_{5i}grade+\varepsilon_i$
- `age`    年龄
- `hours`    一周工作时长
- `ttl_exp`  工龄
- `tenure`   任期
- `grade`    学历（grade=i表示i年级毕业）


现在我们想探究grade这个变量对lnwage的解释作用：
```stata
 sysuse "nlsw88.dta",clear
(NLSW, 1988 extract)

. drop if wage==. 
(0 observations deleted)

. drop if age==.
(0 observations deleted)

. drop if hours==.
(4 observations deleted)

. drop if ttl_exp==.
(0 observations deleted)

. drop if tenure==.
(15 observations deleted)

. drop if grade==.
(2 observations deleted)
```
引用Stata自带的数据`nlsw88.dta`,先处理缺失值

```stata
 gen lnwage=ln(wage)

. local vlist "age hours ttl_exp tenure"

. local v "grade"

. nestreg: reg lnwage (`vlist') (`v') 

Block  1: age hours ttl_exp tenure

      Source |       SS           df       MS      Number of obs   =     2,225
-------------+----------------------------------   F(4, 2220)      =    116.03
       Model |  126.078463         4  31.5196156   Prob > F        =    0.0000
    Residual |  603.069008     2,220  .271652706   R-squared       =    0.1729
-------------+----------------------------------   Adj R-squared   =    0.1714
       Total |  729.147471     2,224  .327854079   Root MSE        =     .5212

------------------------------------------------------------------------------
      lnwage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         age |  -.0120988   .0036423    -3.32   0.001    -.0192413   -.0049562
       hours |   .0063814   .0010853     5.88   0.000      .004253    .0085097
     ttl_exp |   .0367415   .0029948    12.27   0.000     .0308687    .0426143
      tenure |   .0120635   .0024562     4.91   0.000     .0072469    .0168801
       _cons |   1.576727   .1483396    10.63   0.000     1.285828    1.867626
------------------------------------------------------------------------------

Block  2: grade

      Source |       SS           df       MS      Number of obs   =     2,225
-------------+----------------------------------   F(5, 2219)      =    180.04
       Model |  210.427726         5  42.0855453   Prob > F        =    0.0000
    Residual |  518.719745     2,219  .233762841   R-squared       =    0.2886
-------------+----------------------------------   Adj R-squared   =    0.2870
       Total |  729.147471     2,224  .327854079   Root MSE        =    .48349

------------------------------------------------------------------------------
      lnwage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         age |  -.0083885   .0033844    -2.48   0.013    -.0150253   -.0017516
       hours |   .0055638   .0010077     5.52   0.000     .0035877      .00754
     ttl_exp |   .0286409   .0028106    10.19   0.000     .0231292    .0341526
      tenure |   .0116949   .0022785     5.13   0.000     .0072267    .0161632
       grade |    .078943   .0041559    19.00   0.000     .0707932    .0870928
       _cons |   .5314121   .1482015     3.59   0.000     .2407839    .8220403
------------------------------------------------------------------------------


  +---------------------------------------------------------
  ----+
  |       |          Block  Residual                     Change |
  | Block |       F     df        df   Pr > F       R2    in R2 |
  |-------+-----------------------------------------------------|
  |     1 |  116.03      4      2220   0.0000   0.1729          |
  |     2 |  360.83      1      2219   0.0000   0.2886   0.1157 |
  +-------------------------------------------------------------+
```

可以看出，`nestreg` 命令相当于进行两次回归，第一个模型只包括基本解释变量，不包括**grade，而第二个模型既包括了基本解释变量，也加入了**grade。并且，最终的增量R2为0.1157。

## 4. 结语

层次回归是检验解释变量对被解释变量解释作用大小的一个有效方法，它克服了多元标准回归时解释变量间存在相关性情况下共同变异不知如何分配的问题。而 `nestreg` 命令则让我们只用一个命令就完成了层次回归的众多繁琐步骤，但需注意的是，在使用 `nestreg` 之前，需要先把各变量的样本数调整到一致，否则会导致命令出错。

## 5. 参考资料
-Acock, A. C. 2016. A Gentle Introduction to Stata. 5th ed. College Station, TX: Stata Press. Section 10.9 Categorical predictors and hierarchical regression.

-Lindsey, C., and S. J. Sheather. 2015. Best subsets variable selection in nonnormal regression models. Stata Journal 15: 1046–1059.

-分层回归（hierarchical multiple regression）http://blog.sina.com.cn/s/blog_61287da50101csmp.html