
&emsp;
>**作者**：张逸夫 
>**E-mail**：zhangyf77@mail2.sysu.edu.cn


## 1. 背景介绍

业界有两种主流的对于因子定价的方式：

### 1.1 第一类：*Fama-MacBeth Regression*

* 1.1.1构建方法

  **第一步，构建投资组合**：运用特征然后进行 double sort 构建资产组合，例如运用代表Fama三因子模型，五因子模型
  首先以 NYSE 股票市值中位数为界，将全部股票分为小市值（small）和大市值（big）；同时按照 *BM* (账面市值比)，将股票分为 30%/40%/30% 三组，分别为价值（value）、中性（medium）和成长（growth）。二者交叉得到6个组合，并进一步按下述方式得到 *SMB* 和 *HML* 因子。

  **第二步，时序回归**：把待检验的因子收益率放在回归方程的右侧，把用来检验这些因子的资产收益率逐一放在回归方程的左侧，使用 multivariate regression 计算每个资产在这些因子上的因子载荷 .
  $$
   \begin{equation}R_{i t}=\alpha_{i}+\beta_{i}^{\prime} f_{t}+\varepsilon_{i t}, \quad t=1,2, \cdots, T, \forall i\end{equation}
  $$
  **第三步，截面回归**：使用第一步得到的 β 作为解释变量放在右侧，使用资产的收益率放在左侧，截面回归求出因子的 risk premium λ；每一期得到每个因子的溢价后，最后检验每个因子溢价的均值是否显著

$$
\begin{equation}R_{i t}=\alpha_{i t} + \hat{\beta_{i}^{\prime}} \lambda_{t}, \quad i=1,2, \cdots, N \text { for each } t\end{equation}
$$

* 1.1.2方法的评价

  该方法是是学界和业界检验因子 risk premium 的主流方法之一，自1973年以来被广泛的运用于检验因子的有效性。

  但是，该方法有一个致命问题，即第一步通过时序回归得到的因子载荷 β 的估计，存在误差；而将这个含有误差的 β 直接放入第二步的回归中会导致模型估计出现误差。为了解决该问题Fama将没有使用个股的收益率，而是将个股按照历史 β 的大小构成了不同的投资组合，然后使用这些投资组合作为资产来进行回归。Black, Jensen, and Scholes (1972) 以及 Fama and MacBeth (1973) 指出：**当使用投资组合时，个股 β 的估计误差会相互抵消，因此对投资组合的 β 估计会更准确，从而在一定程度上降低 EIV 的影响。**

  但是Jegadeesh et al. (2019) 这篇文章指出**将个股按照某种属性分组实际上是一种降维处理，投资组合会丢掉很多个股截面上的特征**。所以我们应该使用个股收益率而不是投资组合。

  由于Fama and MacBeth (1973)文章的成功，业界开始广泛的使用double sort的方法，Nagel 和 Shanken 在 Lewellen, Nagel, and Shanken (2010) 一文中指出如果一个因子与 HML, SMB 因子有较强的的相关性，哪怕因子并没有很强的解释力，也能获得较高的收益。

  

### 1.2 第二类：直接运用公司特征进行回归

* 1.2.1构建方法

  在公司特征中，我们不需要第一步的时间序列回归，我们将公司特征替代 β 估计量直接进行第二步回归。

  我们在 *Fama-MacBeth Regression* 第二步所使用的回归方程	
  $$
  \begin{equation}R_{i t}=\alpha_{i t} + \hat{\beta_{i}^{\prime}} \lambda_{t}, \quad i=1,2, \cdots, N \text { for each } t\end{equation}
  $$
  以往的 **B** 是时序回归后所得到的定价因子载荷矩阵，但是在这里我们用公司特征 ( firm characteristics ) 替代了 **B** 来作为我们的因子载荷

  例如：$BM_i$ 等特征作为为公司i的账面市值比，那么我们运用这个特征所进行的回归就是
  $$
  \begin{equation}R_{i t}-R_{z t}=\mathrm{MC}_{i t-1} R_{\mathrm{MCt}}+\mathrm{BM}_{i t-1} R_{\mathrm{BM} t}+\mathrm{OP}_{i t-1} R_{\mathrm{OPt}}+\mathrm{INV}_{i t-1} R_{\mathrm{INVt}}+e_{i t}\end{equation}
  $$
  

$$
\begin{equation}R_{i t}-R_{z t}=\mathrm{MC}_{i t-1} R_{\mathrm{MCt}}+\mathrm{BM}_{i t-1} R_{\mathrm{BM} t}+\mathrm{OP}_{i t-1} R_{\mathrm{OPt}}+\mathrm{INV}_{i t-1} R_{\mathrm{INVt}}+e_{i t}\end{equation}
$$

* 1.2.2方法的评价

  Jegadeesh et al. (2019)  在美股上进行了检验，发现当他们将 Size 和 B/M 加入到 Fama-MacBeth Regression 的第二步，和 HML 以及 SMB 的 β estimate 一起进行了截面回归时。HML, SMB 因子变得不显著，但是 Size 和 B/M两个公司因子却能获得显著超额收益

  但是，我们仅有实证的数据，并没有公司特征显著的理论依据。即从实证结果来看，firm characteristics 确实是更好使的因子载荷，但背后的原因依然未知。

​	

## 2.理论介绍

公司特征与 Fama-MacBeth Regression，究竟选择谁

### 2.1.解释力能力强弱

Jegadeesh et al. (2019)  基于美股的数据上进行了研究

![FIG1](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张逸夫_两种回归_figure1.JPG)



在第 (2) 列中，投资组合 SMB 与 HML 都对资产有着显著的解释能力，但是当用于构建 SMB 与 HML 的公司特征 SIZE 与 BM 被加入回归方程后，上述两个组合都变得不显著

同理在中国市场上石川也在[ which beta？](https://mp.weixin.qq.com/s/HFLxJ7QwMAkJJ5WnJLEqvA)文章中展示了


![FIG2](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张逸夫_两种回归_figure2.JPG)



我们可以公司特征的解释能力强于因子指标，因为当公司特征与因子共同回归时，Fama and French (2015) 五因子都变得不显著，但是 B/P 和 OP/TA 这两个公司指标可以获得显著的风险溢价

### 2.2来自Fama and French (2019)的比较

* Fama and French 回归（ Fama and MacBeth Regression 1973）

  TS model：

$$
R_{i t}-R_{f t}=a_{i}+b_{i}\left(R_{m t}-R_{f t}\right)+s_{i} \mathrm{SMB}_{t}+h_{i} \mathrm{HML}_{t}+r_{i} \mathrm{RMW}_{t}+c_{i} \mathrm{CMA}_{t}+e_{i t}
$$

* 截面回归（firm characteristics）

  CS model：

$$
R_{i t}-R_{z t}=\mathrm{MC}_{i t-1} R_{\mathrm{MCt}}+\mathrm{BM}_{i t-1} R_{\mathrm{BM} t}+\mathrm{OP}_{i t-1} R_{\mathrm{OPt}}+\mathrm{INV}_{i t-1} R_{\mathrm{INVt}}+e_{i t}
$$

Fama and French (2019)对上述的两个模型进行了比较，最终结果发现：

*  **以 FM regression 得到的 CS 因子作为解释变量的 CS 定价模型优于传统的 FF5，即 TS 定价模型（两个 CS 模型均优于 TS 模型）。**

* **当使用 CS 因子时，对应的 factor loadings 应采用 firm characteristics，而非像 Panels B1/2 中的时序回归。**

* **当使用 firm characteristics 作为 CS 模型的 factor loadings 时，时变 loadings 的效果好于恒定 loadings 的效果（Panels B5/6 优于 Panels B3/4），但这种差异并不明显；CS 模型之所以优秀源于通过 FM regression 得到的 CS 因子 factor returns（那些纯因子组合的收益率）。**



即：运用公司特征直接进行截面回归的效果比double sort构建资产组合的方式能够更好地对资产进行定价。

文章内容解析可以参考[which beta(II)](https://mp.weixin.qq.com/s/K0OvEjH4G0nFrjgjnk0Uug)



接下来让我们自己开始运用中国的数据复现上述

## 3. 数据及代码

数据来源：国泰安数据库

月度数据.dta ，上证综指.dta，财务报表.dta，均来源于国泰安数据库



## 4. 实证结果及代码

### 4.1 公司特征的计算

* （1）仿照 Liu et al. (2018) ，我们在构建 fama 因子时需考虑 “壳污染” 的问题，即由于A股IPO的发审制度不完善，使得上市公司的壳资源价值非常高，并且， ST 企业由于具有很高的壳价值，可能会使得“借壳上市”成为可能，所以我们需要去除市值低于 30% 的企业。

```Stata
use 实证金融作业.dta , clear

**数据处理，清除壳污染 		
bysort stkcd: egen sum1=sum(月个股总市值市值)    
egen shelldrop = xtile(sum1), by(ym) nq(10)	   //按规模生成10组
drop if shelldrop<=3                           //去除市值<3的部分
drop sum1 shelldrop

```



* （2）另外仿照 李志冰等 (2017) ，我们采用如下方法进行 Fama-French 五因子的因子计算

  对于股票 i，以其在第 t 年 6 月底的流通市值作为( $Size_{it}$ ) 指标; “账面市值比”( $B /M_{i，t} $) 是用第 t - 1 年末的账面价值， 除以第 t - 1 年 12 月底股票 i 的流通市值; 鉴于中美会计准则存在的差异，我们直接使用 “营业利润/股东权益合计”来反映 A 股市场的“营运利润率”指标; 而“投资风格”( Invit ) 是用第 t - 1 年末相对于第 t - 2 年末的总资产增加额，除以第 t - 2 年末的总资产。

```Stata
**计算公司特征
*size 表示上一年6月份流通的市值
bys stkcd year : gen size1 = 月个股总市值市值 if month == 6
bys stkcd year : egen size = mean(size1)                      //将size填充到缺失的值内

*12月底流通市值
bys stkcd year : gen  年底市值1 = 月个股总市值市值 if month == 12
bys stkcd year : egen 年底市值 = mean(年底市值1)


sort  stkcd T
xtset stkcd T
gen BM    = 所有者权益合计          / L12.年底市值
gen	OP    = 经营利润               / 所有者权益合计	  
gen invit = (资产总计-L12.资产总计)  /L12.资产总计
gen EP    = 经营利润               / 年底市值

```

* （3）BM与EP因子的构建

  在 Fama and French (1993) 这篇正式提出三因子模型的前作 Fama and French (1992) 中，Fama 和 French 研究了多个价值因子的指标 ，包括 EP（Earnings-to-Price），BM（Book-to-Market），以及 AM（Assets-to-Market）， 并因 BM 的效果最好而选择它构建了三因子中大名鼎鼎的 HML 投资组合。

  针对 A 股市场，Liu et al. (2018) 采用了同样的分析思路，并发现 EP 特征最适合我国市场，所以我们在构建因子时加入了 EP 指标并构建了 VMG 指标来替换 HML 因子

### 4.2  Fama 五因子模型的因子构建

***SMB* 与 *HML***

***Fama and French (1993)*** 实际使用的是 double sort：首先以 NYSE 股票市值中位数为界，将全部股票分为小市值（small）和大市值（big）；同时按照$ BM$ (账面市值比)，将股票分为 30%/40%/30% 三组，分别为价值（value）、中性（medium）和成长（growth）。二者交叉得到6个组合，并进一步按下述方式得到$SMB$和$HML$因子：

$$
\begin{equation}\begin{aligned}
SMB_{t} &=\frac{1}{3}(\text {SmallValue}_{t}-\text {BigValue}_{t})+\frac{1}{3}(\text {SmallNeutral}_{t}-\text {BigNeutral}_{t})+\frac{1}{3}(\text {SmallGrowth}_{t}-\text {BigGrowth}_{t}) \\
&= \frac{1}{3}(\text {SmallValue}_{t}+\text {SmallNeutral}_{t} +\text {SmallGrowth}_{t}) - \frac{1}{3}(\text {BigValue}_{t} + \text {BigNeutral}_{t}+ \text {BigGrowth}_{t})
\end{aligned}\end{equation}
$$

$$
\begin{equation}\begin{aligned}
HML_{t}&=\frac{1}{2}(\text {SmallValue}_{t}-\text {SmallGrowth}_{t})+\frac{1}{2}(\text {BigValue}_{t}-\text {BigGrowth}_{t}) \\
&=\frac{1}{2}(\text {SmallValue}_{t}+\text {BigValue}_{t})-\frac{1}{2}(\text {SmallGrowth}_{t}+\text {BigGrowth}_{t}) \\
\end{aligned}\end{equation}
$$

|          |        |              | 账面市值比     |               |
| -------- | ------ | ------------ | -------------- | ------------- |
|          |        | 价值         | 中性           | 成长          |
| **市值** | 高市值 | $BigValue$   | $BigNeutral$   | $BigGrowth$   |
|          | 低市值 | $SmallValue$ | $SmallNeutral$ | $SmallGrowth$ |



```Stata
**根据文献，先将资产分为Big，Small两组，然后再在两组内部按特征进行划分
egen size_group = xtile(size), by(ym) nq(2)	          //按size分为两组，每月分一次
save temp1.dta ,replace

use temp1.dta ,clear
egen bm_group = xtile(BM), by(ym size_group) nq(10)	  //按BM分成两组，每月分一次
sort ym size_group bm_group

bys ym : gen  growth = 3 if bm_group >7
bys ym : replace  growth = 1 if bm_group <=3
bys ym : replace  growth = 2 if bm_group >3 & bm_group <=7        //按3/4/3生成3组


bys ym : egen BG=mean(mretwd)  if size_group==2 & growth == 3
bys ym : egen BG1=mean(BG)
drop BG
rename BG1 BG

bys ym : egen SG=mean(mretwd)  if size_group==1 & growth == 3
bys ym : egen SG1=mean(SG)
drop SG
rename SG1 SG

bys ym : egen BN=mean(mretwd)  if size_group==2 & growth == 2
bys ym : egen BN1=mean(BN)
drop BN
rename BN1 BN

bys ym : egen SN=mean(mretwd)  if size_group==1 & growth == 2
bys ym : egen SN1=mean(SN)
drop SN
rename SN1 SN

bys ym : egen BV=mean(mretwd)  if size_group==2 & growth == 1
bys ym : egen BV1=mean(BV)
drop BV
rename BV1 BV

bys ym : egen SV=mean(mretwd)  if size_group==1 & growth == 1
bys ym : egen SV1=mean(SV)
drop SV
rename SV1 SV

gen SMB = (SV+SN+SG)/3-(BV+BN+BG)/3
gen HML = (SV+BV)/2-(SG+BG)/2

save SMB_HML.dta ,replace
```



 ***RMW* ， *CMA*，*VMG***  

而 *RMW* , *CMA*, *VMG* 构造同理，将营运利润率与投资风格替代账面市值比，重复以上步骤即可



### 4.3 构建因子的描述性统计


![描述性统计](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张逸夫_两种回归_figure4.JPG)




### 4.4 代码及结果展示

#### 4.4.1 代码的介绍

* `asreg`介绍

  * **FM regression的第一阶段**可以通过`asreg`命令实现，通过`bys`指令进行时间序列回归得到资产在因子上的暴露*β*

  * **FM regression的第二阶段**则可以通过`asreg`的`fmb`选项与`xtfmb`两个命令实现,该选项会在每个时期*t*上进行一次回归，最终汇报的是系数与$R^2$是各截面的均值

* `asreg`命令语法

  ```
   [bysort varlist:] asreg depvar indepvars [if] [in] [, window(rangevar # ) noconstant  					  recursive minimum(#) by(varlist) {statistics_options} newey(#) fmb 					save(file name) first]
  ```

  本文主要介绍下面几个选项：

  * `fmb`: **进行 FM 第二阶段回归**；

  * `save(filename)`: **仅当使用了** `fmb` **后才能使用,** `save` **选项会保留第一步阶段 FM regression 回归中的系数在 filename 文件下**

  * `first`: **仅当使用了** `fmb` **后才能使用**, `first` **选项会在窗口显示第一步阶段 FM regression 回归中的系数**

  * `newey(integer)`: **能够同时在滚动回归和 FM regression 中使用，并且会报告指定滞后阶数的 Newey-West 标准误**

* 注意事项

  * `asreg` 的 `fmb` 虽然有两步，但是本身都是 **FM regression的第二阶段** 内的两步，并不包含第一阶段的时序回归

  * 在使用 `asreg` 的 `fmb` 选项前需要进时间序列设定 `xtset` 或 `tsset` ,否则会报错

#### 4.4.2 采用 Liu et al. (2018)的方法进行回归

* 第一步，我们运用**时间序列回归**获得**资产**在因子上的暴露 *β* 

```Stata
bys stkcd : asreg F超额收益率 mrk_rf SMB VMG RMW CMA , se 
```

​	所获得的β会以_b_x的方式存储在数据中

* 第二步，我们将 *β* 值纳入**截面回归方程**，求得各因子的定价能力

```Stata
asreg F超额收益率 _b_mrk_rf _b_SMB _b_VMG _b_RMW _b_CMA  ,fmb  newey(4)
```

​	在这里我们运用了`asreg`中的`fmb`与`newey`option。其中`fmb`表示采用FM regression方法，`newey`则表示采用Newey-West标准误计算。

* 第三步，为了比较公司特征 ( firm characteristic ) 与定价因子的优劣，我们将两者纳入统一方程进行回归

```
asreg F超额收益率 _b_mrk_rf _b_SMB _b_VMG _b_RMW _b_CMA lnsize EP OP invit , fmb newey(4)
```

结果如下：

***Figure N***

![fig4](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张逸夫_两种回归_figure3.JPG)


根据第 (1) ~ (3) 列的回归结果，我们可以看到**投资组合因子**对于我国的市场解释能力 (1) 是不如**公司特征( firm charateristic )** 的解释能力 (2) 的。同时在将**投资组合因子**和**公司特征**同时纳入回归方程中时，我们可以看到，**公司特征**的解释能力比**因子**更强，这也印证了石川在which beta中的结论。即在中国市场上**公司特征**的表现优于**因子**组合。



我们数值结果和石川在 which beta 中结果有些出入，具体原因可能是计算 fama 因子时所采用的的数据处理方式不一样，我们依据 Liu et al. (2018)采用了去除壳污染的公司进行回归，并且将BM替换为了EP。

#### 4.4.3 仿照 Fama 进行因子构建

```
use fama因子数据.dta, clear
sort stkcd ym
bys stkcd: replace T = _n

xtset stkcd T
gen F超额收益率=F.超额收益率

drop _b_* _se_*
bys stkcd : asreg F超额收益率 mrk_rf SMB HML RMW CMA , se  //第一步回归
drop _Nobs _R2 _adjR2

asreg F超额收益率 _b_mrk_rf _b_SMB _b_HML _b_RMW _b_CMA  ,fmb  newey(4)
est store m4
asreg F超额收益率 lnsize BM OP invit , fmb newey(4)
est store m5
asreg F超额收益率 _b_mrk_rf _b_SMB _b_HML _b_RMW _b_CMA lnsize BM OP invit , fmb newey(4)
est store m6

local mm1  " (1) (2) (3) "
local mm2  "m4 m5 m6"  
esttab `mm2' using "table2.rtf", mtitle(`mm1') t b(%6.3f)  s(N r2,fmt(%12.0fc %6.4fc)) ///
compress nogaps star(* 0.1 ** 0.05 *** 0.01) replace	
```

在这一部分我们使用 HML 来替换 VMG 因子，根据第 (4) ~ (6) 列的回归结果，我们仍然发现，在中国市场上**公司特征**的表现优于**因子**组合



## 5. 结论

​	

**因子组合作为以往因子动物园的质检员，如今在实用性与可靠性方面已经收到多方面质疑。而由于公司特征凭借着其在截面回归上的优异表现，和逐渐完善理论依据，正逐渐被大家所接受，甚至提出两步法的 Fama 都在 Fama and French (2019) 这篇文章中证明了公司特征的有效性。所以公司特征作为β的实证方法应该会被更多人所接受**。
















文献清单：

李志冰,杨光艺,冯永昌,景亮.Fama-French五因子模型在中国股票市场的实证检验[J].金融研究,2017(06):191-206.

Fama, E. F. and K. R. French (1993). Common Risk Factors in the Returns on Stocks and Bonds. *Journal of Financial Economics*, Vol. 33(1), 3 – 56.

Fama, E. F. and K. R. French (2015). A Five-Factor Asset Pricing Model. *Journal of Financial Economics*, Vol. 116(1), 1 – 22.

Fama, E. F. and K. R. French (2019). Comparing cross-section and time-series factor models. *The Review of Financial Studies*, forthcoming.

Jegadeesh, N., J. Noh, K. Pukthuanthong, R. Roll, and J. Wang (2019). Empirical tests of asset pricing models with individual assets: Resolving the errors-in-variables bias in risk premium estimation. *Journal of Financial Economics*, Vol. 133(2), 273 – 298.

Liu, J., R. F. Stambaugh, and Y. Yuan (2018). Size and Value in China. *Journal of Financial Economics*, forthcoming.

[which beta？石川](https://mp.weixin.qq.com/s/HFLxJ7QwMAkJJ5WnJLEqvA)

[which beta？(II) 石川](https://mp.weixin.qq.com/s/K0OvEjH4G0nFrjgjnk0Uug)







### Stata相关命令
> by 连玉君 `2020/12/11 8:49`
- `findit mvport` // 一组计算投资效率和投资组合的命令
- `help grsftest` // Gibbons, Ross, Shanken (1989, GRS) test of mean-variance efficiency of asset returns
  - Gibbons, M.R., S. Ross, and J. Shanken, 1989. "A test of the efficiency of a given portfolio" Econometrica, 57(5), 1121-1152.
  - Kamstra, M.J., R. Shi, 2020. "A Note on the GRS Test" Working Paper
- `search fetchcomponents` // Stata Journal 13-3
  - Mehmet F. Dicle, 2013, Financial Portfolio Selection using the Multifactor Capital Asset Pricing Model and Imported Options Data, Stata Journal, 13(3): 603–617. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300310)