

## Stata相关命令
> by 连玉君 `2020/12/11 8:49`
- `findit mvport` // 一组计算投资效率和投资组合的命令
- `help grsftest` // Gibbons, Ross, Shanken (1989, GRS) test of mean-variance efficiency of asset returns
  - Gibbons, M.R., S. Ross, and J. Shanken, 1989. "A test of the efficiency of a given portfolio" Econometrica, 57(5), 1121-1152.
  - Kamstra, M.J., R. Shi, 2020. "A Note on the GRS Test" Working Paper
- `search fetchcomponents` // Stata Journal 13-3
  - Mehmet F. Dicle, 2013, Financial Portfolio Selection using the Multifactor Capital Asset Pricing Model and Imported Options Data, Stata Journal, 13(3): 603–617. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300310)