drop _all
set obs 500
set seed 1011

scalar a0 = 0
scalar a1 = 1
scalar a2 = 2
scalar a3 = -1
scalar beta = 1
scalar beta2 = -0.5
scalar sita = 1

gen exo = rnormal(0,2)
gen control = rnormal(-1,4)
gen control2 = control*control
gen iv1 = rnormal(0,4)
gen v1 = rnormal(0,1)
gen e1 = rnormal(0,1)

gen u1 = sita*v1+e1

gen end = 1+0.5*exo+3*iv1+v1
gen inter = end*exo

gen y1 = a0+a1*end+a2*exo+a3*inter+beta*control+beta2*control2+u1

gen y = 0 if y1<=0
replace y = 1 if y1>0

gen MPE1 = (a1+a3*exo)*normalden(y1-e1)
gen MPE2 = (a2+a3*end)*normalden(y1-e1)
gen INT = a3*normalden(y1-e1)-(a1+a3*exo)*(a2+a3*end)*(y1-e1)*normalden(y1-e1)
gen MAR = (beta+2*beta2*control)*normalden(y1-e1)
gen QUA = 2*beta2*normalden(y1-e1)-(beta+2*beta2*control)^2*(y1-e1)*normalden(y1-e1)

timer clear 1
timer on 1
eivprobit y control (end=iv1), interact(exo) quaeffect(control) bootstrap(200) seed(10101)
timer off 1
//结束计时
timer list 1


local effects MPE1 MPE2 INT MAR QUA
foreach A of local effects {
qui sum `A'
scalar  `A'_mean=r(mean)
dis `A'_mean
}  