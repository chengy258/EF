*利用可选项 rclass，生成并调用返回值
*------------------------begin---myfact6.ado----------------------*
capture program drop myfact6
program define myfact6, rclass // New
version 15

syntax anything(name=k) [, Format(string)]

  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  
  dis "`k'! = " `format' `a'
  
  return scalar kk = `a'        // New
  
end 
*------------------------end-----myfact6.ado----------------------*
