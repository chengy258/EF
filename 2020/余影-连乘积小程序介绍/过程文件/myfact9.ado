*自行创建 Mata 函数，并在 ado 文件中调用
*------------------------begin---myfact9.ado----------------------*
capture program drop myfact9
program define myfact9
version 15
args k    // 定义暂元 k，用于接收用户输入的数字
mata: m1(`k')
end

capture mata mata drop m1()
version 15
mata:
real rowvector m1(real scalar k)
{
real rowvector a
a = (1..k)
sum1 =1
for (i=1; i<=length(a); i++) {
  sum1 = sum1*a[i]
}
return(sum1) 
 }
end

myfact9 5
*------------------------end-----myfact9.ado----------------------*
