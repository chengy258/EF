 **唐佳敏**：中山大学  
**E-Mail:** <tangjm9@mail2.sysu.edu.cn>   

[TOC]

# **1. 引言**  

本文主要介绍了箱型图和小提琴图的原理及作用，以及在 Stata 中的实现，并且介绍了小提琴图在 Kellogg, M., Mogstad, M., Pouliot, G. A., & Torgovitsky, A. (2021) 论文中的应用----用蒙特卡洛模拟来评估不同反事实结果的估计方法。

# **2. vioplot 命令介绍及安装**  

## 2.1 安装方法  

`vioplot ` 是外部命令， Stata 中未自带需要安装，可使用如下命令进行安装  

```Stata
ssc install vioplot
```

## 2.2 语法结构  

`vioplot` 的基本语法结构如下：  

```Stata
vioplot varlist [if] [in] [weight] [,option]
```

# **3. 箱型图**  

## 3.1 箱型图的内容介绍  

箱型图（Box-plot）又称为盒须图、盒式图、盒状图或箱线图，因型状如箱子而得名，于 1977 年由美国著名统计学家**约翰·图基**（John Tukey）发明，是一种用作显示一组数据分散情况资料的统计图，它能显示出一组数据的最大值、最小值、中位数、及上下四分位数。在各种领域也经常被使用，常见于品质管理，快速识别异常值。箱形图最大的优点就是**不受异常值的影响，能够准确稳定地描绘出数据的离散分布情况，同时也利于数据的清洗**。  

在箱型图中，我们从上四分位数到下四分位数绘制一个盒子，然后用一条垂直触须（形象地称为“盒须”）穿过盒子的中间。上垂线延伸至上边缘（最大值），下垂线延伸至下边缘（最小值）。箱型图结构(实例）如下所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_1_box-plot.png)  

## 3.2  六大统计量  

箱形图的绘制主要包含六个数据节点，需要先将数据从小到大进行排列，然后分别计算出它的下边缘，下四分位数，中位数，上四分位数，上边缘，还有异常值。  

1. 下四分位数（$Q1$)：又称为“较小四分位数”，把一组值从小到大排列并分成四等分，处于三个分割点位置的得分就是四分位数，也就是样本所有数值从小到大排列后第 25% 的数字。可由 $Q1 = (n+1) * 0.25$ 计算。
2. 中位数（$Q2$）：又称“第二四分位数”，等于该样本中所有数值由小到大排列后第 50% 的数字。可由 $Q2 = (n+1) * 0.5$ 计算。
3. 上四分位数 ($Q3$)：又称“较大四分位数”，等于该样本中所有数值由小到大排列后第 75% 的数字。可由 $Q3 = (n+1) * 0.75$ 计算。
4. 下边缘：也称下限，是非异常范围内的最小值。可由 $QL=Q1-1.5IQR$ 计算。（$IQR$ 称为四分位距，$IQR=Q3-Q1$）。
5. 上边缘：也称上限，是非异常范围内的最大值。 可由$QL=Q1+1.5IQR$计算。
6. 异常值：大于上四分位数 1.5 倍四分位数差的值，或者小于下四分位数 1.5 倍四分位数差的值，划为异常值；超出四分位数差 3 倍距离的异常值称为极端异常值。

## 3.3 箱形图的优劣势

**优势：**

1. 直观明了地识别数据批中的异常值：箱型图的绘制依靠实际数据，不需要事先假定数据服从特定的分布形式，没有对数据作任何限制性要求，它只是真实直观地表现数据形状的本来面貌；另一方面，箱形图判断异常值的标准以四分位数和四分位距为基础，四分位数具有一定的耐抗性，多达 25% 的数据可以变得任意远而不会很大地扰动四分位数，所以异常值不会影响箱形图的数据形状，箱线图识别异常值的结果比较客观。由此可见，箱线图在识别异常值方面有一定的优越性。

2. 利用箱线图判断数据批的偏态和尾重：对于标准正态分布的样本，只有极少值为异常值。异常值越多说明尾部越重，自由度越小（即自由变动的量的个数）；而偏态表示偏离程度，异常值集中在较小值一侧，则分布呈左偏态；异常值集中在较大值一侧，则分布呈右偏态。
   
3. 利用箱线图比较几批数据的形状同一数轴上，几批数据的箱线图并行排列，几批数据的中位数、尾长、异常值、分布区间等形状信息便昭然若揭；各批数据的四分位距大小，正常值的分布是集中还是分散，观察各方盒和线段的长短便可明了。

**劣势：** 

1. 不能精确地衡量数据分布的偏态和尾重程度。
2. 对于批量比较大的数据，反映的信息更加模糊以及用中位数代表总体评价水平有一定的局限性。

# **4. 小提琴图**  

## 4.1 核密度估计介绍及 Stata 命令实现

由给定样本集合求解随机变量的分布密度函数问题是概率统计学的基本问题之一，解决这一问题的方法包括参数估计和非参数估计。参数估计，简单来讲，即假定样本集符合某一概率分布，然后根据样本集拟合该分布中的参数，例如：似然估计，混合高斯等，由于参数估计方法中需要加入主观的先验知识，参数模型的这种基本假定与实际的物理模型之间常常存在较大的差距，往往很难拟合出与真实分布一致的模型。和参数估计不同，非参数估计并不利用有关数据分布的先验知识，而是根据数据本身的特点、性质来拟合分布，这样能比参数估计方法得出更好的模型。

核密度估计（Kernel density estimation）就是一种用于估计概率密度函数的非参数方法，由 Rosenblatt (1955) 和 Emanuel Parzen(1962) 提出。核密度估计的原理其实是很简单的。在我们对某一事物的概率分布的情况下。如果某一个数在观察中出现了，我们可以认为这个数的概率密度很比大，和这个数比较近的数的概率密度也会比较大，而那些离这个数远的数的概率密度会比较小。基于这种想法，针对观察中的第一个数，我们都可以 $f(x-xi)$ 去拟合我们想象中的那个远小近大概率密度,当然其实也可以用其他对称的函数。针对每一个观察中出现的数拟合出多个概率密度分布函数之后，取平均；如果某些数是比较重要，某些数反之，则可以取加权平均。总而言之，核密度估计就是用来估计密度的，如果你有一系列空间点数据，那么核密度估计往往是比较好的可视化方法。

在 Stata 里，我们使用`kdensity`命令进行核密度估计，画出核密度图。`kdensity`命令是对变量未知密度函数分布时的一种估计，不是一种标准的密度分布函数（比如 $t$ 分布、正态分布）；由于以上原因，当设置不同的宽度时就会导致不同的形状，并且 Stata 有一个默认的 width（由于你两两比较时，默认 width 会跟随两个变量的分布同时发生变化，因此看起来就好像不一样，如果你做柱状图分析就更明显了）；当然，Stata 提供了 8 种 `kdensity` 函数，你也可以采用高斯核函数 `kernel(gaussian)`、三角核函数 `kernel(triangle)`等。

`kdensity`的基本语法结构如下：

```Stata
kdensity varname [if] [in] [weight] [, options]
```

Stata 实例及代码如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_2_Kdensity.png)

```Stata
 sysuse "auto.dta"
 kdensity price  if foreign==0 ,  addplot( ///  
     kdensity price if foreign==1)             ///  
     legend(label(1 "国内") label(2 "国外") col(2))
```

## 4.2 小提琴图的介绍

小提琴图  (Violin-plot) 是箱线图与核密度图的结合，用来展示多组数据的分布状态以及概率密度。这种图表结合了箱形图和密度图的特征，主要用来显示数据的分布形状。前文提到箱型图在批量较大的数据时有一定的局限性，而小提琴图跟箱形图类似，但是在密度层面展示更好，并且在数据量非常大不方便一个一个展示的时候小提琴图特别适用。

小提琴图是箱形图的升级加强版，它使用数据的核密度估计值代替了箱形图，并可选择叠加数据点本身，对数据分布有更丰富的理解，同时不必占用更多空间。在小提琴图中，可以轻松发现过于稀疏的数据或多模式分布，而这些在箱形图中可能不会被注意到。

小提琴图结构(实例）如下所示：中间的蓝色方块表示四分位数范围，从其延伸的幼细蓝线代表 95% 置信区间，而白点则为中位数。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_3_violin-plot.png)

与箱形图相比，小提琴图毫无疑问的优势在于：除了获取与箱形图中相同的统计信息外，它还显示了数据的整体分布。这个差异点很有意义，特别是在处理多模态数据时，即有多峰值的分布。小提琴图中较宽的部分代表观测值取值的概率较高，较窄的部分则对应于较低的概率。

Stata 另一实例以及代码如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_4_violin-plot.png)

```Stata
sysuse "auto.dta"
vioplot gear head, over(rep78)    ///
    legend( ring(0) pos(2) cols(1))   ///
    xtitle("Categories of Repair Record")    ///
    scheme(s2color)
```

# **5. 应用论文**  

「**Source[Full article: Combining Matching and Synthetic Control to Tradeoff Biases From Extrapolation and Interpolation (tandfonline.com)](https://www.tandfonline.com/doi/full/10.1080/01621459.2021.1979562)」

前面介绍了许多关于箱型图以及小提琴图的原理及作用，下面主要介绍小提琴图在 Kellogg, M., Mogstad, M., Pouliot, G. A., & Torgovitsky, A. (2021). Combining Matching and Synthetic Control to Trade oﬀ Biases from Extrapolation and Interpolation. Journal of the American Statistical Association, 1–13., [-PDF-](https://gitee.com/link?target=https%3A%2F%2Fsci-hub.ren%2F10.3386%2FW26624) 中的应用。

## 5.1 论文介绍

估计干预的因果效应是整个社会科学中的一项共同任务。当未处理单位的预处理趋势或特征与未处理单位有显著差异时，对于这些类型的比较案例研究，文章介绍了合成控制 (SC) 法、匹配估计法、惩罚性的合成控制估计法以及本文创造性提出的匹配合成控制法（MASC) 共四种方法来估计被处理单位的反事实结果，并通过蒙特卡洛模拟和安慰剂研究来评估不同的估计法，最终通过小提琴图显示了 MASC 在模拟和安慰剂研究中的表现要远远优于 SC 和其他替代方案。

### 5.1.1 合成控制法（SC）

由 Abadie and Gardeazabal (2003) 提出，并在后来由 Abadie et al. (2010, 2015) 阐述的合成控制（SC）法，其基本思想是，尽管控制组个体和处理组个体的特征不相似，但是可以对这些控制组个体进行某种加权，构造出处理组个体的反事实状态，分别称为“真实个体”与“合成个体”，因此叫做合成控制法。

合成控制法在 Abadie 等人 (2010, 2015) 的文章中被定义为
$$
\hat{u}_{t}^{sc} \equiv y_{0}^{'}\omega_{}^{sc}
$$
其中,
$$
\omega_{}^{sc} \equiv \mathop{arg min}\limits_{\omega \epsilon\\ S }||x_{1}-x_{0}^{'}\omega||^2
$$
其中 $y_{0}^{'}\epsilon R_{}^{n}$  为 $t$ 时刻未处理单位的观察结果，$x_{0}\epsilon R_{}^{n*k}$ 是一个包含未处理单元的协变量向量的矩阵， ǁ · ǁ 通俗点来说是指向量的模长。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_5_SC.png)

合成控制法可以比较好的解决由于时间产生的混杂因素的内生性问题，但如果处理组的特征远远大于或者小于控制组的特征，那么将找不到合适的权重构造处理组的反事实状态，也就无法利用控制组的特征向量的凸组合构建处理组的特征向量，也会产生内生性偏差。

### 5.1.2 匹配法

匹配方法也是政策评估中常用的一种方法，其主要思想是，根据某种“距离”将在控制组个体与处理组进行匹配，并用成功匹配的控制组个体的观测结果近似表示处理组个体的反事实结果，通过比较两组的平均差异作为政策的平均处理效应。

匹配估计法在本文中被定义为
$$
\hat{u}_{t}^{lo} \equiv \sum_{i\geq2}\kappa(||x_{i}-x_{1}||)y_{it}
$$
其中，$\kappa$ 是一个核函数。
$$
\kappa(\upsilon) = \begin{cases} 
1/m, & \text{if } m \geq\sum_{j\geq2}1[\upsilon\geq||x_{j}-x_{1}||]\\
0, & \text{otherwise}\\
\end{cases}
\\
\\
\hat{u}_{t}^{ma}(m) \equiv \frac{1}{m}\sum_{i\epsilon M} \equiv y_{0}^{'}\omega_{}^{ma}
$$
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_6_MA.png)

直观地说，匹配估计法只是取 m 个未处理单位的平均结果，这些单位具有最接近被处理单位的期前的特征。但是匹配法容易受到外推偏差的影响，即无法解决内生性问题。

### 5.1.3 惩罚性的合成控制法

基于合成控制法的缺陷，Abadie 和 L‘Hour（2019）提出了对合成控制法的补充，他们从 SC 估计法开始，对控制处理前特征不同于处理单元的未处理单元施加一个惩罚，不鼓励选择远离处理单位的单位。但是，Abadie 和 L‘Hour（2019）也讨论得出，如果有多个处理单位时，这个估计法就更有可能出现问题。他们将其定义为：
$$
\hat{u}_{t}^{pen} \equiv y_{0t}^{'}\omega_{}^{pen}\\

\text{当}\quad\omega_{}^{pen} \equiv \mathop{arg min}\limits_{\omega \epsilon\\ S }(1-\pi)||x_{1}-x_{0}^{'}\omega||^2 + \pi(\sum_{i\geq2}\omega_{i}||x_{i}-x_{1}||^2)
$$
其中，$\pi \epsilon[0,1]$ 是一个调整参数，它控制处理前特征不同于被处理单位的未处理单位加权所产生的惩罚。当 $\pi=0$ 时，$\hat{u}_{t}^{pen}=\hat{u}_{t}^{sc}$ ; 而对于 $\pi=1$, $\hat{u}_{t}^{pen}$ 等于 m=1 时的 $\hat{u}_{t}^{ma}(m)$。

### 5.1.4 匹配合成控制法（MASC）

本文作者通过对 SC 和匹配估计法进行比较，发现它们的缺点是不同的，而且是截然相反的：SC 估计器控制内生性，而不是插值偏差，而匹配估计器则相反。这种互补性表明，一个模型平均估计法将能够利用匹配和 SC 估计法的最佳性质。他们提出增加一个调整参数，使得 MASC 的结果是匹配法和合成控制法估计出来的结果的加权平均。

基于此，文章作者将 MASC 估计法定义为：
$$
\hat{u}_{t}^{masc} \equiv \phi\hat{u}_{t}^{ma}(m)+(1-\phi)\hat{u}_{t}^{sc} \equiv y_{0t}^{'}\omega_{}^{masc}
$$
其中，$\phi\epsilon[0,1]$ 是一个调整参数，并且 $\omega_{}^{masc} \equiv \phi\omega_{}^{ma}(m)+(1-\phi)\omega_{}^{sc}$。当 $\phi=1$，MASC 估计法等于匹配估计法；当 $\phi=0$，MASC 估计法等价于 SC 估计法。

## 5.2 应用

前面介绍了四种不同的估计方法，而后作者说明了不同的估计法在蒙特卡洛模拟中的结果。作者通过 $T=24$ 个时期抽取产生数据：
$$
Y_{it}=trend_{it}(\alpha)+V_{it}
$$
其中 $trend_{it}(\alpha)$ 是一个单位特定的时间趋势，取决于一个标量参数 $\alpha$ ；$V_{it}$ 对所有单位都是均值为 0 和标准差为 5 的正态分布。$trend_{it}(\alpha)$ 的完整表达式如下：
$$
\operatorname{trend}_{i t}=\rho_{i} \operatorname{trend}_{i(t-1)}^{\alpha}
$$
对于 $\alpha=1$，这是一个 AR(1)，而对于 $\alpha=0.95$，它变成了非线性的。被处理单位的自回归系数为 $\rho_{1}=100^{1-\alpha}$。对于 $i\geq2$ 的 7 个未处理单位，设为
$$
\log \left(\rho_{i}\right)=\left(\frac{1-\alpha}{1-0.95}\right)\left(\frac{13}{\sum_{s=0}^{12} 0.95^{s}}\right) \log \left(\rho_{1} \frac{\text { trend }_{11}}{\text { trend }_{i 1}}\right)
$$
作者指定初始条件为下列二者之一：
$$
\left(\begin{array}{c}\text { trend }_{1 t} \\ \text { trend }_{2 t} \\ \text { trend }_{3 t} \\ \text { trend }_{4 t} \\ \text { trend }_{5 t} \\ \text { trend }_{6 t} \\ \text { trend }_{7 t} \\ \text { trend }_{8 t}\end{array}\right)=\left(\begin{array}{c}100 \\ 10 \\ 40 \\ 70 \\ 110 \\ 130 \\ 160 \\ 190\end{array}\right) \quad \text or \quad\left(\begin{array}{c}\text { trend }_{1 t} \\ \text { trend }_{2 t} \\ \text { trend }_{3 t} \\ \text { trend }_{4 t} \\ \text { trend }_{5 t} \\ \text { trend }_{6 t} \\ \text { trend }_{7 t} \\ \text { trend }_{8 t}\end{array}\right)=\left(\begin{array}{c}100 \\ 10 \\ 25 \\ 40 \\ 110 \\ 160 \\ 175 \\ 190\end{array}\right),
$$
当 $\alpha=1$ ，使用第一组数据，通过蒙特卡洛模拟画出的图如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_7_α=1.png)

以上数据生成及画图过程转换为 Stata 代码如下：

```Stata
set seed 1357
local a = 1

local rho1 = 100^(1-`a')
dis "rho1 = " `rho1'

*导入第一组数据
clear
input id  t  trend
      1   1  100
      2   1  10
      3   1  40
      4   1  70
      5   1  110
      6   1  130
      7   1  160
      8   1  190
end
save sim01.dta, replace 

use "sim01.dta", clear 
gen trend11_divide_trendi1 = trend[1]/trend

forvalues i = 2/8{
   local trend_ratio_`i' = trend11_divide_trendi1[`i'] 
}

local sumSeason = 0
forvalues s=0/12{
  local sumSeason = `sumSeason' + 0.95^`s' 
}
dis "`sumSeason'"

*计算rho
  local rA = (1-`a')/(1-0.95)
  local rB = 13/`sumSeason' 
  *local rB = `sumSeason'/13
  dis "rA = " `rA'
  dis "rB = " `rB'
  
forvalues i=2/8{
  local rC  "log10(`rho1'*(`trend_ratio_`i''))"  // not ln() !
  dis "rC = " `rC'
  local rho`i' = 10^(`rA' * `rB' * `rC')
}

// display rho values
forvalues i=1/8{
  dis "rho`i' = " `rho`i''
}

*生成AR(1)数据集
drop trend11_di
reshape wide trend, i(t) j(id)  
expand 24  // t=1,2, ..., 24 期
replace t= _n
tsset t
forvalues i=1/8{
   replace trend`i' = `rho`i'' * trend`i'[_n-1]^`a' if t>1
}

forvalues i=1/8{
    gen y`i' = trend`i' + rnormal(0,5)
}

sum trend*
sum y*

*画图
set scheme s1mono
tw line y1 t, lw(*1.5) lc(black) ///
   legend(off) ylabel(0(50)300)
forvalues i=1/8{
  addplot: line y`i' t,  ///
      lp(dash) lc(black*0.7) ///
	  legend(off) ylabel(0(50)300)
}
```

以 SC 估计法为例，利用 $MATLAB$ 求解最优 $\omega_{}^{sc}$：

```matlab
p = [0; 0; 0; 0; 0; 0; 0]  %从p点开始求最小值点
[w,fval,exitflag,output] = fminunc(@objfun,p);

w  %最小值点
fval %
exitflag
output

function f = objfun(w)
x1 = 100;
x0 = [10 40 70 110 130 160 190];
f = (x1-x0*w)*(x1-x0*w);
end
```

```matlab
ans：
w =
    0.0103
    0.0411
    0.0719
    0.1131
    0.1336
    0.1644
    0.1953
```

将上面求解的 $\omega_{}^{sc}$ 结果输入到 Stata 中进行画图：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/唐佳敏_箱型图与小提琴图_8_sc-vioplot.png)

以上画图过程转换为 Stata 代码如下：

```Stata
use "sim01.dta", clear 
gen trend11_divide_trendi1 = trend[1]/trend

forvalues i = 2/8{
   local trend_ratio_`i' = trend11_divide_trendi1[`i'] 
}

local sumSeason = 0
forvalues s=0/12{
  local sumSeason = `sumSeason' + 0.95^`s' 
}
dis "`sumSeason'"

*计算rho
  local rA = (1-`a')/(1-0.95)
  local rB = 13/`sumSeason' 
  *local rB = `sumSeason'/13
  dis "rA = " `rA'
  dis "rB = " `rB'
  
forvalues i=2/8{
  local rC  "log10(`rho1'*(`trend_ratio_`i''))"  // not ln() !
  dis "rC = " `rC'
  local rho`i' = 10^(`rA' * `rB' * `rC')
}

forvalues i=1/8{
  dis "rho`i' = " `rho`i''
}

drop trend11_di
reshape wide trend, i(t) j(id)  
expand 24  // t=1,2, ..., 24 期
replace t= _n
tsset t
forvalues i=1/8{
   replace trend`i' = `rho`i'' * trend`i'[_n-1]^`a' if t>1
}

*输入w的值
gen w2 = 0.0103
gen w3 = 0.0411
gen w4 = 0.0719
gen w5 = 0.1131
gen w6 = 0.1336
gen w7 = 0.1644
gen w8 = 0.1953

*进行蒙特卡洛模拟
set seed 1234
postfile buffer sc using mcs,replace
gen uhat = 0
set obs 1000
gen v = 0
forvalues j=1/1000{
    replace uhat = 0
	replace v = rnormal(0,5)
    forvalues i=1/8{
        gen y`i' = trend`i' + v
		}
    forvalues i=2/8{
        replace uhat = uhat + w`i'*y`i'
	    }
    gen y_uhat = 24*1/4*[(y1-uhat)^2]
    qui mean y_uhat
	post buffer (_b[y_uhat])
	drop y*
}
postclose buffer

*画小提琴图
ssc install scheme_tufte, replace
use mcs,clear
set scheme tufte
vioplot sc,horizontal xlabel(0(45)90)
```

# **6. 参考文献**  

- [Stata：蒙特卡洛模拟A-(Monte-Carlo-Simulation)没那么神秘| 连享会主页 (lianxh.cn)](https://www.lianxh.cn/news/102cac954fa40.html)
- [百度百科-四分位数](https://baike.baidu.com/item/四分位数/5040599?fr=aladdin)
- [python绘制盒状图和小提琴图](https://blog.csdn.net/xc_zhou/article/details/87195643)
- [箱形图绘制-水平，并列等](https://blog.csdn.net/qq_41080850/article/details/83829045)
- [箱形图与小提琴图概念介绍](https://blog.csdn.net/ac540101928/article/details/79235591)
- [seaborn小提琴图](https://www.bobobk.com/206.html)
