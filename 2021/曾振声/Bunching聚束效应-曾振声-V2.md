# Bunching: 聚束效应及命令

&emsp;

> **作者：** 曾振声 (中山大学)
>
> **E-Mail:** <zengzhsh6@mail2.sysu.edu.cn>

---

**目录**

[toc]

---

## 1.引言

### 1.1 什么是聚束效应？

为了方便大家理解，笔者先举一个生活中的例子。

考虑一条正常的步行街，在天晴的时候，街道上和屋檐下的人群密集程度大致相近。天空中突然下起大雨，没带伞的人迅速躲到了屋檐下，而带了伞的人则撑开伞继续行走。此时，屋檐下的人数密度会突然增加，出现了所谓的聚集。

取个学术点的名字，就叫做`聚束`。

聚束效应（ bunching ) 是指在面临政策扰动时，个体出于自身效用最大化主动选择集中于一点，形成了类似离散分布才存在的质量点。从概率密度函数看，原连续的分布出现截断，质量点对应的概率大于 0 。

与断点回归（ RDD ）和拐点回归（ RK ）类似，聚束效应也出现了断点或拐点。但聚束效应与这两者之间存在一个关键的不同之处——在 RDD 或 RK 模型中，个体无法干预政策，其分布高于或低于阈值是外生的；而 bunching 恰好相反，个体面对突如其来的政策变化可以自由选择扎堆。

在聚束效应中，政策是内生的，我们研究的正是个体面对政策所表现出来的个人选择。因此在分析特定阈上观察到的离散跳跃时，要依据分配变量的可操作来选择方法。

### 1.2 为什么要讨论聚束效应？

在介绍聚束理论模型之前，我们不妨来看看关于聚束的讨论是如何起源的。聚束效应本质上是经济学家开发出来的方法，所以研究的问题一定是建立在现实背景下的。有关于聚束效应的讨论，实际上是来源于税收方面相关的研究。

许多国家的个人所得税实行阶梯税率，即累进税制。当个人收入突破一定阈值的时候，超出阈值的部分会征收更高的所得税，阈值前后对应了边际税率的改变。与此同时，存在另一种征税制度，在阈值前后，其平均税率发生改变。

经济学家关心的是所得税对于劳动力供给存在什么影响，并尝试求出劳动力供给（或收入）对税率变动的弹性。直观来看，在征收阶梯税的情况下，对于恰好高出零界点某个较小区间的人来说，由于边际税率的提高造成边际收益的下降，他们最优的选择不是提供原有量级的劳动，反而是减少劳动投入以获得更多的闲暇时间，将税前收入封顶在零界点。

尽管他们的税后可支配收入降低，会造成了消费效用的降低，但是闲暇时间的增多却带来了更大的效用提升，最终个人效用在零界点实现了最大化。与此同时，更高收入的人群也会相应的减少劳动投入，以获得新税率下的最高效用。

税率的突然改变，使原本分布在一个较小区间内的人群主动选择聚集在一个点上，于是就出现了聚束效应。对于政府而言，如何设计税率的变动量以防止过度伤害劳动者的积极性便值得深思。因此，收入对税率变动的弹性估计便成为了一个具有现实意义的研究问题。

随着行政数据可获得性的提高，聚束效应分析方法的应用日渐普遍。该方法在逐渐开始在社会保障、社会保险、福利项目、教育、管制、私人部门定价及参照依赖偏好等方面得以应用。

### 1.3 传统非线性预算集方法

> 传统非线性预算集方法也对聚束效应中的弹性进行了研究，但其出发点与本篇介绍的聚束理论有很大不同。

传统非线性预算集的计量经济学研究最早是 Burtless & Hausman (1978) 和 Hausman (1981) 开发的，他们考察了劳动力供给在负所得税和联邦所得税条件下的反应。

通过观察，他们发现了具有两种类型拐点的片状线形预算集。边际税率离散增加会造成凸型拐点，而离散下降会造成非凸型拐点。前者在收入分布中可以观察到聚束，后者则直接产生一个洞。

这种参数估计方法优点在于理论与经验间有明确的关系，但缺点是尽管其理论假设了模型存在聚束，调查数据却没有显示出任何聚束现象。按照 Saez (1999, 2010) 证实的拐点聚束的大小与劳动力供给的补偿弹性成正比，传统非线性预算集方法会导出零补偿弹性。

允许模型中存在两个误差则可以解决这个问题。第一个误差代表未能观测到的偏好异质性，第二个误差为优化误差，也可称为测量误差。偏好误差会影响个体最终选择的位置是否在拐点处，优化误差反映了个体无法精确调整工作时间，从而聚束受到现实约束。

为了估计补偿弹性和人们的行为反应，非线性预算集方法需要对这两个误差的分布进行假设，并采用参数估计方法对聚束和拐点进行识别。因此，数据中是否存在聚束或拐点就更多成为了数据拟合的技术性问题。而聚束点本身提供的反应性信息却没被用上。

本篇介绍的聚束理论与其不同，采用非参数估计的方法，仅仅从拐点局部发生的情况出发获得补偿弹性的估计。这种聚束理论可以捕捉政策的准实验变化，充分利用事实。

## 2. 聚束理论

以税收为例子，税率的改变大致分为两种情况：边际税率的改变及平均税率的改变。在不考虑摩擦的情况下，两种税率变动机制会造成两种不同的聚束效应。边际税率的变动会造成收入分布出现拐点，而平均税率的变动造成收入分布出现断点。

### 2.1 拐点（Kinks)

#### 2.1.1 模型搭建

边际税率不连续造成拐点的分析是由 Saez (2010) 开创的。考虑个人对税后收入 （ 消费价值 ）和税前收入 （ 努力成本 ）的偏好，定义如下效用函数：

$$u(z-T(z), z / n)      \quad (1)$$

其中，$z$ 代表收入，$T(z)$ 代表税收函数，$n$ 衡量个人能力, 能力的异质性由密度分布 $f(n)$ 捕获。假设能力分布、偏好和税收制度是平滑的，个人自我优化产生的收入分布也是平滑的，并用 $b_{0}(z)$ 表示收入的分布。

为了简化讨论，考虑线性税制 $T(z)=t \cdot z$ ，则边际税率变化前后的函数如（2）式：

$$T(z)=t \cdot z+\Delta t \cdot \left(z-z^{*}\right) \cdot \mathbf{I}\left(z>z^{*}\right) \quad (2)$$

考虑一般情况，即边际税率增长，则可做出如下预算集图。

![Fig1_kinks预算集图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig1_kinks预算集图_曾振声 .png)

当边际税率在节点 $z^*$ 从原先的 $t$ 提升至 $t+\Delta t$ 时，具有能力为 $n^* + \Delta n^*$ 的个体成为了边际聚束个体。在税率变化之前，他们的效用曲线和线性约束相切点的横坐标为 $z^* + \Delta z^* $ ，在边际税率产生的新线性约束下，其效用函数与新约束集不存在切点，因此拐点为其最大效用点。边际聚束个体的收入水平为 $z^*$.

所有最初位于区间（$z^*$，$z^* + \Delta z^* $) 上的个体都会聚集在拐点处，而所有最初位于 $z^* + \Delta z^* $ 以上的劳动者都会依据新的约束降低劳动投入。由于收入大于边际聚束个体的劳动者同步降低了劳动投入，聚束发生后，收入密度分布图中不会存在“空洞”。

![Fig2_kinks收入密度分布图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig2_kinks收入密度分布图_曾振声.png)

由上图所示，劳动者的收入分布从原先的连续下降，变为 $z^*$ 处存在聚束。聚束质量恰好等于 $\Delta z^* $ 阴影部分面积，也即 $\left(z^{*}, \infty\right)$ 处损失的全部质量。

#### 2.1.2 同质性偏好下的补偿弹性

进一步地，补偿收入弹性可以从边缘聚束个体中求出。边缘个体收入变化量 $z^* $ 可以理解为劳动力减少量，同时 $z^* $ 与超额聚束量成比例。

定义劳动力投入变化量对税率变化量的弹性：

$$e=\frac{\Delta z^{*} / z^{*}}{\Delta t /(1-t)} \quad(3)$$

在 $ \Delta t$ 变动很小的情况下，税率变动不会产生收入效应，式（3）中求得的弹性为补偿弹性, 它是聚束点处的局部弹性。当税率变动很大时，个体收入选择的改变还会收到收入效应的影响，此时弹性应由补偿弹性和未补偿弹性加权平均求得。

一般而言，我们仅考虑边际税率变动较小的情况，故本文不对未补偿弹性做过多赘述，仅提供未补偿弹性计算公式供读者参考。未补偿弹性 $e^{u}$ 的计算方法为 $e^{u}=e^{c}+\eta$ ， 其中 $\eta=(1-\tilde{t})(\partial z / \partial Y)$.

通过（4）式将总聚束质量 $B$ 与收入变动量 $ \Delta z^* $ 联系起来：

$$B=\int_{z^{*}}^{z^{*}+\Delta z^{*}} b_{0}(z) \mathrm{d} z \simeq b_{0}\left(z^{*}\right) \Delta z^{*} \quad(4)$$

由于 $ \Delta z^* $ 通常很小，一般可认为收入分布 $b_{0}(z)$ 在聚束段上为常数。

#### 2.1.3 异质性偏好假设下的弹性

上述分析建立在所有劳动者拥有同质效用函数 $u(\cdot)$ 的基础上，故得出 $z^*$ 处的弹性仅取一个值。若放宽同质性偏好假设，则可求得异质弹性。

定义 $\hat{f}(n, e)$ 为劳动者能力和弹性的联合概率分布， $\hat{b}_{0}(z, e)$ 为弹性 $e$ 的个体的收入分布。 对弹性进行积分求得总收入分布 $b_{0}(z)=\int_{e} \hat{b}_{0}(z, e) \mathrm{d} e$.

在任一弹性水平下，边际聚束个体减少的收入量为 $\Delta z_{e}^{*}$。不同弹性下收入减少量的均值与总的聚束质量有如下关系：

$$
B=\int_{e} \int_{z^{*}}^{z^{*}+\Delta z_{e}^{*}} \hat{b}_{0}(z, e) \mathrm{d} z \mathrm{~d} e \simeq b_{0}\left(z^{*}\right) E\left[\Delta z_{e}^{*}\right] \quad(5)
$$

上式将局部平均收益弹性与聚束质量联系了起来。当拐点税率变动很小时，式（5）有效。

但是，当拐点处税率变动很大时，弹性还受收入效应的影响。因此有必要以参数形式对劳动者的效用函数进行假设，以获得补偿收入弹性的准确估计。

设准线性等弹性效用函数形式如下：

$$
u=z-T(z)-\frac{n}{1+1 / e} \cdot\left(\frac{z}{n}\right)^{1+1 / e}
\quad(6)
$$

上式的假定可将收入效应剔除在外，在该假设下劳动者此时的收入：

$$
z=n(1-t)^{e} \quad(7)
$$

为了求解一般化的补偿弹性，我们考虑边际聚束个体在聚束前后均满足预算集与效用函数相切的条件.

通过对（6）式进行假设得出（7）式，用（7）式表示两个相切条件：

`真实情况` $z^{*}=\left(n^{*}+\Delta n^{*}\right)(1-t-\Delta t)^{e}$ ;

`反事实` $z^{*}+\Delta z^{*}=\left(n^{*}+\Delta n^{*}\right)(1-t)^{e}$;

联立可解出下式：

$$
\frac{z^{*}+\Delta z^{*}}{z^{*}}=\left(\frac{1-t}{1-t-\Delta t}\right)^{e}
\quad(8)
$$

等价于：

$$
e=-\frac{\log \left(1+\Delta z^{*} / z^{*}\right)}{\log (1-\Delta t /(1-t))}
\quad(9)
$$

式（9）给出的弹性估计，是式（3）弹性的一般化形式。当 $\Delta t$ 很小的时候，$\Delta z^*$ 很小，我们有 $\log \left(1+\Delta z^{*} / z^{*}\right) \approx \Delta z^{*} / z^{*}$ 且 $\log (1-\Delta t /(1-t)) \approx-\Delta t /(1-t)$.

参数化估计的得出的弹性在税率变动量很小的时候，结果与非参数估计得出的补偿弹性一致。这也意味着当税率变动很小的时候，可简单采用非参数估计，而当税率变动大的时候，需要引入参数假定。

#### 2.1.4 非凸拐点

前面讨论的聚束效应，是在边际税率上升的假设下开展的，约束集产生凸拐点。而当边际税率离散下降时，会产生非凸拐点。

非凸拐点的聚束表现与凸拐点大为不同，劳动者收入分布在聚束点 $z^* $ 周围会产生一个洞。本来收入在阈值以下某特定范围内的个体由于税收激励，劳动投入严格聚集在了阈值以上。

而收入低于边际聚束个体的劳动者则无动于衷，他们并不会受到边际税率下降的激励。这类劳动者提高其劳动投入使自身税前收入位于 （$z^* - \Delta z^*$，$z^* $) 对其并没有任何好处，反而会降低效用。

因此，在该区间内，收入分布出现空洞。

### 2.2 断点 ( Notches )

同样是税率变动，平均税率的变化引起的聚束与边际税率变动引起的聚束完全不同。

Kleven & Waseem ( 2013 ) 研究了税务责任不连续的问题，提出了断点聚束效应。关于劳动者收入分布及能力分布均等假设均与拐点聚束效应时一致。断点聚束分析同样可以先从同质性收入弹性开始，推广到异质性弹性。

#### 2.2.1 模型建立

考虑平均税率在阈值后上升的情况 ( 比例税率的不连续性 ) ，税收函数表示如下：

$$
T(z)=t \cdot z+\Delta t \cdot z \cdot \mathbf{I}\left(z>z^{*}\right)
\quad(10)
$$

如下图所示，收入位于 $\left(z^{*}, z^{*}+\Delta z^{*}\right)$ 内的个体由于平均税率提升在断点处聚束。对于收入为 $z^* + \Delta z^* $ 的边际聚束个体，他们在断点 $z^* $ 与税率变动后的最优内部点 $z^I$ 中无偏好。

![Fig3_Notches预算集图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig3_Notches预算集图_曾振声.png)

此时，没有任何个体愿意选择其劳动收入位于区间 $\left(z^{*}, z^{I}\right)$ 内，从收入分布上看则对应出现了空洞。收入高于边际聚束个体的劳动者由于税收降低积极性，也会相应减少劳动投入，但是其收入仍然位于阈值以上。

在这种情况下，断点聚束产生了一个完全被占优的区间 $\left(z^{*}, z^{*}+\Delta z^{D}\right)$ ，如下图所示。

![Fig4_Notches同质弹性收入分布](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig4_Notches同质弹性收入分布_曾振声.png)

在该区间内，劳动者通过向断点 $z^* $ 移动可以同时实现剩余消费和闲暇时间的提升。因此，在任何偏好假定下，该区间都是被完全占优的。被占优区间的存在，为聚束区间提供了下界。当补偿收入的弹性为 0 时，聚束区间恰好为被占优区间。

同时，图中阴影部分面积即为断点处的聚束量。

#### 2.2.2 同质偏好下的弹性

在效用函数同质的情况下，与拐点相同，断点聚束同样利用边际聚束个体的反应估计补偿收入弹性。利用边际聚束个体在断点 $z^* $ 和内部最优点 $z^I$ 效用无差异可进行求解，过程如下。

基于式(6)假定，可写出断点 $z^* $ 处的效用:

$$
u^{*}=(1-t) z^{*}-\frac{n^{*}+\Delta n^{*}}{1+1 / e}\left(\frac{z^{*}}{n^{*}+\Delta n^{*}}\right)^{1+1 / e}
\quad(11)
$$

基于偏好假定的一阶导数条件，得出 $z^I$, 代入效用函数求出 $z^I$ 处效用：

$$
u^{\mathrm{I}}=\left(\frac{1}{1+e}\right)\left(n^{*}+\Delta n^{*}\right)(1-t-\Delta t)^{1+e}
\quad(12)
$$

利用效用相等进行求解，得下式：

$$
\frac{1}{1+\Delta z^{*} / z^{*}}-\frac{1}{1+1 / e}\left[\frac{1}{1+\Delta z^{*} / z^{*}}\right]^{1+1 / e}-\frac{1}{1+e}\left[1-\frac{\Delta t}{1-t}\right]^{1+e}=0
\quad(13)
$$

式（13) 给出了收入变动百分比、税率变动百分比以及收入补偿弹性的关系，此时弹性为前两者的隐函数。

在无优化摩擦的情况下，当补偿弹性趋近 0 时，式 （13） 表明聚束区间存在下界，该下界大小恰好为完全被占优区间的大小：

$$
\lim _{e \rightarrow 0} \Delta z^{*}=\frac{\Delta t \cdot z^{*}}{1-t-\Delta t} \equiv \Delta z^{\mathrm{D}}
\quad(14)
$$

#### 2.2.3 异质偏好下的弹性

与拐点聚束中同质偏好类似，将 $(0, \bar{e})$ 区间内的弹性进行积分，可得到收入分布如下图所示：

![Fig5_Notches异质弹性收入分布](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig5_Notches异质弹性收入分布_曾振声.png)

异质性偏好下，收入分布仅仅在严格被占优的区间出现空洞，这是因为拥有异质偏好的劳动者有部分可能会在区间 $\left(z^{*} +\Delta z^{D}, z^{I}\right)$ 内取得最大效用。故断点聚束引发收入分布的空洞，会逐渐收敛于反事实。

利用式 （5） 可将聚束量 $B$ 与平均收入反应 $E\left[\Delta z_{e}^{*}\right]$ 联系起来。

再利用式 （13） 可以估计出在平均收入反应 $E\left[\Delta z_{e}^{*}\right]$ 下的弹性，该弹性通常不等于所有弹性的均值。

#### 2.2.4 平均税率下降

平均税率下降的情况与平均税率上升的情况最大不同之处在于：它不会创造一个完全被占优区间。

这是因为在平均税率下降时，目标个体向上进行聚束，在获得更多消费剩余的同时一定伴随闲暇时间的减少。当弹性趋近于 0 时，聚束区间大小 $\Delta z^* $ 也会趋近于 0 ，而非 $\Delta z^D$.

在这种情况下，同样可以利用断点及边际聚束个体原切点等效用，求解补偿弹性。这种方法依赖效用函数的参数假设，下式给出收入变动、税率变动及补偿弹性的隐函数，详细分析不再赘述。

$$
\left(1-\frac{\Delta z^{*}}{z^{*}}\right)+e\left(1-\frac{\Delta z^{*}}{z^{*}}\right)^{-1 / e}-(1+e)\left(1+\frac{\Delta t}{1-t}\right)=0
\quad(15)
$$

由于弹性实际上可以同式 （3）中一样进行非参数识别，Kleven & Waseem (2013) 开发出了一套简化的方法。

这种方法通过引入隐性边际税率 —— 一种类似在聚束区间的平均税率:

$$
t^{*} \equiv\left[T\left(z^{*}+\Delta z^{*}\right)-T\left(z^{*}\right)\right] / \Delta z^{*} \approx t+\Delta t \cdot z^{*} / \Delta z^{*}
\quad(16)
$$

从而给出了简化的弹性表达，如下式所示：

$$
e_{\mathrm{R}} \equiv \frac{\Delta z^{*} / z^{*}}{\Delta t^{*} /\left(1-t^{*}\right)} \approx \frac{\left(\Delta z^{*} / z^{*}\right)^{2}}{\Delta t /(1-t)}
\quad(17)
$$

### 2.3 优化摩擦

现实中，聚束效应的发生并没有前面讨论的那么理想。

个体可能会面临较高的调整成本、注意力成本 ( Chetty et al. 2011, Chetty 2012, Kleven & Waseem2013 )。个体还可能无法精确地瞄准零界点。这都会导致聚束现象表现为弥漫性多余质量而非点质量。

在这种情况下，原先聚束量表达式 $B=B(e, \mathbf{x})$ 就应该扩展为 $B=B(e, \phi, \mathbf{x})$，其中 $\phi$ 表征优化摩擦。

断点聚束相比于拐点聚束而言，更不容易收到摩擦的影响，原因是平均税率的改变相比边际税率改变力度更大，劳动者拥有更强的动力去克服摩擦。

## 3. 聚束估计量

本文主要对拐点聚束展开下述讨论。聚束效应中，各种方法均是采用质量点去估计弹性参数。

Bertanha et al. (2021) 开发了半参数估计和非参数估计的方法，这种相比之前讨论的估计方法依赖更少的假设，因此本文主要介绍该方法及其估计。

考虑聚束发生过程，代理人会将其等弹性的准线性效用函数最大化，这导致了其最优收入的数据生成过程（ DGP ）如下：

$$
y_{i}= \begin{cases}\varepsilon s_{0}+n_{i}^{*}, & \text { if } n_{i}^{*}<\underline{n}\left(k, \varepsilon, s_{0}\right) \\ k, & \text { if } \underline{n}\left(k, \varepsilon, s_{0}\right) \leq n_{i}^{*} \leq \bar{n}\left(k, \varepsilon, s_{1}\right) \\ \varepsilon s_{1}+n_{i}^{*}, & \text { if } n_{i}^{*}>\bar{n}\left(k, \varepsilon, s_{1}\right) .\end{cases}
\quad(18)
$$

- 其中 $y_{i}=\log \left(Y_{i}\right)$ 表示报告收入的自然对数

- $n_{i}^{*}=\log \left(N_{i}^{*}\right)$ 表示个体 $i$ 未被观测到的能力异质性

- $\varepsilon$ 为待估计弹性；

- 预算集的斜率在拐点 $k$ 由 $s_0$ 变为 $s_1$ ，其中 $s_{j}=\log \left(1-t_{j}\right), j \in\{0,1\}$ ，$t_j$ 表示边际税率，考虑边际税率递增

- 式中的阈值分别表示能力的下限和上限 $\underline{n}\left(k, \varepsilon, s_{0}\right)=k-\varepsilon s_{0}$ ； $\bar{n}\left(k, \varepsilon, s_{1}\right)=k-\varepsilon s_{1}$ ；

上述的数据生成过程表明聚束的质量取决于区间 $\left[\underline{n}\left(k, \varepsilon, s_{0}\right), \bar{n}\left(k, \varepsilon, s_{1}\right)\right]$ 的大小。

下式给出代估计变量间的关系：

$$
\begin{gathered}
B \equiv \mathbb{P}\left(y_{i}=k\right)=\mathbb{P}\left(\underline{n}\left(k, \varepsilon, s_{0}\right) \leq n_{i}^{*} \leq \bar{n}\left(k, \varepsilon, s_{1}\right)\right) \\
=F_{n^{*}}\left(\bar{n}\left(k, \varepsilon, s_{1}\right)\right)-F_{n^{*}}\left(\underline{n}\left(k, \varepsilon, s_{0}\right)\right)
\end{gathered}
\quad(19)
$$

式（19）包括了 5 个变量: 分别为 ( a ） $y$ 的累积分布函数，( b ) 聚束点 $k$ ，( c ) 线性约束 $s_j$ ，( d ) 表征能力异质性的累积分布函数 $F_n*$ 以及 ( e ) 弹性 $\varepsilon$.

式（18）将变量 （ b ) ~ ( e ) 映射到了可观测的 $y$ 的累积分布中。而变量 ( a ) ~ ( c ) 可直接被观测。因此，通过上面 2 个式可以解出弹性。

传统的弹性估计方法需要对能力的累积分布进行相关假设，特别地，需要为 $F_n*$ 设定特定函数。

Bertanha et al. (2021) 则采用了两种不同的策略。第一个策略是通过对非参数的异质性分布系列 $F_n∗$ 做一个温和的形状限制来确定弹性的上界和下界，从而部分确定弹性。第二种策略是利用协变量和对异质性分布的半参数化限制来确定弹性。

## 4. Stata 命令介绍

本文着重介绍 Bertanha et al. (2021) 提出的估计方法。其第一种策略由命令 `bunchbounds` 实现，该策略通过假设异质性概率密度函数（ PDF ）的斜率大小的约束，即 Lipschitz 连续性，来部分识别弹性。

第二种策略由`bunchtobit`实现，这是一种半参数方法，利用协变量来进行点识别。它依赖于 bunching 可以被重写为一个中间删减的回归模型。

由于现实存在摩擦误差，因此在实施上述两种策略前，通常先使用`bunchfilter`对 $y$ 的摩擦误差进行过滤。

命令 `bunching`则以集成的方式，实现上述三个命令同样的效果。

### 4.1 bunchbounds 命令

```Stata
*- 安装外部命令
ssc install bunching
net install lpdensity, from(https://raw.githubusercontent.com/nppackages/lpdensity/master/stata) replace
// 使用 bunchbounds 命令前必须安装 lpdensity
```

所有命令只需安装`bunching`即可，安装了这个后其余命令等同于也安装了。

```
*- bunchbounds语法结构
bunchbounds depvar [if] [in] [weight] , kink( ) s0( ) s1( ) m( )  ///
[ nopic savingbounds(filename[,replace]) ]
```

其中，

- `varname`: 响应变量，如收入的对数；
- `kink`: 结点的位置，必须是一个与响应变量单位相同的实数，在该节点税率等发生改变；
- `s0`: 是一个实数；在很多应用中，它是扭结点之前斜率的对数;
- `s1`: 必须是一个严格小于 s0 的实数；在许多应用中，它是扭结点之后斜率的对数;
- `m`: 是未观察到的异质性的概率密度函数（PDF）的最大斜率幅度，是一个严格的正标量

`options` 选项：

- `nopic`: 抑制显示图形；默认是显示图形
- `savingbounds(filename[,replace])`: 将部分识别集的坐标作为异质性 PDF 的斜率大小的函数保存在文件名.dta 中；

### 4.2 bunchtobit 命令

采用串联、Tobit 回归和协变量来确定响应变量对预算斜率变化的弹性。 在 Stata 中，根据 Bertanha, McCallum, and Seegert (2021)的程序设定。

该命令用数据的不同子样本运行一系列中间删失的 tobit 回归，且该过程从全样本开始，按照扭结点为中心的对称窗口进行收缩。

弹性估计值被看成是使用数据百分比的函数，最终该命令会给出每个截断窗口中最合适的 tobit 分布。

```Stata
*- bunchtobit语法结构
bunchtobit depvar [indepvars] [if] [in] [weight] , kink( ) s0( ) s1( )  ///
[ binwidth(#) grid(numlist) nopic numiter(#) savingtobit(filename[,replace]) verbose ]
```

其中，

- `depvar`: 响应变量
- `kink`: 结点的位置，必须是一个与响应变量单位相同的实数，在该节点税率等发生改变；
- `s0`: 是一个实数；在很多应用中，它是扭结点之前斜率的对数;
- `s1`: 必须是一个严格小于 s0 的实数；在许多应用中，它是扭结点之后斜率的对数;

`options` 选项：

- `grid(numlist)`: 是一个从 1 到 99 的整数列表；列表中的值对应于样本的百分比，这些百分比定义了关键点周围的对称截断窗口；截断的 Tobit 模型在这些样本和全样本上进行估计，因此估计的数量总是比列表中的条目数量多一个。例如，如果 grid(15 82)，那么 bunchtobit 就会对 Tobit 模型进行三次估计，分别使用 kink 点周围 100%、82% 和 15% 的数据；numlist 的默认值是 10(10)90 ，它提供 10 个估计值；
- `verbose`: 显示 Tobit 估计的详细输出，包括最大化似然的迭代；默认不显示；
- `numiter(#)`: 最大化 Tobit 可能性时允许的最大迭代次数；它必须是一个正整数，默认为 500；
- `binwidth(#)`: 直方图的分档宽度
- `nopic`: 抑制显示图形；默认是显示图形
- `savingtobit(filename[,replace])`: 保存带有每个截断窗口的 Tobit 估计值的 filename.dta；filename.dta 文件包含八个变量，对应于代码存储在 r( ) 中的矩阵；

### 4.3 bunchfilter 命令

带有摩擦误差的数据的分布是连续的，没有质量点。这种类型的数据在经济学的扎堆应用中很常见。例如，应税收入的分布通常在边际税率变化的拐点处有一个驼峰，而不是在拐点处有一个质量点。

该命令可在该类混合离散数据中，去除连续分布的摩擦误差。

```Stata
*- bunchfilter语法结构
bunchfilter depvar [if] [in] [weight] , kink(#) deltam(#) deltap(#) generate(varname)  ///
[ binwidth(#) nopic pctobs(#) polorder(#) ]

```

其中，

- `depvar`: 响应变量
- `kink`: 结点的位置，必须是一个与响应变量单位相同的实数，在该节点税率等发生改变；
- `deltam`: 是结点与要过滤的摩擦误差的下限之间的距离；它必须是一个实数，且与响应变量的单位相同；
- `deltap`: 是结点与要过滤的摩擦误差的上限之间的距离；它必须是一个实数，且与响应变量的单位相同；
- `generate(varname)`: 生成过滤后的变量，用户指定的名称为 varname

`options` 选项：

- `binwidth(#)`: 直方图的分档宽度
- `nopic`: 抑制显示图形；默认是显示图形
- `pctobs(# real)`: 为了达到更好的拟合效果，多项式回归使用的是结点周围的对称窗口中百分之 pctobs 的样本观测值。默认值为 40（ 取整数，最小=1，最大=99）；
- `polorder(# integer)`: 多项式回归的最大阶数；默认值为 7（最小=2；最大=7）；

### 4.4 bunching 命令

`bunching` 是 `bunchbounds`,`bunchtobit`,`bunchfilter` 命令的组合。

```Stata
*- 语法结构
bunching depvar [indepvars] [if] [in] [weight] , kink(#) s0(#) s1(#) m(#)  ///
[ nopic savingbounds(filename[,replace]) binwidth(#) grid(numlist) numiter(#)  ///
savingtobit(filename[,replace]) verbose  ///
deltam(#) deltap(#) generate(newvar) pctobs(#) polorder(#) ]
```

该命令的详细介绍可从上述三个命令的介绍中获得，故不赘述。

## 5. Stata 应用实例

采用 (18) 式的数据生成过程，让扭结点 $k=\ln (8)$，弹性为 0.5， 斜率 $s0=\ln (1.3)$, $s1=\ln (0.9)$, 按照如下式生成数据：

$$
y_{i}= \begin{cases}0.5 \ln (1.3)+n_{i}^{*}, & \text { if } n_{i}^{*}<\ln (8)-0.5 \ln (1.3) \\ \ln (8), & \text { if } \ln (8)-0.5 \ln (1.3) \leq n_{i}^{*} \leq \ln (8)-0.5 \ln (0.9) \\ 0.5 \ln (0.9)+n_{i}^{*}, & \text { if } n_{i}^{*}>\ln (8)-0.5 \ln (0.9)\end{cases}
\quad(20)
$$

数据生成可直接用`webuse`连接，代码如下。

```Stata
*- DGP
webuse set "http://fmwww.bc.edu/repec/bocode/b/"
webuse bunching.dta
histogram y
```

做出模拟数据的直方图如下，观察到数据存在聚束效应。

![Fig6_模拟数据直方图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig6_模拟数据直方图_曾振声.png)

### 5.1 估计弹性边界

运用 `bunchbounds` 命令估计弹性边界，选择 $k$ = 2.0794, 最大斜率为 2， 税率按照数据生成过程。具体代码如下：

```Stata
bunchbounds y [fweight=w], k(2.0794) s0(0.2624) s1(-0.1054) m(2)
```

Stata 运行结果如下：

```Stata
. bunchbounds y [fweight=w], k(2.0794) s0(0.2624) s1(-0.1054) m(2)

Your choice of M:
2.0000

Sample values of slope magnitude M
 minimum value M in the data (continuous part of the PDF):
  0.0000
 maximum value M in the data (continuous part of the PDF):
  0.3879
 maximum choice of M for finite upper bound:
  1.5933
 minimum choice of M for existence of bounds:
  0.0090

Elasticity Estimates
 Point id., trapezoidal approx.:
  0.4893
 Partial id., M = 2.0000 :
  [0.3912 , +Inf]
 Partial id., M = 1.59 :
  [0.4055 , 0.9353]

```

做图如下：

![Fig7_弹性边界估计](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig7_弹性边界估计_曾振声.jpg)

如图所示，在选择最大斜率为 2 的情况下，弹性的边界为 0.3914 至正无穷。同时，有图中的实竖线给出了弹性存在有限上界时的最大斜率为 1.5923， 此时弹性估计的范围为 [0.4055 , 0.9353]。注意到，在任何情况下，真实弹性 0.5 都在估计的弹性边界内。

该图还显示了随着最大斜率的增加，上界如何增加及以下界如何减少。使用梯形近似法确定的弹性点发生在边界聚集的水平红线处。

### 5.2 弹性的半参数点估计

运用 `bunchtobit` 实现弹性的半参数点估计，该估计不要去异质性 PDF 服从正态分布（Bertanha 等人，2021）。可以通过比较正确指定的模型的估计值和错误指定的模型的估计值来进行稳健性检验。

该命令默认对十个不同的子样本进行了弹性估计。每个子样本都是对称截断的，且以临界点为中心，并包括临界点的观测值。

Stata 代码及运行结果如下：

```Stata
. bunchtobit y x1 x2 x3 , k(2.0794) s0(0.2624) s1(-0.1054) binwidth(0.08)

Obtaining initial values for ML optimization.
Truncation window number 1 out of 10, 100% of data.
Truncation window number 2 out of 10, 90% of data.
Truncation window number 3 out of 10, 80% of data.
Truncation window number 4 out of 10, 70% of data.
Truncation window number 5 out of 10, 60% of data.
Truncation window number 6 out of 10, 50% of data.
Truncation window number 7 out of 10, 40% of data.
Truncation window number 8 out of 10, 30% of data.
Truncation window number 9 out of 10, 20% of data.
Truncation window number 10 out of 10, 10% of data.

bunchtobit_out[10,5]
        data %  elasticity     std err  # coll cov        flag
 1         100   .50526488   .00688012           0           0
 2          90   .50574235   .00710933           0           0
 3          80   .50635666    .0071992           0           0
 4          70   .50547742   .00724238           0           0
 5          60   .50612254   .00732554           0           0
 6          50   .50550213   .00750714           0           0
 7          40   .50566617    .0079238           0           0
 8          30   .50529854   .00857912           0           0
 9          20   .50243633   .01001321           0           0
10          10     .492104    .0118377           0           0

```

由于模型被正确地指定，对于任何被截断的子样本来说，报告的弹性估计值都非常接近真实值 0.5。

`bunchtobit` 还为每个子样本生成一个最佳拟合图，以及所有子样本的弹性估计图。下图列示了 100% 截断子样本的最佳拟合图。下图中黑色拟合线为截断的 Tobit 模型隐含结果变量的估计值。

![Fig8_bunchtobit_distr_100](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig8_bunchtobit_distr_100_曾振声.png)

同时，画出弹性对应的每个截断子样本的估计值（黑线）和 95%置信区间（灰色阴影）如下图。

![Fig9_bunchtobit弹性估计值](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig9_bunchtobit弹性估计值_曾振声.png)

最后一个子样本的协变量系数可以通过使用 estimates replay 命令获得，命令和代码如下：

```Stata
. estimate replay

---------------------------------------------------------------------------------------------------------
active results
---------------------------------------------------------------------------------------------------------

Log pseudolikelihood =  .14897296               Number of obs     =     10,000

 ( 1)  [eq_l]x1 - [eq_r]x1 = 0
 ( 2)  [eq_l]x2 - [eq_r]x2 = 0
 ( 3)  [eq_l]x3 - [eq_r]x3 = 0
------------------------------------------------------------------------------
             |               Robust
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
eq_l         |
          x1 |     -0.313      0.165    -1.90   0.057       -0.636       0.010
          x2 |      4.021      1.483     2.71   0.007        1.114       6.928
          x3 |      0.444      0.179     2.47   0.013        0.092       0.795
       _cons |      4.553      0.938     4.86   0.000        2.716       6.391
-------------+----------------------------------------------------------------
eq_r         |
          x1 |     -0.313      0.165    -1.90   0.057       -0.636       0.010
          x2 |      4.021      1.483     2.71   0.007        1.114       6.928
          x3 |      0.444      0.179     2.47   0.013        0.092       0.795
       _cons |      4.169      0.866     4.81   0.000        2.471       5.866
-------------+----------------------------------------------------------------
lngamma      |
       _cons |      0.754      0.201     3.76   0.000        0.361       1.147
-------------+----------------------------------------------------------------
       sigma |      0.470      0.427                         0.317       0.697
      cons_l |      2.142      1.564                        -0.923       5.208
      cons_r |      1.961      1.448                        -0.878       4.800
         eps |      0.492      0.012                         0.469       0.515
------------------------------------------------------------------------------

```

### 5.3 摩擦误差

当摩擦误差存在时，必须首先将其过滤掉，然后才能应用聚类估计方法。采用 `bunchfilter` 操作及结果如下：

```Stata
. bunchfilter yfric [fw=w], kink(2.0794) generate(yfiltered) deltam(0.12) deltap(0.12)

[ 10% 20% 30% 40% 50% 60% 70% 80% 90% 100% ]

```

对比下图过滤摩擦前后的 CDF 和 PDF 图，未过滤摩擦前，CDF 几乎连续，没有出现聚束。而 PDF 函数聚束也不明显。

实际上，如果没有摩擦误差，按照数据生成过程，有 5.16%的响应集中在扭结处。但存在摩擦误差会将这一比例降低到零。在用 bunchfilter 去除摩擦后，过滤后的数据有 5.15%的响应在扭结处。

![Fig10_bunchfilter_two_cdf](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig10_bunchfilter_two_cdf_曾振声.jpg)

![Fig11_bunchfilter_pdf](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/bunching聚束效应_Fig11_bunchfilter_pdf_曾振声.jpg)

- 上述三条命令可以使用 `bunching` 一次性实现，操作如下，不展开赘述分析。

```Stata
.         bunching yfric x1 x2 x3, k(2.0794) s0(0.2624) s1(-0.1054) m(2) gen(yfilter) ///
>         deltam(0.1054) deltap(0.0953) pctobs(30) polorder(7)

***********************************************
Bunching - Filter
***********************************************

[ 10% 20% 30% 40% 50% 60% 70% 80% 90% 100% ]
***********************************************
Bunching - Bounds
***********************************************


Your choice of M:
2.0000

Sample values of slope magnitude M
 minimum value M in the data (continuous part of the PDF):
  0.0000
 maximum value M in the data (continuous part of the PDF):
  0.3334
 maximum choice of M for finite upper bound:
  1.5388
 minimum choice of M for existence of bounds:
  0.0787

Elasticity Estimates
 Point id., trapezoidal approx.:
  0.4960
 Partial id., M = 2.0000 :
  [0.3944 , +Inf]
 Partial id., M = 1.54 :
  [0.4112 , 0.9586]


***********************************************
Bunching - Tobit
***********************************************

Obtaining initial values for ML optimization.
Truncation window number 1 out of 10, 100% of data.
Truncation window number 2 out of 10, 90% of data.
Truncation window number 3 out of 10, 80% of data.
Truncation window number 4 out of 10, 70% of data.
Truncation window number 5 out of 10, 60% of data.
Truncation window number 6 out of 10, 50% of data.
Truncation window number 7 out of 10, 40% of data.
Truncation window number 8 out of 10, 30% of data.
Truncation window number 9 out of 10, 20% of data.
Truncation window number 10 out of 10, 10% of data.

bunchtobit_out[10,5]
        data %  elasticity     std err  # coll cov        flag
 1         100   .50547489   .00688255           0           0
 2          90   .50598003   .00711342           0           0
 3          80   .50660355   .00720293           0           0
 4          70   .50573242   .00724542           0           0
 5          60   .50638729   .00732774           0           0
 6          50   .50580319   .00750924           0           0
 7          40   .50607059   .00792865           0           0
 8          30   .50599607   .00859321           0           0
 9          20   .50440375   .01006951           0           0
10          10   .50887838   .01033949           0           0

```

## 6. 参考资料

- Bertanha, Marinho, Andrew H. McCallum, Alexis Payne, and Nathan Seegert (2021). “Bunching estimation of elasticities using Stata,” Finance and Economics Discussion Series 2021-006. [-Link-](https://doi.org/10.17016/FEDS.2021.006).

- Kleven Henrik Jacobsen. 2016. Bunching. Annual Review of Economics, Vol. 8, pp. 435-464. [-Link-](https://doi.org/10.1146/annurev-economics-080315-015234), [-PDF-](http://sci-hub.ren/10.1146/annurev-economics-080315-015234)
- Bertanha, M., McCallum, A., Seegert, N. (2021), "Better Bunching, Nicer Notching". Working paper SSRN 3144539.
- Bertanha, M., McCallum, A., Payne, A., Seegert, N. (2021), "Bunching Estimation of Elasticities Using Stata".
- Finance and Economics Discussion Series 2021-006. [-Link-](https://ideas.repec.org/p/fip/fedgfe/2021-06.html)
- Shawn Xiaoguang Chen, 2017. The effect of a fiscal squeeze on tax enforcement: evidence from a natural experiment in china. Journal of Public Economics, 147, 62-76.
- Saez, Emmanuel. “Do taxpayers bunch at kink points?.” American Economic Journal: Economic Policy 2.3 (2010): 180-212.
- Kleven, Henrik J., and Mazhar Waseem. “Using notches to uncover optimization frictions and structural elasticities: Theory and evidence from Pakistan.” The Quarterly Journal of Economics 128.2 (2013): 669-723.
- Chetty, Raj, et al. “Adjustment costs, firm responses, and micro vs. macro labor supply elasticities: Evidence from Danish tax records.” The Quarterly Journal of Economics 126.2 (2011): 749-804.
- Foremny, Dirk, Jordi Jofre-Monseny, and Albert Sol´e-Oll´e. “‘Ghost citizens’: Using notches to identify manipulation of population-based grants.” Journal of Public Economics 154 (2017): 49-66.
- Piketty, Thomas, and Emmanuel Saez. “Optimal labor income taxation.” Handbook of Public Economics. Vol. 5. Elsevier, 2013. 391-474.
- Kleven, Henrik Jacobsen, and Esben Anton Schultz. “Estimating taxable income responses using Danish tax reforms.”American Economic Journal: Economic Policy 6.4 (2014): 271-301.
- Le Maire, Daniel, and Bertel Schjerning. “Tax bunching, income shifting and self-employment.”Journal of Public Economics 107 (2013): 1-18.
- Saez, Emmanuel, Joel Slemrod, and Seth H. Giertz. “The elasticity of taxable income with respect to marginal tax rates: A critical review.” Journal of Economic Literature 50.1 (2012): 3-50.
- Liu, Li, et al. “VAT notches, voluntary registration, and bunching: Theory and UK evidence.” Review of Economics and Statistics (2019): 1-45.
- Einav, Liran, Amy Finkelstein, and Paul Schrimpf. “Bunching at the kink: implications for spending responses to health insurance contracts.” Journal of Public Economics 146 (2017): 27-40.
- Chen, Zhao, et al. “Notching R&D investment with corporate income tax cuts in China.” No. w24749. National Bureau of Economic Research, 2018.
- Best, Michael Carlos, et al. “Estimating the elasticity of intertemporal substitution using mortgage notches.” The Review of Economic Studies 87.2 (2020): 656-690.
- Lu, Yi, Julie Shi, and Wanyu Yang. “Expenditure response to health insurance policies: Evidence from kinks in rural China.” Journal of Public Economics 178 (2019): 104049.
- Slemrod, Joel, Caroline Weber, and Hui Shan. “The behavioral response to housing transfer taxes: Evidence from a notched change in DC policy.” Journal of Urban Economics 100 (2017): 137-153.
- Best, Michael Carlos, and Henrik Jacobsen Kleven. “Housing market responses to transaction taxes: Evidence from notches and stimulus in the UK.” The Review of Economic Studies 85.1 (2018): 157-193.
- Blomquist, Soren, and Whitney K. Newey. “The bunching estimator cannot identify the taxable income elasticity.” (2017).
- Blomquist, Soren, et al. “Individual heterogeneity, nonlinear budget sets, and taxable income.” (2015).
- Garicano, Luis, Claire Lelarge & John Van Reenen, 2016. "Firm Size Distortions and the Productivity Distribution: Evidence from France," American Economic Review, American Economic Association, vol. 106(11): 3439-3479. [-PDF-](https://sci-hub.yncjkj.com/10.1257/aer.20130232)
