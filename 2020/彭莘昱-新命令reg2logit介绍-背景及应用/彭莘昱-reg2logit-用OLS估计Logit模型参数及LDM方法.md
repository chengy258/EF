> **作者：**彭莘昱 (中山大学)  
> **E-Mail：** <sonya_p@foxmail.com>

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

&emsp;

## 1. 应用背景

`reg2logit` 是 2020 年由 Paul T. von Hippel 等人提出的由线性回归的 OLS 估计 logistic 模型参数的新命令，其估计的理论基础是 Haggstrom (1983) 提出的变换公式。该公式后来被 Allison ( 2020 ) 应用为 " 线性判别模型 " (LDM) 方法。

LDM 模型来源于 LPM 和 logit 模型的转换。首先，LDM 可以转换为一个 logit 模型，其次，线性概率模型 (LPM) 的参数估计能够转换为 "第一步转换后的 Logit 模型" 参数的极大似然估计。故而只需将转换后的参数插入到 logit 模型中即可得到 LDM 模型，并且预测概率 P(Y|x)。

LDM 模型的前提假设是解释变量 x 是基于离散的被解释变量 y 的值 (取 0 和 1) 的多元正态变量，即在 y 确定的 cluster 中，向量 x 的联合分布是多元正态分布。

### 1.1 LPM 与 logit 之辩

线性概率模型 ( LPM ) 即被解释变量 y 是二元或多元离散变量 (dichotomous) 的线性回归模型。以二元变量为例，当 y 是取值为 0 或 1 时，假设 \$E(y|x)=\beta_0+\beta_1x_1+···+\beta_nx_n$，在满足经典线性假定 ( CLM ) 1 ~ 5 的情境下，$E(y|x)=P(Y=1|x)$。即：

$$P(Y=1|x)=\beta_0+\beta_1x_1+···+\beta_nx_n$$

以上就是线性概率模型 (下简称 LPM)，在估计参数时使用的是 OLS 线性回归。

LPM 和 logit 模型均可以作为预测事件概率值 P(y|x) 的方法，然而孰优孰劣，学者们展开了激烈的讨论。

**论点 1: LPM 优于 Logit**

`reg2logit`命令的作者 Paul von Hippel 曾认为 LPM 优于 Logit。他在[2017 年发布的一篇文章](https://statisticalhorizons.com/when-can-you-fit)阐述了他的理由。**第一**，OLS 回归速度要比 logistic 回归更快，这对于数据量大、模型复杂、模型频繁更新迭代的情况更为便捷。**第二**，概率的变化要比胜算比 $(P(y|x)/(1-P(y|x))$ 的变化更加直观，在解释模型时参数的意义更加直观。**第三**， Paul von Hippel 证明了在预测概率区间 [0.2，0.8] 内，LPM 非常接近 Logit 模型，在小范围超过这个区间，LPM 的表现也较好。**第四**，由于 Logit 模型使用的极大似然估计 (下简称 MLE) 的准完全分离 (quansi-complete separation) ， Logit 模型可能会崩溃，但是 LPM 没有这方面的问题。

**论点 2: Logit 优于 LPM**

`reg2logit`命令的另一作者 Paul Allison 更加偏好使用 Logit 模型，他有以下几个[观点](https://statisticalhorizons.com/in-defense-of-logit-part-1)。**第一**，LPM 可能会产生无效的概率预测值，而 Logit 不会。由于 LPM 的被解释变量是 1 和 0 ，在进行线性回归时，最终得到的概率预测值可能大于 1 或小于 0，然而这是无效的概率预测值。**第二**，对于二分类的情况，Logit 的系数及估计值将比 LPM 更稳定。Allison 认为 LPM 不是真实的产生二分结果的机制。**第三**，Logit 模型受解释变量间相互作用影响小，参数更加稳定。在[一篇文章](https://statisticalhorizons.com/in-defense-of-logit-part-2)里，Allison 具体地阐释了这一观点。

Logit 模型和 LPM 之间的优劣难以比较，是否有另一种方法能够结合两者的优势，估计二元变量的概率值呢？

答案是肯定的，就是开篇所介绍的 LDM 模型。下面将从判别分析说起，推导 LDM 的具体转换方式，模拟 reg2logit 命令的应用，并对本文内容做一个小结，总结 LDM 的优势和应用场景。

### 1.2 判别分析 (DA) 与线性判别模型 (LDM)

判别分析是根据个体的特征将个体分为若干类别中的一类。其基本原理如下：按照一定的判别准则，建立一个或多个判别函数，用研究对象的大量信息确定判别函数中的待定系数，并计算判别指标。例如，对于一个两类问题的判别，就是将模式 y 划分成 $\omega _{1}$、$\omega _{2}$。

判别分析和 Logistic 回归有着密不可分的联系。两者都可以达到对样本进行分类的目的。在 Logistic 回归中，变量 y 可能反映几个群体中的一个群体的隶属关系，而在判别分析的每个群体中，向量 x 都有一个多变量正态分布。

判别分析和 Logistic 回归也有区别。logit 模型主要是探究解释变量对离散的被解释变量的影响，而判别分析需要用已有的训练集进行训练，得到判别准则系数，从而实现对目标样品的归类，从这个维度看，判别分析是一种机器学习的方法。

然而部分用于进行判别分析的计算机程序不提供 Logit 模型系数的估计值、标准误、T 值。Haggstrom (1983) 指出对于大型数据集，在判别分析中使用 OLS 可以实现。这就是这位大牛推导由 OLS 转换 Logit 模型参数方法的初衷，其推导出发点是线性判别模型 ( LDM )。

## 2. 理论推导

### 2.2 由 LDM 推导 logit 模型

LDM 方法是一种根据解释变量的线性函数将被解释变量分为两类 (0\1) 的方法，线性判别模型在给定 y 值的情况下指定了 x 的条件分布。使用贝叶斯公式，可以将线性判别模型重新表示为每个 y 的条件概率(后验概率)，并以此推导出 logit 模型，具体推导过程如下。

假设数据集服从同方差假设，$\mu(x|Y=y)$ 代表当 Y 的类别为 y 时 x 的概率密度。

$$
\forall y \in ( 0, 1), x \in R，X|Y\sim N(m_{y}, \sigma ^{2})
$$

即 $\mu (x|Y=y)=\frac{1}{\sqrt{2\pi \sigma } }  \exp (-\frac{1}{2}(x-m_y)\sigma ^{-1}(x-m_y)') \quad (1)$

根据贝叶斯公式，由先验概率$\mu(x|Y=y)$推出 Y=y 的后验概率，此时只考虑二分情况，即 Y 只有两个可能的取值，分别是 1 和 0。下面简单介绍 Y=1 的后验概率公式推导过程。

$$
P(Y=1|X=x) = \frac{\mu (x|Y=1)P(Y=1)}{\mu (x)} 
$$

将分母由全概率公式展开，可以得到：

$$
P(Y=1|x) = \frac{ \mu (x|Y=1)P(Y=1)}{\mu (x|Y=0)P(Y=0)+ \mu (x|Y=1)P(Y=1)}
$$

分子分母同时除以$\mu (x|Y=1)P(Y=1)$

$$
P(Y=1|x) = \frac{1}{1+\frac{\mu (x|Y=0)}{\mu (x|Y=1)}\cdot \frac{P(Y=0)}{P(Y=1)}}
$$

为了将上式写为 logistic 分布概率 $f (x) = \frac{1}{1+e^{ - m}}$ 的形式，使 $e^{ - m}  = \frac{\mu (x|Y=0)}{\mu (x|Y=1)}\cdot \frac{P(Y=0)}{P(Y=1)}$，得到判别函数:

$$
P(Y=1|x) = f(\log \frac{\mu (x|Y=1)}{\mu (x|Y=0)} + \log\frac{P(Y=1)}{P(Y=0)})
$$

将 (1) 式中正态分布密度函数代入下式:

$$
\begin{aligned}
\log \frac{\mu (x|Y=1)}{\mu (x|Y=0)} +\log\frac{P(Y=1)}{P(Y=0)} 
&= \log\mu(x|Y=1) - \log \mu(x|Y=0)+\log \frac{P(Y=1)}{P(Y=0)} \\
&=  - \frac{1}{2}(x - m_1)\sigma ^{ - 1}(x - m_1)' + \frac{1}{2}(x - m_0)\sigma ^{ - 1}(x - m_0)' + \log \frac{P(Y=1)}{P(Y=0)} \\
&= (m_1 - m_0)\sigma ^{ - 1}x'+\frac{1}{2}(m_0 \sigma ^{ - 1}m_0' - m_1 \sigma ^{ - 1}m_1') + \log \frac{P(Y=1)}{P(Y=0)} \quad (2) \\
&=  \beta \cdot x  + \alpha \quad (3)
\end{aligned}
$$

最终
$$
P(Y=1|x) = f(  \beta \cdot x + \alpha) = \frac{1}{1+exp( - \beta \cdot x - \alpha)} 
$$

若 $Y=0$，相应的概率为 $\frac{exp( - \beta \cdot x - \alpha)}{1+exp( - \beta \cdot x - \alpha)} $，那么

$$
\begin{aligned}
logit\ (P(Y=1|x)) = \ln (\frac{P(Y=1|x)}{P(Y=0|x)}) \\
=\beta \cdot x+\alpha\quad (4)
\end{aligned}
$$

此时就得到了服从线性的 logistic 回归模型了。

从以上推导过程中可以看出，在二元情况下，假设$P(Y=j)=p_j$，有下式成立：

$$
\beta = (m_1 - m_0)\sigma ^{ - 1}，\alpha=\log \frac{p_1}{p_0} - \frac{1}{2}
\beta'(m_0+m_1) \quad (5)
$$

上式可以泛化为一般情况，即在多元条件下，Y=1、2……k，共有k组，且 (1) 式仍然成立。

$$
\begin{aligned}
P(Y=j|x)=P(Y=j)\mu (x|Y=j)/\sum_{i=1}^{k}P(Y=i) \mu (x|Y=i) \\
=p_j\mu (x|Y=j)/\sum_{i=1}^{k}p_i \mu (x|Y=i)
\end{aligned}
$$

将上式写为 logistic 回归形式：

$$P(Y=j|x)=\exp (\gamma _j+\delta_j'x)/\sum_{i=1}^{k} \exp (\gamma _i+\delta _i'x)$$

其中:

$$\delta _j=\sigma ^{ - 1}m_j， \gamma _j=\log p_j -\frac{1}{2}  m_j'\sigma^{ - 1}m_j \quad (6)$$

由 (1) 可得，当 $Y=j$ 时，$E(x|Y=j)=m_j$，$\sigma$ 是每组的同方差。总之，后验概率 P (Y=j |x ) 满足一个参数等于判别函数系数组合的 Logit 模型。

### 2.2 "推导后 Logit" 参数的 MLE

对由LDM推导后的 Logit 模型进行参数估计时，假定存在一个 n 个独立观测值 $(x_i, y_i)$ 的训练集，i = 1、2、...、n， 使得对任意对 (x, y) ，给定 Y = j (j=1、 2、...、k) 时，X 的分布为 $N(m_j, \sigma)$。设 $n_j$ 是 $Y = j$ 的 cluster 里观察值的个数。

在 $y_i$ 是常数的情况下，使用极大似然估计方法估计 (5) 、(6) 的系数。

似然函数如下：

$$L = \prod_{i=1}^{n}\prod_{j=1}^{k}  [p_j\mu (x|Y=j)]^{v_{ji}}$$

其中当 Y = j 时 $ v_{ij} = 1$，否则 $ v_{ij} = 0$。参数 $m_j$ 和 $\sigma$ 的 MLE 都是用 (5) 中 $\hat{m_j} $ 和 $\hat{\sigma}$ 的 MLE 代替得到的判别函数估计值。

$p_j$ 的极大似然估计是样本均值，即 $\hat{p_j}=n_j/n $。

### 2.3 由 OLS 计算 "推导后 Logit" 参数的 MLE

在二元情况下，即 k = 2，设 a 和 b 表示 OLS 估计值，将观测值 $(x_i, y_i)$ 当作满足线性模型来处理。

$$y_i=a+b'x_i+\epsilon _i\quad (7)$$

此时该回归的残差平方和为 $SSR=\sum_{i=1}^{n}(y_i-a-b'x_i)^{2}  $

根据 Haggstorm (1983) 的推导，推导后 Logit 系数的MLE与 OLS 估计中参数 a 和 b 有关。具体关系为下式：

$$\hat{ \beta }=Kb，\space K=n/SSR\quad (8)$$

$$\hat{a}=\log (\hat{p_1}/\hat{p_0})+K(a-0.5)+0.5(\frac{1}{ n_1/n}-\frac{1}{n_0/n})\quad (9)$$

从上式可以看出，在 $p_j$ 不确定时，$n_1/n=\bar{y}, \space n_0/n=1-\bar{y}$，且 $\hat{p_1}/\hat{p_0} =n_1/n_0=\bar{y} /(1-\bar{y})$。所以 (9) 式可以写为：

$$\hat{a}=\log (\frac{\bar{y}}{1-\bar{y}} )+K(a-0.5)+0.5(\frac{1}{\bar{y}} -\frac{1}{1-\bar{y}} )\quad (10)$$

在多元情况下，以最后一组 Y = k 作为基准组，使用 $\alpha _j=\gamma _j-\gamma _k\space ，\space \beta _j=\delta _j-\delta _k$ 两式替换 (5) 中的 $\gamma$ 和 $\delta$，得到

$$\beta _j=\sigma ^{-1}（m_j-m_k)，\space\alpha _j=\log (p_j/p_k)-\beta _j'(m_j+m_k)/2$$

以上就是二元和多元情况下用 OLS 估计 logit 模型参数的表达式。

## 3.  reg2logit 命令介绍

### 3.1 理论步骤

根据第二部分的理论推导，可以看出，通过 OLS 获取 logit 模型的参数只需要两步。

**Step1: 通过 OLS 估算 LPM 的参数 a、b**

即上文 1.4 中 (7) 式 $y_i=a+b'x_i+\epsilon _i $ 中斜率 b 和截距项 a。

**Step2: 变换参数为 $\hat{\alpha}$、$\hat{\beta}$**

按照 ( 8 )、( 10 ) 两式变换参数，可以得到 ( 4 )式的估计值 $\hat{\alpha}$、$\hat{\beta}$

$$logit\ (P(Y=1|x)) = \ln (\frac{P(Y=1|x)}{P(Y=0|x)})=\beta \cdot x+\alpha\quad (4)$$

以上就是命令`reg2logit`的具体运行步骤。进一步地，我们还可以继续得到在 Y=1 或 Y=0 时后验概率 P(Y=1|x)、P(Y=0|x)的预测概率值，从 LPM 转化到 Logit 模型获取概率预测值的过程就是 LDM 方法。

### 3.2 语法结构

`reg2logit`的语法结构如下：

```stata
reg2logit yvar [xvars] [if] [in] ，  iterate(#)
```

- `yvar`：离散的被解释变量 $y_i$
- `xvars`: 解释变量 $x_i$
- `iterate(#)`: 转换 OLS 估计参数后要迭代的次数。默认值为 0。如果将 iter()选项设置为大于零的值，转换后的 OLS 估计值为迭代极大似然估计值提供了起点。

### 3.3 模拟实例

```stata
*-生成数据
set obs 1000
gen id = _n

set seed 123
gen y = (id <= 750) //将前750个数的y设置为1

gen x1 = rnormal(3，1) if id <= 750 //当y=1时，X1|Y~N(3，0.5)
replace x1 = rnormal(6，1) if id > 750 //当y=0时，X1|Y~N(6，0.5)

gen x2 = rnormal(10，1) if id <= 750 //当y=1时，X2|Y~N(10，1)
replace x2 = rnormal(8，1) if id > 750 //当y=0时，X2|Y~N(8，1)


*-参数比较
qui logit y x1 x2
est store m1
qui reg2logit y x1 x2
est store m2
qui reg2logit y x1 x2， iter(1)
est store m3
qui reg2logit y x1 x2， iter(3)
est store m4
```

上面生成了 1000 个观察值，其中 x 在 y 已知的 cluster 中，满足独立的正态分布，故而其联合分布是多元正态分布，其满足多元正态性。将常规 logit 模型作为标准，比较 LDM 方法、迭代一次以及迭代三次的模型参数和拟合效果。因为对被解释变量的条件更苛刻，我们假设 LDM 方法生成的 logit 参数不会比 logit 模型本身更好。

```stata
. esttab m1 m2 m3 m4， s(r2_p) b(%6.4f) star(* 0.1 ** 0.05 *** 0.01)

----------------------------------------------------------------------------
                      (1)             (2)             (3)             (4)
                        y               y               y               y
----------------------------------------------------------------------------
y
x1                -3.0198***      -3.1425***      -2.9943***      -3.0198***
                  (-9.58)         (-9.24)         (-9.63)         (-9.58)

x2                 1.8406***       1.9772***       1.8190***       1.8406***
                   (7.28)          (7.20)          (7.30)          (7.28)

_cons             -2.0693         -2.5695         -2.0062         -2.0693
                  (-0.95)         (-1.13)         (-0.93)         (-0.95)
----------------------------------------------------------------------------
r2_p               0.8534          0.8526          0.8534          0.8534
----------------------------------------------------------------------------
t statistics in parentheses
* p<0.1， ** p<0.05， *** p<0.01
```

在本次模拟中，logit 模型 (1) 需要迭代 6 次才能获得最终结果，而使用`reg2logit`命令，即 LDM (2) 模型则无需迭代，这解释了使用`reg2logit`的速度快于`logit`的原因。在解释变量满足多元正态性的条件下，LDM 模型近似 logit 模型的效果非常好，系数和拟合优度均十分接近。且 LDM (4) 模型只需迭代三次，就可以得到和使用`logit`命令几乎一样的系数估计。

```stata
*-预测概率pi
qui logit y x1 x2
predict pr1
sum pr1

qui reg2logit y x1 x2
predict pr2
sum pr2

ttest pr1==pr2
```

使用`ttest`命令比较 LDM 方法和典型 logit 方法预测出的概率 P ( Y=1|x) 是否存在显著差异。

```stata
Paired t test
------------------------------------------------------------------------------
Variable |     Obs        Mean    Std. Err.   Std. Dev.   [95% Conf. Interval]
---------+--------------------------------------------------------------------
     pr1 |   1，000         .75    .0128063    .4049695    .7248697    .7751303
     pr2 |   1，000    .7535263    .0128085    .4050392    .7283918    .7786609
---------+--------------------------------------------------------------------
    diff |   1，000   -.0035263    .0002747    .0086865   -.0040654   -.0029873
------------------------------------------------------------------------------
     mean(diff) = mean(pr1 - pr2)                                 t = -12.8375
 Ho: mean(diff) = 0                              degrees of freedom =      999

 Ha: mean(diff) < 0           Ha: mean(diff) != 0           Ha: mean(diff) > 0
 Pr(T < t) = 0.0000         Pr(|T| > |t|) = 0.0000          Pr(T > t) = 1.0000
```

可以看出在此例中两者预测的概率均值仍然存在显著差异，然而大部分差异仅在 -0.004~0.003 之间，难以对预测 Y 的准确度产生决定性的影响。

```stata
*-预测准确率
gen y1 = (pr1 >= 0.5)
gen y2 = (pr2 >= 0.5)

count if y1 == y // accuracy = 969/1000 = 96.9%
count if y2 == y // accuracy = 969/1000 = 96.9%
count if y1 != y2 & y1 ==y // logit win while LDM lose : 2
count if y1 != y2 & y2 ==y // LDM win while logit lose : 2
```

### 3.4 应用实例

这里使用了 753 名已婚妇女的劳动力参与情况的数据集（Mroz 1987）来比较 LPM \ Logit \ LDM 模型的概率预测情况。被解释变量 **inlf** 表示，处于劳动力中的妇女为 1，否则为 0。解释变量 **kidslt6** 是该妇女抚养的六岁以下儿童的人数， **age** 是妇女年龄，**educ** 是受教育程度(年)，**exper** 劳动经验 (年)，以及 **expersq** 经验的平方。

```stata
*-导入数据
use "MROZ.dta"， clear

*-LPM Logit LDM
qui reg inlf kidslt6 age educ exper expersq //LPM
predict yhat_lpm， xb

qui logit inlf kidslt6 age educ exper expersq //Logit
predict yhat_logit

reg2logit inlf kidslt6 age educ exper expersq //LDM
predict yhat_ldm
```

从下表中可以看出，虽然 LPM 的均值合 Logit 模型最为接近，然而 LPM 预测值的最大值 1.12 超过了 1，最小值为 -0.28 小于 0，对于概率预测值来说，这是不合常理的。此外，LDM 和 logit 的预测值都在 (0, 1) 之间，极值也较为相近。

```stata

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
    yhat_lpm |        753    .5683931    .2517531  -.2782369   1.118993
    yhat_ldm |        753    .5745898    .2605278   .0136687   .9676127
  yhat_logit |        753    .5683931    .2548012   .0145444   .9651493

```

从三类模型预测概率的相关系数来看，logit 模型与 LDM 的概率预测值相关性最高。

```stata

             | yhat_lpm yhat_l~t yhat_ldm
-------------+---------------------------
    yhat_lpm |   1.0000
  yhat_logit |   0.9880   1.0000
    yhat_ldm |   0.9870   0.9994   1.0000
```

下图绘制了 LDM\LPM 同 Logit 模型概率预测值的散点图。可以看到，当 Logit 模型预测概率值接近于 1 和 0 时，LPM 概率预测值出现了较大的偏差，而 LDM 模型概率预测值与 Logit 概率预测值的关系较为稳定。我认为 LDM 在一定程度上要优于 LPM。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BD%AD%E8%8E%98%E6%98%B1_%E7%94%A8OLS%E4%BC%B0%E8%AE%A1logit%E6%A8%A1%E5%9E%8B%E5%8F%82%E6%95%B0_Fig01.jpg)

## 4. 总结

这篇 blog 简要地介绍了以 OLS 估计转换为 Logit 模型参数的方法，以及应用此方法预测二元概率值的线性判别模型 ( LDM )。值得注意的是，该转换方法和 LDM 均需满足被解释变量的条件多元正态性假设，且 LDM 仅是应用该转换方法的一种实践。对于 y 的非 0 \ 1 取值和多元情况，虽然本文没有介绍，但是以 OLS 转化估计 Logit 模型参数的方法都是普适的。

总之，LDM 方法是一种有效的方法，`reg2logit` 命令是一种实现 LDM 的有效方式，本文结论如下。

**第一，LDM 方法运算速度更快且估计值有效。**对于大数据或数量多、相关系数大的被解释变量，`logit` 这样的迭代命令可能很慢，致使其无法产生 MLE ( minka 2003 ; ji&telgarsky 2018 )。`Reg2logit`无需迭代，只需进行 OLS 估计并进行转换，故而可以快速估计参数。LDM 估计的参数值通常是有效的。

**第二，LDM 能够替代 LPM。**LDM 推导的基础是 LPM 的 OLS 估计，LDM 吸收了 LPM 模型的大部分优势，且能够获得更有效的预测概率 。

**第三，`reg2logit`还具有其他应用场景。**Paul T. von Hippel 提到该命令还可以在拟合一个多元正态插补模型时插补虚拟变量。

虽然 LPM 和 Logit 之辩暂时还没有结论，然而可以确定的是，LDM 在一定程度上 (多元正态性限制) 要优于 LPM ， 故而 LPM 和 Logit 之辩的命题似乎能够转换为 LDM 和 Logit 之辩了。

## 5. 参考文献
- Haggstrom G W. Logistic regression and discriminant analysis by ordinary least squares[J]. Journal of Business & Economic Statistics, 1983, 1(3): 229-238.[-PDF-](http://dx.doi.org/10.1080/07350015.1983.10509346)
- Mroz T A. The sensitivity of an empirical model of married women's hours of work to economic and statistical assumptions[J]. Econometrica: Journal of the econometric society, 1987: 765-799.[-PDF-](http://sgpwe.izt.uam.mx/files/users/uami/gma/metria2/SEM9/Mroz_Econometrica_LaborSupply_1987.pdf)
- Minka T P. A comparison of numerical optimizers for logistic regression[J]. Unpublished draft, 2003: 1-18.[-PDF-](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.85.7017&rep=rep1&type=pdf)
- Ji Z, Telgarsky M. Risk and parameter convergence of logistic regression[J]. arXiv preprint arXiv:1803.07300, 2018.[-PDF-](https://arxiv.org/pdf/1803.07300.pdf)
- PAUL ALLISON, Statistical Horizons 博客, [Better Predicted Probabilities from Linear Probability Models](https://statisticalhorizons.com/better-predicted-probabilities)
- 维刚, csdn 博客, [第3章-线性概率模型(1)-logistics/probit模型](https://blog.csdn.net/tongweiganglp/article/details/49934621)
- tang_1994, csdn博客, [从线性判别分析（LDA）来理解线性分类（linear classifiers）和概率模型 (probabilistic modeling)](https://blog.csdn.net/tang_1994/article/details/87645917)