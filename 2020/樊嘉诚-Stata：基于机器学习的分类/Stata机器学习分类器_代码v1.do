*- Stata 机器学习分类器: c_ml_stata
*- 作者：樊嘉诚 (中山大学)
*- 邮箱：fanjch676@163.com

cd "`c(sysdir_personal)'\EF-final"

use "c_ml_stata_data_example.dta", clear  
br

** 1. 描述性统计
	sum x1-x4		// 解释变量 (特征)
	tab y			// 因变量 (标签)

	
** 2. 模型训练
	// Model I: 支持向量机 SVM
	c_ml_stata y x1-x4, mlmodel(svm) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_svm") out_prediction("out_pred_svm") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_svm")

	ereturn list
	
	// Model II: 决策树 Decesion Tree
	c_ml_stata y x1-x4, mlmodel(tree) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_ctree") out_prediction("out_pred_ctree") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_ctree")
	
	ereturn list
	
	// Model III: 神经网络 Neural Network
	c_ml_stata y x1-x4, mlmodel(neuralnet) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_neuralnet") out_prediction("out_pred_neuralnet") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_neuralnet")
	
	ereturn list
	
	// Model IV: 随机森林 Random Forest
	c_ml_stata y x1-x4, mlmodel(randomforest) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_randomforest") out_prediction("out_pred_randomforest") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_randomforest")
	
	ereturn list	
	
	// Model V: 提升树 Boosting
	c_ml_stata y x1-x4, mlmodel(boost) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_boost") out_prediction("out_pred_boost") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_boost")
	
	ereturn list
	
	// Model VI: 正则化多项式 Regularized multinomial
	c_ml_stata y x1-x4, mlmodel(regularizedmultinomial) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_regular") out_prediction("out_pred_regular") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_regular")
	
	ereturn list	
	
	// Model VII: 朴素贝叶斯 Navie Bayes
	c_ml_stata y x1-x4, mlmodel(naivebayes) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_naivebayes") out_prediction("out_pred_naivebayes") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_naivebayes")

	ereturn list
	
	// Model IX: K近邻 KNN
	c_ml_stata y x1-x4, mlmodel(nearestneighbor) out_sample("c_ml_stata_data_new_example")  ///
	in_prediction("in_pred_knn") out_prediction("out_pred_knn") ///
	cross_validation("CV") seed(10) save_graph_cv("graph_cv_knn")

	ereturn list	
	