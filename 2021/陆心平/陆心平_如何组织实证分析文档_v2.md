&emsp;

> **作者**：陆心平 （ 中山大学 ）
>
> **邮箱**：<luxp5@mail2.sysu.edu.cn>

> **备注**：本推文部分摘译自以下论文，特此致谢！
>
> **Source**:
>
> - Acemoglu, Daron, and Pascual Restrepo. 2019. "Automation and New Tasks: How Technology Displaces and Reinstates Labor." _Journal of Economic Perspectives_, 33 (2): 3-30. [-PDF-](https://www.aeaweb.org/articles?id=10.1257/jep.33.2.3)
> - Orozco, Valerie, Christophe Bontemps, Elise Maigne, Virginie Piguet, Annie Hofstetter, Anne Marie Lacroix, Fabrice Levert, and Jean-Marc Rousselle. 2018. “How To Make A Pie: Reproducible Research for Empirical Economics & Econometrics.” Post-Print. 2017.[-PDF-](https://www.tse-fr.eu/sites/default/files/TSE/documents/doc/wp/2018/wp_tse_933.pdf)

**目录**

[TOC]

## 1.引言

在做实证分析的过程中，面对来源多样的数据和繁复的处理过程，我们应该如何整理文档结构，以便未来的自己和读者能够清晰、便捷地了解实证分析过程、重现论文结果，而不至于对着混杂的代码文件无所适从？已有的推文如[可重复性研究：如何保证你的研究结果可重现？](https://www.lianxh.cn/news/6d3f9bbbdef36.html)以及[可重复研究：如何让你的研究明了易懂？](https://www.lianxh.cn/news/d1d9439120489.html)已经为我们列出了许多注意要点和建议。

在此基础上，本篇推文将以 Daron Acemoglu 和 Pascual Restrepo 在 2019 年发表在 JPE 上的论文 **_ Automation and New Tasks: How Technology Displaces and Reinstates Labor_** 为例，着重分析其实证分析文档的组织特点，并简要介绍作者用到的核心命令及研究方法，以期能够给出具体的做法参考，从而为我们未来的实证分析工作提供借鉴。

&emsp;

## 2.论文介绍

### 2.1 主要内容

思想是研究工作的出发点，通过 Stata 编写程序进行数据分析是为了帮助我们发现问题、验证问题，因此，在介绍作者对实证分析文档的组织方法前，我们首先需要了解作者想讲述一个什么样的故事。

自工业革命以来，技术进步对于劳动力需求和生产力的影响引起了社会各界的广泛讨论，作者基于已有的研究（ Acemoglu and Restrepo ，2018a, 2018b； Acemoglu and Autor ，2011；Autor, Levy, and Murnane ，2003； Zeira ，1998 ），认为新技术不仅提高了资本和劳动力的生产力，而且还影响了这些生产因素的任务分配——我们称之为生产中的任务内容。而生产任务内容的变化可能会对劳动力需求的变化和生产率产生重大影响。如下图所示。

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20220104153109.png" alt="陆心平_如何组织实证分析文档_Fig01.png" style="zoom: 50%;" />

> 图解：生产需要完成一系列的任务，标准化到位于 N-1 和 N 之间。I 上面的任务只能通过人工生产，下面的任务是自动化的，将用资本生产。I 的增加代表了自动化技术或简称为自动化的引入。N 的增加对应于新的劳动密集型任务或简称的新任务的引入。

### 2.2 实证分析框架

作者从四个途径阐明了二战以来，技术进步对美国劳动力需求（表现为美国总工资账单的变化）的影响：

> **Change in aggregate wage bill =Productivity effect + Composition effect**
>**+ Substitution effect + Change in task content**

其中，各个效应的含义及在实证中的测量方法如下：

- **Productivity effect** 是指各种技术来源对 GDP 的贡献的总和，在实证中，我们用**人均 log （ GDP ） 的变化**来测量该效应。

- **Composition effect** 是指新技术、结构转型和偏好导致的跨部门增加值 （ value-added ） 的重新分配对劳动力需求的影响。举一个例子，服务业部门相对于农业部门是劳动密集型的产业，若某种技术的进步使得服务业部门在总生产中的占比更高，那么就会增加对劳动力的需求，反之会降低对劳动力的需求。在实证中，我们用**劳动力份额加权的行业增值份额变化的总和**来测量该效应。

- **Substitution effect** 是指任务价格的变化导致行业中劳动密集型和资本密集型任务的相互替代。比如，随着有效工资相对于资本的有效租金率上升，劳动力产生的任务的价格相对于资本产生的任务的价格增加，就会在任务之间产生了替代效应，从而改变行业中的劳动力份额。实证中，总的替代效应是**行业替代效应的就业加权之和**。

  > 劳动力份额的表达式如下：
  >
  > $$
  > s^{L}=\frac{1}{1+\frac{1-\Gamma(I, N)}{\Gamma(I, N)}\left(\frac{R / A^{K}}{W / A^{L}}\right)^{1-\sigma}}
  > $$
  >
  > - $A^{K}$ 和 $A^{L}$ 分别指提高资本生产力的技术和提高劳动力生产力的技术，实证中, $A_{i}^{L} / A_{i}^{K}$ 的增长速度等于平均劳动生产率的增长速度，即在 1947 至 1987 年间每年增长 2%，在 1987 至 2017 年间每年增长 1.46%
  > - $R$ 和 $ W$ 分别为资本生产部门和劳动生产部门的要素价格，实证中，数据来自经济分析局、劳工统计局、国民收入和产品账户
  > - $\Gamma(I, N)$ 表示生产中通过劳动生产的任务的份额，反之，$1-\Gamma(I, N)$ 表示生产中通过资本生产的任务的份额
  > - $\sigma$ 是任务之间替代的弹性，即用一个任务替代另一个任务的容易程度，也就是资本和劳动力之间替代的（派生）弹性，在实证中，作者以 Oberfield and Raval’s （ 2014 ） 对资本和劳动力之间的替代弹性作为基线，即 $\sigma=0.8$

- **Change in task content** 是指行业生产内容的变化对劳动力需求的影响，总的变化可表述为各行业生产任务内容变化的就业加权之和。实证中，作者认为只有行业中生产内容的变化和替代效应 （ Substitution effect ） 会影响一个行业中的劳动力份额，因此，行业生产内容的变化表现为不能用替代效应来解释的**劳动力份额的剩余变化**，即：

  > **Change in task content in $i=$ Percent change in labor share in $i$ -Substitution effect in $i$**

  进一步地，**Change in task content** 可分解为:

  **Change in task content** = **Displacement effect**+**Reinstatement effect**

  其中，**Displacement effect** 是指由于新技术的开发和采用，使资本能够在一系列任务中取代劳动力，从而 减少了对劳动力的需求（ 如上图的第三种情形 ） ，在实证中，作者通过计算**行业任务内容负变化的五年移动平均值**来测量其大小；**Reinstatement effect** 是指新技术的开发和采用，引入了劳动力具有相对优势的新任务（ 如上图的第二种情形 ），创造了新的就业机会，从而增加了对劳动力的需求，在实证中，作者通过计算**行业任务内容正变化的五年移动平均值**来测量其大小。

- 特别地，作者为 **Displacement effect** 和 **Reinstatement effect** 分别寻找了代理变量，来验证这两种效应对任务内容变化的影响。

  > - **Displacement effect** 代理变量：
  >   1. 行业的机器人渗透率 （ Acemoglu and Restrepo，2018b ）
  >   2. 1990 年从事日常重复性工作的职业在行业就业中的占比 （ Acemoglu and Autor ，2011 ）
  >   3. 148 个制造业 （ 详细分类 ） 中使用自动化技术的公司份额 （ 以就业加权 ）
  > - **Reinstatement effect** 代理变量：
  >   1. 新职业（基于 Lin（2011）编写的 1991 _Dictionary of Occupational Titles_）在 1990 年总就业中的占比
  >   2. 1990 年有大量新兴任务的职业在总就业中的占比（基于 O\*NET）
  >   3. “新职业”（ 1990 年不在该行业但在 2016 年存在的职业 ）所占行业的就业增长份额
  >   4. 1990 年至 2016 年间，行业中职业数量增长的百分比

&emsp;

## 3.组织实证分析文档

作为研究人员，我们通常希望能够清晰、准确地传达我们的工作 —— 包括我们做了什么/发现了什么，以及我们是如何发现它的。通过实证分析文档记录我们如何以及为什么做我们所做的事情是这个沟通过程的重要组成部分，记录的方式很大程度上影响了沟通的有效性。

比如，这篇文章的主要脉络就是通过上述的实证分析框架解释技术进步对于美国在 1947-1987 年和 1987-2017 年两个时期劳动力需求的变化，其实证分析过程非常复杂，数据来源也非常多样化。但得益于作者对实证分析文档的有效组织，我们可以清晰地了解到作者的工作过程，并能够轻松地理解和重现。

作者是如何达到这一效果的呢？接下来将着重介绍作者实证分析的文档结构和代码的编写风格，盘点一下有哪些值得我们借鉴和学习的技巧。

### 3.1 组织文件

这一部分，我们从目录结构和命名方式两个角度看看作者是如何组织文件、管理文件夹的。

#### 3.1.1 目录结构

总体上，根据文件类型，作者提供的实证分析文档有三个组成部分，分别是数据 （ dta&xlsx ） 、程序 （ dofiles ） 和结果的输出 （ graph & figures ），这一分类方式有两个优点：一是清晰明了，便于查找和调用；二是符合实现逻辑——先有原始数据，再对数据进行清洗和分析，最后输出结果。（ 见下图 ）

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20220111174939.png" alt="陆心平_如何组织实证分析文档_Fig02.png" style="zoom: 67%;" />

其次，作者明确区分了对项目的输入和对项目的产出，如作者将数据分为原始数据（ raw_data ）和清洗后的数据（ clean_data ）；产出的图表结果分别放入 figs 和 tables 两个文件夹当中，文件目录结构如下：

```
D:.
├─cleaners
├─clean_data
├─dofiles
├─figs
├─raw_data
├─tables
├─temp_data
└─xwalks

```

值得一提的是，作者并没有明确地分出输入、程序和产出三个部分，我们也可以把文件目录改为：

```
D:.
├─Inputs
│  ├─raw_data
│  └─xwalks
├─outputs
│  ├─clean_data
│  ├─figs
│  └─tables
└─programs
    ├─cleaners
    └─dofiles

```

在这种方式下，目录结构也许更加清楚，但树形结构层级会增加搜索文件时的浏览时间，需要我们在结构的复杂性和效率之间权衡。

#### 3.1.2 命名方式

**在命名之前，我们需要明确命名对象的内容和目的，并以此为依据命名。**

**数据** 由于作者在后续的实证分析当中需要用不同来源的数据来验证结论，证实结论的可信度，作者对于清洗后的数据的命名多采用 **“ 内容+来源 ”** 的方式，使得调用时可以一目了然，如 "price_bea.dta" 是来自 BEA 统计局的关于价格水平的数据。

```
D:.
    apr_measure.dta
    beaio_smt.dta
    census_characteristics_BEA.dta
    census_education1990_BEA.dta
    census_education2014_BEA.dta
    census_occdiversity_BEA.dta
    china-sag_bea.dta
    china-sag_sic87dd.dta
    FH_offshoring_bea.dta
    FH_offshoring_sic87dd.dta
    panel_bea72.dta
    panel_beaNAICS.dta
    panel_budd.dta
    price_bea.dta
    qty_bea.dta
    tfp_bea.dta
```

**程序** 程序的命名应该明确地反映出它的目的，作者对于程序的命名也基于了这一原则，且多采用 “**动作+对象**”的格式，比如我们可以在不打开文件的情况下，知道命名为 "correlates automation final.do" 的文件是一个 Stata 程序，其目的是分析生产任务内容的变化和自动化代理变量之间的关系（当然，前提是我们对文章内容有一定了解）。

```
D:.
    analyze_var_jep.do
    correlates automation final.do
    correlates newtasks final.do
    correlates prices and quantities.do
    decomposition 1947-1987, alt order.do
    decomposition 1947-1987, manufacturing.do
    decomposition 1947-1987.do
    decomposition 1987-2017, alt order.do
    decomposition 1987-2017, BLS.do
    decomposition 1987-2017, manufacturing.do
    decomposition 1987-2017.do
    executer_final.do
    sectoral trends.do
```

此外，我们会发现，作者根据实证分析的需求，将每一部分的代码写入了独立的 do-file 文件，这一做法的好处在于我们可以灵活地根据文中实证分析的过程重现不同的部分，而不需要再从头开始重新生成数据集，也不会对其它部分的数据造成“污染”。

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20220111175212.png" alt="陆心平_如何组织实证分析文档_Fig03.png"  />

#### 3.1.3 Readme!

在将文件分门别类放好之后，我们还需要一张“地图” 来帮助读者了解文件中包含的数据和程序，以及在重现时运行的顺序。例如，作者在文件中专门放入了一个名为 "readme" 的 PDF 文档，里面详细介绍了在实证分析过程中包含的数据的来源和类型，并说明了每一个 do 文件的作用和运行时需要注意的地方。

这一点非常重要，它既是对我们工作过程的一个总体记录，也是给读者的“入门手册”，不仅能够帮助我们节省搜索和调用查看的时间，还能提高沟通的有效性。

### 3.2 编写代码

在面对复杂的实证分析过程和繁多的数据时，我们又应该如何编写代码，增强文档的可读性和可操作性呢？这一部分将介绍一些小 tips。

**首先**，我们可以在 dofile 的开头，标明文章的名字、作者、生成日期、最新修改日期等基本信息，并注明该程序的作用以及产出的成果在文章中的体现。

```
/*******************************************************************************
"Automation and New Tasks: How Technology Changes Labor Demand"
by D. Acemoglu and P. Restrepo, JEP 2019

2.22.2019
Pascual Restrepo

This do file implements decomposition of the sources of the changes in labor demand
for the years 1947-1987 and produces:
 - Figure 3   - panel A and B
 - Figure A1  - panel A
 - Figure A2  - panel A
 - Figure A8  - panel A
 - Figure A9
 - Figure A10

(revised by G. Marcolongo on 2.28.2019)
*******************************************************************************/
```

**其次**，在编写代码的过程中，我们可以根据代码的内容给代码分段，并在每一段的开头标明这一部分代码的作用，这样即使 dofile 写得很长，仍然可以通过 **Ctrl+F** 快捷键快速搜索关键词定位。同时，在适当的地方添上注释，注明某句代码的作用，也有助于 72 小时后的自己和读者理解。

我们以作者通过计算行业任务内容变化的五年移动平均值测量 Displacement effect 和 Reinstatement effect 的代码为例：

```
***Disentangle the DISPLACEMENT and REINSTATEMENT effect using 5 year moving averages ***
sort indcode year
* Calculate mean of task content for each industry at 5 years MA centered at current year
* (Baseline: 5-year moving averages)
rangestat (mean) task_content_*, interval(year -2 2) by(indcode)

*Industry element of the Displacement effect:
gen task_negative_5yr=min(task_content_i_mean,0)

*Industry element of the Reinstatement effect:
gen task_positive_5yr=max(task_content_i_mean,0)
```

一般来说，Stata 中有三种方法键入注释：

```
*用于某行开头，表示整行注释
//用在某行中的任意位置，该位置后的所有内容都是注释
/*可用于跨行注释 */
```

适当地运用这三种方法在编写代码的过程中添加注释，根据程序目的和内容分行分段可以极大地增强代码的可读性。

**再次**，对于路径的设置，我们可以采用两种方式：一是设置相对路径，即在 do 文件的开头标识主位置，之后均用相对位置；二是运用`global`命令（作者多次用到这一命令，后文将详细介绍），定义文件的存储路径和子文件夹的名称简写，如作者在主 dofile 文件的开头定义了文件的存储路径：

```
*******************************************************************************/
*Set Working Directory:
global project "...\replication_acemoglu_restrepo_jep\"
*******************************************************************************/
```

引号内需要读者根据自己存储的习惯和文件所在位置填入完整的路径（我们需要在开头提醒读者这一点！）：

```
global project "D:\stata15\homework\大作业\replication_acemoglu_restrepo_jep"
```

这一做法的好处在于我们可以比较清晰、便捷地将不同文件分门别类地存放，如作者通过`graph export "${project}\figs\sector_labsh_1850_1910.eps"`的命令就可以将输出的图片对应放入 figs 文件夹中，从而避免输入绝对路径的繁琐和可能出现的路径混乱的问题。

**最后**，当我们有许多 do 文件时，可以设置一个用于调用其它所有程序的主程序，便于日后的操作。

&emsp;

## 4. global 命令介绍

`global`是作者在编写程序中用到次数最多的命令之一，尤其是在用于调用其它所有程序的主程序中。那么`global`究竟是何方神圣？这一小节主要介绍`global`的基本语法和作用。

### 4.1 什么是 global?

`global`实际上是 Stata 中暂元 （ macros ） 的一种，又被称为全局暂元。而所谓暂元，我们可以将其直观地理解为用一个简单的名词来代表一系列复杂和冗长的字符或式子。除了全局暂元 （ global ） 外，还有局部暂元 （ local ） , 顾名思义，它们的区别在于局部暂元仅在定义它的 do 文件中有效，而全局暂元可以应用到不同的 do 文件当中，直至我们退出 Stata 时才会失效。

因此，当我们需要定义一个**可为多个 do 文件共同调用**的暂元，就只能选用全局暂元——就如作者需要在多个 do 文件中定义 $\sigma$ 和 $A_{i}^{L} / A_{i}^{K}$ 的增长速度。

### 4.2 global 的基本语法

1. **设置暂元**

   定义全局暂元的基本语法结构为：

   ```stata
   global name[=]value
   ```

   其中，**name** 为全局暂元的名称，**value** 为全局暂元存储的内容。

   - 储存任意文本信息

     第一种变体下，不需要等号，可以用于储存任意文本信息，如作者将 $\sigma$ 和 $A_{i}^{L} / A_{i}^{K}$ 的增长速度，乃至绘制代表不同效应影响的直线所需的特定格式，储存在了不同的暂元中。

     这种做法的好处显而易见：在重复性的工作中，运用全局暂元会更加简洁清晰，不易出错，如作者在绘制不同的时期、不同的行业中 **Productivity effect** 的折线时，只需调用对应暂元，就能保证前后格式的一致性；且在后续程序中，如想更改 $\sigma$ 的值，只需改变暂元的定义即可，比如将 **0.8** 改为 **0.6** ,对暂元所做的改变会在所有后续模型中应用。

   - 储存结果

     第二种类型的暂元定义`global name = text`（ 含等于号 ）用于存储结果。在此定义下，Stata 会将右侧的文本视为表达式，会先计算，再将计算结果赋给暂元。

     举个例子，我们利用 Stata 自带的数据 auto.dta，生成一个新变量`gen price2=price+15`,分别用`global m1=price2`和`global m2 price2`两种形式储存在暂元中，此时调用这两个暂元，输出值都是 4114。 也许你会认为，**m1** 和 **m2** 是一样的，那么我们接下来修改一下**price2**:`replace price2=price-15`,再次调用两个暂元，我们发现，**m1** 依旧是 4114，而 **m2** 变为了 4084。

     这是由于 **m2** 储存的是公式 **price2** , 而添加等号后 Stata 会在计算 **price2** 后，再将结果储存在暂元中。

     ```stata
     . sysuse auto.dta
     (1978 Automobile Data)
     
     . gen price2=price+15
     
     . global m1=price2
     
     . global m2 price2
     
     . disp $m1
     4114
     
     . disp $m2
     4114
     
     .
     . replace price2=price-15
     (74 real changes made)
     
     . disp $m1
     4114
     
     . disp $m2
     4084
     ```

2) **调用暂元**

   前面的演示过程中，已经涉及了全局暂元的调用方法——`$name`, 其中 “name” 是指代我们设定的暂元的名字。值得注意的一点是，如果是字符型的暂元，调用时我们需要添加双引号，否则 Stata 会报错。

   ```stata
   . global effects productivity composition

   . disp $effects
   productivity not found

   ```

   这时候，我们只需要在调用时添加双引号，就可以纠正这一错误。

   ```stata
   . global effects productivity composition

   . disp "$effects"
   productivity composition

   ```

3) **删除暂元**

   `macro drop name`可用于删除特定暂元；`macro drop _all` 删除非系统自带的所有人为的暂元。

4) **查看暂元**

   `macro list`即可查看现有的暂元，我们也可以通过这个命令非常方便地查看作者定义的暂元。

面对复杂、重复的实证分析过程，巧妙地运用`global`命令，可以让我们的代码结构更加简洁，实现各个 do 文件间的协同，降低出错的概率，达到事半功倍的效果。

## 参考资料及相关推文

- Acemoglu, Daron, and Pascual Restrepo. 2019. "Automation and New Tasks: How Technology Displaces and Reinstates Labor." _Journal of Economic Perspectives_, 33 (2): 3-30. [-PDF-](https://www.aeaweb.org/articles?id=10.1257/jep.33.2.3)
- Orozco, Valerie, Christophe Bontemps, Elise Maigne, Virginie Piguet, Annie Hofstetter, Anne Marie Lacroix, Fabrice Levert, and Jean-Marc Rousselle. 2018. “How To Make A Pie: Reproducible Research for Empirical Economics & Econometrics.” Post-Print. 2017.[-PDF-](https://www.tse-fr.eu/sites/default/files/TSE/documents/doc/wp/2018/wp_tse_933.pdf)
- Michael S. Hill. [In Stata coding, Style is the Essential: A brief commentary on do-file style](https://michaelshill.net/2015/07/31/in-stata-coding-style-is-the-essential/?tdsourcetag=s_pctim_aiomsg)
- [可重复研究：如何让你的研究明了易懂？| 连享会主页 (lianxh.cn)](https://www.lianxh.cn/news/d1d9439120489.html)
- [可重复性研究：如何保证你的研究结果可重现？| 连享会主页 (lianxh.cn)](https://www.lianxh.cn/news/6d3f9bbbdef36.html)
- [Stata 编程：暂元，local！暂元，local！| 连享会主页 (lianxh.cn)](https://www.lianxh.cn/news/4d57e771feba7.html)
- [普林斯顿Stata教程(三) - Stata编程| 连享会主页 (lianxh.cn)](https://www.lianxh.cn/news/0c64a3d1b235d.html)

