&emsp;

> **作者**：钟声 (中山大学)    
> **邮箱**：<zhongsh56@mail2.sysu.edu.cn>  



---

## **目录**

[TOC]

---


&emsp;

## 1 引言

分仓散点图 (binned scatterplot) 是一种显示变量之间非参数关系的图形工具。本文将简要介绍其原理、功能，并附上相关指令 (`binscatter`、`binscatter2`) 的使用说明。

&emsp;

## 2. binscatter 方法介绍

### 2.1 简介
`binscatter` 是用于生成分仓散点图 (binned scatterplot) 的 Stata 命令，其生成的图像显示了给定 **x** 条件下 **y** 的条件期望的非参数估计，该指令可以帮助我们在控制其他变量或者控制固定效应的情况下快速检测非线性、异常值、分布性问题和最佳拟合函数形式，还可以很好地显示不同子组的异构关系。


### 2.2 用图形展示结果的重要性

我们借用英国统计学家 F.J. Anscombe (1973) 曾经举过的例子，来说明图形可视化的必要性：

<img style="width: 450px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig1_Anscombe举例表格_钟声.png">
<img style="width: 450px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig2_Anscombe举例图像_钟声.png">


以上4个数据集显然具有相同的线性关系的关系。然而，在第一个数据集中，函数关系基本上是线性的，在第二个数据集中则呈曲线关系，在第三个数据集中一个异常值影响了整体，而第四个数据集中的线性关系则完全是由一个异常值驱动。Anscombe 的观点是，如果没有将两个变量之间的关系可视化，假设任何函数形式的回归分析都可能会产生误导。 

但是在数据量非常大的时候，样本的散点图可能无法很直观地反应变量之间的关系，这时候分仓散点图 (binned scatterplot) 就可以帮上忙了。



### 2.3 实现步骤

为了创建一个分仓散点图, `binscatter` 指令：

1. 将X轴的变量分为样本量相同的 bins ;

2. 计算每个 bin 内 **x** 轴和 **y** 轴变量的平均值 ;

3.  创建这些数据点的散点图 ;

4.  绘制总体回归线 (采用原始数据，而非生成的散点) 。
 
<img style="width: 450px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig3_实现步骤_钟声.jpg">

### 2.4 理论基础
**(1) 对条件期望函数的估计**

一个典型的分仓散点图显示了条件期望函数 (CEF) 的非参数估计值 (分级后的散点) 和最佳线性估计 (回归拟合线).
- 考虑两个随机变量 $Y_{i}$ 和 $X_{i}$, 条件期望函数 (CEF) 为
$\mathrm{E}\left[Y_{i} \mid X_{i}=x\right] \equiv h(x)$
。当 $X_{i}=x$ 时, CEF 则为 $Y$ 的均值。
而且 CEF 最小化了平均平方误差，因此它是给定 $X$ 时 $Y$ 的最佳预测。

- 假设运行 OLS 回归 $Y_{i}=\alpha+\beta X_{i}+\epsilon$，将得到估计的系数 $\hat{\alpha} 、 \hat{\beta}$ 和回归拟合线: $\hat{h}(x)=\hat{\alpha}+\hat{\beta} x$ 
。该拟合线小化了平均平方误差，因此是 CEF 的最佳线性近似。

  <img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig4_条件期望函数_钟声.png">

当 N 变大时，保持 bins 的数量不变，每个散点会更加接近真实的条件期望
，而且分仓散点（binned scatters）在回归线周边的分散程度将具有一定统计学意义：

- 如果分仓散点紧靠回归线，则可以较精确估计斜率，回归标准误差较小
- 如果分箱散点对回归线而言较为分散，则斜率估计不精确，回归标准误差较大




**(2) 控制变量的方法**

`binscatter` 指令采用 Frisch-Waugh-lovell (FWL) 的方法排除控制变量的影响，其图像展示的是 **x** 和 **y** 的残差值的分仓散点图。我们假设在下式中，Z是控制变量矩阵：
$$
y_{i}=B_{0}+B_{t} x_{i}+B Z+e_{i}
$$
依据FWL定理进行 OLS 估计的步骤如下：
1. 将 $x$ 对 ${B Z}$ 回归，取残差  $\tilde{x}$ 
2. 将 $y$ 对 ${B Z}$ 回归，取残差  $\tilde{y}$ 
3. 将 $\tilde{y}$ 对 $\tilde{x}$ 回归，得到 $\tilde{x}$ 的系数 $b_{t}$.


采用此方法绘制的分仓散点图选用的是$\tilde{y}$ 和 $\tilde{x}$ 数值，往往很难反映出真实的非参数关系。其不准确的原因是线性残差的非线性函数很难反映实际非线性关系。想要解决这个问题，就应该在做估计之前，这一方法可以用 `binscatter2` 命令实现，在本文第四部分先建立 bin会有相关介绍。


 
### 2.5 分仓散点图的功能
我们借用 Sorenson (2000) 使用的 1980-1996 年计算机工作站公司数据集来说明分仓散点的功能和价值。

**(1) 更清晰地将复杂数据之间的关系可视化**

在数据量很大，或者数据种类多的情况下，普通的散点图往往很难体现变量之间的关系。下图中，Panel A 描绘了 1980年 至 1996 年间市场上竞争产品的数量与公司年销售额之间关系的散点图，在散点图中很难辨别出函数关系。在 Panel B 中，我们采用了分仓散点图，函数关系马上变得清晰很多。

 <img style="width: 450px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig5_Sorenson固定效应_钟声.jpg">

&emsp;

**(2) 显示控制固定效应后的图像**

虽然我们在采用 `binscatter` 之后，函数关系变得明显，这样的关系却不符合直觉 (竞争品数量多，公司销量应该下降才对，出现该情况可能是每年经济形势对销量产生影响)。 上图 Panel C 中，我们控制了年份固定效应 ，图像显示的关系更加符合实际。


**(3) 帮助检验函数形式**

分仓散点显示的是 **y** 在定 **x** 的条件期望的非参数估计，不同于OLS估计，我们不需要对 **x** 和 **y** 的关系进行假设。分仓散点图可以帮助我们确定函数关系是线性、二次、对数的假说是否合理，以及判断是否应该对函数进行分段。

如 (1) 中图片的 Panel C 所示，当竞争对手数量较少时，竞争对手与销售之间的均值关系是水平的，当竞争对手数量较大时，关系是下降的，而线性函数形式的假设掩盖了这种关系，用分仓散点图就可以很直观地观察到。


**(4) 帮助发现异常值**

下图中的分箱散点图显示了公司生产的产品数量和公司研发支出之间的关系。直觉表明，拥有更多产品的公司将从事更多的研发，与 Panel A 中的回归线情况不符。通过仔细观察我们可以发现， Panel A 中存在一个异常值，它拥有非常高的研发水平和很少的产品，导致 OLS 回归线显示出负相关关系。当我们排除该点后时，斜率就变成了正的，如 Panel B 所示。
 

<img style="width: 450px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig6_Sorenson异常值&分组_钟声.jpg">

&emsp;

**(5) 展示不同子组的差异**

除了帮助我们检查离群值，`binscatter` 还可以为不同的子组分别创建散点。如上图 Panel C 中，我们可以观察到私营企业、上市公司和子公司的差异。

**(6) 帮助研究时间趋势**

许多时间趋势的研究都在对照组和实验组之间进行差异分析。分仓散点可以用来显示预处理趋势在实验组可对照组之间是否平行。

如下图 Panel A 所示，在1985年以前，产品的平均数量相当稳定，1985年以后，平均生产的产品数量急剧增加。Panel B 按所有权类型分解了这些差异：在1985年之前，每种类型的公司的发展都处于相似的、相对平稳的情况；然而，1985年之后，上市公司大幅扩大了产品供应，而私营公司和子公司扩大的速度则慢得多。

<img style="width: 450px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig7_Sorenson时间趋势_钟声.jpg">

### 2.6 如何选择 bins 的数量

构建分仓散点图时最重要的选择之一是分仓的数量。更多的 bins 将帮助我们识别更多的曲线模式，但由于每个 bin 的数据点较少，会产生更大的特异方差。相比之下，分仓数量少时每个 bin 包含更多的数据点，从而提高精度，但在识别非线性方面可能不太有效。这是方差和偏差之间的权衡。

`binscatter`中，bins默认数量为20，该命令的作者 Michael (2014) 称，根据他的个人经验，这一数值的表现较好。

CCFF (2019) 证明了使综合均方误差最小化的分仓数与 $n^{1 /3}$ 成正比，其中 $n$ 是观测数，因此，更多的观测通常会导致更多的箱数。不过，其他因素也很重要。例如，保持x的分布不变，x和y之间的关系曲线越复杂，bins 的数量也应该越多（否则均方误差会增加）。这意味着即使有很大的n，关系相对简单，将选择很少的 bin。下载 `binsreg` 之后， ` binsregselect` 可以为我们提供使综合均方误差最小化的分仓数，以下是 Stata 指令的演示：
 ```stata
ssc install binsreg
sysuse auto
binsregselect mpg weight foreign
```
有时，我们也可能希望放弃默认选择以增强分箱散点图的解释意义。例如，在检查跨年的趋势时，跨年份的 bin 的均匀间隔（而不是跨年份的数据均匀分布）可能更直观。


&emsp;
## 3. binscatter 命令安装及演示 
### 4.1 安装指令

```stata
ssc insatll binscatter
```

### 4.2 具体指令演示
首先，我们调用 Stata 自带的数据，用于以下指令的展示：
```stata
sysuse nlsw88
keep if inrange(age,35,44) & inrange(race,1,2)
scatter wage tenure, msize(tiny) //展示原始数据
```
<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig8_普通散点图_钟声.png">

**(1) 生成最基础的 binscatter 图像**
```stata
binscatter wage tenure
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig19_分仓散点图_钟声.png">

&emsp;

**(2) 进行二次拟合**

```stata
binscatter wage tenure, line(qfit)
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig9_二次拟合_钟声.png">

&emsp;

**(3) 分段回归**

该命令对散点无影响，但是生成分段回归曲线。
```stata
binscatter wage tenure, rd(2.5)
binscatter wage tenure, rd(2.5 10)
```

<img style="width: 220px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig10_分段1_钟声.png">
<img style="width: 220px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig11_分段2_钟声.png">

&emsp;

**(4) 分组回归**
```stata
binscatter wage age, by(race)  
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig12_分组_钟声.png">

&emsp;

**(5) 分组回归并控制固定效应**
```stata
binscatter wage age, by(race) absorb(occupation) 
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig13_固定效应_钟声.png">


**(6) 修改图表样式**
```stata
binscatter wage age, by(race) absorb(occupation) msymbols(O T)
    xtitle(Age) ytitle(Hourly Wage) legend(lab(1 White) lab(2 Black))
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig14_修改样式_钟声.png">

**(7) 设置bins数量**
```stata
binscatter wage age,n(10)
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig15_修改bins_钟声.png">

&emsp;

**(8) 将散点连线**
```stata
binscatter y x, line(connect) 
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig16_连线_钟声.png">

&emsp;

**(9) 将两个因变量展示在同一个坐标轴中**
```stata
binscatter wage  tenure  grade
```

<img style="width: 400px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig17_双因变量_钟声.png">

&emsp;

**(10) 保存指令**
```stata
savegraph (filename)// 可以从扩展名[ex: .gpg .jpg .png]自动检测格式导出。如果没有指定文件扩展名，则默认为.gph。
savedata(filename)// 同时保存 dta 文件和 do-file
```

&emsp;

## 4 基于 binscatter 的改进—— binscatter2
`binscatter2` 继承了 `binscatter` 的语法和功能，不过在数据量较大的情况下，运行速度要快得多 (3 至 8 倍)。此外，`binscatter2`还提供了一些新功能:

**4.1 binscatter2 安装指令**
```stata
ssc install gtools
ssc install reghdfe
ssc install ftools
net install binscatter2, from("https://raw.githubusercontent.com/mdroste/stata-binscatter2/master/")
```

**4.2 binscatter2 的新功能**
1. 下载 ftools 和 reghdfe 之后， `absorb( )` 选项中可以添加多个变量，以达到同时控制多个固定效应的效果。

2.  可以展示关于条件概率分布的相关信息，例如：分位数区间。帮助使用者更好地了解在给定x条件下测量y的条件分布的变化。输入以下命令可以得到相应的分位数区间：
   
 ```stata
binscatter2 y x, quantiles(25 75) 
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Stata：binscatter和binscatter2命令简介_Fig18_分位数区间_钟声.png)
<img style="width: 450px" src="">


3.   `binscatter2` 拥有更灵活的保存命令。在 `savedata()` 后加上 `nodofile` 就可以只保存数据而不保存do-file。

4. `line()` 选项面临着更多元的选择，相比之前增加了指数 (`line(expfit)`) 和对数 (`line(logfit)`) 的拟合形式.

5. 前文所提到的另一种处理控制变量的方法（先建立 bins，再进行 OLS 估计），可以通过在 `control()` 后加 ` altcontrols` 实现。 

&emsp;

### 参考资料

- Starr, E, Goldfarb, B. Binned scatterplots: A simple tool to make research easier and better. Strat Mgmt J. 2020; 41: 2261– 2274. [-PDF-](https://doi.org/10.1002/smj.3199)
  
- Anscombe, F. (1973). Graphs in statistical analysis. American Statistician, 27(1), 17–21.  [-PDF-](https://www.sjsu.edu/faculty/gerstman/StatPrimer/anscombe1973.pdf)
  
- Michael, S. (2014), Binscatter:Binned Scatterplots in Stata.  [-PDF-](https://michaelstepner.com/binscatter/binscatter-StataConference2014.pdf)
  
- Sorenson, O. (2000), Letting the market work for you: an evolutionary perspective on product strategy. Strat. Mgmt. J., 21: 577-592.   [-PDF-](https://doi.org/10.1002/(SICI)1097-0266(200005)21:5<577::AID-SMJ105>3.0.CO;2-C)
  
- Cattaneo, M. D., Crump, R. K., Farrell, M. H., Feng, Y. (2019). Binscatter Regressions. arXiv:1902.09615.  [-PDF-](https://nppackages.github.io/references/Cattaneo-Crump-Farrell-Feng_2021_Stata.pdf)
  
- Cattaneo, Matias D. and Crump, Richard K. and Farrell, Max and Feng, Yingjie, On Binscatter (February 1, 2019). FRB of New York Staff Report No. 881, Rev. August 2021, Available at SSRN: https://ssrn.com/abstract=3344739 or http://dx.doi.org/10.2139/ssrn.3344739
  
- [stata-binscatter2: Really fast binned scatterplots in Stata ](https://gitee.com/arlionn/stata-binscatter2)