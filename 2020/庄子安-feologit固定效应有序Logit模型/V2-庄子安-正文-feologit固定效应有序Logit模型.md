# feologit: 固定效应有序 Logit 模型

&emsp;

> **作者：** 庄子安（中山大学）  
> **邮箱：** [1484712416@qq.com](mailto:1484712416@qq.com)
>   
> **指导老师：** 连玉君 （中山大学，[arlionn@163.com](mailto:arlionn@163.com)）

--------

**目录**

[TOC]
--------
## 1. 应用背景
### 1.1 模型使用场景

你可能听说过固定效应模型，你也可能听说过有序 Logit 模型，那你有听说过固定效应有序Logit模型吗？本文将详细介绍固定效应有序 Logit 模型，以及该模型在 `Stata` 中的命令 `feologit` 的使用方法。

在[多元 Logit 模型](https://www.lianxh.cn/news/270f2c9e75d4a.html)中，如果各个类别变量是有序的，但各个类别变量之间的差距未知，此时应该使用**有序 Logit 模型**。

例如：

> - 在做性格测试问卷调查时，选项通常为“非常同意（SA）”、“同意（A）”、“不同意（D）”和“非常不同意（SD）”。这四个选项是有序的，满意程度递减，但“非常同意（SA）”到“同意（A）”的距离不一定等于“同意（A）”到“不同意（D）”的距离：
> 
> $$
> y_i=\left\{
> \begin{array}{rcl}
> SD   &      &  {τ_0 = -∞ \leq y^{*}_i \leq τ_1}\\
> D    &      & {τ_1 \leq y^{*}_i \leq τ_2}\\
> A    &      & {τ_2 \leq y^{*}_i \leq τ_3}\\
> SA   &      & {τ_3 \leq y^{*}_i \leq τ_4 = +∞}
> \end{array} \right.
> $$
> - 教育程度也是一个多类别变量，包括“小学”、“初中”、“高中”、“本科及以上”，学历有序，是递增的。同样，我们无法界定不同学历之间的差距是否相同。

另外，如果数据为[面板数据](https://www.lianxh.cn/news/bf27906144b4e.html)，为了研究每个个体难以观测的不随时间变化的差异，例如个人消费习惯、企业文化、国家社会制度等，应结合采用固定效应模型（`Fiexed effect`）。

因此，当数据特征为面板数据且被解释变量为有序类别变量时，应该采用**固定效应有序 Logit 模型**：
> `panel data + ordered response = Fiexed effect ordered logit model`

### 1.2 典型应用领域
> **A.** [Anna Cristina D’Addio (2007)](https://www.econ.ku.dk/cam/wp0910/wp0203/2003-16.pdf/
): 工作满意度（Job Satisfication）的影响因素研究

  该文献研究了当个体之间对工作满意度度量标准可能存在差异时，影响工作满意度的因素，即在前人对工作满意度影响因子的基础上加入了个体效应。

  本文使用的数据源于 1995-1999 年欧洲共同体住户小组(ECHP)调查。数据每年收集一次，涉及家庭结构、家庭和家庭成员的收入和就业等几个问题，并提供了关于社会变化和个人行为动态的特殊信息。

  被采访者需要对自己的工作打一个分，范围为`1-6`，作为实证模型的被解释变量。被解释变量有序但类别间的“距离”无法界定，且为面板数据，因此可以使用固定效应有序 Logit 模型。

  类似的文献还有：[Anna Cristina D’Addio (2007)](https://www.econ.ku.dk/cam/wp0910/wp0203/2003-16.pdf/)，[Liliana Winkelmann and Rainer Winkelmann (1998)](https://www.econ.uzh.ch/dam/jcr:59d43fc7-11ad-40b2-b1b9-bd6c4c50046a/economica.pdf)，[Ada Ferrer-i-Carbonell and Paul Frijters(2004)](http://www.iae.csic.es/investigatorsMaterial/a9177124814archivoPdf99016.pdf)。

> **B.** [Anne Boring (2014)](http://www.iae.csic.es/investigatorsMaterial/a9177124814archivoPdf99016.pdf
)：学生对教师评教中的性别偏见（Gender Biases）

  该文献旨在研究 `SET(Student Evaluation of Teachers)` 中的性格差异问题，即不同性别的学生在给不同性别的老师做评价时是否会受到性别的外生影响。

  本文使用了一个来自法国大学的特殊数据库来研究学生对教师的评价是否存在性别偏见。从2008年起，学生需每年完成SET测评，该测评要求学生从四个维度评价教师：课程内容、作业和测验、授课风格、课程广度。学生对此需分别打出一个范围为`0-4`的分数，分数越高代表越满意。实证中，为了研究性别差异，最终使用了`固定效应有序 Logit 模型`。

-------

## 2. 固定效应有序 Logit 模型

### 2.1 模型设定
固定效应有序 logit 模型使用潜在变量 $y^{*}$ 将可观测特征 $x$ 与可观测有序因变量 $y$ 相关联，可观测有序因变量 $y$ 可以取值`0-K`。个体 $i$ 在时间 $t$ 的潜在变量 $y^{*}_{it}$ 线性决定于 $x_{it}$ 和两个无法观测的变量 $\alpha_i$ 和 $\varepsilon_{it}$：

$$
y^{*}_{it} = \mathbf x^{'}_{it}\beta + \alpha_i + \varepsilon_{it}\quad i = 1,\cdots,N  \quad t=1,\cdots,T
$$

其中，$\alpha_i$ 为个体异质性截距项，且统计上取决于 $\mathbf{x}_{it}$，$\varepsilon_{it}$ 为残差项。另外，潜在变量 $y^{*}_{it}$ 与 $y_{it}$ 的联系定义如下：

$$
y_{it}=k \quad if \quad \tau_{ik} < y^{*}_{it} < \tau_{ik+1} \quad k=1,\cdots,K
$$

其中，$\tau_{ik}$ 为不同个体 $i$ 的阈值。在固定效应有序 Logit 模型中，阈值可以因人而异。除了规定最低和最高阈值为负无穷大和正无穷大之外，关于个体特定阈值的唯一假设为，每个个体的阈值一直在增加：

$$
\tau_{i1}=-\infty; \quad -\infty<\tau_{ik}<\tau_{ik+1}<\infty, \quad \forall k=2,\cdots,K-1; \quad \tau_{iK+1}=\infty \quad (1)
$$

另外，该模型还假设残差项 $\varepsilon_{it}$ 独立同分布于标准 `Logistic` 分布，因此 $\varepsilon_{it}$ 的分布函数为：

$$
F(\varepsilon_{it}|\mathbf{x}_{it},\alpha_i)=F(\varepsilon_{it})=\frac{1}{1+\exp(-\varepsilon_{it})}\equiv\Lambda(\varepsilon_{it})
$$

因此，个体 $i$ 在时间 $t$ 的观测值等于 $k$ 的概率为：

$$
\Pr(y_{it}=k|\mathbf{x}_{it},\alpha_i)=\Lambda(\tau_{ik+1}-\mathbf{x}'_{it}\beta-\alpha_i)-\Lambda(\tau_{ik}-\mathbf{x}'_{it}\beta-\alpha_i)
\quad (2)
$$

由等式（2）可知，概率不仅取决于 $\mathbf{x}_{it}$ 和 $\beta$，还取决于 $\alpha_i$，$\tau_i$ 和 $\tau_{ik+1}$。在有序 Logit 模型中，采用极大似然估计来得到 $\beta$ 和 $\tau_{ik}$，但在该模型中，由于个体异质性截距项 $\alpha_i$ 的存在，我们只能识别出 $\tau_{ik}-\alpha_i=\alpha_{ik}$ ，产生了 `Incidental Parameter Problem` （Chamberlain 1980)，导致无法得到 $\beta$ 的一致估计量。解决方法以条件极大似然估 `CML` 为基础。

### 2.2 估计方法
#### 2.2.1 CML estimator
固定效应有序 Logit 模型使用的估计方法是以 `CML estimator` 为基础的。在 `Stata` 中，```clogit``` 就是使用该估计方法。类似的，`feologit` 也是以 `clogit` 为基础的。其原理很简单，就是通过条件极大似然估计摆脱 $\alpha_i$ 的影响。具体估计过程如下：

- 定义二元变量 $d^k_{it}$ 为在截断点 $k$ 处的有序变量二分所得：$\mathbf d^k_{it}=\mathbb{1}(y_{it} \ge k)$ 。我们假设个体 $i$ 的因变量被观察了 $T$ 次，定义：

$$
g^k_i=\sum_{t=1}^Td^k_{it}
$$

- 那么在 $g^k_{i}=\sum_{t=1}^Td^k_{it}$ 的条件下，观察到 $\mathbf{d}^k_i=(d^k_{i1},\cdots,d^k_{iT})'$ 的概率为：

  $$
  P^k_i(\beta)=\Pr(\mathbf{d}^k_i|\sum_{t=1}^Td^k_{it}=g^k_i)=\frac{\exp(\mathbf{d}^{k'}_{i}\mathbf{x}_i\beta)}{\sum_{\mathbf{j}\in\mathbf{B}_i}\exp(\mathbf{j'x}_i\beta)} \quad (3)
  $$

  其中，$\mathbf{j}$ 代表一个由 $T$ 个等于 $0$ 或 $1$ 的 $j_t$
  组成的 $T$ 维向量，且满足 $\sum_{t=1}^Tj^k_{it}=g^k_i$。另外，$\mathbf{B}_i$ 代表所有满足条件的集合，里面有 $g^k_i$ 个 $1$ 和 $T-g^k_i$ 个 $0$。

可见，（3）式中的条件概率不取决于 $\alpha_i$ 和 阈值。因此可以得到条件对数似然函数：

$$
{LL}^k(\mathbf{b})=\sum^N_{i=1}\log P^k_i(\mathbf{b}) \quad (4)
$$

通过极大化（4）式即可得到 $\beta$ 的一致估计量。但这种方法只使用了一个截断点，很多时候我们需要同时考察多个截断点。

#### 2.2.2 BUC estimator
`BUC estimator` (Baetschmann, Staub, and Winkelmann [2015])结合了使用不同截断点 $k$ 的对数似然函数，其方程如下：

$$
\ LL^{BUC}(\mathbf{b})=\sum^K_{k=2}\ LL^k(\mathbf{b})
\quad (5)
$$

其中，$\ LL^k(\mathbf{b})$ 来源于公式 $(4)$。`BUC estimator` 原理为最大化 $(5)$ 式，我们也可以把它理解为一个施加了约束条件的 `CML estimator`，因为它隐含了如下的约束条件：$\hat\beta^2=...=\hat\beta^K$。其原理分为三步：

1. 样本中每个个体的观察值都被替换为 $K-1$ 个自身的副本
2. 每个副本在不同的截断点二分
3. 使用扩充过的样本和 `CML estimator` 来估计 $\beta$

> 举个例子：  
> 考虑一个个体被观察两次。我们首先复制 $K-1$ 份该个体的观察值，然后每一份都在不同的截断点二分。假如一份副本 $i$ 在截断点 3 二分，那么有 $\mathbf{d}_i=\{1(y_{i1} \ge 3), 1(y_{i2} \ge 3)\}'$，则下一份副本 $j$ 在截断点 4 二分，有 $\mathbf{d}_j=\{1(y_{i1} \ge 4), 1(y_{i2} \ge 4)\}'$。

假设初始数据 ( $K=7$, $T=2$ )：  
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BA%84%E5%AD%90%E5%AE%89_feologit_Fig04.jpg)

扩充后的数据：  
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BA%84%E5%AD%90%E5%AE%89_feologit_Fig05.jpg)

#### 2.2.3 BUC - τ estimator
在前面的模型设定中，我们提到过每个个体的阈值 $\tau_i$ 是不同的，且 `BUC estimator` 中也遵循该设定。但在学术研究中，标准的固定效应有序 Logit 模型中假设不同个体的阈值是相同的：$\tau_{1k}=\tau_{2k}=\cdots=\tau_{ik}=\tau_k$ 。

因此模型设定中的 (2) 式转变为:

$$
\Pr(y_{it}=k|\mathbf{x}'_{it}, \alpha_i)=\Lambda(\tau_{k+1}-\mathbf{x}'_{it}\beta-\alpha_i)-\Lambda(\tau_{k}-\mathbf{x}'_{it}\beta-\alpha_i)
$$

如同我们在 `2.1` 节中所说，该公式无法单独识别出 $\tau_k$ 和 $\alpha_i$ 。因此，我们假设 $\tau_2=0$。 则 $(1)$ 变为：

$$
\tau_{1}=-\infty; \quad \tau_2=0; \quad -\infty<\tau_{k}<\tau_{k+1}<\infty, \quad k=3,\cdots,K-1; \quad \tau_{K+1}=\infty \quad (1)
$$

在前面的 `BUC estimator` 中，每个副本内是使用同一个截断点，但在 BUC- $\tau$ estimator 中，每个副本内使用随机的截断点。让 $\tau_i^{cut}$ 代表副本 $i$ 使用的阈值向量。则 $(3)$ 式可以重新表述为：

$$
\Pr(\mathbf{d}^k_i|\sum_{t=1}^Td_{it}=g_i)=\frac{\exp\{\mathbf{d}^{'}_{i}(\mathbf{x}_i\beta-\tau^{cut}_i)\}}{\sum_{\mathbf{j}\in\mathbf{B}_i}\exp\{\mathbf{j'}(\mathbf{x}_i\beta-\tau^{cut}_i)\}} \quad (6)
$$

可以看到，$(6)$ 式取决于阈值。
> 考虑 `2.2.2` 中相同的例子：  
> 假设副本 $i$ 中的第一个观察值在截断点 3 二分，第二个观察值在截断点 4 二分。那么 $\mathbf{d}_i=\{1(y_{it} \ge 3), 1({y_{it} \ge 4)} \}'$，相应的 $\tau^{cut}_i=(\tau_3, \tau_4)'$。

将上述的两个估计方法进行比较：
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BA%84%E5%AD%90%E5%AE%89_feologit_Fig01.png)

但在实际运用中，$N(K-1)^T$ 超出了很多计算机的运算能力。因此，我们在程序 `feologit, threshold` 中，只使用一部分使用不同截断点的副本，默认为每个个体 10 个。当然，用户也可以使用 `clones()` 来改变数量。另外，截断点是随机挑选出来的，受随机数 `seed(#)` 影响。

-----------
## 3. 模型解释
### 3.1 基于方向和 compensating variation 解读 β

- $\beta$ 的符号反映了 $x$ 的增加影响 $y$ 的累积分布变动的方向：

$$
\Pr(y_it \ge k|x_{it},\alpha_i)=\Lambda(\mathbf{x}'_{it}\beta+\alpha_i-\tau_k) \quad (7)
$$

例如：
> 假如 $\beta_l>0$ ，那么自变量 $x_l$ 的增加会减少最小截断点处的 $\Pr(y_{it} \ge 1|x_{it},\alpha_i)$ 并增加最大截断点处的 $\Pr(y_{it} \ge K|x_{it},\alpha_i)$ 。

- 另外，也可以通过计算 compensating variation 来解读 $\beta$：  

> 假如自变量 $x_l$ 和 $x_r$ 的变化使得潜在变量 $y^*$ 不变，进而导致因变量 $y$ 不变，那么 compensating variation 可以由相应的 $\beta$ 得出：增加 1 单位的 $x_l$ 和增加 $\beta_l/\beta_r$ 单位的 $x_r$ 具有相同的效果。

### 3.2 基于胜算比（Odds）解读 β

在 Logit 模型中，胜算比（Odds）是一个很重要的指标，它表示某一件事发生的概率与其互补事件发生的概率的比值。在本文阐述的模型中，根据 $(7)$ 式，胜算比可以表示为：

$$
Odds(k, \mathbf{x}_{it}) \equiv \frac{\Pr(y_{it}>k|\mathbf{x}_{it})}{\Pr(y_{it} \le k|\mathbf{x}_{it})}=\exp(\mathbf{x}'_{it}\beta-\tau_{ik})
$$

假设第 $l$ 个解释变量增加了 $\bigtriangleup\mathbf{x}_{itl}$ 个单位，那么胜算比的变动为：

$$
Odds(k, \bigtriangleup\mathbf{x}_{itl}) \equiv \frac{Odds(k,\mathbf{x}_{it}+\bigtriangleup\mathbf{x}_{itl})}{Odds(k,\mathbf{x}_{it})}=\exp(\bigtriangleup\mathbf{x}'_{itl}\beta)
$$

因此，系数 $\beta_l$ 可以解释为：
> 第$x_l$ 每增加 1 个单位，除了最小截断点处的所有截断点处的胜算比都会变为原来的 $\exp(\beta_l)$ 倍。  

`feologit` 命令的选项 `or` 可以将结果呈现为 $exp(\beta)$ 。例如：  
```Stata
. use nlswork.dta, clear

. recode hours (0/6 = 1) (7/29 = 2) (30/186 = 3), gen(hourscat)

. feologit hourscat age union msp nev_mar tenure ln_wage,or nolog group(idcode)

note: multiple positive outcomes within groups encountered.

Fixed-effects ordered logistic regression

                                         N. of obs. (inc. copies) =       6874
                                         N. of observations       =       6168
                                         N. of panel units        =       1148
                                         Wald chi2(6)             =     145.74
                                         Prob > chi2              =     0.0000
Log conditional likelihood = -2398.5032  Pseudo R2                =     0.0616
                             (Std. Err. adjusted for 1,148 clusters in idcode)
------------------------------------------------------------------------------
             |               Robust
    hourscat | Odds Ratio   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         age |      0.973      0.008    -3.16   0.002        0.957       0.990
       union |      2.160      0.284     5.86   0.000        1.670       2.794
         msp |      0.472      0.069    -5.15   0.000        0.355       0.628
     nev_mar |      2.103      0.585     2.68   0.007        1.220       3.626
      tenure |      1.134      0.022     6.56   0.000        1.092       1.177
     ln_wage |      0.986      0.128    -0.11   0.912        0.764       1.271
------------------------------------------------------------------------------
```

### 3.3 边际效应（Marginal Effects）解读

在实际应用中，我们更加关注概率值的边际效用，即一个解释变量 $x_l$ 的小幅度变动对 $y_{it}=k$ 的概率值的影响，结合 $(2)$ 和 $(7)$ 式可得：

$$
ME_{itkl}=\frac{\partial\Pr(y_{it}=k|\mathbf{x}_{it},\alpha_i)}{\partial\mathbf{x}_{itl}} \\ =[\Lambda(\tau_{ik+1}-\mathbf{x}_{it}-\alpha_i)\{1-\Lambda(\tau_{ik+1}-\mathbf{x}_{it}-\alpha_i)\}\\ \qquad \qquad -\Lambda(\tau_{ik}-\mathbf{x}_{it}-\alpha_i)\{1-\Lambda(\tau_{ik}-\mathbf{x}_{it}-\alpha_i)\}]\beta_l 
\quad (8)
$$

为了计算上式，常见方法为使用样本均值，计算出来的 Marginal Effects 称为平均边际效应 (ME at the average)：

$$
\overline{ME}_{kl}=\{\overline{d}^{k+1}(1-\overline{d}^{k+1})-\overline{d}^k(1-\overline{d}^k) \}\hat{\beta}_l
$$

其中，$\overline{d}^k$ 为 $d_{it}^k$ 的样本均值。$\overline{ME}_{kl}$ 的标准误可以使用 [Delta method](https://en.wikipedia.org/wiki/Delta_method) 得到。在 `Stata` 中，边际效用可以通过命令 `logitmarg` 得到。例如（续上例）：
```Stata
. use nlswork.dta, clear

. recode hours (0/6 = 1) (7/29 = 2) (30/186 = 3), gen(hourscat)

. feologit hourscat age union msp nev_mar tenure ln_wage, or nolog group(idcode)

. logitmarg

Marginal effects at the average                 N. of observations=       6168
                                                N. of panel units =       1148
------------------------------------------------------------------------------
             |     Margin   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
age          |
           1 |      0.001      0.000     3.16   0.002        0.000       0.001
           2 |      0.005      0.002     3.16   0.002        0.002       0.009
           3 |     -0.006      0.002    -3.16   0.002       -0.010      -0.002
-------------+----------------------------------------------------------------
union        |
           1 |     -0.025      0.004    -5.86   0.000       -0.034      -0.017
           2 |     -0.154      0.026    -5.86   0.000       -0.206      -0.103
           3 |      0.179      0.031     5.86   0.000        0.119       0.240
-------------+----------------------------------------------------------------
msp          |
           1 |      0.025      0.005     5.15   0.000        0.015       0.034
           2 |      0.150      0.029     5.15   0.000        0.093       0.208
           3 |     -0.175      0.034    -5.15   0.000       -0.242      -0.109
-------------+----------------------------------------------------------------
nev_mar      |
           1 |     -0.024      0.009    -2.68   0.007       -0.042      -0.007
           2 |     -0.149      0.056    -2.68   0.007       -0.258      -0.040
           3 |      0.173      0.065     2.68   0.007        0.046       0.300
-------------+----------------------------------------------------------------
tenure       |
           1 |     -0.004      0.001    -6.56   0.000       -0.005      -0.003
           2 |     -0.025      0.004    -6.56   0.000       -0.033      -0.018
           3 |      0.029      0.004     6.56   0.000        0.021       0.038
-------------+----------------------------------------------------------------
ln_wage      |
           1 |      0.000      0.004     0.11   0.912       -0.008       0.009
           2 |      0.003      0.026     0.11   0.912       -0.048       0.054
           3 |     -0.003      0.030    -0.11   0.912       -0.063       0.056
------------------------------------------------------------------------------
```

### 3.4 阈值（Thresholds）解读

前面我们已经提到，在标准固定效应有序 Logit 模型中，假设不同个体的阈值是相同的。在 `Stata` 里可以通过在 `feologit` 命令后面加上 `threshold` 选项实现。在前面的分析中，我们视 $\varepsilon_{it}$ 为残差项，但在接下来的讨论中，我们将它视为固定值，因此解释变量的变化会对有序变量 $y$ 是留在原类别里还是上升一个类别起到决定性作用。
> 例如，假如解释变量 $\mathbf{x}_{it}$ 增加 $(\tau_3-\tau_4)/\beta_l$ 个单位，那么 $y$ 一定会上升到下一个类别。

$\tau$ 和 $\beta$ 都可以通过前面的 BUC estimator 估计得到，那么 $(8)$ 式只有 $\alpha_i$ 是未知的。在固定效应 Logit 模型中，假设所有个体 $i$ 的阈值都为 $\alpha_i=\widetilde{\alpha}$ ：

$$
\widetilde{\alpha}=\max_{a} \sum_i \sum_t d_{it}^2 \log\Lambda(a+z_{it})+(1-d_{it}^2)\{1-\Lambda(a+z_{it}) \}
$$

其中， $z_{it} \equiv \mathbf{x}_{it} \hat{\beta}$ 。在 `Stata` 中，如果在 `feologit` 后加上 `threshold` 选项，$\widetilde{\alpha}$ 就被存储在 `e(cut1)` 中。接上例：
```Stata
. use nlswork.dta, clear

. recode hours (0/6 = 1) (7/29 = 2) (30/186 = 3), gen(hourscat)

. qui feologit hourscat age union msp nev_mar tenure ln_wage, or nolog group(idcode) threshold

. qui ereturn list

. dis e(cut1)

-5.3477665
```
---------

## 4. `feologit` 命令

### 4.1 `feologit` 命令下载
```Stata
. net sj 20-2
. net install st0596 (to install program files, if available)
. net get st0596 (to install ancillary files, if available)
```
完成安装后，可以进一步输入如下命令获取帮助文件：
```Stata
help feologit
help feologit postestimation
```

### 4.2 语法结构
`feologit` 的语法结构如下：
```Stata
feologit depvar indepvars [if] [in] [weight], group(varname)  ///
[thresholds clones(#) keepsample seed(#) cluster(clustvar) or otheropts]
```

其中:
- `depvar` 是有序类别变量，即被解释变量
- `indepvars` 是解释变量
- `if` 是设定样本范围的条件语句
- `in` 用于设定观察值范围
- `weight` 用于设定权重

### 4.3 选项
| **具体命令**   | **解释** |
|-----------|-------------|
| `group(varname)`      | 如果没有用 `xtset panelvar` 处理数据，则需要调用该选项以告诉 `Stata` 以哪个变量分组  |
| `thresholds` | 参见 3.4 节 |
| `clones(#)` | 参见 2.3.3 节 |
| `keepsample` | 该选项使得估计完成后保留估计时使用的样本，新增了一些估计过程中产生的新变量。如果不加该选项，样本将还原为初始样本 |
| `seed(#)` | 参见 2.3.3 节 |
| `cluster(clusvar)` | 设置计算聚类标准误时的分类变量 |
| `or` | 参见 3.2 节 |

详情请参考 `help feologit` 

----------

## 5. `feologit` ：Stata 实操

### 5.1 安装命令 + 下载范例数据

我们通过研究一胎对母亲生活满意度的影响的案例介绍 `feologit` 命令的应用。使用的数据集为 `bbsw.dta`。数据下载方式：
```Stata
. view net describe st0596
```

然后点击下图中的 (click here to ……)，即可安装命令以及作者提供的范例 dofile 和数据文件。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BA%84%E5%AD%90%E5%AE%89_feologit_Fig02.png)

你也可以完全采用命令方式完成上述安装和下载工作：
```Stata
. net install st0596.pkg, replace  
. net get     st0596.pkg
```

### 5.2 数据结构描述

首先了解一下该数据集，它是一个面板数据集。在本案例中，被解释变量 **lifesat** 为母亲生活满意度，是一个序别变量：`[0: completely dissatisfied - 10: completely satisfied]`；解释变量包括： **kidage01** 为第一个孩子的年龄，**age** 为母亲年龄，**lhinc** 为收入的对数，**work** 为虚拟变量，代表受访者上周是否工作。

- 首先观察被解释变量的分布情况：
```Stata
histogram lifesat, discrete percent addlabel
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BA%84%E5%AD%90%E5%AE%89_feologit_Fig03.png)

可以看到，有 2/3 的受访者的生活满意度大于 7 。其中，生活满意度为 8 的受访者居多，占比29%。我们要研究的是第一个孩子对其生活满意度的影响，以及之后其生活满意度的变化。另外，我们也加入了母亲年龄、收入的自然对数以及工作情况这三个控制变量。

- 为了研究第一个孩子对其生活满意度的影响，以及之后变化，生成了5个代表第一个孩子年龄的虚拟变量。如 **kidage01_2**，第一个孩子年龄为 2 时即等于 1 ，否则为 0。

因此我们估计的模型为：

$$
\Pr(lifesat_{i}=k|\mathbf{x}_{it},\alpha_i)=\Lambda(\tau_{k+1}-\mathbf{x}'_{i}\beta-\alpha_i)-\Lambda(\tau_{k}-\mathbf{x}'_{i}\beta-\alpha_i)
$$

其中：

$$
\mathbf{x}_{i}\beta=\beta_0kidage01\_0+\cdots+\beta_4kidage01\_4+\beta_{age}age+\beta_{inc}lhinc+\beta_{work}work
$$

### 5.2 模型估计

首先，使用 `xtset` 命令处理面板数据：
```Stata
xtset idpers
```

然后，用 BUC estimator 拟合模型：
```Stata

. feologit lifesat kidage01_0-kidage01_4 age lhinc work
note: group() not specified; assuming group(idpers) from panel identifier

note: multiple positive outcomes within groups encountered.

Iteration 0:   log conditional likelihood = -170463.48  
Iteration 1:   log conditional likelihood = -170187.36  
Iteration 2:   log conditional likelihood = -170187.26  
Iteration 3:   log conditional likelihood = -170187.26  

Fixed-effects ordered logistic regression

                                         N. of obs. (inc. copies) =     461227
                                         N. of observations       =     113382
                                         N. of panel units        =      11015
                                         Wald chi2(8)             =     650.37
                                         Prob > chi2              =     0.0000
Log conditional likelihood = -170187.26  Pseudo R2                =     0.0068
                            (Std. Err. adjusted for 11,015 clusters in idpers)
------------------------------------------------------------------------------
             |               Robust
     lifesat |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
  kidage01_0 |      0.893      0.052    17.25   0.000        0.791       0.994
  kidage01_1 |      0.668      0.050    13.45   0.000        0.570       0.765
  kidage01_2 |      0.253      0.048     5.25   0.000        0.159       0.348
  kidage01_3 |      0.189      0.045     4.19   0.000        0.100       0.277
  kidage01_4 |      0.059      0.043     1.37   0.172       -0.025       0.142
         age |     -0.009      0.001   -13.81   0.000       -0.010      -0.007
       lhinc |      0.082      0.011     7.65   0.000        0.061       0.103
        work |      0.092      0.018     5.18   0.000        0.057       0.127
------------------------------------------------------------------------------
```
> - 结果显示经过3次迭代后得到了对数 CML 估计量  
> - Wald Test 表明模型整体在统计上是显著的  
> - 可以看到，第一个孩子对母亲生活满意度的影响在其出生那一年的影响最大（即 **kidage01_0** 的系数）。之后逐年递减，四年后该影响在统计上不显著  
> - 年龄 **age** 对母亲生活满意度起到负面的影响  
> - 家庭收入和工作状态对母亲生活满意度起到正面作用，二者的 compensating variation 为 1.12 (0.092/0.082)，意味着家庭收入对数值必须增加 1.12 才能弥补不工作对生活满意度的影响

接下来从胜算比来解释结果，使用 `or` 选项即可：
```Stata
. feologit, or nolog

Fixed-effects ordered logistic regression

                                         N. of obs. (inc. copies) =     461227
                                         N. of observations       =     113382
                                         N. of panel units        =      11015
                                         Wald chi2(8)             =     650.37
                                         Prob > chi2              =     0.0000
Log conditional likelihood = -170187.26  Pseudo R2                =     0.0068
                            (Std. Err. adjusted for 11,015 clusters in idpers)
------------------------------------------------------------------------------
             |               Robust
     lifesat | Odds Ratio   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
  kidage01_0 |      2.442      0.126    17.25   0.000        2.206       2.702
  kidage01_1 |      1.950      0.097    13.45   0.000        1.769       2.149
  kidage01_2 |      1.288      0.062     5.25   0.000        1.172       1.416
  kidage01_3 |      1.208      0.055     4.19   0.000        1.106       1.320
  kidage01_4 |      1.060      0.045     1.37   0.172        0.975       1.153
         age |      0.991      0.001   -13.81   0.000        0.990       0.993
       lhinc |      1.086      0.012     7.65   0.000        1.063       1.109
        work |      1.096      0.019     5.18   0.000        1.059       1.135
------------------------------------------------------------------------------
```

> 结果显示，第一个孩子出生那年使胜算比提高了大概 144% ($exp(\beta)-1$) ，第一年为大概 95% ，第二年为 28.8% ，第三年为20.8% ，到了第四年，效果在统计上已经不显著了。

接下来考察第一个孩子出生当年对母亲生活满意度各个等级的边际影响，使用 `logitmarg` 即可，加上 `dydx(kidage01_0)` 可以只输出出生当年的边际影响：
```Stata
. logitmarg, dydx(kidage01_0)

Marginal effects at the average                 N. of observations=     113382
                                                N. of panel units =      11015
------------------------------------------------------------------------------
             |     Margin   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
           1 |     -0.004      0.000   -17.25   0.000       -0.005      -0.004
           2 |     -0.004      0.000   -17.25   0.000       -0.004      -0.003
           3 |     -0.010      0.001   -17.25   0.000       -0.011      -0.009
           4 |     -0.022      0.001   -17.25   0.000       -0.025      -0.020
           5 |     -0.029      0.002   -17.25   0.000       -0.032      -0.026
           6 |     -0.082      0.005   -17.25   0.000       -0.091      -0.073
           7 |     -0.046      0.003   -17.25   0.000       -0.051      -0.041
           8 |     -0.023      0.001   -17.25   0.000       -0.026      -0.021
           9 |      0.101      0.006    17.25   0.000        0.089       0.112
          10 |      0.076      0.004    17.25   0.000        0.068       0.085
          11 |      0.044      0.003    17.25   0.000        0.039       0.049
------------------------------------------------------------------------------
```

> - 需要注意的是，`logitmarg` 输出的结果从 1 开始，实际上就是数据中的 0    
> - 结果显示，在第一个孩子出生那年，母亲生活满意度为 0-7 的可能性均降低，而为 8-10 的可能性均上升。例如，第一个孩子出生那年使得母亲生活满意度为 10 的可能性提高了 4.4% 

为了估计阈值，我们使用 BUC-$\tau$ estimator 来估计模型，加入 `threshold` 选项即可：
```Stata
feologit lifesat kidage01_0-kidage01_4 age lhinc work, group(idpers) threshold nolog

Fixed-effects ordered logistic regression

                                         N. of obs. (inc. copies) =    1520404
                                         N. of observations       =     114750
                                         N. of panel units        =      11474
                                         Wald chi2(17)            =   33720.95
                                         Prob > chi2              =     0.0000
Log conditional likelihood = -315915.36  Pseudo R2                =     0.5184
                            (Std. Err. adjusted for 11,474 clusters in idpers)
------------------------------------------------------------------------------
             |               Robust
     lifesat |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
  kidage01_0 |      0.881      0.053    16.73   0.000        0.778       0.984
  kidage01_1 |      0.668      0.051    13.14   0.000        0.569       0.768
  kidage01_2 |      0.267      0.050     5.37   0.000        0.169       0.364
  kidage01_3 |      0.192      0.046     4.13   0.000        0.101       0.283
  kidage01_4 |      0.044      0.044     1.00   0.316       -0.042       0.131
         age |     -0.009      0.001   -13.68   0.000       -0.010      -0.007
       lhinc |      0.086      0.011     7.78   0.000        0.064       0.108
        work |      0.096      0.018     5.20   0.000        0.060       0.132
-------------+----------------------------------------------------------------
       /cut1 |      0.000  (constrained)
       /cut2 |      0.646      0.067                         0.514       0.777
       /cut3 |      1.529      0.070                         1.391       1.667
       /cut4 |      2.598      0.074                         2.454       2.743
       /cut5 |      3.358      0.075                         3.210       3.505
       /cut6 |      4.771      0.076                         4.622       4.920
       /cut7 |      5.604      0.077                         5.453       5.756
       /cut8 |      6.914      0.079                         6.760       7.069
       /cut9 |      8.953      0.083                         8.791       9.115
      /cut10 |     10.497      0.089                        10.322      10.671
------------------------------------------------------------------------------
```

> 可以看到：  
> - 对比 BUC estimator 的回归结果，BUC-τ estimator 的回归结果只有略微的变动    
> - `/cut1` 对应 $\tau_2$ ，模型假设其等于 0  
> - 阈值之间的差距呈递增趋势。这说明对于生活满意度低的人来说，解释变量的变动对于生活满意度的变动有更大的效果  

### 5.3 假设检验

> - $H_{0}: constant \quad thresholds$  
> - 方法：比较 BUC estimator 和 BUC-τ estimator 。若拒绝原假设，则应使用 BUC estimator ，否则使用后者。

**Step 1**：首先，使用 `keepsample` 选项再次进行 `feologit` 估计：
```Stata
quietly: feologit lifesat kidage01_0-kidage01_4 age lhinc work, group(idpers) threshold keep
```

生成了如下新变量：
```Stata
              storage   display 
variable name   type    format 
-------------------------------
clone           double  %10.0g
bucsample       double  %10.0g
dkdepvar        double  %10.0g
dkthreshold     double  %10.0g
```

> 其中：  
> - 经二分后的被解释变量存储在 **dkdepar** 中，对应的截断点存储在 **dkthreshold** 中    
>  **bucsample** 是一个虚拟变量，等于 0 时代表副本内使用随机的截断点，用于 BUC-τ estimator ，反之则为 BUC estimator

**Step 2**：生成 **bucsample** 与所有解释变量的交叉项，用 `clogit` [条件 Logit 模型(详情参考 `help clogit`)] 拟合模型，以 **clone** 分组，并对 **idpers** 聚类：
```Stata
. foreach i of var kidage01_0-kidage01_4 age lhinc work {
  2. quietly: generate tau_`i' =`i'*(bucsample==0)
  3. local interact="`interact' tau_`i'"
  4. }

. clogit dkdepvar kidage01_0-kidage01_4 age lhinc work `interact' i.dkthreshold, group(clonegroup) cluster(idpers) nolog

note: multiple positive outcomes within groups encountered.
note: 119,392 groups (803,096 obs) dropped because of all positive or
      all negative outcomes.

Conditional (fixed-effects) logistic regression

                                                Number of obs     =  1,520,404
                                                Wald chi2(25)     =   33992.40
                                                Prob > chi2       =     0.0000
Log pseudolikelihood = -315913.71               Pseudo R2         =     0.5184

                              (Std. Err. adjusted for 11,474 clusters in idpers)
--------------------------------------------------------------------------------
               |               Robust
      dkdepvar |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
---------------+----------------------------------------------------------------
    kidage01_0 |      0.893      0.052    17.25   0.000        0.791       0.994
    kidage01_1 |      0.668      0.050    13.45   0.000        0.570       0.765
    kidage01_2 |      0.253      0.048     5.25   0.000        0.159       0.348
    kidage01_3 |      0.189      0.045     4.19   0.000        0.100       0.277
    kidage01_4 |      0.059      0.043     1.37   0.172       -0.025       0.142
           age |     -0.009      0.001   -13.81   0.000       -0.010      -0.007
         lhinc |      0.082      0.011     7.65   0.000        0.061       0.103
          work |      0.092      0.018     5.18   0.000        0.057       0.127
tau_kidage01_0 |     -0.026      0.030    -0.86   0.390       -0.084       0.033
tau_kidage01_1 |      0.002      0.028     0.06   0.949       -0.053       0.057
tau_kidage01_2 |      0.030      0.026     1.14   0.256       -0.022       0.081
tau_kidage01_3 |      0.006      0.025     0.25   0.803       -0.043       0.055
tau_kidage01_4 |     -0.031      0.025    -1.25   0.212       -0.079       0.018
       tau_age |     -0.000      0.000    -0.94   0.345       -0.001       0.000
     tau_lhinc |      0.008      0.005     1.44   0.151       -0.003       0.018
      tau_work |      0.008      0.009     0.87   0.383       -0.010       0.027
               |
   dkthreshold |
            2  |     -0.646      0.067    -9.64   0.000       -0.777      -0.514
            3  |     -1.529      0.070   -21.74   0.000       -1.667      -1.391
            4  |     -2.598      0.074   -35.24   0.000       -2.743      -2.454
            5  |     -3.358      0.075   -44.55   0.000       -3.506      -3.210
            6  |     -4.771      0.076   -62.71   0.000       -4.921      -4.622
            7  |     -5.605      0.077   -72.68   0.000       -5.756      -5.454
            8  |     -6.915      0.079   -87.60   0.000       -7.070      -6.760
            9  |     -8.953      0.083  -108.35   0.000       -9.115      -8.792
           10  |    -10.498      0.089  -117.99   0.000      -10.672     -10.323
--------------------------------------------------------------------------------
```

> 结果释义：  
> - 非交叉项的解释变量的系数等于前面用 BUC estimator 估计的系数    
> - 交叉项的解释变量反映了 BUC estimator 和 BUC-τ estimator 估计结果的差异  
> - 差异都很小，并且统计上皆不显著

**Step 3**：联合显著性检验

```Stata
. test `interact'

 ( 1)  [dkdepvar]tau_kidage01_0 = 0
 ( 2)  [dkdepvar]tau_kidage01_1 = 0
 ( 3)  [dkdepvar]tau_kidage01_2 = 0
 ( 4)  [dkdepvar]tau_kidage01_3 = 0
 ( 5)  [dkdepvar]tau_kidage01_4 = 0
 ( 6)  [dkdepvar]tau_age = 0
 ( 7)  [dkdepvar]tau_lhinc = 0
 ( 8)  [dkdepvar]tau_work = 0

           chi2(  8) =    7.64
         Prob > chi2 =    0.4693
```
> 结果显示，无法拒绝原假设 $H_0: constant thresholds$ ，因此，应该使用 BUC-τ estimator 。 

---------------

## 6. 参考文献
- Gregori Baetschmann, Alexander Ballantyne, Kevin E. Staub, Rainer Winkelmann. feologit: A new command for fitting fixed-effects ordered logit models[J]. Stata Journal, 2020(2):253-275. [-PDF-](https://sci-hub.tw/10.1177/1536867x20930984)
- Baetschmann, G. 2012. Identification and estimation of thresholds in the fixed effects ordered logit model[J]. Economics Letters 115: 416–418. [-PDF-](https://linkinghub.elsevier.com/retrieve/pii/S0165176511006239)
- Gregori Baetschmann, Kevin E. Staub, Rainer Winkelmann. Consistent estimation of the fixed effects ordered logit model[J]. Journal of the Royal Statistical Society, Series A 178:685-703. [-PDF-](https://doi.org/10.1111/rssa.12090)
- 杨柳，连玉君，连享会推文，[Stata：Logit模型一文读懂](https://www.lianxh.cn/news/e18031ffad4f3.html)
- 钟经樊 连玉君. 计量分析与 STATA 应用.
- J.Scott Long, Jeremy Freese. Regression Models For Categorical Dependent Variables Using Stata.