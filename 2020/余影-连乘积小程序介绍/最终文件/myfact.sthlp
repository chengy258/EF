{smcl}
{* 9 Dec 2020}{...}
{hline}
help for {hi:myfact}
{hline}

{title:Title}

{p 4 4 2}
{bf:myfact}——Calculate Factorial of Positive Integers

{title:Syntax}

{p 4 4 2}
{cmdab:myfact} {anything},
[
{cmdab:F:ormat:}{cmd:(}string{cmd:)}
{cmdab:GEN:erate:}{cmd:(}string{cmd:)}
]

{title:Description}

{p 4 4 2}
{cmd:myfact} makes it easy for users to calculate the factorial of positive integers, 
specify the format of the output, create new varibales and store the results in new variables. 

{title:Options}

{p 4 4 2}{cmd:Format(}{it:string}{cmd:)} specifies the format of the output.{p_end}
{p 4 4 2}{cmd:GENerate(}{it:string}{cmd:)} creates new varibales and stores the results of calculation in new variables. {p_end}

{title:Examples}

{p 4 4 2} *- Calculate 10!. {p_end}
{p 4 4 2}{inp:.} {stata `"myfact 10"'}{p_end}

{p 4 4 2} *- Calculate 10! and specify the format of the output. {p_end}
{p 4 4 2}{inp:.} {stata `"myfact 10, format(%-20.0g)"'}{p_end}

{p 4 4 2} *- Calculate 10! and 11!. {p_end}
{p 4 4 2}{inp:.} {stata `"myfact 10 11"'}{p_end}

{p 4 4 2} *- Calculate 10! and 11! and stores the results in new variables. {p_end}
{p 4 4 2}{inp:.} {stata `"myfact 11 12, gen(x)"'}{p_end}

{title:Author}

{p 4 4 2}
{cmd:Ying,Yu}{break}
Lingnan College, Sun Yat-Sen University.{break}
E-mail: {browse "mailto:yuying29@mail2.sysu.edu.cn":yuying29@mail2.sysu.edu.cn}. {break}


