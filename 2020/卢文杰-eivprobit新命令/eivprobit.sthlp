{smcl}
{* 30 Nov 2020}{...}
{hline}
help for {hi:eivprobit}
{hline}

{title:Title}

{p 4 4 2}
{bf:eivprobit} —— Calculate What?

{title:Syntax}

{p 8 17 2}
{cmd:eivprobit} {depvar} [{it:{help varlist:varlist1}}]
{cmd:(}{it:{help varlist:variable1}} {cmd:=}
       {it:{help varlist:varlist_iv}}{cmd:)} {ifin}
       [{it:{help ivprobit##weight:weight}}]
{cmd:,}  {opth interact:(varlist:variable2)} [{it:{help eivprobit##options:options}}]

{phang}
{it:varliable1} is the endogenous variable.{p_end}

{phang}
{it:variable2} is the interact variable.{p_end}

{phang}
{it:varlist1}is the list of exogenous variables. (exclude {it:variable2}).{p_end}

{phang}
{it:varlist_iv} is the list of exogenous variables used with {it:variable2}
 as instruments for (it isn't required to enter {it:varible2} in this part). {it:varable1}.

{title:Description}

{p 4 4 2}
{cmd:myfact} 描述功能+附上周先波老师论文的链接

{synoptset 28 tabbed}{...}
{marker options}{...}
{synopthdr :options}
{synoptline}
{syntab :Model}

{synopt :{opt mae:ffect}}choose model1?{p_end}

{synopt :{opt qua:ffect}}choose model2?{p_end}

{synoptline}

{title:Examples}



{title:Author}

{p 4 4 2}
{cmd:Author}{break}
Lingnan College, Sun Yat-Sen University.{break}
E-mail: {browse "email":384735253qq.com}. {break}