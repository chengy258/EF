capture program drop myfact
program define myfact, rclass
version 15

capture syntax anything(name=numlist) [, GENerate(string) Format(string)] 
  
*----若用户输入的参数不是正整数，报错并提示---
  
  if _rc{  // 用户忘了输入数字
    dis as error "You must enter positive integers"
	exit
  }  
  
  tokenize "`numlist'"
  local j = 1  
  while "``j''" !=""{     // `j' 表示房间号；``j'' 表示房间中的内容
  
    capture confirm integer number ``j''  // 用户输入了非整数或文字
    if _rc{
      dis as error "You must enter positive integers"
	  exit
    }
    
	if ``j''<0{  // 用户输入了负数
      dis as error "You must enter positive integers"
	  exit
    }
  local j = `j' + 1
  }
  
  //自定义结果的显示格式
  if "`format'" != ""{
    capture confirm format `format'
	if _rc{
	  dis as error "invalid format(`format'). It should be format(%10.0g)."
	  dis as text  "for help, see {help format}"
	  exit 198
	}
  } 
  
  tokenize "`numlist'"
  local j = 1  
  while "``j''" !=""{     // `j' 表示房间号；``j'' 表示房间中的内容
    local a = 1
    local k = ``j''       // 注意暂元的引用方式
    forvalues i = 1/`k'{
      local a = `a'*`i'
    }

    //产生新变量
    if "`generate'" != ""{
      gen `generate'_`k' = `a'
    }
    else{
      dis "`k'! = " `format' `a'
	}
	
    return scalar k`k' = `a'   
    local j = `j' + 1
  }

end 
