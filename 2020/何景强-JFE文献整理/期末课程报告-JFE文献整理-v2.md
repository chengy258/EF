&emsp;

> **何景强**：2018级金融学 (中山大学)    
> **E-Mail:** <18286349285@163.com> 

# JFE最高引文献链接汇总-EF

**前言：** 在本推文中，我将列示下面的论文清单中 274 篇论文的 [微软学术](https://academic.microsoft.com) 链接和 PDF 原文链接，除了第 132 篇、第 170 篇、第 191 篇和第 269 篇四份论文，它们在微软学术中无法被检索到。该四篇论文的微软学术链接将取代为 Bing 学术链接， PDF 链接则与其他论文一致。

论文清单：- [JFE-Top Cited Papers: 1974-2012](http://jfe.rochester.edu/allstar.htm)

 ## 1. 文献来源
 该论文清单中的 274 篇论文均出自国际顶级金融学期刊——金融经济学期刊（Journal of Financial Economics , 简称 JFE ）。这些论文是 1974 年至 2012 年历年被引用量最高的那些，代表了各个时期国际金融学研究的主要方向和前沿成果。希望这篇推文可以帮助大家了解前几十年来金融学的发展与相关知识，帮助各自的学习与进步。

 ## 2. 链接的使用
 点击论文的 PDF 链接，你将转到该论文的 PDF 阅读界面。通过这种方式大家就不用按照论文标题去自行搜索文章并下载 PDF 或者其他文件来看论文，而是做到随时在线阅读。 PDF 链接的原理是利用 DOI ，DOI 的全称是 “ digital object identifier ”，即数字对象唯一标识，通过它可以方便、可靠地链接到论文全文。 DOI 代码具有唯一性，且一个数字化对象的 DOI 标识符一经产生就永久不变，不随其所标识的数字化对象的版权所有者或存储地址等属性的变更而改变。通过论文的 DOI 我们便可以在专门的论文阅读网站上看到它了。

 点击论文的微软学术链接，你将跳转到该论文的微软学术搜索结果界面，在该界面中你可以直接看到论文的标题、出版信息、 DOI 、作者与机构。同时可以看到该论文的参考文献书目数量以及其被引用量，通过这个我们就可以直观地了解该论文的学术价值。此外在微软学术链接中还可以看到该论文的摘要以及学科/内容标签，帮助大家更快地理解论文的主要内容。

 ## 3. 部分论文简介
 这些论文都是国际金融学研究的宝贵财富，值得观摩学习。为了避免内容繁多，我将在下面论文的各自链接中，对 1974 年至 2012 年间年均被引用量最高的20篇文献进行简要的介绍。

各论文作者、出版日期、标题、出版信息、PDF 链接、微软学术链接如下所示：


- 1.Mandelker, Gershon. 1974. “Risk and Return: The Case of Merging Firms.” Journal of Financial Economics 1 (4): 303–35.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(74)90012-9)，[-Link-](https://academic.microsoft.com/paper/2126806822)

- 2.Black, Fischer, and Myron Scholes. 1974. “The Effects of Dividend Yield and Dividend Policy on Common Stock Prices and Returns.” Journal of Financial Economics 1 (1): 1–22.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(74)90006-3)，[-Link-](https://academic.microsoft.com/paper/2003644932)
- 3.Ibbotson, Roger G. 1975. “Price Performance of Common Stock New Issues.” Journal of Financial Economics 2 (3): 235–72.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(75)90015-X)，[-Link-](https://academic.microsoft.com/paper/2154585860)
- 4.Bawa, Vijay S. 1975. “OPTIMAL, RULES FOR ORDERING UNCERTAIN PROSPECTS+.” Journal of Financial Economics 2 (1): 95–121.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(75)90025-2)，[-Link-](https://academic.microsoft.com/paper/2117278512)
- 5.Jensen, Michael C., and William H. Meckling. 1976. “Theory of the Firm: Managerial Behavior, Agency Costs and Ownership Structure.” Journal of Financial Economics 3 (4): 305–60.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(76)90026-X)，[-Link-](https://academic.microsoft.com/paper/3023446773)
 > 上面这篇出版自 1976 年的文献是 JFE 在 1974 - 2012 年间年均被引用数最高的文献， 年均被引用了 195.8 次。该论文本文综合了代理理论、产权理论和财务理论，提出了企业所有权结构理论，定义了代理成本的概念，说明了代理成本与“分离与控制”问题的关系，考察了债务和外部股权的存在所产生的代理成本的性质，论证了谁承担这些成本以及为什么承担这些成本，并考察了它们存在的帕累托最优性。是为公司金融学科的一份奠基之作。
- 6.Merton, Robert C. 1976. “Option Pricing When Underlying Stock Returns Are Discontinuous.” Journal of Financial Economics 3: 125–44.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(76)90022-2)，[-Link-](https://academic.microsoft.com/paper/2151065060)

- 7.Cox, John C., and Stephen A. Ross. 1976. “The Valuation of Options for Alternative Stochastic Processes.” Journal of Financial Economics 3: 145–66.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(76)90023-4)，[-Link-](https://academic.microsoft.com/paper/1966119267)
- 8.Black, Fischer. 1976. “The Pricing of Commodity Contracts.” Journal of Financial Economics 3: 167–79.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(76)90024-6)，[-Link-](https://academic.microsoft.com/paper/2050067494)
- 9.Roll, Richard. 1977. “A Critique of the Asset Pricing Theory’s Tests Part I: On Past and Potential Testability of the Theory.” Journal of Financial Economics 4 (2): 129–76.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(77)90009-5)，[-Link-](https://academic.microsoft.com/paper/2011043606)
- 10.Boyle, Phelim P. 1977. “Options: A Monte Carlo Approach.” Journal of Financial Economics 4 (3): 323–38.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(77)90005-8)，[-Link-](https://academic.microsoft.com/paper/1982039177)
- 11.Myers, Stewart C. 1977. “Determinants of Corporate Borrowing.” Journal of Financial Economics 5 (2): 147–75.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(77)90015-0)，[-Link-](https://academic.microsoft.com/paper/2000578291)
> 上面这篇出版自 1977 年的文献年均被引用了 44.1 次。该文解释了企业借贷与实物期权占市场价值的比例成反比，使公司借贷行为合理化。
- 12.Vasicek, Oldrich A. 1977. “An Equilibrium Characterization of the Term Structure.” Journal of Financial Economics 5 (2): 177–88.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(77)90016-2)，[-Link-](https://academic.microsoft.com/paper/2076057077)

- 13.Scholes, Myron, and Joseph Williams. 1977. “Estimating Betas from Nonsynchronous Data.” Journal of Financial Economics 5 (3): 309–27.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(77)90041-1)，[-Link-](https://academic.microsoft.com/paper/2043619364)
- 14.Fama, Eugene F., and G.William Schwert. 1977. “Asset Returns and Inflation.” Journal of Financial Economics 5 (2): 115–46.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(77)90014-9)，[-Link-](https://academic.microsoft.com/paper/2007785900)
- 15.Jensen, Michael C. 1978. “Some Anomalous Evidence Regarding Market Efficiency.” Journal of Financial Economics 6: 95–101.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.244159)，[-Link-](https://academic.microsoft.com/paper/2162347444)
- 16.Ball, Ray. 1978. “Anomalies in Relationships between Securities’ Yields and Yield-Surrogates.” Journal of Financial Economics 6: 103–26.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(78)90026-0)，[-Link-](https://academic.microsoft.com/paper/2052398032)
- 17.Cox, John C., Stephen A. Ross, and Mark Rubinstein. 1979. “Option Pricing: A Simplified Approach☆.” Journal of Financial Economics 7 (3): 229–63.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(79)90015-1)，[-Link-](https://academic.microsoft.com/paper/2146612430)
- 18.Smith, Clifford W., and Jerold B. Warner. 1979. “On Financial Contracting: An Analysis of Bond Covenants.” Journal of Financial Economics 7 (2): 117–61.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(79)90011-4)，[-Link-](https://academic.microsoft.com/paper/1579112106)
- 19.Breeden, Douglas T. 1979. “AN INTERTEMPORAL ASSET PRICING MODEL WITH STOCHASTIC CONSUMPTION AND INVESTMENT OPPORTUNITIES.” Journal of Financial Economics 7 (3): 265–96.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(79)90016-3)，[-Link-](https://academic.microsoft.com/paper/2082330181)
- 20.Dimson, Elroy. 1979. “Risk Measurement When Shares Are Subject to Infrequent Trading.” Journal of Financial Economics 7 (2): 197–226.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(79)90013-8)，[-Link-](https://academic.microsoft.com/paper/1984272629)
- 21.Brown, Stephen J., and Jerold B. Warner. 1980. “MEASURING SECURITY PRICE PERFORMANCE.” Journal of Financial Economics 8 (3): 205–58.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(80)90002-1)，[-Link-](https://academic.microsoft.com/paper/2101718392)
- 22.Merton, Robert C. 1980. “On Estimating the Expected Return on the Market: An Exploratory Investigation.” Journal of Financial Economics 8 (4): 323–61.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(80)90007-0)，[-Link-](https://academic.microsoft.com/paper/2135395072)
- 23.DeAngelo, Harry, and Ronald W. Masulis. 1980. “Optimal Capital Structure Under Corporate and Personal Taxation.” Journal of Financial Economics 8 (1): 3–29.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(80)90019-7)，[-Link-](https://academic.microsoft.com/paper/2063955807)
- 24.Banz, Rolf W. 1981. “The Relationship between Return and Market Value of Common Stocks.” Journal of Financial Economics 9 (1): 3–18.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(81)90018-0)，[-Link-](https://academic.microsoft.com/paper/2116347131)
- 25.Reinganum, Marc Richard. 1981. “Misspecification of Capital Asset Pricing : Empirical Anomalies Based on Earnings’ Yields and Market Values.” Journal of Financial Economics 9 (1): 19–46.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(81)90019-2)，[-Link-](https://academic.microsoft.com/paper/1493117078)
- 26.Christie, Andrew A. 1982. “The Stochastic Behavior of Common Stock Variances : Value, Leverage and Interest Rate Effects.” Journal of Financial Economics 10 (4): 407–32.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(82)90018-6)，[-Link-](https://academic.microsoft.com/paper/1965635191)
- 27.Gibbons, Michael R. 1982. “MULTIVARIATE TESTS OF FINANCIAL MODELS A New Approach.” Journal of Financial Economics 10 (1): 3–27.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(82)90028-9)，[-Link-](https://academic.microsoft.com/paper/1607636060)
- 28.Jensen, Michael C., and Richard S. Ruback. 2002. “The Market for Corporate Control: The Scientific Evidence.” Journal of Financial Economics 11: 5–50.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.244158)，[-Link-](https://academic.microsoft.com/paper/2156027511)
- 29.Dodd, Peter, and Jerold B. Warner. 1983. “On Corporate Governance: A Study of Proxy Contests.” Journal of Financial Economics 11: 401–38.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(83)90018-1)，[-Link-](https://academic.microsoft.com/paper/1559170486)
- 30.Keim, Donald B. 1983. “SIZE-RELATED ANOMALIES AND STOCK RETURN SEASONALITY Further Empirical Evidence.” Journal of Financial Economics 12 (1): 13–32.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(83)90025-9)，[-Link-](https://academic.microsoft.com/paper/2141673418)
- 31.Blume, Marshall E., and Robert F. Stambaugh. 1983. “BIASES IN COMPUTED RETURNS An Application to the Size Effect.” Journal of Financial Economics 12 (3): 387–404.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(83)90056-9)，[-Link-](https://academic.microsoft.com/paper/1583227791)
- 32.Myers, Stewart C., and Nicholas S. Majluf. 1984. “Corporate Financing and Investment Decisions When Firms Have Information That Investors Do Not Have.” Journal of Financial Economics 13 (2): 187–221.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(84)90023-0)，[-Link-](https://academic.microsoft.com/paper/2140358882)
 > 上面这篇出版自 1984 年的文献年均被引用了 73.1 次。该文建立了投资决策均衡模型，解释了企业融资行为的几个方面。
- 33.Titman, Sheridan Dean. 1984. “The Effect of Capital Structure on a Firm’s Liquidation Decision☆.” Journal of Financial Economics 13 (1): 137–51.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(84)90035-7)，[-Link-](https://academic.microsoft.com/paper/2089052482)

- 34.Brown, Stephen J., and Jerold B. Warner. 1985. “Using Daily Stock Returns: The Case of Event Studies.” Journal of Financial Economics 14 (1): 3–31.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(85)90042-X)，[-Link-](https://academic.microsoft.com/paper/2128778916)
> 上面这篇出版自 1985 年的文献年均被引用了 36.9 次。该文研究了股票日收益率的性质，以及这些数据的特殊特征如何影响事件和研究方法。

- 35.Glosten, Lawrence R., and Paul R. Milgrom. 1985. “Bid, Ask and Transaction Prices in a Specialist Market with Heterogeneously Informed Traders.” Journal of Financial Economics 14 (1): 71–100.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(85)90044-3)，[-Link-](https://academic.microsoft.com/paper/1985808284)
> 上面这篇出版自 1985 年的文献年均被引用了 33.4 次。该文研究了股市中的套利问题。

- 36.Rock, Kevin F. 1986. “Why New Issues Are Underpriced.” Journal of Financial Economics 15: 187–212.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90054-1)，[-Link-](https://academic.microsoft.com/paper/2073488258)

- 37.Beatty, Randolph P., and Jay R. Ritter. 1986. “INVESTMENT BANKING, REPUTATION, AND THE UNDERPRICING OF INITIAL PUBLIC OFFERINGS*.” Journal of Financial Economics 15: 213–32.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90055-3)，[-Link-](https://academic.microsoft.com/paper/1969632991)
- 38.Asquith, Paul, and David W. Mullins. 1986. “Equity Issues and Offering Dilution.” Journal of Financial Economics 15: 61–89.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90050-4)，[-Link-](https://academic.microsoft.com/paper/2010917876)
- 39.Mikkelson, Wayne H., and M.Megan Partch. 1986. “Valuation Effects of Security Offerings and the Issuance Process.” Journal of Financial Economics 15: 31–60.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90049-8)，[-Link-](https://academic.microsoft.com/paper/2085189944)
- 40.Smith, Clifford W. 1986. “INVESTMENT BANKING AND THE CAPITAL ACQUISITION PROCESS.” Journal of Financial Economics 15: 3–29.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90048-6)，[-Link-](https://academic.microsoft.com/paper/2171798971)
- 41.Harris, Lawrence. 1986. “A Transaction Data Study of Weekly and Intradaily Patterns in Stock Returns.” Journal of Financial Economics 16 (1): 99–117.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90044-9)，[-Link-](https://academic.microsoft.com/paper/2030548181)
- 42.Seyhun, H.Nejat. 1986. “Insiders’ Profits, Costs of Trading, and Market Efficiency.” Journal of Financial Economics 16 (2): 189–212.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90060-7)，[-Link-](https://academic.microsoft.com/paper/2098630070)
- 43.Amihud, Yakov, and Haim Mendelson. 1986. “Asset Pricing and the Bid-Ask Spread.” Journal of Financial Economics 17 (2): 223–49.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90065-6)，[-Link-](https://academic.microsoft.com/paper/2046519558)
- 44.French, Kenneth R., and Richard Roll. 1986. “Stock Return Variances: The Arrival of Information and the Reaction of Traders.” Journal of Financial Economics 17 (1): 5–26.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90004-8)，[-Link-](https://academic.microsoft.com/paper/1485872351)
- 45.Keim, Donald B., and Robert F. Stambaugh. 1986. “Predicting Returns in the Stock and Bond Markets.” Journal of Financial Economics 17 (2): 357–90.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(86)90070-X)，[-Link-](https://academic.microsoft.com/paper/1975161917)
- 46.Campbell, John Y. 1987. “Stock Returns and the Term Structure.” Journal of Financial Economics 18 (2): 373–99.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(87)90045-6)，[-Link-](https://academic.microsoft.com/paper/2118775685)
- 47.Brickley, James A., and Frederick H. Dark. 1987. “The Choice of Organizational Form The Case of Franchising.” Journal of Financial Economics 18 (2): 401–20.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(87)90046-8)，[-Link-](https://academic.microsoft.com/paper/2035438980)
- 48.French, Kenneth R., G.William Schwert, and Robert F. Stambaugh. 1987. “Expected Stock Returns and Volatility.” Journal of Financial Economics 19 (1): 3–29.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(87)90026-2)，[-Link-](https://academic.microsoft.com/paper/1966268097)
- 49.Easley, David, and Maureen O’Hara. 1987. “PRICE, TRADE SIZE, AND INFORMATION IN SECURITIES MARKETS*.” Journal of Financial Economics 19 (1): 69–90.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(87)90029-8)，[-Link-](https://academic.microsoft.com/paper/1979986072)
- 50.James, Christopher M. 1987. “Some Evidence on the Uniqueness of Bank Loans.” Journal of Financial Economics 19 (2): 217–35.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(87)90003-1)，[-Link-](https://academic.microsoft.com/paper/1978352128)
- 51.Morck, Randall, Andrei Shleifer, and Robert W. Vishny. 1988. “Management Ownership and Market Valuation: An Empirical Analysis.” Journal of Financial Economics 20: 293–315.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90048-7)，[-Link-](https://academic.microsoft.com/paper/1606272114)
> 上面这篇出版自 1988 年的文献年均被引用了 42.7 次。该文研究了管理层持股与公司绩效之间的关系，结论是绩效随着董事会持股的增加先增加后下降。

- 52.Weisbach, Michael S. 1988. “Outside Directors and CEO Turnover.” Journal of Financial Economics 20: 431–60.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90053-0)，[-Link-](https://academic.microsoft.com/paper/2111562586)

- 53.Stulz, Rene M. 1988. “Managerial Control of Voting Rights: Financing Policies and the Market for Corporate Control.” Journal of Financial Economics 20: 25–54.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90039-6)，[-Link-](https://academic.microsoft.com/paper/1494703832)
- 54.Warner, Jerold B., Ross L. Watts, and Karen Hopper Wruck. 1988. “Stock Prices and Top Management Changes.” Journal of Financial Economics 20: 461–92.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90054-2)，[-Link-](https://academic.microsoft.com/paper/2166593106)
- 55.Brickley, James A., Ronald C. Lease, and Clifford W. Smith. 1988. “Ownership Structure and Voting on Antitakeover Amendments.” Journal of Financial Economics 20: 267–91.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90047-5)，[-Link-](https://academic.microsoft.com/paper/2063313351)
- 56.Bradley, Michael, Anand Desai, and E.Han Kim. 1988. “Synergistic Gains from Corporate Acquisitions and Their Division between the Stockholders of Target and Acquiring Firms.” Journal of Financial Economics 21 (1): 3–40.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90030-X)，[-Link-](https://academic.microsoft.com/paper/2087582955)
- 57.Glosten, Lawrence R, and Lawrence E Harris. 1988. “Estimating the Components of the Bid/Ask Spread.” Journal of Financial Economics 21 (1): 123–42.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90034-7)，[-Link-](https://academic.microsoft.com/paper/2035606631)
- 58.Fama, Eugene F, and Kenneth R French. 1988. “Dividend Yields and Expected Stock Returns.” Journal of Financial Economics 22 (1): 3–25.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(88)90020-7)，[-Link-](https://academic.microsoft.com/paper/2089556049)
- 59.Poterba, James M., and Lawrence H. Summers. 1987. “Mean Reversion in Stock Prices: Evidence and Implications.” Journal of Financial Economics 22 (1): 27–59.
 [-PDF-](http://sci-hub.ren/10.3386/W2343)，[-Link-](https://academic.microsoft.com/paper/2150779817)
- 60.Allen, Franklin, and Gerald R. Faulhaber. 1989. “Signalling by Underpricing in the IPO Market.” Journal of Financial Economics 23 (2): 303–23.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(89)90060-3)，[-Link-](https://academic.microsoft.com/paper/2089858078)
- 61.Wruck, Karen Hopper. 1989. “Equity Ownership Concentration and Firm Value: Evidence from Private Equity Financings.” Journal of Financial Economics 23 (1): 3–28.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(89)90003-2)，[-Link-](https://academic.microsoft.com/paper/1538548636)
- 62.Benveniste, Lawrence M., and Paul A. Spindt. 1989. “How Investment Bankers Determine the Offer Price and Allocation of New Issues.” Journal of Financial Economics 24 (2): 343–61.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(89)90051-2)，[-Link-](https://academic.microsoft.com/paper/2091957067)
- 63.Kaplan, Steven. 1989. “The Effects of Management Buyouts on Operating Performance and Value.” Journal of Financial Economics 24 (2): 217–54.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(89)90047-0)，[-Link-](https://academic.microsoft.com/paper/2043159738)
- 64.Fama, Eugene F., and Kenneth R. French. 1989. “BUSINESS CONDITIONS AND EXPECTED RETURNS ON STOCKS AND BONDS.” Journal of Financial Economics 25 (1): 23–49.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(89)90095-0)，[-Link-](https://academic.microsoft.com/paper/2128475248)
- 65.Shleifer, Andrei, and Robert W. Vishny. 1989. “Management Entrenchment : The Case of Manager-Specific Investments.” Journal of Financial Economics 25 (1): 123–39.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(89)90099-8)，[-Link-](https://academic.microsoft.com/paper/2098861776)
- 66.Stulz, RenéM. 1990. “Managerial Discretion and Optimal Financing Policies.” Journal of Financial Economics 26 (1): 3–27.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90011-N)，[-Link-](https://academic.microsoft.com/paper/2129123189)
- 67.Rosenstein, Stuart, and Jeffrey G Wyatt. 1990. “Outside Directors, Board Independence, and Shareholder Wealth☆.” Journal of Financial Economics 26 (2): 175–91.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90002-H)，[-Link-](https://academic.microsoft.com/paper/2037832385)
- 68.McConnell, John J., and Henri Servaes. 1990. “Additional Evidence on Equity Ownership and Corporate Value.” Journal of Financial Economics 27 (2): 595–612.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90069-C)，[-Link-](https://academic.microsoft.com/paper/1979927021)
- 69.Sahlman, William A. 1990. “The Structure and Governance of Venture-Capital Organizations.” Journal of Financial Economics 27 (2): 473–521.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90065-8)，[-Link-](https://academic.microsoft.com/paper/1966598925)
- 70.Gilson, Stuart C. 1990. “Bankruptcy, Boards, Banks, and Blockholders: Evidence on Changes in Corporate Ownership and Control When Firms Default.” Journal of Financial Economics 27 (2): 355–87.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90060-D)，[-Link-](https://academic.microsoft.com/paper/1513613701)
- 71.Gilson, Stuart C., Kose John, and Larry H.P. Lang. 1990. “Troubled Debt Restructurings*1: An Empirical Study of Private Reorganization of Firms in Default.” Journal of Financial Economics 27 (2): 315–53.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90059-9)，[-Link-](https://academic.microsoft.com/paper/1562628026)
- 72.Hoshi, Takeo, Anil K. Kashyap, and David S. Scharfstein. 1990. “The Role of Banks in Reducing the Costs of Financial Distress in Japan.” Journal of Financial Economics 27 (1): 67–88.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90021-Q)，[-Link-](https://academic.microsoft.com/paper/1966761380)
- 73.Benveniste, Lawrence M., and William J. Wilhelm. 1990. “A Comparative Analysis of IPO Proceeds under Alternative Regulatory Environments.” Journal of Financial Economics 28: 173–207.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90052-2)，[-Link-](https://academic.microsoft.com/paper/2047518615)
- 74.James, Christopher, and Peggy Wier. 1990. “Borrowing Relationships, Intermediation, and the Cost of Issuing Public Securities.” Journal of Financial Economics 28: 149–71.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(90)90051-Z)，[-Link-](https://academic.microsoft.com/paper/2052479428)
- 75.Mankiw, N.Gregory, and Stephen P. Zeldes. 1991. “The Consumption of Stockholders and Nonstockholders.” Journal of Financial Economics 29 (1): 97–112.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(91)90015-C)，[-Link-](https://academic.microsoft.com/paper/2058861354)
- 76.Lang, Larry H.P., RenéM. Stulz, and Ralph A. Walkling. 1991. “A Test of the Free Cash Flow Hypothesis.” Journal of Financial Economics 29 (2): 315–35.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(91)90005-5)，[-Link-](https://academic.microsoft.com/paper/1484815137)
- 77.Boehmer, Ekkehart, Jim Masumeci, and Annette B. Poulsen. 1991. “Event-Study Methodology under Conditions of Event-Induced Variance.” Journal of Financial Economics 30 (2): 253–72.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(91)90032-F)，[-Link-](https://academic.microsoft.com/paper/2033910434)
- 78.Kim, Oliver. 1991. “Market Reaction to Anticipated Announcements.” Journal of Financial Economics 30 (2): 273–309.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(91)90033-G)，[-Link-](https://academic.microsoft.com/paper/2077426561)
- 79.Campbell, John Y., and Ludger Hentschel. 1992. “No News Is Good News: An Asymmetric Model of Changing Volatility in Stock Returns.” Journal of Financial Economics 31 (3): 281–318.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(92)90037-X)，[-Link-](https://academic.microsoft.com/paper/1879215575)
- 80.Healy, Paul M., Krishna G. Palepu, and Richard S. Ruback. 1992. “Does Corporate Performance Improve after Mergers?” Journal of Financial Economics 31 (2): 135-75.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(92)90002-F)，[-Link-](https://academic.microsoft.com/paper/2025875426)
- 81.Smith, Clifford W., and Ross L. Watts. 1992. “The Investment Opportunity Set and Corporate Financing, Dividend, and Compensation Policies.” Journal of Financial Economics 32 (3): 263–92.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(92)90029-W)，[-Link-](https://academic.microsoft.com/paper/2020970716)
- 82.Byrd, John W., and Kent A. Hickman. 1992. “Do Outside Directors Monitor Managers.” Journal of Financial Economics 32 (2): 195–221.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(92)90018-S)，[-Link-](https://academic.microsoft.com/paper/1496020752)
- 83.Lakonishok, Josef, Andrei Shleifer, and Robert W. Vishny. 1992. “The Impact of Institutional Trading on Stock Prices.” Journal of Financial Economics 32 (1): 23–43.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(92)90023-Q)，[-Link-](https://academic.microsoft.com/paper/2047803175)
- 84.Fama, Eugene F., and Kenneth R. French. 1993. “Common Risk Factors in the Returns on Stocks and Bonds.” Journal of Financial Economics 33 (1): 3–56.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(93)90023-5)，[-Link-](https://academic.microsoft.com/paper/1995834279)
> 上面这篇出版自 1993 年的文献年均被引用了 130 次。该文分析并解释了影响股票和债券收益率的五个常见风险因素。

- 85.Chan, Louis K.C., and Josef Lakonishok. 1993. “Institutional Trades and Intraday Stock Price Behavior.” Journal of Financial Economics 33 (2): 173–99.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(93)90003-T)，[-Link-](https://academic.microsoft.com/paper/2052147470)

- 86.Barclay, Michael J., and Jerold B. Warner. 1993. “Stealth Trading and Volatility: Which Trades Move Prices?” Journal of Financial Economics 34 (3): 281–305.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(93)90029-B)，[-Link-](https://academic.microsoft.com/paper/1547280275)
- 87.Hanley, Kathleen Weiss. 1993. “The Underpricing of Initial Public Offerings and the Partial Adjustment Phenomenon.” Journal of Financial Economics 34 (2): 231–50.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(93)90019-8)，[-Link-](https://academic.microsoft.com/paper/2071869434)
- 88.Brickley, James A., Jeffrey L. Coles, and Rory L. Terry. 1994. “Outside Directors and the Adoption of Poison Pills.” Journal of Financial Economics 35 (3): 371–90.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)90038-8)，[-Link-](https://academic.microsoft.com/paper/2028407599)
- 89.Lerner, Joshua. 1994. “VENTURE CAPITALISTS AND THE DECISION TO GO PUBLIC.” Journal of Financial Economics 35 (3): 293–316.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)90035-3)，[-Link-](https://academic.microsoft.com/paper/2034306329)
- 90.Kaplan, Steven N., and Bernadette A. Minton. 1994. “Appointments of Outsiders to Japanese Boards: Determinants and Implications for Managers.” Journal of Financial Economics 36 (2): 225–58.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)90025-6)，[-Link-](https://academic.microsoft.com/paper/2124701251)
- 91.Blanchard, Olivier Jean, Florencio Lopez-de-Silanes, and Andrei Shleifer. 1994. “What Do Firms Do with Cash Windfalls.” Journal of Financial Economics 36 (3): 337–60.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)90009-4)，[-Link-](https://academic.microsoft.com/paper/2043607849)
- 92.Berger, Philip G., and Eli Ofek. 1995. “Diversification’s Effect on Firm Value.” Journal of Financial Economics 37 (1): 39–65.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)00798-6)，[-Link-](https://academic.microsoft.com/paper/2049324641)
- 93.Comment, Robert, and Gregg A. Jarrell. 1995. “Corporate Focus and Stock Returns.” Journal of Financial Economics 37 (1): 67–87.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)00777-X)，[-Link-](https://academic.microsoft.com/paper/2018474480)
- 94.Mehran, Hamid. 1995. “Executive Compensation Structure, Ownership, and Firm Performance.” Journal of Financial Economics 38 (2): 163–84.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)00809-F)，[-Link-](https://academic.microsoft.com/paper/2036281662)
- 95.Spiess, D.Katherine, and John Affleck-Graves. 1995. “Underperformance in Long-Run Stock Returns Following Seasoned Equity Offerings.” Journal of Financial Economics 38 (3): 243–67.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)00817-K)，[-Link-](https://academic.microsoft.com/paper/2088782463)
- 96.Ikenberry, David L., Josef Lakonishok, and Theo Vermaelen. 1995. “Market Underreaction to Open Market Share Repurchases.” Journal of Financial Economics 39: 181–208.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(95)00826-Z)，[-Link-](https://academic.microsoft.com/paper/2129935464)
- 97.Yermack, David. 1995. “Do Corporations Award CEO Stock Options Effectively.” Journal of Financial Economics 39: 237–69.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(95)00829-4)，[-Link-](https://academic.microsoft.com/paper/2088111862)
- 98.Comment, Robert, and G. William Schwert. 1995. “Poison or Placebo? Evidence on the Deterrent and Wealth Effects of Modern Antitakeover Measures.” Journal of Financial Economics 39 (1): 3–43.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(94)00823-J)，[-Link-](https://academic.microsoft.com/paper/2141623098)
- 99.Yermack, David. 1996. “Higher Market Valuation of Companies with a Small Board of Directors.” Journal of Financial Economics 40 (2): 185–211.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(95)00844-5)，[-Link-](https://academic.microsoft.com/paper/2074132263)
> 上面这篇出版自 1996 年的文献年均被引用了 39.3 次。该文发现并证明了董事会规模与公司价值之间存在反向关联、拥有小型董事会的公司在财务比率方面也表现出更有利的价值，即小董事会更有效。

- 100.Lang, Larry H. P., Eli. Ofek, and René M. Stulz. 1996. “Leverage, Investment, and Firm Growth.” Journal of Financial Economics 40 (1): 3–29.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(95)00842-3)，[-Link-](https://academic.microsoft.com/paper/2069841552)

- 101.Barber, Brad M., and John D. Lyon. 1996. “Detecting Abnormal Operating Performance: The Empirical Power and Specification of Test Statistics.” Journal of Financial Economics 41 (3): 359–99.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(96)84701-5)，[-Link-](https://academic.microsoft.com/paper/2101346776)
- 102.Huang, Roger D., and Hans R. Stoll. 1996. “Dealer versus Auction Markets: A Paired Comparison of Execution Costs on NASDAQ and the NYSE.” Journal of Financial Economics 41 (3): 313–57.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(95)00867-E)，[-Link-](https://academic.microsoft.com/paper/2149625909)
- 103.Brennan, Michael J., and Avanidhar Subrahmanyam. 1996. “Market Microstructure and Asset Pricing: On the Compensation for Illiquidity in Stock Returns.” Journal of Financial Economics 41 (3): 441–64.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(95)00870-K)，[-Link-](https://academic.microsoft.com/paper/2129525919)
- 104.Mitchell, Mark L., and J.Harold Mulherin. 1996. “The Impact of Industry Shocks on Takeover and Restructuring Activity.” Journal of Financial Economics 41 (2): 193–229.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(95)00860-H)，[-Link-](https://academic.microsoft.com/paper/2060542710)
- 105.Gray, Stephen F. 1996. “Modeling the Conditional Distribution of Interest Rates as a Regime-Switching Process.” Journal of Financial Economics 42 (1): 27–62.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(96)00875-6)，[-Link-](https://academic.microsoft.com/paper/2012242451)
- 106.Gompers, Paul A. 1996. “Grandstanding in the Venture Capital Industry.” Journal of Financial Economics 42 (1): 133–56.
 [-PDF-](http://sci-hub.ren/10.1016/0304-405X(96)00874-4)，[-Link-](https://academic.microsoft.com/paper/2117477253)
- 107.Fama, Eugene F., and Kenneth R. French. 1997. “Industry Costs of Equity.” Journal of Financial Economics 43 (2): 153–93.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(96)00896-3)，[-Link-](https://academic.microsoft.com/paper/2047134779)
> 上面这篇出版自 1997 年的文献年均被引用了 50.2 次。该文证明以往的对工业部门股本对公司影响的估计是不精确的，同时说明了误差的来源。

- 108.Barber, Brad M., and John D. Lyon. 1997. “Detecting Long-Run Abnormal Stock Returns: The Empirical Power and Specification of Test Statistics.” Journal of Financial Economics 43 (3): 341–72.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(96)00890-2)，[-Link-](https://academic.microsoft.com/paper/2104796460)

- 109.Bekaert, Geert, and Campbell R. Harvey. 1997. “Emerging Equity Market Volatility.” Journal of Financial Economics 43 (1): 29–77.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(96)00889-6)，[-Link-](https://academic.microsoft.com/paper/1968880732)
- 110.Kothari, S.P., and Jerold B. Warner. 1997. “Measuring Long-Horizon Security Price Performance.” Journal of Financial Economics 43 (3): 301–39.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(96)00899-9)，[-Link-](https://academic.microsoft.com/paper/2131456352)
- 111.Mikkelson, Wayne H., M. Megan Partch, and Kshitij Shah. 1997. “Ownership and Operating Performance of Companies That Go Public.” Journal of Financial Economics 44 (3): 281–307.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(97)00006-8)，[-Link-](https://academic.microsoft.com/paper/1995208417)
- 112.Kothari, S.P., and Jay A. Shanken. 1997. “Book-to-Market, Dividend Yield, and Expected Market Returns: A Time-Series Analysis.” Journal of Financial Economics 44 (2): 169–203.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(97)00002-0)，[-Link-](https://academic.microsoft.com/paper/2093571522)
- 113.Denis, David J., Diane K. Denis, and Atulya Sarin. 1997. “Ownership Structure and Top Executive Turnover.” Journal of Financial Economics 45 (2): 193–221.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(97)00016-0)，[-Link-](https://academic.microsoft.com/paper/2093482446)
- 114.Amihud, Yakov, Haim Mendelson, and Beni Lauterbach. 1997. “Market Microstructure and Securities Values: Evidence from the Tel Aviv Stock Exchange.” Journal of Financial Economics 45 (3): 365–90.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(97)00021-4)，[-Link-](https://academic.microsoft.com/paper/2036344053)
- 115.Kang, Jun-Koo, and Rene´M. Stulz. 1997. “Why Is There a Home Bias? An Analysis of Foreign Portfolio Equity Ownership in Japan.” Journal of Financial Economics 46 (1): 3–28.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(97)00023-8)，[-Link-](https://academic.microsoft.com/paper/2170931553)
- 116.Parrino, Robert. 1997. “CEO Turnover and Outside Succession A Cross-Sectional Analysis.” Journal of Financial Economics 46 (2): 165–97.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(97)00028-7)，[-Link-](https://academic.microsoft.com/paper/2120570605)
- 117.Black, Bernard S., and Ronald J. Gilson. 1997. “Venture Capital and the Structure of Capital Markets: Banks Versus Stock Markets.” Journal of Financial Economics 47 (3): 243–77.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.46909)，[-Link-](https://academic.microsoft.com/paper/2110765734)
- 118.Cho, Myeong-Hyeon. 1998. “Ownership Structure, Investment, and the Corporate Value: An Empirical Analysis.” Journal of Financial Economics 47 (1): 103–21.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(97)00039-1)，[-Link-](https://academic.microsoft.com/paper/2041380708)
- 119.Eisenberg, Theodore, Stefan Sundgren, and Martin T. Wells. 1998. “Larger Board Size and Decreasing Firm Value in Small Firms.” Journal of Financial Economics 48 (1): 35–54.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00003-8)，[-Link-](https://academic.microsoft.com/paper/1526996522)
- 120.Carpenter, Jennifer N. 1997. “The Exercise and Valuation of Executive Stock Options.” Journal of Financial Economics 48 (2): 127–58.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00006-3)，[-Link-](https://academic.microsoft.com/paper/2164335397)
- 121.Fama, Eugene F. 1998. “Market Efficiency, Long-Term Returns, and Behavioral Finance1The Comments of Brad Barber, David Hirshleifer, S.P. Kothari, Owen Lamont, Mark Mitchell, Hersh Shefrin, Robert Shiller, Rex Sinquefield, Richard Thaler, Theo Vermaelen, Robert Vishny, Ivo Welch, and a Referee Have Been Helpful. Kenneth French and Jay Ritter Get Special Thanks.1.” Journal of Financial Economics 49 (3): 283–306.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00026-9)，[-Link-](https://academic.microsoft.com/paper/2111255674)
> 上面这篇出版自 1998 年的文献年均被引用了 39.7 次。该文通过实例与理论解释，有力证明并支撑了有效市场假说的合理性。

- 122.Barberis, Nicholas, Andrei Shleifer, and Robert W. Vishny. 1997. “A Model of Investor Sentiment.” Journal of Financial Economics 49 (3): 307–43.
 [-PDF-](http://sci-hub.ren/10.3386/W5926)，[-Link-](https://academic.microsoft.com/paper/2132200203)
> 上面这篇出版自 1997 年的文献年均被引用了 33.7 次。该文针对股价对财报等消息反应不足、股价对一系列好消息或坏消息反应过度两个问题，提出了投资者情绪简约模型以说明投资者如何形成与实证结果一致的信念。

- 123.Brennan, Michael J., Tarun Chordia, and Avanidhar Subrahmanyam. 1998. “Alternative Factor Specifications, Security Characteristics, and the Cross-Section of Expected Stock Returns.” Journal of Financial Economics 49 (3): 345–73.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00028-2)，[-Link-](https://academic.microsoft.com/paper/1560849420)

- 124.Welch, Ivo, Siew Hong Teoh, and T.J. Wong. 1998. “Earnings Management and the Post-Issue Underperformance in Seasoned Equity Offerings.” The Finance.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00032-4)，[-Link-](https://academic.microsoft.com/paper/2142242808)
- 125.Christensen, Bent Jesper, and Nagpurnanand Prabhala. 1998. “The Relation between Implied and Realized Volatility.” Journal of Financial Economics 50 (2): 125–50.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00034-8)，[-Link-](https://academic.microsoft.com/paper/1594842258)
- 126.Core, John E., Robert W. Holthausen, and David F. Larcker. 1999. “Corporate Governance, Chief Executive Officer Compensation, and Firm Performance.” Journal of Financial Economics 51 (3): 371–406.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00058-0)，[-Link-](https://academic.microsoft.com/paper/2101376317)
> 上面这篇出版自 1999 年的文献年均被引用了 31.6 次。该文研究发现：治理结构较弱的公司存在更大的代理问题；代理问题越严重的公司的首席执行官获得的报酬越高；代理问题越严重的公司表现越差。

- 127.Shyam-Sunder, Lakshmi, and Stewart C. Myers. 1999. “Testing Static Tradeoff against Pecking Order Models of Capital Structure.” Journal of Financial Economics 51 (2): 219–44.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(98)00051-8)，[-Link-](https://academic.microsoft.com/paper/2167351159)

- 128.Opler, Tim C., Lee Pinkowitz, René M. Stulz, and Rohan Williamson. 1999. “The Determinants and Implications of Corporate Cash Holdings.” Journal of Financial Economics 52 (1): 3–46.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00003-3)，[-Link-](https://academic.microsoft.com/paper/2162225546)
- 129.Guercio, Diane Del, and Jennifer Hawkins. 1999. “The Motivation and Impact of Pension Fund Activism.” Journal of Financial Economics 52 (3): 293–340.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00011-2)，[-Link-](https://academic.microsoft.com/paper/2144862241)
- 130.Himmelberg, Charles P., R. Glenn Hubbard, and Darius Palia. 1999. “Understanding the Determinants of Managerial Ownership and the Link Between Ownership and Performance.” Journal of Financial Economics 53 (3): 353–84.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00025-2)，[-Link-](https://academic.microsoft.com/paper/2095608235)
- 131.Guay, Wayne R. 1999. “The Sensitivity of CEO Wealth to Equity Risk: An Analysis of the Magnitude and Determinants ☆.” Journal of Financial Economics 53 (1): 43–71.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00016-1)，[-Link-](https://academic.microsoft.com/paper/2004601982)
- 132.Robert F. Stambaugh. 1999. “Predictive regressions.” Journal of Financial Economics 54 (3): 375–421.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00041-0)，[-Link:Bing学术界面-](http://www.sciencedirect.com/science/article/pii/S0304405X99000410)
- 133.Choe, Hyuk, Bong-Chan Kho, and René M. Stulz. 1999. “Do Foreign Investors Destabilize Stock Markets? The Korean Experience in 1997.” Journal of Financial Economics 54 (2): 227–64.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00037-9)，[-Link-](https://academic.microsoft.com/paper/1538364569)
- 134.Grinblatt, Mark, and Matti Keloharju. 2000. “The Investment Behavior and Performance of Various Investor Types: A Study of Finland’s Unique Data Set.” Journal of Financial Economics 55 (1): 43–67.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00044-6)，[-Link-](https://academic.microsoft.com/paper/2171168084)
- 135.Loughran, Tim, and Jay R. Ritter. 2000. “Uniformly Least Powerful Tests of Market Efficiency.” Journal of Financial Economics 55 (3): 361–89.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00054-9)，[-Link-](https://academic.microsoft.com/paper/2094442264)
- 136.Chordia, Tarun, Richard Roll, and Avanidhar Subrahmanyam. 2000. “Commonality in Liquidity.” Journal of Financial Economics 56 (1): 3–28.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(99)00057-4)，[-Link-](https://academic.microsoft.com/paper/2140268443)
- 137.Brav, Alon, Christopher C Geczy, and Paul A Gompers. 2000. “Is the Abnormal Return Following Equity Issuances Anomalous.” Journal of Financial Economics 56 (2): 209–49.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00040-4)，[-Link-](https://academic.microsoft.com/paper/2064067537)
- 138.Gillan, Stuart L., and Laura T. Starks. 2000. “Corporate Governance Proposals and Shareholder Activism: The Role of Institutional Investors.” Journal of Financial Economics 57 (2): 275–305.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00058-1)，[-Link-](https://academic.microsoft.com/paper/2051140949)
- 139.Jagannathan, Murali, Clifford P Stephens, and Michael S Weisbach. 2000. “Financial Flexibility and the Choice between Dividends and Stock Repurchases.” Journal of Financial Economics 57 (3): 355–84.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00061-1)，[-Link-](https://academic.microsoft.com/paper/2095719731)
- 140.Claessens, Stijn, Simeon Djankov, and Larry H.P Lang. 2000. “The Separation of Ownership and Control in East Asian Corporations.” Journal of Financial Economics 58 (12): 81–112.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00067-2)，[-Link-](https://academic.microsoft.com/paper/2105547916)
> 上面这篇出版自 2000 年的文献年均被引用了 48.1 次。该文研究了东亚诸国的“财阀”问题，发现少数家族如何以交叉持股和金字塔结构的方式控制了巨大的资本。

- 141.Porta, Rafael La, Florencio Lopez-de-Silanes, Andrei Shleifer, and Robert W. Vishny. 2000. “Investor Protection and Corporate Governance.” Journal of Financial Economics 58 (12): 3–27.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00065-9)，[-Link-](https://academic.microsoft.com/paper/2162012454)
> 上面这篇出版自 2000 年的文献年均被引用了 45.5 次。该文描述了各国法律的差异及其执行效力，讨论了这些差异的可能根源，总结了其后果，并评估了公司治理改革的潜在战略。

- 142.Beck, Thorsten, Ross Levine, and Norman Loayza. 2000. “Finance and the Sources of Growth.” Journal of Financial Economics 58 (1): 261–300.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00072-6)，[-Link-](https://academic.microsoft.com/paper/2063609809)

- 143.Morck, Randall, Bernard Yin Yeung, and Wayne Yu. 2000. “The Information Content of Stock Markets: Why Do Emerging Markets Have Synchronous Stock Price Movements?” Journal of Financial Economics 58 (1): 215–60.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00071-4)，[-Link-](https://academic.microsoft.com/paper/2102678177)
- 144.Wurgler, Jeffrey. 2000. “Financial Markets And The Allocation Of Capital.” Journal of Financial Economics 58 (1): 187–214.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00070-2)，[-Link-](https://academic.microsoft.com/paper/1632858629)
- 145.Johnson, Simon, Peter D. Boone, Alasdair Breach, and Eric Friedman. 2000. “Corporate Governance in the Asian Financial Crisis.” Journal of Financial Economics 58 (12): 141–86.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00069-6)，[-Link-](https://academic.microsoft.com/paper/2126555709)
- 146.Hasbrouck, Joel, and Duane J. Seppi. 2001. “Common Factors in Prices, Order Flows and Liquidity.” Journal of Financial Economics 59 (3): 383–411.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00091-X)，[-Link-](https://academic.microsoft.com/paper/2080566469)
- 147.Froot, Kenneth A., Paul G.J. O’Connell, and Mark S. Seasholes. 2001. “The Portfolio Flows of International Investors.” Journal of Financial Economics 59 (2): 151–93.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(00)00084-2)，[-Link-](https://academic.microsoft.com/paper/2030100740)
- 148.Graham, John R, and Campbell R Harvey. 2001. “The Theory and Practice of Corporate Finance: Evidence from the Field.” Journal of Financial Economics 60 (23): 187–243.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(01)00044-7)，[-Link-](https://academic.microsoft.com/paper/2160867884)
> 上面这篇出版自 2001 年的文献年均被引用了 41.4 次。该文发现了不同金融主体在决策上的不同之处，大公司更接近理论，而小公司注重于收益，另外发现公司高管很少去利用金融学的知识。

- 149.Fama, Eugene F., and Kenneth R. French. 2001. “DISAPPEARING DIVIDENDS: CHANGING FIRM CHARACTERISTICS OR LOWER PROPENSITY TO PAY?” Journal of Applied Corporate Finance 14 (1): 67–79.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.203092)，[-Link-](https://academic.microsoft.com/paper/2101365736)

- 150.Andersen, Torben G., Tim Bollerslev, Francis X. Diebold, and Heiko Ebens. 2001. “The Distribution of Realized Stock Return Volatility.” Journal of Financial Economics 61 (1): 43–76.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(01)00055-1)，[-Link-](https://academic.microsoft.com/paper/1963787328)
- 151.Chakravarty, Sugato. 2002. “Stealth-Trading: Which Traders’ Trades Move Stock Prices?” The Finance.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(01)00063-0)，[-Link-](https://academic.microsoft.com/paper/1589622760)
- 152.Daines, Robert. 2001. “Does Delaware Law Improve Firm Value.” Journal of Financial Economics 62 (3): 525–58.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(01)00086-1)，[-Link-](https://academic.microsoft.com/paper/2098730268)
- 153.Zhou, Xianming. 2001. “Understanding the Determinants of Managerial Ownership and the Link between Ownership and Performance: Comment.” Journal of Financial Economics 62 (3): 559–71.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(01)00085-X)，[-Link-](https://academic.microsoft.com/paper/1967358294)
- 154.Pan, Jun. 2002. “The Jump-Risk Premia Implicit in Options: Evidence from an Integrated Time-Series Study $.” Journal of Financial Economics 63 (1): 3–50.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(01)00088-5)，[-Link-](https://academic.microsoft.com/paper/2163191305)
- 155.Ang, Andrew, and Joseph Chen. 2002. “Asymmetric Correlations of Equity Portfolios.” Journal of Financial Economics 63 (3): 443–94.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00068-5)，[-Link-](https://academic.microsoft.com/paper/2025116214)
- 156.Dai, Qiang, and Kenneth J. Singleton. 2002. “Expectation Puzzles, Time-Varying Risk Premia, and Dynamic Models of the Term Structure.” Journal of Financial Economics 63 (3): 415–41.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00067-3)，[-Link-](https://academic.microsoft.com/paper/2103736934)
- 157.Mitton, Todd. 2002. “A Cross-Firm Analysis of the Impact of Corporate Governance on the East Asian Financial Crisis.” Journal of Financial Economics 64 (2): 215–41.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00076-4)，[-Link-](https://academic.microsoft.com/paper/2157252233)
- 158.Beck, Thorsten, and Ross Levine. 2002. “Industry Growth and Capital Allocation.” Journal of Financial Economics 64 (2): 147–80.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00074-0)，[-Link-](https://academic.microsoft.com/paper/3094402076)
- 159.Faccio, Mara, and Larry H.P Lang. 2002. “The Ultimate Ownership of Western European Corporations.” Journal of Financial Economics 65 (3): 365–95.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00146-0)，[-Link-](https://academic.microsoft.com/paper/2152283134)
> 上面这篇出版自 2002 年的文献年均被引用了 37.5 次。该文研究了西欧企业的“财团”问题，发现金融和大型企业更可能被广泛持有，而非金融和小型企业更可能被家族控制。但总体而言巨型家族财团很少。本文可以与上面研究东亚地区财阀问题的文献对比观看。

- 160.Chordia, Tarun, Richard Roll, and Avanidhar Subrahmanyam. 2002. “Order Imbalance, Liquidity, and Market Returns.” Journal of Financial Economics 65 (1): 111–30.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00136-8)，[-Link-](https://academic.microsoft.com/paper/2115708895)

- 161.D’Avolio, Gene. 2002. “The Market for Borrowing Stock.” Journal of Financial Economics 66 (2): 271–306.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00206-4)，[-Link-](https://academic.microsoft.com/paper/2104121425)
- 162.Shleifer, Andrei, and Daniel Wolfenzon. 2002. “Investor Protection and Equity Markets.” Journal of Financial Economics 66 (1): 3–27.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00149-6)，[-Link-](https://academic.microsoft.com/paper/2166053178)
- 163.Chen, Joseph, Harrison G. Hong, and Jeremy C. Stein. 2002. “Breadth of Ownership and Stock Returns.” Journal of Financial Economics 66 (2): 171–205.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00223-4)，[-Link-](https://academic.microsoft.com/paper/1978815239)
- 164.Jr., William A. Reese, and Michael S. Weisbach. 2002. “Protection of Minority Shareholder Interests, Cross-Listings in the United States, and Subsequent Equity Offerings.” Journal of Financial Economics 66 (1): 65–104.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00151-4)，[-Link-](https://academic.microsoft.com/paper/2096903686)
- 165.Johnson, Simon, and Todd Mitton. 2003. “Cronyism and Capital Controls: Evidence from Malaysia☆.” Journal of Financial Economics 67 (2): 351–82.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00255-6)，[-Link-](https://academic.microsoft.com/paper/2120822293)
- 166.Frank, Murray Z., and Vidhan K. Goyal. 2003. “Testing the Pecking Order Theory of Capital Structure.” Journal of Financial Economics 67 (2): 217–48.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(02)00252-0)，[-Link-](https://academic.microsoft.com/paper/2169891939)
- 167.Anderson, Ronald C., Sattar A. Mansi, and David M. Reeb. 2003. “Founding Family Ownership and the Agency Cost of Debt.” Journal of Financial Economics 68 (2): 263–85.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00067-9)，[-Link-](https://academic.microsoft.com/paper/2167298310)
- 168.Nenova, Tatiana. 2003. “The Value of Corporate Voting Rights and Control: A Cross-Country Analysis.” Journal of Financial Economics 68 (3): 325–51.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00069-2)，[-Link-](https://academic.microsoft.com/paper/2127839771)
- 169.Goyenko, Ruslan Y., Craig W. Holden, and Charles A. Trzcinka. 2009. “Do Liquidity Measures Measure Liquidity.” Journal of Financial Economics 92 (2): 153–81.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2008.06.002)，[-Link-](https://academic.microsoft.com/paper/2039682891)
- 170.Barberis Nicholas and Shleifer Andrei. 2003. “Style investing.” Journal of Financial Economics 68(2): 161–99.
[-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00064-3)，[-Link:Bing学术界面-](http://www.sciencedirect.com/science/article/pii/S0304405X03000643)
- 171.Joh, Sung Wook. 2003. “Corporate Governance and Firm Profitability: Evidence from Korea before the Economic Crisis.” Journal of Financial Economics 68 (2): 287–322.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00068-0)，[-Link-](https://academic.microsoft.com/paper/2056549006)
- 172.Leuz, Christian, Dhananjay Nanda, and Peter D. Wysocki. 2003. “Earnings Management and Investor Protection: An International Comparison.” Journal of Financial Economics 69 (3): 505–27.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00121-1)，[-Link-](https://academic.microsoft.com/paper/2169837563)
> 上面这篇出版自 2003 年的文献年均被引用了 43.7 次。该文考察了31个国家盈余管理的系统性差异，并提出了对这些差异的解释，表明公司治理与报告盈余质量之间存在内在联系。

- 173.Rajan, Raghuram G., and Luigi Zingales. 2003. “The Great Reversals: The Politics of Financial Development in the 20th Century.” Journal of Financial Economics 69 (1): 5–50.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00125-9)，[-Link-](https://academic.microsoft.com/paper/2158386584)
> 上面这篇出版自 2003 年的文献年均被引用了 36.6 次。该文为了解释金融发展的跨国差异，提出了金融发展的“利益集团”理论：在位者反对金融发展，因为它会滋生竞争。这解释了为什么金融业不是在所有国家总体向前发展的。

- 174.Shleifer, Andrei, and Robert W. Vishny. 2003. “Stock Market Driven Acquisitions.” Journal of Financial Economics 70 (3): 295–311.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00211-3)，[-Link-](https://academic.microsoft.com/paper/2156900158)

- 175.Stulz, René M., and Rohan Williamson. 2003. “Culture, Openness, and Finance.” Journal of Financial Economics 70 (3): 313–49.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00173-9)，[-Link-](https://academic.microsoft.com/paper/1489759298)
- 176.Sun, Qian, and Wilson H.S Tong. 2003. “China Share Issue Privatization: The Extent of Its Success.” Journal of Financial Economics 70 (2): 183–222.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00145-4)，[-Link-](https://academic.microsoft.com/paper/2081106815)
- 177.Beck, Thorsten, Asli Demirgüç-Kunt, and Ross Levine. 2003. “Law, Endowments, and Finance.” Journal of Financial Economics 70 (2): 137–81.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00144-2)，[-Link-](https://academic.microsoft.com/paper/2032265894)
- 178.Doidge, Craig, G.Andrew Karolyi, and René M Stulz. 2004. “Why Are Foreign Firms Listed in the U.S. Worth More.” Journal of Financial Economics 71 (2): 205–38.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00183-1)，[-Link-](https://academic.microsoft.com/paper/2082471173)
- 179.Baek, Jae-Seung, Jun-Koo Kang, and Kyung Suh Park. 2004. “Corporate Governance and Firm Value: Evidence from the Korean Financial Crisis.” Journal of Financial Economics 71 (2): 265–313.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00167-3)，[-Link-](https://academic.microsoft.com/paper/1991223823)
- 180.Sapienza, Paola. 2004. “The Effects of Government Ownership on Bank Lending.” Journal of Financial Economics 72 (2): 357–84.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2002.10.002)，[-Link-](https://academic.microsoft.com/paper/2156471341)
- 181.Doidge, Craig. 2004. “U.S. Cross-Listings and the Private Benefits of Control: Evidence from Dual-Class Firms.” Journal of Financial Economics 72 (3): 519–53.
 [-PDF-](http://sci-hub.ren/10.1016/S0304-405X(03)00208-3)，[-Link-](https://academic.microsoft.com/paper/1991403074)
- 182.Moeller, Sara B, Frederik P Schlingemann, and René M Stulz. 2004. “Firm Size and the Gains from Acquisitions.” Journal of Financial Economics 73 (2): 201–28.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2003.07.002)，[-Link-](https://academic.microsoft.com/paper/2023006733)
- 183.Fama, Eugene F., and Kenneth R. French. 2004. “New Lists: Fundamentals and Survival Rates.” Journal of Financial Economics 73 (2): 229–69.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2003.04.001)，[-Link-](https://academic.microsoft.com/paper/1781020828)
- 184.Getmansky, Mila, Andrew W. Lo, and Igor Makarov. 2004. “An Econometric Model of Serial Correlation and Illiquidity in Hedge Fund Returns.” Journal of Financial Economics 74 (3): 529–609.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.04.001)，[-Link-](https://academic.microsoft.com/paper/2163980978)
- 185.Rossi, Stefano, and Paolo F. Volpin. 2004. “Cross-Country Determinants of Mergers and Acquisitions.” Journal of Financial Economics 74 (2): 277–304.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2003.10.001)，[-Link-](https://academic.microsoft.com/paper/2169352117)
- 186.Lewellen, Jonathan. 2004. “Predicting Returns with Financial Ratios.” Journal of Financial Economics 74 (2): 209–35.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.309559)，[-Link-](https://academic.microsoft.com/paper/2148709680)
- 187.Huson, Mark R., Paul H. Malatesta, and Robert Parrino. 2004. “Managerial Succession and Firm Performance.” Journal of Financial Economics 74 (2): 237–75.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2003.08.002)，[-Link-](https://academic.microsoft.com/paper/2080453520)
- 188.Asquith, Paul, Michael B. Mikhail, and Andrea S. Au. 2005. “Information Content of Equity Analyst Reports.” Journal of Financial Economics 75 (2): 245–82.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.332662)，[-Link-](https://academic.microsoft.com/paper/2077567135)
- 189.Cochrane, John H. 2005. “The Risk and Return of Venture Capital.” Journal of Financial Economics 75 (1): 3–52.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.03.006)，[-Link-](https://academic.microsoft.com/paper/1991778769)
- 190.Siegel, Jordan. 2005. “Can Foreign Firms Bond Themselves Effectively by . . Renting U.S. Securities Laws.” Journal of Financial Economics 75 (2): 319–59.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.02.001)，[-Link-](https://academic.microsoft.com/paper/1986820532)
- 191.Nicholas Barberis, Andrei Shleifer and Jeffrey Wurgler. 2005. “Comovement.” Journal of Financial Economics 75 (2): 283–317.
 [-PDF-](http://sci-hub.ren/10.1016/j.jfineco.2004.04.003)，[-Link:Bing学术界面-](http://www.sciencedirect.com/science/article/pii/S0304405X04001308)
- 192.Berger, Allen N., Nathan H. Miller, Mitchell A. Petersen, Raghuram G. Rajan, and Jeremy C. Stein. 2005. “Does Function Follow Organizational Form? Evidence from the Lending Practices of Large and Small Banks.” Journal of Financial Economics 76 (2): 237–69.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.06.003)，[-Link-](https://academic.microsoft.com/paper/2148820661)
- 193.Ghysels, Eric, Pedro Santa-Clara, and Rossen I. Valkanov. 2005. “There Is a Risk-Return Tradeoff after All.” Journal of Financial Economics 76 (3): 509–48.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.03.008)，[-Link-](https://academic.microsoft.com/paper/2160183711)
- 194.Fama, Eugene F., and Kenneth R. French. 2005. “Financing Decisions: Who Issues Stock?” Journal of Financial Economics 76 (3): 549–82.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.429640)，[-Link-](https://academic.microsoft.com/paper/2128968861)
- 195.Allen, Franklin, Jun “Qj” Qian, and Meijun Qian. 2005. “Law, Finance, and Economic Growth in China.” Journal of Financial Economics 77 (1): 57–116.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.06.010)，[-Link-](https://academic.microsoft.com/paper/1964052724)
- 196.Acharya, Viral V., and Lasse Heje Pedersen. 2005. “Asset Pricing with Liquidity Risk.” Journal of Financial Economics 77 (2): 375–410.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.366300)，[-Link-](https://academic.microsoft.com/paper/2123272754)
- 197.Bekaert, Geert, Campbell R. Harvey, and Christian T. Lundblad. 2005. “Does Financial Liberalization Spur Growth.” Journal of Financial Economics 77 (1): 3–55.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.264818)，[-Link-](https://academic.microsoft.com/paper/2144493137)
- 198.Brav, Alon, John R. Graham, Campbell R. Harvey, and Roni Michaely. 2005. “Payout Policy in the 21st Century.” Journal of Financial Economics 77 (3): 483–527.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.571046)，[-Link-](https://academic.microsoft.com/paper/1983888044)
- 199.Rhodes-Kropf, Matthew, David T. Robinson, and S. Viswanathan. 2005. “Valuation Waves and Merger Activity: The Empirical Evidence ☆.” Journal of Financial Economics 77 (3): 561–603.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.06.015)，[-Link-](https://academic.microsoft.com/paper/2016668951)
- 200.Harford, Jarrad. 2005. “What Drives Merger Waves.” Journal of Financial Economics 77 (3): 529–60.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.05.004)，[-Link-](https://academic.microsoft.com/paper/2069223897)
- 201.Cull, Robert, and Lixin Colin Xu. 2005. “Institutions, Ownership, and Finance: The Determinants of Profit Reinvestment Among Chinese Firms.” Journal of Financial Economics 77 (1): 117–46.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.550504)，[-Link-](https://academic.microsoft.com/paper/2152698625)
- 202.Bebchuk, Lucian Arye, and Alma Cohen. 2005. “The Costs of Entrenched Boards.” Journal of Financial Economics 78 (2): 409–33.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.556987)，[-Link-](https://academic.microsoft.com/paper/2131120940)
- 203.Asquith, Paul, Parag A. Pathak, and Jay R. Ritter. 2005. “Short Interest, Institutional Ownership, and Stock Returns.” Journal of Financial Economics 78 (2): 243–76.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.525623)，[-Link-](https://academic.microsoft.com/paper/2058776456)
- 204.Nagel, Stefan. 2005. “Short Sales, Institutional Investors and the Cross-Section of Stock Returns.” Journal of Financial Economics 78 (2): 277–309.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.08.008)，[-Link-](https://academic.microsoft.com/paper/2067814853)
- 205.Coles, Jeffrey L., Naveen D. Daniel, and Lalitha Naveen. 2006. “Managerial Incentives and Risk-Taking.” Journal of Financial Economics 79 (2): 431–68.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.09.004)，[-Link-](https://academic.microsoft.com/paper/2004463963)
- 206.Burns, Natasha, and Simi Kedia. 2006. “The Impact of Performance-Based Compensation on Misreporting.” Journal of Financial Economics 79 (1): 35–67.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.12.003)，[-Link-](https://academic.microsoft.com/paper/1966260228)
- 207.Flannery, Mark J., and Kasturi P. Rangan. 2006. “Partial Adjustment Toward Target Capital Structures.” Journal of Financial Economics 79 (3): 469–506.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.467941)，[-Link-](https://academic.microsoft.com/paper/2017064883)
- 208.Bandi, Federico M., and Jeffrey R. Russell. 2006. “Separating Microstructure Noise from Volatility.” Journal of Financial Economics 79 (3): 655–92.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.642323)，[-Link-](https://academic.microsoft.com/paper/2041865734)
- 209.Jin, Li, and Stewart C. Myers. 2006. “R2 around the World: New Theory and New Tests.” Journal of Financial Economics 79 (2): 257–92.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.11.003)，[-Link-](https://academic.microsoft.com/paper/1976162410)
- 210.Villalonga, Belen, and Raphael ('Raffi") H. Amit. 2006. “How Do Family Ownership, Control, and Management Affect Firm Value?” Journal of Financial Economics 80 (2): 385–417.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.12.005)，[-Link-](https://academic.microsoft.com/paper/2024822924)
> 上面这篇出版自 2006 年的文献年均被引用了 48.2 次。该文研究了家族企业与非家族企业的代理问题，发现当家族企业由家族族长控制时，其代理问题更小；由后代控制时，其代理问题更大。

- 211.Bergstresser, Daniel, and Thomas Philippon. 2006. “CEO Incentives and Earnings Management.” Journal of Financial Economics 80 (3): 511–29.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2004.10.011)，[-Link-](https://academic.microsoft.com/paper/2088039507)

- 212.Sadka, Ronnie. 2006. “Momentum and Post-Earnings-Announcement Drift Anomalies: The Role of Liquidity Risk.” Journal of Financial Economics 80 (2): 309–49.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.428160)，[-Link-](https://academic.microsoft.com/paper/1964613181)
- 213.Campbell, John Y., and Motohiro Yogo. 2006. “Efficient Tests of Stock Return Predictability.” Journal of Financial Economics 81 (1): 27–60.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2005.05.008)，[-Link-](https://academic.microsoft.com/paper/2104839685)
- 214.DeAngelo, Harry, Linda DeAngelo, and René M. Stulz. 2006. “Dividend Policy and the Earned/Contributed Capital Mix: A Test of the Life-Cycle Theory.” Journal of Financial Economics 81 (2): 227–54.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2005.07.005)，[-Link-](https://academic.microsoft.com/paper/2171962194)
- 215.Klapper, Leora F., Luc Laeven, and Raghuram G. Rajan. 2006. “Entry Regulation as a Barrier to Entrepreneurship.” Journal of Financial Economics 82 (3): 591–629.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2005.09.006)，[-Link-](https://academic.microsoft.com/paper/2116184502)
- 216.Lewellen, Jonathan, and Stefan Nagel. 2006. “The Conditional CAPM Does Not Explain Asset-Pricing Anomalies.” Journal of Financial Economics 82 (2): 289–314.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2005.05.012)，[-Link-](https://academic.microsoft.com/paper/2125898654)
- 217.Dittmar, Amy K., and Jan Mahrt-Smith. 2007. “Corporate Governance and the Value of Cash Holdings.” Journal of Financial Economics 83 (3): 599–634.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.687464)，[-Link-](https://academic.microsoft.com/paper/2051930701)
- 218.Duffie, Darrell, Leandro Saita, and Ke Wang. 2007. “Multi-Period Corporate Default Prediction with Stochastic Covariates☆.” Journal of Financial Economics 83 (3): 635–65.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.647862)，[-Link-](https://academic.microsoft.com/paper/2125036751)
- 219.Kayhan, Ayla, and Sheridan Titman. 2007. “Firms’ Histories and Their Capital Structures.” Journal of Financial Economics 83 (1): 1–32.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2005.10.007)，[-Link-](https://academic.microsoft.com/paper/2088026236)
- 220.Heron, Randall A., and Erik Lie. 2007. “Does Backdating Explain the Stock Price Pattern Around Executive Stock Option Grants.” Journal of Financial Economics 83 (2): 271–95.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2005.12.003)，[-Link-](https://academic.microsoft.com/paper/2001727087)
- 221.Piazzesi, Monika, Martin Schneider, and Selale Tuzel. 2007. “Housing, Consumption, and Asset Pricing.” Journal of Financial Economics 83 (3): 531–69.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.01.006)，[-Link-](https://academic.microsoft.com/paper/1970035215)
- 222.Djankov, Simeon, Caralee McLiesh, and Andrei Shleifer. 2007. “Private Credit in 129 Countries.” Journal of Financial Economics 84 (2): 299–329.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.03.004)，[-Link-](https://academic.microsoft.com/paper/2069985282)
> 上面这篇出版自 2007 年的文献年均被引用了 34 次。该文利用129个国家的法定债权和私人和公共信贷登记的数据，研究发现了私人信贷的跨国决定因素，证明了公共信贷登记处制度有利于发展中国家的私人信贷市场发展。

- 223.Fan, Joseph P.H., T.J. Wong, and Tianyu Zhang. 2007. “Politically Connected CEOs, Corporate Governance, and Post-IPO Performance of China’s Newly Partially Privatized Firms.” Journal of Financial Economics 84 (2): 330–57.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.03.008)，[-Link-](https://academic.microsoft.com/paper/3023160244)

- 224.Acharya, Viral V., and Timothy C. Johnson. 2007. “Insider Trading in Credit Derivatives.” Journal of Financial Economics 84 (1): 110–41.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.05.003)，[-Link-](https://academic.microsoft.com/paper/1998697275)
- 225.Boone, Audra L., Laura Casares Field, Jonathan M. Karpoff, and Charu G. Raheja. 2007. “The Determinants of Corporate Board Size and Composition: An Empirical Analysis.” Journal of Financial Economics 85 (1): 66–101.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.605762)，[-Link-](https://academic.microsoft.com/paper/2166814197)
- 226.Efendi, Jap, Anup Srivastava, and Edward P. Swanson. 2007. “Why Do Corporate Managers Misstate Financial Statements? The Role of Option Compensation and Other Factors.” Journal of Financial Economics 85 (3): 667–708.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.05.009)，[-Link-](https://academic.microsoft.com/paper/1510736945)
- 227.Laeven, Luc, and Ross Levine. 2007. “Is There a Diversification Discount in Financial Conglomerates.” Journal of Financial Economics 85 (2): 331–67.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2005.06.001)，[-Link-](https://academic.microsoft.com/paper/2058877415)
- 228.Acharya, Viral V., Sreedhar T. Bharath, and Anand Srinivasan. 2007. “Does Industry-Wide Distress Affect Defaulted Firms? Evidence from Creditor Recoveries.” Journal of Financial Economics 85 (3): 787–821.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.05.011)，[-Link-](https://academic.microsoft.com/paper/1974169858)
- 229.Doidge, Craig, G. Andrew Karolyi, and René M. Stulz. 2007. “Why Do Countries Matter so Much for Corporate Governance.” Journal of Financial Economics 86 (1): 1–39.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.09.002)，[-Link-](https://academic.microsoft.com/paper/2111392627)
- 230.Chen, Xia, Jarrad Harford, and Kai Li. 2007. “Monitoring: Which Institutions Matter?” Journal of Financial Economics 86 (2): 279–305.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.09.005)，[-Link-](https://academic.microsoft.com/paper/2044161527)
- 231.Coval, Joshua D., and Erik Stafford. 2007. “Asset Fire Sales (and Purchases) in Equity Markets.” Journal of Financial Economics 86 (2): 479–512.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.718201)，[-Link-](https://academic.microsoft.com/paper/1975616424)
- 232.Coles, Jeffrey L., Naveen D. Daniel, and Lalitha Naveen. 2008. “Boards: Does One Size Fit All?” Journal of Financial Economics 87 (2): 329–56.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.08.008)，[-Link-](https://academic.microsoft.com/paper/2055662098)
> 上面这篇出版自 2008 年的文献年均被引用了 31 次。该文重新审视了公司价值与董事会结构的关系以及董事会结构中与横截面变化相关的因素。总结发现对以往的“小董事会更有效”结论并不适用于所有公司和一切情景，董事会规模对公司价值的影响需要具体分析。
- 233.Linck, James S., Jeffry M. Netter, and Tina Yang. 2008. “The Determinants of Board Structure.” Journal of Financial Economics 87 (2): 308–28.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2007.03.004)，[-Link-](https://academic.microsoft.com/paper/2054294817)

- 234.Harford, Jarrad, Sattar A. Mansi, and William F. Maxwell. 2008. “Corporate Governance and Firm Cash Holdings in the US.” Journal of Financial Economics 87 (3): 535–55.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2007.04.002)，[-Link-](https://academic.microsoft.com/paper/2066777296)
- 235.Korajczyk, Robert A., and Ronnie Sadka. 2008. “Pricing the Commonality across Alternative Measures of Liquidity.” Journal of Financial Economics 87 (1): 45–72.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.12.003)，[-Link-](https://academic.microsoft.com/paper/1965152622)
- 236.Ferreira, Miguel A., and Pedro P. Matos. 2008. “The Colors of Investors’ Money: The Role of Institutional Investors around the World.” Journal of Financial Economics 88 (3): 499–533.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.885777)，[-Link-](https://academic.microsoft.com/paper/2041934684)
- 237.Claessens, Stijn, Erik Feijen, and Luc Laeven. 2008. “Political Connections and Preferential Access to Finance: The Role of Campaign Contributions.” Journal of Financial Economics 88 (3): 554–80.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2006.11.003)，[-Link-](https://academic.microsoft.com/paper/2136647651)
- 238.Frazzini, Andrea, and Owen A. Lamont. 2008. “Dumb Money: Mutual Fund Flows and the Cross-Section of Stock Returns.” Journal of Financial Economics 88 (2): 299–322.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2007.07.001)，[-Link-](https://academic.microsoft.com/paper/2061978002)
- 239.Karpoff, Jonathan M., D. Scott Lee, and Gerald S. Martin. 2008. “The Consequences to Managers for Financial Misrepresentation.” Journal of Financial Economics 88 (2): 193–215.
 [-PDF-](http://sci-hub.ren/10.1007/978-1-4614-8097-6_14)，[-Link-](https://academic.microsoft.com/paper/2031861336)
- 240.Malmendier, Ulrike, and Geoffrey A. Tate. 2008. “Who Makes Acquisitions? CEO Overconfidence and the Market’s Reaction.” Journal of Financial Economics 89 (1): 20–43.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2007.07.002)，[-Link-](https://academic.microsoft.com/paper/104671870)
- 241.Beck, Thorsten, Asli Demirgüç-Kunt, and Vojislav Maksimovic. 2008. “Financing Patterns around the World : Are Small Firms Different?” Journal of Financial Economics 89 (3): 467–87.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2007.10.005)，[-Link-](https://academic.microsoft.com/paper/2115849388)
- 242.Nahata, Rajarishi. 2008. “Venture Capital Reputation and Investment Performance.” Journal of Financial Economics 90 (2): 127–51.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2007.11.008)，[-Link-](https://academic.microsoft.com/paper/2067536600)
- 243.Bizjak, John M., Michael L. Lemmon, and Lalitha Naveen. 2008. “Does the Use of Peer Groups Contribute to Higher Pay and Less Efficient Compensation.” Journal of Financial Economics 90 (2): 152–68.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2007.08.007)，[-Link-](https://academic.microsoft.com/paper/1980198021)
- 244.Ang, Andrew, Robert J. Hodrick, Yuhang Xing, and Xiaoyan Zhang. 2009. “High Idiosyncratic Volatility and Low Returns: International and Further U.S. Evidence.” Journal of Financial Economics 91 (1): 1–23.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.1108216)，[-Link-](https://academic.microsoft.com/paper/1983727023)
- 245.Fu, Fangjian. 2009. “Idiosyncratic Risk and the Cross-Section of Expected Stock Returns.” Journal of Financial Economics 91 (1): 24–37.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2008.02.003)，[-Link-](https://academic.microsoft.com/paper/2042941394)
- 246.Doidge, Craig, G. Andrew Karolyi, and René M. Stulz. 2009. “Has New York Become Less Competitive than London in Global Markets? Evaluating Foreign Listing Choices over Time.” Journal of Financial Economics 91 (3): 253–77.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2008.02.010)，[-Link-](https://academic.microsoft.com/paper/2034033306)
- 247.Duarte, Jefferson, and Lance A. Young. 2008. “Why Is PIN Priced.” Journal of Financial Economics 91 (2): 119–38.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.971197)，[-Link-](https://academic.microsoft.com/paper/2010711832)
- 248.Laeven, Luc, and Ross Levine. 2009. “Bank Governance, Regulation, and Risk Taking.” Journal of Financial Economics 93 (2): 259–75.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.1142967)，[-Link-](https://academic.microsoft.com/paper/2016441547)
- 249.Hong, Harrison, and Marcin Kacperczyk. 2009. “The Price of Sin: The Effects of Social Norms on Markets.” Journal of Financial Economics 93 (1): 15–36.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.766465)，[-Link-](https://academic.microsoft.com/paper/2015152948)
- 250.Hail, Luzi, and Christian Leuz. 2009. “Cost of Capital Effects and Changes in Growth Expectations Around U.S. Cross-Listings.” Journal of Financial Economics 93 (3): 428–54.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2008.09.006)，[-Link-](https://academic.microsoft.com/paper/2086825613)
- 251.Hwang, Byoung-Hyoun, and Seoyoung Kim. 2009. “It Pays to Have Friends.” Journal of Financial Economics 93 (1): 138–58.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2008.07.005)，[-Link-](https://academic.microsoft.com/paper/2096242048)
- 252.Adams, Renée B., and Daniel Ferreira. 2009. “Women in the Boardroom and Their Impact on Governance and Performance.” Journal of Financial Economics 94 (2): 291–309.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2008.10.007)，[-Link-](https://academic.microsoft.com/paper/2108113779)
- 253.Hutton, Amy P., Alan J. Marcus, and Hassan Tehranian. 2009. “Opaque Financial Reports, R2, and Crash Risk.” Journal of Financial Economics 94 (1): 67–86.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2008.10.003)，[-Link-](https://academic.microsoft.com/paper/1966586504)
- 254.Chen, Shuping, Xia Chen, Qiang Cheng, and Terry J. Shevlin. 2010. “Are Family Firms More Tax Aggressive Than Non-Family Firms?” Journal of Financial Economics 95 (1): 41–61.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2009.02.003)，[-Link-](https://academic.microsoft.com/paper/1982518833)
- 255.Giroud, Xavier, and Holger M. Mueller. 2010. “Does Corporate Governance Matter in Competitive Industries.” Journal of Financial Economics 95 (3): 312–31.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2009.10.008)，[-Link-](https://academic.microsoft.com/paper/2112857910)
- 256.Lewellen, Jonathan, Stefan Nagel, and Jay A. Shanken. 2010. “A Skeptical Appraisal of Asset Pricing Tests.” Journal of Financial Economics 96 (2): 175–94.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2009.09.001)，[-Link-](https://academic.microsoft.com/paper/1970764861)
- 257.Duchin, Ran, John G. Matsusaka, and Oguzhan Ozbas. 2010. “When Are Outside Directors Effective.” Journal of Financial Economics 96 (2): 195–214.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2009.12.004)，[-Link-](https://academic.microsoft.com/paper/2156311802)
- 258.Ivashina, Victoria, and David S. Scharfstein. 2010. “Bank Lending During the Financial Crisis of 2008.” Journal of Financial Economics 97 (3): 319–38.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.1297337)，[-Link-](https://academic.microsoft.com/paper/2001773843)
- 259.Campello, Murillo, John R. Graham, and Campbell R. Harvey. 2010. “The Real Effects of Financial Constraints: Evidence from a Financial Crisis.” Journal of Financial Economics 97 (3): 470–87.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.1357805)，[-Link-](https://academic.microsoft.com/paper/2135633464)
- 260.Longstaff, Francis A. 2010. “The Subprime Credit Crisis and Contagion in Financial Markets.” Journal of Financial Economics 97 (3): 436–50.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2010.01.002)，[-Link-](https://academic.microsoft.com/paper/1969418588)
- 261.Duchin, Ran, Oguzhan Ozbas, and Berk A. Sensoy. 2010. “Costly External Finance, Corporate Investment, and the Subprime Mortgage Credit Crisis.” Journal of Financial Economics 97 (3): 418–35.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2009.12.008)，[-Link-](https://academic.microsoft.com/paper/2008096660)
- 262.Jiang, Guohua, Charles M.C. Lee, and Heng Yue. 2010. “Tunneling through Intercorporate Loans: The China Experience☆.” Journal of Financial Economics 98 (1): 1–20.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2010.05.002)，[-Link-](https://academic.microsoft.com/paper/2113942631)
- 263.Demirgüç-Kunt, Asli, and Harry Huizinga. 2009. “Bank Activity and Funding Strategies: The Impact on Risk and Returns.” Journal of Financial Economics 98 (3): 626–50.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.1336474)，[-Link-](https://academic.microsoft.com/paper/2014914289)
- 264.Thompson, Samuel Brodsky. 2011. “Simple Formulas for Standard Errors That Cluster by Both Firm and Time.” Journal of Financial Economics 99 (1): 1–10.
 [-PDF-](http://sci-hub.ren/10.2139/SSRN.914002)，[-Link-](https://academic.microsoft.com/paper/2151747420)
- 265.Fahlenbrach, Ruediger, and Rene M. Stulz. 2011. “Bank CEO Incentives and the Credit Crisis.” Journal of Financial Economics 99 (1): 11–26.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2010.08.010)，[-Link-](https://academic.microsoft.com/paper/2157489850)
- 266.Aggarwal, Reena, Isil Erel, Miguel A. Ferreira, and Pedro P. Matos. 2011. “Does Governance Travel Around the World? Evidence from Institutional Investors.” Journal of Financial Economics 100 (1): 154–81.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2010.10.018)，[-Link-](https://academic.microsoft.com/paper/2162756319)
- 267.Lin, Chen, 马跃) Yue Ma, Paul H. Malatesta, and Yuhai Xuan. 2011. “Ownership Structure and the Cost of Corporate Borrowing.” Journal of Financial Economics 100 (1): 1–23.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2010.10.012)，[-Link-](https://academic.microsoft.com/paper/2125728726)
- 268.Rooij, Maarten van, Annamaria Lusardi, and Rob J. M. Alessie. 2011. “Financial Literacy and Stock Market Participation.” Journal of Financial Economics 101 (2): 449–72.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2011.03.006)，[-Link-](https://academic.microsoft.com/paper/1974533200)
- 269.Gary Gorton, and Andrew Metrick. 2012. “Securitized banking and the run on repo.” Journal of Financial Economics 104 (3): 425-51.
 [-PDF-](http://sci-hub.ren/10.1016/j.jfineco.2011.03.016)，[-Link:Bing学术界面-](https://www.sciencedirect.com/science/article/abs/pii/S0304405X1100081X)
- 270.Billio, Monica, Mila Getmansky, Andrew W. Lo, and Loriana Pelizzon. 2012. “Econometric Measures of Connectedness and Systemic Risk in the Finance and Insurance Sectors.” Journal of Financial Economics 104 (3): 535–59.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2011.12.010)，[-Link-](https://academic.microsoft.com/paper/2157996961)
- 271.Gennaioli, Nicola, Andrei Shleifer, and Robert W. Vishny. 2012. “Neglected Risks, Financial Innovation, and Financial Fragility.” Journal of Financial Economics 104 (3): 452–68.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2011.05.005)，[-Link-](https://academic.microsoft.com/paper/2166822681)
- 272.Baker, Malcolm P., Jeffrey Wurgler, and Yu Yuan. 2009. “Global, Local, and Contagious Investor Sentiment.” Journal of Financial Economics 104 (2): 272–87.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2011.11.002)，[-Link-](https://academic.microsoft.com/paper/2118959968)
- 273.Stambaugh, Robert F., Jianfeng Yu, and Yu Yuan. 2012. “The Short of It.” Journal of Financial Economics 104 (2).
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2011.12.001)，[-Link-](https://academic.microsoft.com/paper/2196996750)
- 274.Wintoki, M. Babajide, James S. Linck, and Jeffry M. Netter. 2012. “Endogeneity and the Dynamics of Internal Corporate Governance.” Journal of Financial Economics 105 (3): 581–606.
 [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2011.12.010)，[-Link-](https://academic.microsoft.com/paper/2157996961)




&emsp;