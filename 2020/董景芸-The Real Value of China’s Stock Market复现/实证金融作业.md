# The Real Value of China’s Stock Market

# 论文复现

---

董景芸 中山大学

---



[TOC]

本文将使用 CSMAR 数据库，对 *The Real Value of China’s Stock Market* (Carpenter, Lu, Whitelaw, 2020) 一文的第一个结果表格进行复现。



## 1. 论文介绍

论文主要借鉴了 Bai, Philippon, and Savov (2016) 的做法，构造

$$
(\sqrt{\nu_{FPE}})_{t,k}=b_t*\sigma_t(log(M/A)) \quad \quad \quad (1)
$$

作为第 t 年价格信息含量的衡量。其中，$b_{t}$ 和  $\sigma_t(log(M/A))$ 来自于回归方程，该方程可以以盈利、投资、研发投入等为被影响/被预测的考察。在 table 1 中价格信息有关的变量为盈利现金流，因此回归方程为：

$$
\frac{E_{i,t+k}}{A_{i,t}}=a_t+b_{t}log(\frac{M_{i,t}}{A_{i,t}})+c_t\frac{E_{i,t}}{A_{i,t}}+d^s_t1^s_{i,t}+\epsilon_{i,t+k} \quad \quad \quad (2)
$$

*(注：如果假设投资与价格信息含量有关，则等式左边为  $\frac{I_{i,t+k}}{A_{i,t}}$ )*

这里，E 即 Earning，指公司该年的净利润；

M 即 Market value，指公司该年A股收盘价乘以总股数（包括 A 股、 B 股、H 股和非流通股）；

A 即 Asset book value，指公司该年年报总资产；

$1^s_{i,t}$ 是用于控制行业的因子变量，使用行业代码 B 

( 注：本文复现时发现行业代码 C 效果比较好，代码 B 效果较差，故选择行业代码 C )

table 1 分别使用 k = 3 和 k = 5 进行分年横截面 OLS 回归，得到系数 $b_t$ 并乘以该年的 $\sigma_t(log(M/A))$ 得到估计的信息含量，最后将各年结果画折线图，与当年金融市场大事联系起来。



## 2. Table 1 复现

本文重点对 k = 3 复现进行说明，k = 5 部分年份复现过程见代码。

### 2.1 数据收集

该论文数据来自于国泰安数据库 (CSMAR)，table 1 数据具体来源如下：

净利润：		公司研究系列 — 财务报表 — 利润表 — 净利润；

总资产：		公司研究系列 — 财务报表 — 资产负债表 — 资产总计；

公司总股数：(1999 年及以后) 公司研究系列 — 治理结构 — 股东股本 — 股本结构文件 — 总股数；

​					   (1995 - 1999 年) 股票市场系列 — 股票市场交易 — 基本数据 — 股本变动文件 — 总股数；

股价：			股票市场系列 — 个股交易数据 — 年收盘价

行业：			股票市场系列 — 基本数据 — 公司文件 — 行业代码 C

补充：从国家统计局下载 1994 - 2017 年的 GDP 指数和名义 GDP



### 2.2 变量处理

#### 2.2.1 分表导入数据

1. 对于财务报表中的 Earning 和 Asset ，分解出 `year` 变量，只保留 12 月（年报）的数据，并且只保留合并报表的数据

   以净利润为例：

```{Stata}
import delimited FS_Comins.csv, clear

rename ïstkcd stkcd
label variable stkcd "Stock Code"
label variable typrep "Type of Report"
drop if typrep=="B" // use the consolidated income statement
rename b002000000 earning
label variable earning "Net Profit" 

split accper, parse(-)
rename accper1 year
rename accper2 month
drop accper3 // split the accounting period into year and month
destring year, replace
destring month, replace
drop if month!=12 // keep the annual report data
isid stkcd year // check if any duplicates
drop month

save income_statement.dta,replace 
```

2. 对于市值，导入收盘价 `yclsprc`

```{Stata}
import delimited TRD_Year.csv,clear

rename ïstkcd stkcd
label variable stkcd "Stock Code"

rename trdynt year

label variable yclsprc "Year Closing Price"

isid stkcd year
save market_value.dta,replace
```

​		导入总股数。 ”治理结构“ 部分的股本结构文件为每年年底报告：

```{Stata}
import delimited CG_Capchg.csv, encoding(UTF-8) clear

label variable stkcd "Stock Code"
label variable nshrttl "Number of Total Shares"

split reptdt, parse(-)
rename reptdt1 year
drop reptdt2 reptdt3
destring year, replace
isid stkcd year

save capital_structure.dta,replace
```

​		股票市场交易系列中的股本变动文件为不定时间报告，因此保留该年最晚时刻的报告：

```{Stata}
import delimited TRD_Capchg.csv, encoding(UTF-8) clear

rename nshrttl nshrttl2
rename nshrstt nshrstt2
rename nshra nshra2
rename nshrb nshrb2
rename nshrh nshrh2
label variable stkcd "Stock Code"
label variable nshrttl2 "Number of Total Shares"

split shrchgdt, parse(-)
rename shrchgdt1 year
rename shrchgdt2 month
rename shrchgdt3 date
destring year, replace
destring month, replace
destring date, replace

bysort year stkcd: egen max_month=max(month)
drop if month<max_month // keep data in the last month
bysort year stkcd: egen max_date=max(date)
drop if date<max_date // keep data in the last date if a month has more than one record

isid stkcd year
drop month
drop date
save capstru_change.dta,replace
```

3. 行业

```{Stata}
import delimited CG_Co.csv, encoding(UTF-8) clear

label variable stkcd "Stock Code"
/*
Here we use code C instead of CODE B in the article to indentify the industry,
and we don't subdivide manufacturing(C)
P7. we construct a version of the one-digit SIC classification from CSMAR’s 
industrial code B
*/
gen industry00=substr(nnindcd,1,1)
encode industry00, gen (industry2)
label variable industry2 "Industry with codeC manufacturing as a whole"
label define indu2 1 "农林牧渔" 2 "采矿业" 3 "制造业" ///
4 "电热气水" 5 "建筑业" 6 "批发零售" 7 "交通运输仓储" 8 "住宿餐饮" ///
9 "信息技术" 10 "金融业" 11 "房地产" 12 "租赁商务" 13 "科学研究" ///
14 "水环公设" 15 "居民服务" 16 "教育" 17 "卫生社会" 18 "文体娱" /// code C
19 "综合"
label val industry2 indu2

drop industry00 
save industry.dta, replace
```

4. GDP

在 Excel 中，用 t-1 年的名义 GDP * t 年的 GDP 指数 (上年 = 100) 得到 t 年的实际 GDP，再得到 t 年的 GDP 平减指数 (上年 = 100)。导入Stata



#### 2.2.2 合并报表

```{Stata}
use income_statement.dta,clear //E
merge 1:1 stkcd year using "balance_sheet.dta", nogen //A
merge 1:1 stkcd year using "market_value.dta" //, nogenM
merge 1:1 stkcd year using "capital_structure.dta", nogen
merge 1:1 stkcd year using "capstru_change.dta",nogen 
merge m:1 stkcd using "industry.dta", nogen
merge m:1 year using "gdp_deflator.dta", nogen
```



#### 2.2.3 处理数据（通用方法）

由于每一年回归使用的样本不同（公司不同），故在不同年份处理数据的方式略有不同。本节首先给出各年份共通的处理方法（合并报表后）

1. 按照论文使用的时间范围，只保留 1995 到 2016 年间的数据（拉长时间间隔会导致结果不一致）

   生成 `mv_cal` 表示由 A 股年收盘价 * 年报总股数得到的市值，并生成 `mv_cal2` 表示 A 股年收盘价 * 股本变动报告的总股数的市值，生成第二个变量是因为 1999 年之前前一个总股数缺失。

   生成参与回归的变量，`log_ma` , `ea3` , `ea`

```{Stata}
xtset stkcd year
drop if year>2016|year<1995

gen mv_cal=nshrttl*yclsprc
label var mv_cal "Market Value Calculated from Number of Total Shares(including a b h nontrable) * Closing Price"
gen mv_cal2=nshrttl2*yclsprc
replace mv_cal=mv_cal2 if mv_cal==. //the nshrttl is all missing in 1995

gen log_ma=log(mv_cal/asset)
gen ea3=F3.earning/asset
gen ea=earning/asset
```

2. 依照论文 P7 要求 "we deflate all nominal quantities by the GDP deflator" ，用 GDP 平减指数对变量进行平减

```{Stata}
replace ea3=ea3/F3.gdp_deflator*100
replace ea=ea/gdp_deflator*100
replace log_ma=log_ma/gdp_deflator*100
```

3. 依照论文 P8 要求 "We also eliminate financial firms from the sample, although this makes little difference to the results"，删去行业属于金融的公司

   根据复现结果，删去前文合并多出的公司，删去 B 股公司，删去净利润与资产比例异常的公司（ea>2 或 ea<-10）

```{Stata}
drop if _merge==2
drop if ea>2
drop if ea<-10
drop if stktype=="B" // it plays the main role in the reduction of Var[log(M/A)]
drop if industry2==10
```

4. 依照论文 P7 要求 "We winsorize all variables at the 1st and 99th percentiles."，在变量 1% 水平缩尾。这里，缩尾可能分年进行，也可能全部样本一起缩尾。

   逐年：

```{Stata}
forvalues k=1995/2013{
winsor2 ea3 if year==`k', replace cuts(1 99)
winsor2 ea if year==`k', replace cuts(1 99)
winsor2 log_ma if year==`k', replace cuts(1 99)
}
```

​		全部样本一起缩尾：

```{Stata}
winsor2 ea3 , replace cuts(1 99)
winsor2 ea , replace cuts(1 99)
winsor2 log_ma, replace cuts(1 99)
```

5. 分年生成 $\sigma_t(log(M/A))$ 。实际上，这一步骤虽然通常发生在上述处理完成后，但有时也可能提前进行

```{Stata}
capture drop sigma_log_ma
bysort year: egen sigma_log_ma=sd(log_ma)
```



#### 2.2.4 部分年份进行的改动

1. 合并报表后，由于一部分总股数来自于股本变动文件，而该文件只在股本变动时报告，一些年份有数据缺失，因此在 1997 - 2002 年的数据处理前使用邻近年份填补，具体如下 (n+1 可继续重复直至无缺失)：

```{Stata}
bysort stkcd: replace nshrttl2=nshrttl2[_n-1] if nshrttl2==.
bysort stkcd: replace nshrttl2=nshrttl2[_n+1] if nshrttl2==.
bysort stkcd: replace nshrttl2=nshrttl2[_n+1] if nshrttl2==.
```

2. 在平减时，论文没有明确说明如何进行平减，因此，如 2006 年使用了同期的 GDP 平减指数，即

```{Stata}
replace ea3=ea3/gdp_deflator*100
```

​		如 2007 年使用了假设分子分母分别用当年 GDP 平减指数平减的方法，即

```{Stata}
replace ea3=ea3/F3.gdp_deflator*gdp_deflator
```

3. 论文中 P9 提到，"In their examination of the size and value effects in China, Liu, Stambaugh, and Yuan (2019) suggest excluding the smallest 30% of firms by market capitalization from the analysis because 83% of reverse mergers in their sample come from these three deciles, and we follow this suggestion." 因此，本次复现对于一些无法重现结果的年份剔除了市值 `mv_cal` 小于 30% 的公司。（这一剔除可能发生在剔除金融等异常值之前，也可能发生在所有剔除之后，选择表现最好的一种情况）

```{Stata}
egen mv_cal_p30=pctile(mv_cal) if year==1997,p(30)
drop if mv_cal<mv_cal_p30 & mv_cal_p30!=.
```

4. 论文中 P10 提到，"Regardless, the absence of a small-firm effect in price informativeness lends additional support to the argument that composition effects, especially those associated with the opening of the Shenzhen SME and ChiNext boards, are not driving our results." 因此，本文对一些无法复现的年份删除了在创业板 (ChiNext) 上市的公司（如 2010 年）。

```{Stata}
tostring stkcd, gen(stkcd0)
replace stkcd0=substr("000000"+stkcd0,-6,6)
gen board0=substr(stkcd0,1,3)
drop if board0=="300"
```

5. 其他步骤也可能发生调换，如 $sigma_t(log(M/A))$ 正常在缩尾之后，但也可能在之前剔除异常值的某一步之间进行，视效果而定。



### 2.3 回归结果

按照第 t 年的处理方法进行处理后，采用异方差稳定的 OLS 进行分年横截面回归，保留 `b_13` (k = 3 时的 $b_t$ )，`sigma_log_ma` ( $sigma_t(log(M/A))$ )，和它们的乘积 `Pred_var13` ( $b_t \times \sigma_t(log(M/A))$ )

```{Stata}
gen Pred_var13=.
gen b_13=.
forvalues k=1995/2013 {
          qui reg ea3 log_ma ea i.industry2 if year==`k', robust 
		  * egen sigma_log_ma_`k'=sd(log_ma) if e(sample)
		  replace b_13= _b[log_ma] if year==`k'
		  replace Pred_var13= _b[log_ma]*sigma_log_ma if year==`k'
}
xtset stkcd year
```

将表中第 t 年的结果保存在 Excel 中，然后开始下一年的数据处理和回归

最终结果如下表：

|      | pred_var | author_pred | b      | author_b |
| ---- | -------- | ----------- | ------ | -------- |
| 1995 | 0.0184   | 0.018       | 0.0377 | 0.037    |
| 1996 | 0.0356   | 0.035       | 0.0767 | 0.077    |
| 1997 | 0.0361   | 0.037       | 0.0676 | 0.069    |
| 1998 | 0.0221   | 0.021       | 0.0417 | 0.04     |
| 1999 | 0.0064   | 0.006       | 0.0118 | 0.011    |
| 2000 | 0.0010   | 0.001       | 0.0019 | 0.002    |
| 2001 | 0.0109   | 0.011       | 0.0208 | 0.021    |
| 2002 | 0.0059   | 0.006       | 0.0099 | 0.01     |
| 2003 | 0.0178   | 0.021       | 0.0310 | 0.037    |
| 2004 | 0.0381   | 0.038       | 0.0669 | 0.067    |
| 2005 | 0.0435   | 0.043       | 0.0729 | 0.072    |
| 2006 | 0.0504   | 0.050       | 0.0752 | 0.075    |
| 2007 | 0.0479   | 0.048       | 0.0707 | 0.071    |
| 2008 | 0.0596   | 0.059       | 0.0825 | 0.082    |
| 2009 | 0.0565   | 0.057       | 0.0798 | 0.08     |
| 2010 | 0.0517   | 0.051       | 0.0663 | 0.066    |
| 2011 | 0.0308   | 0.031       | 0.0426 | 0.043    |
| 2012 | 0.0357   | 0.035       | 0.0470 | 0.047    |
| 2013 | 0.0475   | 0.047       | 0.0546 | 0.055    |

可以看到，仅 2003 年无法复现，其他年份系数和预测值结果准确度均在 0.002 之内，效果较好。



### 2.4 k = 5 补充

| b       | author_b |
| ------- | -------- |
| 0.0634  | 0.063    |
| 0.0633  | 0.064    |
| 0.0395  | 0.040    |
| 0.0104  | 0.001    |
| -0.0018 | -0.004   |
| -0.0168 | -0.019   |
| 0.0113  | 0.012    |
| 0.0327  | 0.030    |
| 0.0628  | 0.063    |
| 0.0919  | 0.093    |
| 0.0719  | 0.073    |
| 0.1432  | 0.142    |
| 0.0979  | 0.098    |
| 0.1097  | 0.108    |
| 0.0677  | 0.069    |
| 0.0963  | 0.104    |
| 0.1103  | 0.108    |

可以看到，仅 2010 年无法复现，其他年份系数结果准确度均在 0.003 之内，效果较好。



## 参考文献

[1] Jennifer N. Carpenter, Fangzhou Lu, Robert F. Whitelaw, The Real Value of China’s Stock Market, Journal of Financial Economics (2020), doi: https://doi.org/10.1016/j.jfineco.2020.08.012

[2] Bai, J.;Philippon, T.;Savov, A..Have financial markets become more informative?(Article)[J].Journal of Financial Economics,2016,Vol.122(3): 625-654