# 分位数回归I：边际效应和条件边际效应如何理解
&emsp;
> **作者**：曹钰潇 (中山大学)
> 
>  **E-Mail:** <caoyx25@mail2.sysu.edu.cn> 

「**Source** [原英文文献完整信息](https://friosavila.github.io/playingwithstata/main_qreg1.html)」

<!-- vscode-markdown-toc -->
* 1. [简介](#简介)
* 2. [从线性回归模型开始](#从线性回归模型开始)
* 3. [三种边际效应解释](#三种边际效应解释)
    * 3.1. [个体效应——对“我”来说](#个体效应——对“我”来说)
    * 3.2. [条件效应——对“像我这样的人”来说](#条件效应——对“像我这样的人”来说)
    * 3.3. [无条件效应——对“所有的人”来说](#无条件效应——对“所有的人”来说)
* 4. [总结](#总结)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->
##  1. <a name='简介'></a>简介

**分位数回归（Quantile regression）**
是回归分析的方法之一，最早由 Roger Koenker 和 Gilbert Bassett 于 1978 年提出。自其出现以来，已经发展出了大量细分模型和估计方法。

今天，我们将集中于三种分位数回归模型：条件分位数回归（CQREG）（ Koenker 和 Bassett，1978年），无条件分位数回归（UQREG）（ Firpo Fortin 和 Lemieux，2009年），以及分位数处理效应（QTE）（ Firpo，2007年），它们之间的区别和解释常常让人混淆。(扩展阅读可参照[作者论文](https://osf.io/preprints/socarxiv/znj38/))

那么，它们之间的区别是什么？什么导致了这些区别呢？

正如分位数回归模型主要测量因变量的分布那样，以上三种模型的区别也主要与我们对**因变量的测量类型**有关。接下来，我们将在线性回归模型的基础上，区分**个体边际效应、条件边际效应以及无条件边际效应**，以帮助我们更好地理解QR模型的含义。

##  2. <a name='从线性回归模型开始'></a>从线性回归模型开始
假设你对分析自变量 $x$ 怎样影响因变量 $y$ 感兴趣，同时其他因素 $v$ 也可能会影响$y$，因此你会从这样的模型开始:
$$
y=f(x,v)　(1)
$$
现在让我们做一个强的假设，即这些变量之间的关系可以写成一个线性函数。具体来说，假设 **数据生成过程（DGP）** 为:
$$
y=b_0+b_1*x+b_2*x^2+v*(a_0+a_1*x)　(2)
$$
同时假设误差项 $v$ 完全独立于 $x$ :
$$
E(v|x)=0;Var(v|x)=c
$$
这个“基本模型”比人们习惯于在“入门级”计量经济学书中看到的单变量模型更先进：首先，模型系数是线性的，但与解释变量没有关系。其次，观察到的因子 $x$ 和未观察到的因子 $v$ 相互作用以确定 $y$ 。由于 $v$ 不可观测，我们可以认为这个模型实际上是异方差的。

接着从个体层面重写一下模型：
$$
y_i=b_0+b_1*x_i+b_2*x_i^2+v_i*(a_0+a_1*x_i)　(3)
$$
我们利用 Stata 生成数据:

>注：虽然在后续解释中我们不需设置 $x$ 或 $v$ 的分布，但在下面生成数据时可以做出一些假设以便模拟数据和可视化。
```
* 设定种子，生成250个观察值
clear
set seed 102
set obs 250
* 假设x服从卡方分布，v服从标准正态分布，同时设定前5个观察值以便研究
* 为作图方便，利用缺失值将x限制在一个范围内
gen x = rchi2(3)/2
replace x=. if x>5
replace x=.75 in 1
replace x=2   in 2/3
replace x=4   in 4
replace x=4.5 in 5
gen v = rnormal()*.5
* 模拟数据
gen y = 5 + 3*x -0.5*x^2+ v * (1+x)
* 设置id以便作图
gen id = _n
* 生成数据图形
set scheme s1color
two (scatter y x       , color(navy%20)) /// 
        (scatter y x in 1/5, color(navy%80) mlabel(id) mlabcolor(black)), ///
         ytitle("Outcome y") xtitle("Independent var X") legend(off)
graph export qr_fig1.png
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/分位数回归I_Fig1_生成数据_曹钰潇.png)
上图展现了模拟样本的分布。按照DGP，$x$ 和 $y$ 之间的关系是非线性的，图中可以看出其异方差性。同时为了方便起见，我们标记5个点作为样本中随机观察的例子。

那么，我们该如何解释这个模型呢？

##  3. <a name='三种边际效应解释'></a>三种边际效应解释
###  3.1. <a name='个体效应——对“我”来说'></a>个体效应——对“我”来说
解释的第一种方法是对数据中的每一个人或观测点进行解释。然而，这是一种很少会看到的解释方式，因为它要求我们知道DGP的每一个信息，这不仅包括特征 $x$，还包括不可观察因素 $v$（或者模型是同方差的）。

通过将 $x$ 看做一个连续变量，我们可以使用方程（3）来获得关于 $x$ 的偏导，这样可以得到对**个人 $i$ 来说 $x$ 对 $y$ 的边际影响**：
$$
\frac{\partial y_{i}}{\partial x_{i, 1}}=b_{1}+2 * b_{2} * x_{i}+v_{i} * a_{1}　(4)
$$
这里有几点需要注意：

- 只有当我们能观察到不可观察因素 $v_i$，或者模型是同方差，即 $a_1=0$ 时，才能得到个人边际效应。
- 该效应不是恒定的，它取决于 $x_i$ 和 $v_i$ 。
- 这是一个局部（线性）的 $x$ 对 $y$ **边际影响的近似值**。如果 $x$ 变化很大，这个近似值可能与模型预测的 $y$ 的变化有很大差别。然而，在大多数情况下，这个近似值被认为是足够好的（主要是因为我们不知道更好的方法）。

我们也可以根据 DGP 得到**边际效应的准确值**，如果 $x$ 增加 $dx$ 单位，个人 $i$ 的结果是：
$$
y_{i}^{\prime}=b_{0}+b_{1} *\left(x_{i}+d x\right)+b_{2} *\left(x_{i}+d x\right)^{2}+v_{i} *\left(a_{0}+a_{1} *\left(x_{i}+d x\right)\right)　(5)
$$
接下来，我们假设 $dx=1$，在 Stata 中绘制出展示边际效应的图。
```
** 数据清理
gen dx=1
gen xdx=x+dx
gen xdxr=x+dx+0.1
gen y_dy = 5 + 3*(xdx) -0.5*(xdx)^2+ v * (1+(xdx))
** 画图
two (scatter y    x  in 1/5, mlabel(id) mlabcolor(black) mlabpos(12) color(navy%80)  ) ///
    (scatter y_dy xdx in 1/5,  mlabcolor(black) mlabpos(12) ms(d) color(maroon%80) ) ///
        (pcarrow  y x y_dy xdx in 1/5, color(black%40)) ///
        (rcap y y_dy xdxr in 1/5, color(black%40) ) ///
        (scatter y    x            , color(navy%10)  ) ///
        (scatter y_dy xdx          , color(maroon%10) ms(d) ) , ///
        ytitle("Outcome y") xtitle("Indep var X") xlabels(0/5) ///
        legend(order(1 "f(x,v)" 2 "f(x+dx,v)" 4 "{&Delta}y") col(3))             
graph export qr_fig2.png       
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/分位数回归I_Fig2_个体效应_曹钰潇.png)
如图所示，不同观测点的 $y$ 的变化取决于它们的特征值 $x$ ，而且它们似乎紧跟总体趋势。

然而，注意观察点 2 和 3，我们会发现两者有相同的 $x$ 和不同的 $v$ ，这就导致了它们 $y$ 的变化不同，即一种未观察到的异质性。换句话说，**一个单位 $x$ 的变化对每个人的影响是特定的**。

我们也可以把边际效应的近似值与准确值进行比较：
```
** 近似值：
gen mfx_aprox=3-x+v
label var mfx_aprox "LL approximation"
** 准确值：
gen mfx_exact=(y_dy-y)
label var mfx_exact "Exact effect"
**
list x v mfx_aprox mfx_exact in 1/5, sep(0) 
```
结果如下：
```
     |   x            v    mfx_aprox    mfx_exact |
     |--------------------------------------------|
  1. | .75     .5645621    2.8145621    2.3145621 |
  2. |   2    .14000927    1.1400093    .64000927 |
  3. |   2   -.16848187    .83151813    .33151813 |
  4. |   4     .0549622    -.9450378   -1.4450378 |
  5. | 4.5   -.52402712   -2.0240271   -2.5240271 |
```
对于观察点 1 ，增加 1 单位 $x$ 将使其增加 2.315 个单位。对于观察点 2 和 3 ，同样的变化将使点 2 的结果增加 0.64 个单位，而点 3 的结果只增加 0.33 个单位，这是因为他们有不同的不可观测因素 $v$ 。
###  3.2. <a name='条件效应——对“像我这样的人”来说'></a>条件效应——对“像我这样的人”来说
第一种解释表明，同样的“政策”适用于具有相同观察特征 $x$ 的人，可能会因为未观察到的异质性 $v$ 而产生不同结果。然而，这种方法是没有用的，因为 $v$ 从来没有被观察过。因此，我们无法解释对任何特定个体的影响。

相比之下，第二种解释是更实际的，它不是试图量化 $v$ ，而是**将不可观察因素通过平均化的方法从方程中剔除！** 

首先回顾一下DGP:
$$
y_{i}=b_{0}+b_{1} * x_{i}+b_{2} * x_{i}^{2}+v_{i} *\left(a_{0}+a_{1} * x_{i}\right)
$$
如果我们把期望值的条件 $x_i$ 看做是一个特定的值 $X$ ，可以得到：
$$
E\left(y_{i} \mid x_{i}=X\right)=b_{0}+b_{1} * X+b_{2} * X^{2}+E\left(v_{i} \mid x_{i}=X\right) *\left(a_{0}+a_{1} * X\right)
$$
使用零条件均值假设$E\left(v_{i} \mid x_{i}=X\right)=0$，可以得到我们更为熟悉的表达式：
$$
E\left(y_{i} \mid X\right)=b_{0}+b_{1} * X+b_{2} * X^{2}
$$
> 注：这也就是人们在谈论线性回归模型时，主要关注正确的条件均值模型假设的原因。

此时，如果我们采取与第一种解释相同的方法，可以得到方程（6）相对于 $X$（不是 $x_i$ )的偏导数，以求出一单位 $X$ 的增加对 $y$ 的“平均/预期”影响:
$$
\frac{\partial E\left(y_{i} \mid X\right)}{\partial X}=b_{1}+2 * b_{2} * X　(7)
$$
> 注：方程（7）也可以通过对方程（4）个人边际效应进行平均而得到。

接下来，我们运用 Stata 得到以 $X$ 为条件的 $y$ 的预期均值，以及如果 $X$ 变化一单位，条件均值将如何变化。
```
* E(y|X)
gen yy    = 5 + 3*x -0.5*x^2
* E(y'|(X+dx))
gen yy_dy = 5 + 3*(x+dx) -0.5*(x+dx)^2
* 画图
two (scatter y    x   in 1/5, color(navy%25)  ) ///
    (scatter y_dy xdx in 1/5, ms(d) color(maroon%25) ) ///
        (pcarrow y x y_dy xdx in 1/5, color(black%40)) ///
        (function 5 + 3*x -0.5*x^2 , range(0 6) color(black%60) ) ///
        (scatter y    x            , color(navy%10)  ) ///
        (scatter y_dy xdx          , color(maroon%10) ms(d) )  ///
        (scatter yy    x         if inlist(id,1,3,4,5)  , mlabel(id) mlabcol(black) mlabpos(12) color(navy%80) ) ///
        (scatter yy_dy xdx       if inlist(id,1,3,4,5)  , mlabel(id) mlabcol(black) mlabpos(6) color(maroon%80) ms(d) ), ///
        ytitle("Outcome y") xtitle("Indep var X") xlabels(0/5) ///
        legend(order(1 "f(x,v)" 2 "f(x+dx,v)" 4 "E(y|X)") col(3))
graph export qr_fig3.png     
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/分位数回归I_Fig3_条件效应_曹钰潇.png)
如图所示，变化前后所有的预期均值都落在同一条“线”上——**条件均值函数**。

不同于第一种解释适用于“我”，解释回归系数的第二种方式适用于“像我这样的人”。这意味着，这些影响并不适用于任何特定的观察值，而是发生在**与“我”有相同特征的观察值中的平均情况**，这就是条件效应的性质。

例如，观察值点 2 和点 3，它们都设定了特征 $X=2$，因此会出现同样的 0.5 个单位的 $y$ 变化。

换句话说，第二种解释指的是**在所有具有相同特征 $X$ 的人中，预期效果或者条件均值上观察到的变化**。

如上，我们也可以解释为：如果一个  $X=2$ 的人经历了一单位 $X$ 的增加，他们的结果 $y$ 预计会增加 0.5 个单位。

同样也可以这样解释：比较两组具有完全相同特征的人，除了其中一组 $X=2$ ，另一组 $X=3$ ，第二组的平均结果 $y$ 将高出 0.5 个单位。

因此再次强调：第二种解释需要从 **群体（由其特征定义）** 的角度思考，而不是从个人的角度思考。
###  3.3. <a name='无条件效应——对“所有的人”来说'></a>无条件效应——对“所有的人”来说
第二种解释是我们最熟悉的一种，因为这也是大多数教科书所采用的解释方法。它也可能是我们最感兴趣的一种，因为它是我们在“微观”层面上最接近测量影响的方法。而第三种解释可以被认为是更多的 **“宏观”层面** 的解释。

从政策制定者的角度来看，第三种解释可能更实用，因为可以了解到由于一项改变了所有人 $X$ 的政策，对人口的平均结果 $y$ 会产生什么影响。比如说：如果人口的受教育年限平均增加1年，玻利维亚的贫困率将如何变化？

首先仍然考虑一下DGP:
$$
y_{i}=b_{0}+b_{1} * x_{i}+b_{2} * x_{i}^{2}+v_{i} *\left(a_{0}+a_{1} * x_{i}\right)
$$
或者更好的是条件期望形式：
$$
E\left(y_{i} \mid X\right)=b_{0}+b_{1} * X+b_{2} * X^{2}
$$
因为我们对均值的无条件影响感兴趣，所以应该用**无条件均值**的方式来写这个方程：
$$
\begin{gathered}
E\left(E\left(y_{i} \mid X\right)\right)=b_{0}+b_{1} * E\left(x_{i}\right)+b_{2} * E\left(x_{i}^{2}\right) \\
E\left(y_{i}\right)=b_{0}+b_{1} * E\left(x_{i}\right)+b_{2} * E\left(x_{i}^{2}\right)
\end{gathered}
$$
为了进一步简化，我们还要使用：
$$
\operatorname{Var}\left(x_{i}\right)=E\left(x_{i}^{2}\right)-E\left(x_{i}\right)^{2} \rightarrow E\left(x_{i}^{2}\right)=\operatorname{Var}\left(x_{i}\right)+E\left(x_{i}\right)^{2}
$$
最后，无条件均值结果方程就可以写成：
$$
E\left(y_{i}\right)=b_{0}+b_{1} * E\left(x_{i}\right)+b_{2} * E\left(x_{i}\right)^{2}+b_{2} * \operatorname{Var}\left(x_{i}\right)　(8)
$$
从方程中可以发现：
1. 人口的无条件均值 $E\left(y_{i}\right)$ 取决于 $x$ 的无条件均值, $E(x_i)$ 。
2. 人口的无条件均值 $E\left(y_{i}\right)$ 也取决于 $x$ 的无条件方差, $var(x_i)$ 。
3. 只有当我们知道 $x$ 的一些分布特性时，我们才能预测 $y$ 的无条件均值。

为简化方程，我们需要一个影响 $E(X_i)$ ，而不是方差的所有 $X$ 的改变，例如，每一个观察值都经历了一单位的增加。这就是Firpo, Fortin, and Lemieux (2000) 所说的 **“位置转移”效应**：分布改变了“位置”，但没有改变形状。

因此，方程可以简化为**只与 $E(X_i)$ 有关的无条件边际效应**：
$$
\frac{\partial E\left(y_{i}\right)}{\partial E\left(x_{i}\right)}=b_{1}+2 * b_{2} * E\left(x_{i}\right)　(9)
$$
> 注：无条件效应也可以通过平均化条件效应（公式 7）或平均化个体效应（公式 4）得出，但这样我们不会注意到 $X$ 的其他矩（方差）也可能影响到 $y$ 的无条件均值。

通过此方程我们可以直接计算出平均变化，不过需要注意：
1. 解释一表明，由于观察到的 $x$ 和未观察到的 $v$ 的异质性，个体边际效应对每个人都是独一无二的。
2. 解释二表明，条件边际效应（均值）的变化只是因为观察到 $x$ 的异质性。
3. 解释三表明，由于对应整个人口，无条件边际效应（均值）是**恒定**的。

接下来我们利用 Stata 来获得结果：
```
** X的均值
** 因为 2*X~Chi(3)
** E(X)=0.5*k =1.5
** Var(X)=0.5*k=1.5
** 所以 E(y)=5 + 3*E(x) -0.5*E(x)^2-0.5*Var(X)
** E(Y ) = 7.625
** E(Y') = 8.625

two (scatter y    x   , color(navy%15)  ) ///
    (scatter y_dy xdx , ms(d) color(maroon%15) ) ///
        (function 5 + 3*x -0.5*x^2 , range(0 6) color(black%60) ) ///
        (scatteri 7.625 1.5 "Before",mcolor(navy ) msize(small) mlabcolor(black) ) ///
        (scatteri 8.625 2.5 "After", mcolor(maroon) msize(small) mlabcolor(black) ) ///
        (pcarrowi 7.625 1.5 8.625 2.5, color(black)), ///
        xline(1.5, lcolor(navy%50)) xline(2.5, lcolor(maroon%50)) ///
        yline(7.625, lcolor(navy%50)) yline(8.625, lcolor(maroon%50)) ylabel( 0(5)15 7.625 8.625, angle(0)) ///
        legend(order(1 "f(x,v)" 2 "f(x+dx,v)" 4 "E(y)" 5 "E(y')") col(4))       ///
        ytitle("Outcome y") xtitle("Indep var X") 
graph export qr_fig4.png
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/分位数回归I_Fig4_无条件效应_曹钰潇.png)
此图中，蓝点表示 $X$ 变化前的数据，红点表示变化后的数据。请注意，点 $（E(y),E(X)）$ 没有落在“条件均值”线上，但是其变化是与它平行的。

那么，该如何解释呢？

如果每个观察值 $x$ 都增加 1 个单位，那么 $E(X)$就会增加 1 个单位，但方差保持不变，从而导致 $y$ 的无条件均值 $(E(y))$ 将增加 1 个单位，从 7.625 增加到 8.625 。
##  4. <a name='总结'></a>总结
今天我们在线性回归模型的框架内，介绍了一种不同的方式来思考个人、条件和无条件的边际效应，这些解释之间的差异取决于我们在进行解释时的对象是谁。

在数学上，边际效应可以被描述如下：
$$
个体边际效应: \frac{\partial y_{i}}{\partial x_{i}}
$$
$$
条件边际效应： \frac{\partial E\left(y_{i} \mid X\right)}{\partial X}
$$
$$
无条件边际效应： \frac{\partial E\left(y_{i}\right)}{\partial E\left(x_{i}\right)}
$$
下一篇推文将在此基础上进行扩展，加入部分条件和分类变量，以帮助我们对分位数回归有更好的理解。

**参考文献**
- Rios-Avila, F., & Maroto, M. L. (2020, December 10). Moving Beyond Linear Regression: Implementing and Interpreting Quantile Regression Models with Fixed Effects. https://doi.org/10.31235/osf.io/znj38
