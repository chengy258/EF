## 实证金融 (Empirical Finance) 课程主页

<br>

## NEWs :apple:
- :dog: 本学习课件已经发布，请登录 FTP 查看。详情见微信群。`2022/9/3 20:05`
- **课件下载：**
  - FTP 地址：ftp://ftp.lingnan.sysu.edu.cn/ &rarr; [2022_实证金融_本科] 文件夹。
  - 账号 = 密码 = lianyjst
  - 请保护版权，未经本人同意，请勿散布于网络。
- **[FAQs](https://gitee.com/arlionn/PX/wikis/%E8%BF%9E%E7%8E%89%E5%90%9B-Stata%E5%AD%A6%E4%B9%A0%E5%92%8C%E5%90%AC%E8%AF%BE%E5%BB%BA%E8%AE%AE.md?sort_id=2662737)** 学习过程中的多数问题，都可以在这里找到答案。亦可访问 [连享会主页](https://www.lianxh.cn) 和 [连享会知乎](https://www.zhihu.com/people/arlionn/) 查阅相关推文。 

## &#x1F34E; 作业发布
- &#x1F449;  [【点击查看作业】](https://gitee.com/arlionn/EF/wikis/%E6%8F%90%E4%BA%A4%E6%96%B9%E5%BC%8F%E5%92%8C%E6%97%B6%E7%82%B9.md?sort_id=1628096) 
  - 待更新 

### &#x1F353; 交作业入口
> &#x1F449; **【[点击提交](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】**，个人作业和期末课程报告均通过此处提交 


> Notes：
> - [1] 若需更新，只需将同名文件再次提交即可。
> - [2] 提交截止时间为 deadline 当日 23:59，逾期不候。


### 课程报告
> Update: 待更新  
- &#x1F34F; 课程报告提纲和任何阶段性文稿都可以随时通过 &#x1F449; **【[交作业入口](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】** 提交，我看到后会联系你，以便及时修改优化。
  - 提交时的标题请按照 **【[交作业入口](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】** 页面中的格式要求填写。
  - 课程报告格式要求：参见 [课程报告格式要求](https://www.lianxh.cn/news/3f40bb6c25f6a.html)
  - 报告范本参见：
    - [格式范本1-纯文字-稳健性检验](https://gitee.com/arlionn/EF/wikis/%E6%A0%BC%E5%BC%8F%E8%8C%83%E6%9C%AC1-%E7%BA%AF%E6%96%87%E5%AD%97-%E7%A8%B3%E5%81%A5%E6%80%A7%E6%A3%80%E9%AA%8C.md?sort_id=3213673)
    - [格式范本2-图形-公式-分位数回归中的样本自选择问题](https://gitee.com/arlionn/EF/wikis/%E6%A0%BC%E5%BC%8F%E8%8C%83%E6%9C%AC2-%E5%9B%BE%E5%BD%A2-%E5%85%AC%E5%BC%8F-%E5%88%86%E4%BD%8D%E6%95%B0%E5%9B%9E%E5%BD%92%E4%B8%AD%E7%9A%84%E6%A0%B7%E6%9C%AC%E8%87%AA%E9%80%89%E6%8B%A9%E9%97%AE%E9%A2%98.md?sort_id=3214194)
- 课程报告截止时间：待定 


&emsp;

### 课程报告选题 - 待更新
> `2022/9/3 20:08`   
  

<br>
<br>


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/底部图片_水墨小舟001.png)
