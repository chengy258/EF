# macrolists 命令介绍-EF


写一篇推文，介绍 `help macrolists` 命令 ，( [**[R]** macrolists](https://www.stata.com/manuals/pmacrolists.pdf) )

可以执行 `lianxh` 命令，到 Stata 官网查找相关介绍和使用范例。

`2021/1/11 9:11` 补充资料
- [SSCC - Stata Programming Tools](https://www.ssc.wisc.edu/sscc/pubs/stata_prog2.htm) &rarr; **Advanced Macros** 部分可以参考
- 写作过程的所有参考资料和文献都要如实标注，以免引起版权纠纷