> **作者**：马丁（中山大学）	**E-mail**:<mading3@mail2.sysu.edu.cn>  
>          刘梦真（中山大学）  **E-mail**:<liumzh27@mail2.sysu.edu.cn>
# 随机森林的 Stata 实现

[TOC]
## 1.决策树相关理论

### 1.1 问题背景 

现代社会中，大数据的应用以及数据挖掘的迅速发展使得人们越来越重视如何高效地处理数据这一问题。有人认为，可以基于人们掌握的各方面信息，对决策空间进行一定的划分，一块空间内的样本就属于一个特定的类别，而这就是决策树思想最早的产生。在 2006 年 12 月的 ICDM 会议上，作为决策树算法之一的 C4.5 算法被评为数据挖掘领域的十大经典算法之首。

决策树的典型算法有 ID3，C4.5，CART 以及 CHAID 等。本文重点介绍 CHAID 分类方法以及由其衍生出的  CHAIDFOREST 。

### 1.2 理论介绍

**决策树算法**是一种逼近离散函数值的方法。它是一种典型的**分类方法**，首先对数据进行处理，利用归纳算法生成可读的规则和决策树，然后使用决策对新数据进行分析。本质上决策树是通过一系列规则对数据进行分类的过程。

决策树方法最早产生于上世纪 60 年代，到 70 年代末。由 J Ross Quinlan 提出了 ID3 算法，此算法的目的在于减少树的深度。但是忽略了叶子数目的研究。C4.5 算法在 ID3 算法的基础上进行了改进，对于预测变量的缺值处理、剪枝技术、派生规则等方面作了较大改进，既适合于分类问题，又适合于回归问题。

**卡方自动交互检测法**（chi-squared automatic interaction detector, **CHAID**）最早由 Kass 于1980年提出，是一个用来发现变量之间关系的工具，是一种基于调整后的显著性检验（邦费罗尼检验）决策树技术。其**核心思想**是：根据给定的反应变量和解释变量对样本进行最优分割，按照卡方检验的显著性进行多元列联表的自动判断分组。利用卡方自动交互检测法可以快速、有效地挖掘出主要的影响因素，它不仅可以处理非线性和高度相关的数据，而且可以将缺失值考虑在内，能克服传统的参数检验方法在这些方面的限制。

CHAID 的**分类过程**是：首先选定分类的反应变量，然后用解释变量与反应变量进行交叉分类，产生一系列二维分类表，分别计算二维分类表的 χ2 值，比较 P 值的大小，以 P 值最小的二维表作为最佳初始分类表，在最佳二维分类表的基础上继续使用解释变量对反应变量进行分类，重复上述过程直到 P 值大于设定的有统计意义的 α 值为止。

在 CHAID 分析过程中，决策树主要有**根节点**（包含因变量或目标变量），**父节点**（将目标变量分割成多个分类的节点），**子节点**（决策树中低于父节点的节点）以及**终端节点**（最后一个分类节点）。通常来说，可以使用ROC 曲线（接受者工作特征曲线， receiver operating characteristic curve）下面积对 CHAID 分析结果进行评价，通过改变分类使用的临界值来测试 CHAID 决策分析的信度和效度。

在实践中，CHAID 经常使用在直销的背景下，选择消费者群体，并预测他们的反应，一些变量如何影响其他变量，而其他早期应用是在医学和精神病学的研究领域。

下面将主要介绍 Stata 中如何使用 CHAID 决策分析命令`chaid`。

## 2. chaid
### 2.1 语法结构
`chaid  `命令作者为 Joseph N. Luchman 编写的用于实现 CHAID (Chi-square automated interaction detection; Kass, 1980; Applied Statistics) 和穷举 CHAID (Biggs et al., 1991; Journal of Applied Statistics) 以决策树的形式发现数据关系的算法，以及用于聚类观察的算法，语法结构如下：

```stata
chaid depvar [if] [in] [weight], ///
     [minnode(integer) minsplit(integer) ///
	 unordered(varlist) ordered(varlist) noisily missing ///
	 mergalpha(pvalue) respalpha(pvalue) spltalpha(pvalue) ///
	 maxbranch(integer) dvordered noadj nodisp predicted importance///
	 xtile(varlist, xtile_opt) permutesvy exhaust]
```

- `depvar`：被解释变量，即目标变量；
- `minnode()`：chaid 决策树节点中允许的最小样本数；
- `minsplit()`： chaid 决策树继续分裂所要求的最少样本数；
- `unordered()`：将有关变量视为无序的(任意变量都可以合并)；
- `ordered()`：将有关变量视为有序 (monotonic) 的(只有邻近变量可以合并)；
- `noisily`：向用户展示 chaid 决策树在分类变量的选择过程；
- `missing`：允许目标变量和分类变量中的缺失值被视作除有序变量 (monotonic) 和名义变量 (ordinal) 外的另一种单独的 (floating) 变量；
- `mergalpha()`：设定了在被分离变量中允许分类的临界 alpha 值；
- `respalpha() `：设置 alpha 值，在该 alpha 上，已优化合并的一组独立变量的 3 个或更多原始类别将被允许进行二元拆分；
- `spltalpha()`：设置调整后的 alpha 级别，在该级别上，最佳合并的预测值将在合并步骤后被拆分；
- `maxbranch()`：设置了 chaid 决策树中树枝数量的最大值；
- `dvordered`：将目标变量从无序变量改成有序 logistic 回归变量；
- `noadj`：防止 chaid 通过 Bonferroni 调整`spltalpha()`的 p 值来防止潜在的“假阳性”推断错误；
- `nodisp`：防止 chaid 显示算法生成的决策树结构和图形；
- `predicted`：生成对所有聚类响应变量的预测值；
- `importance`：生成一个重要性排列矩阵，对分裂变量进行排序；
- `xtitle()`：处理分类变量，生成一组有序分类的分位数。将有关变量被视为有序的，并添加到名为 "xtvarname" 的数据集中。可以在逗号后通过 nquantiles(#) 来选择创建的分位数，默认采用中位数进行拆分
- `permute`：将用于拆分和合并步骤的 p 值的计算方式从传统的大样本近似更改为基于蒙特卡洛模拟生成数值的方法；
- `svy`：将 svyset 复杂测量设计特性纳入 p 值计算；
- `exhaust`：用于实现 Biggs、de Ville 和 Suen（1991）描述的穷举 CHAID 算法，否则将进行一般性 CHAID 决策分析；

### 2.2 要点提示

- 因为 chaid 结果有随机性的成分，所以用户应该在使用 chaid 之前设置种子值；
- chaid 利用 Mata 保存一个字符串矩阵，用户可以获得 post estimation ;
- 一个名为 “CHAIDsplit” 的 Mata 矩阵包含用于创建 e(split#) 和 e(path#) 宏的信息,使用者可以从 “CHAIDsplit” 中获取某些情况下可能缺失的 e(path#) 中包含的字符串；
- 将相同观测值上的数据折叠并使用 fweight 是加快较大数据集估计时间的一种方法。考虑到 chaid 需要分类数据，使用 collapse 命令是一种特别有用的方法，但是 svy 或 permute 不可使用；
- chaid 使用 Akaike 信息标准来决定 p 值相同时的分割；

### 2.3 结果储存

命令执行结果均储存在 e() 中：

|scalars |                 details                 |
| :------------: | :-----------------------------------: |
|    e(N)    |      观测值数量      |
|    e(N_clusters)    |  chaid 产生的聚类的数量  |
|   e(fit)    |      簇的纯度（每个簇只有一个响应变量值的程度），基于克莱姆 V      |


|macros |                 details                 |
| :------------: | :-----------------------------------: |
|   e(cmdline)   |              输入的命令               |
|     e(cmd)     |              chaid              |
|    e(title)    |             预测结果名称              |
|   e(path#)   |             显示指向聚类#的拆分的级别，每个分隔用分号间隔              |
|  e(split#)  |             展示 chaid 命令所产生的第 # 次分类             |
|   e(depvar)    |           解释变量/响应变量名称            |
|   e(sample)    |             标记预测样本              |


|matrices |                 details                 |
| :------------: | :-----------------------------------: |
|   e(importance)   |              重要性排列矩阵               |
|     e(sizes)     |              每个聚类的样本大小              |
|    e(branches)    |             从每个聚类的根节点产生的树枝的数量             |


| functions |                 details                 |
| :------------: | :-----------------------------------: |
|   e(sample)   |             标记预测样本              |

##  3. 随机森林相关理论

### 3.1 问题背景      

使用单个学习器(如决策树等)时，往往会出现 **过拟合** 的问题。在实证研究中，许多学者通过 **集成学习**  (Ensemble Learning)，即策略性地构建并结合多个学习器，将弱学习器转换成强学习器来完成学习任务。集成学习中最主要的两种方法分别是提升法  (Boosting)  和套袋法 (Bagging)。

以 Adaboosting 为代表的**提升法**主要是通过对学习器多轮逐步优化来提高算法的精度。以随机森林 (Random Forest , RF) 为代表的**套袋法**主要是采取自抽样聚集的方法通过训练一系列平行的模型并从中选出最好的学习器来改善算法。两者的主要区别如下表：

|          |         Bagging          |                Boosting                |
| :------: | :----------------------: | :------------------------------------: |
| 样本选择 |  采用 Boostrap 生成数据集  |             采用原有数据集             |
| 样本权重 | 均匀取样，各样本权重相同 |     据历史模型调整，权重不一定均等     |
| 预测函数 |   所有预测函数权重相等   |       误差更小的预测函数权重更大       |
| 计算顺序 |       可以并行运算       | 不可以并行运算，在历史模型的基础上计算 |
|   降低   |           方差 (Variance)           |                  偏误 (Bias)                  |

### 3.2 理论介绍

**随机森林**是 Leo Breiman 在 2001 年将其提出的 Bagging (1996) 理论和 Ho 提出的随机子空间理论相结合，提出的一种集成学习方法。总的来说，随机森林是以一系列由独立同分布的随机变量 $\theta_{k}$  [k= 1,2, ..., n]决定的决策树为基本学习器，进行集成学习并选出最优方案的学习方法。随机变量 $\theta_{k}$ 主要是由随机森林的两大随机思想决定的：

1.**Bagging 思想**：使用 Boostrap 的方式随机生成一系列数据集。具体的做法是从原始数据集中有放回的抽取若干次， 生成一个与原数据集大小相同的数据集样本$D_{1}$；重复上述抽样 (n-1) 次，即可得到 n 个训练样本$D_{1},D_{2},...,D_{n}$。由 (1) 式可知，在每次自抽样时，都会有部分的数据一次也不会被抽中，这些数据被成为袋外 (Out-of-Bag, OOB) 数据。

$$ \lim_{m\to\infty }(1-\frac{1}{m})^{m}=\frac{1}{e}\approx 0.37\% $$

2.**特征子空间思想**：在每个节点上从 K (通常是$|log_{2}m|+1$, m 为特征变量总数)个最优预测变量中随机选取某个变量作为分类变量，本质上是随机赋予各变量参与结果预测的权重。

- 随机森林的相关**步骤**如下图所示：

	

![随机森林的Stata实现_Fig_随机森林步骤示意图_马丁&刘梦真.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E7%9A%84Stata%E5%AE%9E%E7%8E%B0_Fig_%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E6%AD%A5%E9%AA%A4%E7%A4%BA%E6%84%8F%E5%9B%BE_%E9%A9%AC%E4%B8%81&%E5%88%98%E6%A2%A6%E7%9C%9F.png)

Breiman 在 [*Random Forest (2001)*](https://schlr.cnki.net/Detail/index/SSJDLAST/SSJD00001340271)  中在统计上验证了随机森林的**收敛性**、随机森林的**泛化误差有上界**、可以通过袋外数据估计随机森林的泛化误差等性质。 OOB 数据可以用来估计模型的泛化误差，对训练模型进行样本内估计。这使得我们不再需要专门设置额外的验证集，进一步充分利用数据，提高了模型的准确度。

总的来说，随机森林具有**精度高、对噪声稳健**等性质，是一种强大的集成学习方法。

下面将主要介绍 Stata 中有关随机森林的两条命令 `chaidforest` 和 `rforest` 。

## 4. chaidforest
### 4.1 语法结构

`chaidforeat` 命令是 Joseph N. Luchman  (2015) 编写的用来生成以一系列 chaid 决策树为基础学习器生成的随机森林 Stata 命令，其语法结构如下：

```stata
chaidforest	 depvar	 [if]  [in]	 [weight] , 				///
	ntree(integer) nvuse(integer) minnode(integer)			/// 
	minsplit(integer) unordered(varlist) ordered(varlist)	/// 
	noisily missing alpha(pvalue) dvordered 				///
	xtile(varlist, xtile_opt) proos(proportion) nosamp
```

- `depvar`：被解释变量，即目标变量；
- `ntree()`: chaidforest 中生长的单个决策树的数目，默认值为 100；
- `nvuse()`： chaid 决策树在节点分裂时的候选变量的数目，默认值为解释变量总数的平方根的取整(四舍五入)；
- `minnode()`：单个 chaid 决策树节点中允许的最小样本数；
- `minsplit()`： chaid 决策树继续分裂所要求的最少样本数；
- `unordered()`：将有关变量视为无序的(任意变量都可以合并)；
- `ordered()`：将有关变量视为有序  (monotonic)  的(只有邻近变量可以合并)；
- `noisily`：向用户展示每个 chaid 决策树在分类变量的选择过程；
- `missing`：允许目标变量和分类变量中的缺失值被视作除有序变量 (monotonic) 和名义变量 (ordinal) 外的另一种单独的 (floating) 变量;
- `alpha()`：设置 alpha 值，作为选择可供选择的最优分类变量的标准；
- `dvordered`：将目标变量从无序变量改成有序变量；
- `xtitle()`：处理分类变量，生成一组有序分类的分位数。将有关变量被视为有序的，并添加到名为 "xtvarname" 的数据集中。可以在逗号后通过 nquantiles(#) 来选择创建的分位数，默认采用中位数进行拆分；
- `proos()`：将默认的 boostrap 替换成不替换的采样，设定外样本 (Out-of-sample, OOS) 的比例，默认的 boostrap 中外样本的比列约为 $\frac{1}{3}$ ;
- `nosample`：跳过套袋 (bagging) 的步骤，采用原始样本作为数据集.

### 4.2 要点提示

- 为保证结果的可重复性，使用 chaidforest 命令前需要设置种子值；
- chaidforest 中所有的 chaid 决策树均为二叉树，没有多类拆分；
- 当分类变量的类别多于 20 个时，需要手动折叠该变量；
- 在训练 chaidforest 中的 chaid 决策树时，采用的是未经 Bonferroni 调整的 p 值，允许种植单个庞大的决策树；
- 可通过折叠数据，使用权重来加快数据处理过程；
- chaidforest中chaid决策树的数目 [`ntree()`] ，拆分变量的数目 [`nvuse()`]，允许拆分的最小大小[`minsplit()`] 和最小节点大小 [`minnode()`] 会影响 `chaidforest` 命令的执行时间。

### 4.3 结果储存

命令执行结果均储存在 e() 中：

|scalars |                 details                 |
| :------------: | :-----------------------------------: |
|    e(ntree)    |      随机森林中 chaid 决策树的数目      |
|    e(nvuse)    |  每个 chaid 决策数中使用分类变量的个数  |
|   e(N_tree)    |      每个决策树中使用的样本数目       |
|  e(minsplit)   | chaid 决策树继续分裂所要求的最少样本数 |
|   e(minnode)   | chaid 决策树每个节点所要求的最少样本数 |
|   e(cmdline)   |              输入的命令               |
|     e(cmd)     |              chaidforest              |
|    e(title)    |             预测结果名称              |
|   e(predict)   |             用于预测程序              |
|  e(splitvars)  |             分类变量列表              |
|   e(depvar)    |           解释变量函数名称            |
|   e(sample)    |             标记预测样本              |

## 5. rforest

rforest 是 Rosie Yuyan Zou 和 Matthias Schonlau 编写可以实现以**分类树/回归树**为基本学习器的随机森林的 Stata 相关命令，可直接通过 `ssc install rforest`下载该命令。

### 5.1 语法结构

其语法结构如下：

```stata
 rforest depvar indepvars [if] [in] , 			///
 	type(str) iterations(str) numvars(int)		///
 	depth(int) lsize(int) variance(real)		///
 	seed(int) numdecimalplaces(int)
```

- `depvar`：被解释变量，即目标变量；
- `indepvars`：解释变量；
- `type()`：决策树类型， "class" (classification，分类树) 或 "reg"（regression, 回归树）；
- `iterations()`：随机森林中决策树的数目，默认值为 100；
- `numvars()`：每次分裂时使用的自变量的个数，默认值为解释变量数目的平方根取整；
- `depth()`：随机森林中决策树的深度，即根节点到叶节点的最长路径的长度；
- `lsize()`：每个叶节点上的最小样本树；
- `variance()`：(仅适用于回归树) 设置每个节点能继续分裂的因变量的最小方差比例；默认值为 e^(-3) 。例如，因变量在整个数据集上的方差为 a ,设定的方差比例为 b，则节点上因变量的方差只有在大于 a*b 时才考虑进一步分裂；
- `seed()`：设置种子值，便于结果执行的可重复性；
- `numdecimalplaces()`：设置计算精度，默认五位小数.

在采用随机森林模型进行预测时，其代码结构如下：

```stata
  predict { newvar | newvarlist | stub* } [if] [in] , [ pr ]
```

- `pr` 选项仅适用于分类问题。`pr` 选项可以返回分类问题中相关类别的概率值。
- predict 和 rforest 中的指定的随机森林的类型必须相同。
- 在预测时，需要指定预测结果的变量名。若是回归问题，需要指定一个新变量名；若只预测相关类别(不提供相关类别的概率)，也只需要指定一个新变量名；若预测相关相关变量及其概率，需要指定一系列变量名。

### 5.2 要点提示

- 缺失值：若某样本的某自变量缺失，该自变量在训练中将被忽视；若训练集中的因变量存在缺失值，则会提示错误信息并退出；
- 分类树：对于分类问题，相关值必须是非负整数；
- 袋外误差 (Out-of-bag error) ：通过训练随机森林决策树时产生的袋外数据计算袋外误差。回归问题的代表统计量为 RMSE，分类问题则计算分类误差计算。通常，可以通过袋外误差和迭代次数的散点图查看袋外误差收敛性。若袋外误差不收敛，则需要增加迭代次数。
- 分类标准：在分类问题中采用熵值 (entropy) 作为分类标准。

### 5.3 Bug 说明

若多次采用`rforest`命令预测同一个目标变量，该预测有时会出现一些问题。

以下是命令作者 Rosie Yuyan Zou 和 Matthias Schonlau 提供的两种可能的解决方案：

```stata
* 解决方案1
foreach i of numlist 1/5 {
            cap drop p //removing any existing prediction variables avoids the bug
            rforest $y $xvars , type(class)
            predict p
}
* 解决方案2
 foreach i of numlist 1/5 {
            rforest $y $xvars , type(class)
            predict p`i' //giving a different name each time avoids the bug also
}
```

### 5.4 结果储存

- rforest命令的结果储存

	rforest命令的结果储存在 e() 中：

	|      scalars       |          details         |
	| :-------------: | :----------------------: |
	| e(Observations) |         样本数目         |
	|  e(features)  | 随机森林中使用的特征数目 |
	|  e(Iterations)  |   随机森林中的迭代次数   |
	|  e(OOB_Error)   |         袋外误差         |
	|    e(depvar)    |        因变量名称        |

- 预测结果储存

	预测结果也储存在 e() 中：

	|      scalars       |                       details                        |
	| :----------------: | :-----------------------------------------------: |
	|      e(MAE)      |         平均绝对误差(仅适用于回归问题)          |
	|      e(RMSE)       |          均方根误差(仅适用于回归问题)           |
	|  e(correct_class)  |       正确分类的样本数(仅使用于分类问题)        |
	| e(incorrect_class) |       错误分类的样本数(仅适用于分类问题)        |
	|   e(error_rate)    |            错误率(仅适用于分类问题)             |
	|    e(fMeasure)    | 由每个类别的F统计量组成的矩阵(仅适用于分类问题) |


## 6. Stata 操作

### 6.1 chaid 示例

- 例一：随机森林的基础操作

  本例采用 Stata 自带的 1978 年汽车数据集，分析变量 `foreign`  (即分析汽车种类为国产车或进口车)的决定因素。将变量 `rep78` , `length`  视作无序变量，并将`length` 用三分位数等分，设定最小节点数为 4 ，最小分裂数为 10 。
  
    - 命令代码：
  
  ```stata
  . clear all
  
  . set seed 1234567
  
  . sysuse auto
  (1978 Automobile Data)
    
  . chaid foreign, unordered(rep78) minnode(4) minsplit(10) xtile(length, n(3))
  ```
  
  - 最终结果(以图片和表格形式展现)
  
  ![随机森林的Stata实现_Fig_chaid结果展示1_马丁&刘梦真.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E7%9A%84Stata%E5%AE%9E%E7%8E%B0_Fig_chaid%E7%BB%93%E6%9E%9C%E5%B1%95%E7%A4%BA1_%E9%A9%AC%E4%B8%81&%E5%88%98%E6%A2%A6%E7%9C%9F.png)

**Chi-Square Automated Interaction Detection (CHAID) Tree Branching Results**

|      | 1      | 2                                    | 3                        | 4                        |
| ---- | ------ | ------------------------------------ | ------------------------ | ------------------------ |
| 1    |  xtlength@1 |  xtweight_2 |xtlength_2 |  xtweight_3 |
| 2    |   | rep78@1 4 5                            | rep78@2 3              |                          |
| 3    |  Cluster #1 |  Cluster #2               |  Cluster #4               |   Cluster #3               |

- 例二：使用例一中的数据进行排列测试
   此方法使用蒙特卡洛模拟进行数据集的训练以及测试，更适合小样本的 chaid 聚类分析。
   
 - 命令代码：

  ```
  . clear all
  
  . set seed 1234567
  
  . sysuse auto
    (1978 Automobile Data)
  
  . chaid foreign, unordered(rep78) minnode(4) minsplit(10) xtile(length, n(3)) permute
  ```
  - 最终结果(以图片和表格形式展现)

![随机森林的Stata实现_Fig_chaid结果展示2_马丁&刘梦真.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E7%9A%84Stata%E5%AE%9E%E7%8E%B0_Fig_chaid%E7%BB%93%E6%9E%9C%E5%B1%95%E7%A4%BA2_%E9%A9%AC%E4%B8%81&%E5%88%98%E6%A2%A6%E7%9C%9F.png)

  **Chi-Square Automated Interaction Detection (CHAID) Tree Branching Results**

|      | 1      | 2                                    | 3                        |
| ---- | ------ | ------------------------------------ | ------------------------ |
| 1    |  xtlength@1 |  xtweight_2 | xtweight_3 |
| 2    |  Cluster #1 |  Cluster #2               |    Cluster #3               |

- 例三：具有有序响应变量和排列重要性的大规模 CHAID 分析
  
    - 命令代码：

  ```
    .  webuse nhanes2f, clear
  
    .  chaid health, dvordered unordered(region race) ordered(houssiz sizplace diabetes sex smsa heartatk) importance
  ```
  - 最终结果(由于表格数量过多，因此仅展示图片)

  ![随机森林的Stata实现_Fig_chaid结果展示3_马丁&刘梦真.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E7%9A%84Stata%E5%AE%9E%E7%8E%B0_Fig_chaid%E7%BB%93%E6%9E%9C%E5%B1%95%E7%A4%BA3_%E9%A9%AC%E4%B8%81&%E5%88%98%E6%A2%A6%E7%9C%9F.png)

- 例四：使用 fweight 的具有有序响应变量的大规模 CHAID 分析
  - 命令代码：

  ```
    .  webuse nhanes2f, clear
  
    .  generate byte fwgt = 1

    .  collapse (sum) fwgt, by(health region race houssiz sizplace diabetes sex smsa heartatk)

    .  chaid health [fweight = fwgt], dvordered unordered(region race) ordered(houssiz sizplace diabetes sex smsa heartatk)
  ```
  - 最终结果(由于表格数量过多，因此仅展示图片)

  ![随机森林的Stata实现_Fig_chaid结果展示4_马丁&刘梦真.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E7%9A%84Stata%E5%AE%9E%E7%8E%B0_Fig_chaid%E7%BB%93%E6%9E%9C%E5%B1%95%E7%A4%BA4_%E9%A9%AC%E4%B8%81&%E5%88%98%E6%A2%A6%E7%9C%9F.png)

- 例五：具有复杂的调查设计的穷举 CHAID 分析

  - 命令代码：
   ```
    .  webuse nhanes2f, clear
  
    .  svyset psuid [pweight=finalwgt], strata(stratid)

    .  chaid health, dvordered unordered(region race) ordered(houssiz sizplace diabetes sex smsa heartatk) svy exhaust
   ```
   - 最终结果(以图片和表格形式展现)
  ```  
	pweight: finalwgt
  VCE: linearized
  Single unit: missing
  Strata 1: stratid
  SU 1: psuid
  FPC 1: <zero>
  ```
  **Chi-Square Automated Interaction Detection (CHAID) Tree Branching Results**
  
  |      | 1      | 2                                    |
  | ---- | ------ | ------------------------------------ |
  | 1    | heartatk@0|   heartatk@1 |
  | 2    |  Cluster #1 |  Cluster #2  |
  
  ![随机森林的Stata实现_Fig_chaid结果展示5_马丁&刘梦真.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E7%9A%84Stata%E5%AE%9E%E7%8E%B0_Fig_chaid%E7%BB%93%E6%9E%9C%E5%B1%95%E7%A4%BA5_%E9%A9%AC%E4%B8%81&%E5%88%98%E6%A2%A6%E7%9C%9F.png)

### 6.2 chaidforest 示例

- 例一：使用 minsplit() 命令和 minnode() 命令的 chaidforest 分析

  本例采用 Stata 自带的 1978 年汽车数据集，分析变量 `foreign`  (即分析汽车种类为国产车或进口车)的决定因素。将变量 `rep78`、`length` 和 `weight` 视作无序变量，并将 `length` 用三分位数等分，设定最小节点数为 2，最小分裂数为 5，设定显著性水平 ($\alpha$)为 0.8 。

  [此处使用了 `noisily`  选项，鉴于森林生成过程的冗杂，故将其舍去，仅展示代码最终结果。]

  - 命令代码：

  ```
  . clear all
  
  . set seed 123
  
  . sysuse auto
  (1978 Automobile Data)
  
  . chaidforest foreign, unordered(rep78) minnode(2) minsplit(5) noisily xtile(length weight, nquantiles(3)) alpha(0.8)
  ```
  - 最终结果(以表格形式展现)

   **CHAID Finished Execution**

  |      | 1      | 2                                    | 3                        | 4                        | 5                         |
  | ---- | ------ | ------------------------------------ | ------------------------ | ------------------------ | ------------------------- |
  | 1    | Splits | xtweight_1 , xtweight_2  xtweight_3, | xtlength_1 , xtlength_2, | xtweight_2 , xtweight_3, | xtlength_2 , xtlength_3 , |
  | 2    | path1  | xtweight_1                           | xtlength_1               |                          |                           |
  | 3    | path2  | xtweight_2  xtweight_3               | xtweight_2               | xtlength_2               |                           |
  | 4    | path3  | xtweight_1                           | xtlength_2               |                          |                           |
  | 5    | path4  | xtweight_2  xtweight_3               | xtweight_3               |                          |                           |
  | 6    | path5  | xtweight_2  xtweight_3               | xtweight_2               | xtlength_3               |                           |

- 例二：指定外样本比例的随机森林

	本例指定外样本比例为 0.25，其余参数设置同例一。

  - 代码展示

  ```stata
   . clear all
		
   . set seed 123
		
   . sysuse auto
   (1978 Automobile Data)
		
   . chaidforest foreign, unordered(rep78) minnode(2) minsplit(5) noisily xtile(length weight, nquantiles(3)) proos(0.25)
  ```

  - 最终结果
  
   **CHAID Finished Execution**

  |      | 1      | 2                                               | 3                                  | 4                        | 5                         | 6                       | 7                                  |
  | ---- | ------ | ----------------------------------------------- | ---------------------------------- | ------------------------ | ------------------------- | ----------------------- | ---------------------------------- |
  | 1    | Splits | rep78_4	rep78_5, rep78_1    rep78_2 rep78_3, | xtlength_1 xtlength_2, xtlength_3, | rep78_4, rep78_5,        | rep78_1 rep78_2, rep78_3, | xtlength_1, xtlength_2, | xtlength_1, xtlength_2 xtlength_3, |
  | 2    | path1  | rep78_4 rep78_5                                 | xtlength_1 xtlength_2              | rep78_4                  |                           |                         |                                    |
  | 3    | path2  | rep78_1 rep78_2 rep78_3                         | rep78_1 rep78_2                    |                          |                           |                         |                                    |
  | 4    | path3  | rep78_4 rep78_5                                 | xtlength_3                         |                          |                           |                         |                                    |
  | 5    | path4  | rep78_4 rep78_5                                 | xtlength_1 xtlength_2              | rep78_5                  | xtlength_1                |                         |                                    |
  | 6    | path5  | rep78_1 rep78_2 rep78_3                         | rep78_3                            | xtlength_1               |                           |                         |                                    |
  | 7    | path6  | rep78_4 rep78_5                                 | xtlength_1 xtlength_2              | rep78_5                  | xtlength_2                |                         |                                    |
  | 8    | path7  | rep78_1 rep78_2 rep78_3                         | rep78_3                            | xtlength_2    xtlength_3 |                           |                         |                                    |
	
- 例三：采用全样本(即跳过套袋操作）的随机森林

  - 代码展示

  ```stata
   . webuse sysdsn1, clear
   . chaidforest insure, ordered(male nonwhite) unordered(site) noisily xtile(age, nquantiles(2)) nosamp
  ```

	- 最终结果

   **CHAID Finished Execution**

	|      |   1    |            2             |            3             |         4         |             5             |         6         |
	| ---- | :----: | :----------------------: | :----------------------: | :---------------: | :-----------------------: | :---------------: |
	| 1    | Splits | site_1 site_3 , site_2 , | nonwhite_0 , nonwhite_1, | site_1 , site_3 , | nonwhite_0 , nonwhite_1 , | site_1 , site_3 , |
	| 2    | path1  |      site_1 site_3       |        nonwhite_0        |      site_1       |                           |                   |
	| 3    | path2  |          site_2          |        nonwhite_0        |                   |                           |                   |
	| 4    | path3  |      site_1 site_3       |        nonwhite_1        |      site_1       |                           |                   |
	| 5    | path4  |      site_1 site_3       |        nonwhite_0        |      site_3       |                           |                   |
	| 6    | path5  |          site_2          |        nonwhite_1        |                   |                           |                   |
	| 7    | path6  |      site_1 site_3       |        nonwhite_1        |      site_3       |                           |                   |

- 例四：包含缺失值处理的随机森林

   - 代码展示

	```stata
	 . webuse sysdsn1, clear
	 . chaidforest insure, ordered(male nonwhite) unordered(site) noisily xtile(age, nquantiles(2)) missing
	```

	- 最终结果
	
   **CHAID Finished Execution**
	
	|      | 1      | 2                        | 3                 | 4                   | 5                   | 6                   |
	| ---- | ------ | ------------------------ | ----------------- | ------------------- | ------------------- | ------------------- |
	| 1    | Splits | site_1 site_3 , site_2 , | site_1 , site_3 , | xtage_1 , xtage_2 , | xtage_1 , xtage_2 , | xtage_1 , xtage_2 , |
	| 2    | path1  | site_1 site_3            | site_1            | xtage_1             |                     |                     |
	| 3    | path2  | site_2                   | xtage_1           |                     |                     |                     |
	| 4    | path3  | site_1 site_3            | site_3            | xtage_1             |                     |                     |
	| 5    | path4  | site_1 site_3            | site_1            | xtage_2             |                     |                     |
	| 6    | path5  | site_2                   | xtage_2           |                     |                     |                     |
	| 7    | path6  | site_1 site_3            | site_3            | xtage_2             |                     |                     |
	

### 6.3  rforest 示例

- 例一：以回归树为基础的随机森林

  ```stata
  . sysuse auto,clear
  (1978 Automobile Data)
  
  . set seed 1
  
  . gen u = uniform()
  
  . sort u
  
  . rforest price weight length, type(reg) iter(500)
  
  . ereturn list
  
  scalars:
         e(Observations) =  74
             e(features) =  2
           e(Iterations) =  500
            e(OOB_Error) =  1493.44966573811
  
  macros:
                  e(cmd) : "rforest"
              e(predict) : "randomforest_predict"
               e(depvar) : "price"
           e(model_type) : "random forest regression"
  
  matrices:
           e(importance) :  2 x 1
  
  . predict p1
  
  . list p1 in 1/5
  
       +-----------+
       |        p1 |
       |-----------|
    1. | 4307.9085 |
    2. | 4112.3419 |
    3. | 7912.8044 |
    4. |   4139.16 |
    5. | 4605.0144 |
       +-----------+
  
  . ereturn list
  
  scalars:
         e(Observations) =  74
             e(features) =  2
           e(Iterations) =  500
            e(OOB_Error) =  1493.44966573811
                  e(MAE) =  655.5163188347408
                 e(RMSE) =  887.4723791281133
  
  macros:
                  e(cmd) : "rforest"
              e(predict) : "randomforest_predict"
               e(depvar) : "price"
           e(model_type) : "random forest regression"
  
  matrices:
           e(importance) :  2 x 1
  
  ```

- 例二：以决策树为基础的随机森林

	```stata
	. sysuse auto, clear
	(1978 Automobile Data)
	
	. set seed 1
	
	. gen u = uniform()
	
	. sort u
	
	. rforest foreign weight length, type(class) iter(500)
	
    . ereturn list
	
	scalars:
	       e(Observations) =  74
	           e(features) =  2
	         e(Iterations) =  500
	          e(OOB_Error) =  .2162162162162162
	
	macros:
	                e(cmd) : "rforest"
	            e(predict) : "randomforest_predict"
	             e(depvar) : "foreign"
	         e(model_type) : "random forest classification"
	
	matrices:
	         e(importance) :  2 x 1
	
	. predict p1
	
	. predict c1 c2, pr
	
	. list p1 foreign c1 c2 in 1/5
	
	     +---------------------------------------------+
	     |       p1    foreign          c1          c2 |
	     |---------------------------------------------|
	  1. |  Foreign    Foreign         .25         .75 |
	  2. |  Foreign    Foreign   .32333333   .67666667 |
	  3. |  Foreign    Foreign        .308        .692 |
	  4. | Domestic   Domestic   .50845238   .49154762 |
	  5. | Domestic   Domestic           1           0 |
	     +---------------------------------------------+
	
	. ereturn list
	
	scalars:
	       e(Observations) =  74
	           e(features) =  2
	         e(Iterations) =  500
	          e(OOB_Error) =  .2162162162162162
	      e(correct_class) =  73
	    e(incorrect_class) =  1
	         e(error_rate) =  .0135135135135135
	
	macros:
	                e(cmd) : "rforest"
	            e(predict) : "randomforest_predict"
	             e(depvar) : "foreign"
	         e(model_type) : "random forest classification"
	
	matrices:
	           e(fMeasure) :  1 x 2
	         e(importance) :  2 x 1
	
	```
	
	

##  参考文献

1. Breiman, L. (2001). Random forests. Machine learning.[PDF](https://schlr.cnki.net/Detail/index/SSJDLAST/SSJD00001340271)
2. Biggs, D., de Ville, B., and Suen, E. (1991). A method of choosing multiway partitions for classification and decision trees. Journal of Applied Statistics.[PDF](https://schlr.cnki.net/Detail/index/STJDLAST/STJDB7E43485CDE057D6001D3E995C943CAC)
3. Goodman, L. A. (1979). Simple models for the analysis of association in cross-classifications having ordered categories. Journal of the AmericanStatistical Association.[PDF](https://schlr.cnki.net/Detail/index/SJST_01/SJST14120300447882)
6. Kass, G. V. (1980). An exploratory technique for investigating large quantities of categorical data. Applied Statistics, 29, 2, 119-127.[PDF](https://schlr.cnki.net/Detail/index/SJST_01/SJSTB7876D1B60EB7F08CFCCC5ACEECCAF54)
7.  Biggs, D., de Ville, B., and Suen, E. (1991). A method of choosing multiway partitions for classification and decision trees. Journal of Applied Statistics, 18, 49-62.[PDF](https://schlr.cnki.net/Detail/index/STJDLAST/STJDB7E43485CDE057D6001D3E995C943CAC)
8. 黄奇.基于 CHAID 决策树的个人收入分析.数学理论与应用，2009 年 04 期.[PDF](https://xuewen.cnki.net/CJFD-LLYY200904008.htmlC)
9. 何凡、沈毅、叶众.卡方自动交互检测法及其应用.中华预防医学杂志，2005 年 3 月，第 39 卷第 2 期.[PDF](http://www.pubhealth.org.cn/cn/wxshow.asp?id=1517)
8. 方匡南等.随机森林方法研究综述.统计与信息论坛，2011 年 3 月，第 26 卷第三期.[PDF](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFD2011&filename=TJLT201103007&v=7KZGkPgQAEmR6ExCCCLqVNui3%25mmd2FQtEcTXo4wh%25mmd2FJ6444drpp0GbRA56ESe2X1aLq28)
9. 董师师、黄哲学.随机森林理论浅析.集成技术，2013 年 1月，第 2 卷第 1 期.[PDF](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFD2013&filename=JCJI201301001&v=6pvMg%25mmd2B0e94CSTqwNkW%25mmd2FSGrprM1HGUeRbDDQo4CmTeE79fzz71KfJkZ77Wy8%25mmd2BrV%25mmd2F7)

