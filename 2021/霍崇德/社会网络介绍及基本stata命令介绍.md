# 社会网络介绍及基本stata命令介绍

&emsp;

> **作者**：霍崇德 (中山大学)    
> **E-Mail**: <huochd@mail2.sysu.edu.cn>  

**目录**

[TOC]

---

## 1. 背景:
### 1.1 什么是社会网络?
- 文字定义

$\qquad$社会网络 (Social Network) 是由许多节点构成的一种社会结构，节点通常是指个人或组织，社会网络代表各种社会关系，经由这些社会关系，把从偶然相识的泛泛之交到紧密结合的家庭关系的各种人们或组织串连起来。社会关系包括朋友关系、同学关系、生意伙伴关系、种族信仰关系等。


$\qquad$如图就是一张的典型社会网络图，表示人与人之间的网络关系。
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig1_网络示例_霍崇德.png)

---

- 数学定义：

$\qquad$在数学上，一个二进制的网络被表示为：$G=\left(V,E\right)$,其中V代表“节点”，节点数为正整数。而E代表“边”，是节点的组合，例如一个简单的网络G为$\left(3,\left\{\left({1,2} \right ),\left({1,3} \right ) \right\}\right )$。该网络对应图如下所示：
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig2_网络示例2_霍崇德.png)

---

$\qquad$社会网络还有一种方便的矩阵表示形式，该矩阵的构造方法为：我们将$Y_{i,j}$定义为节点i与节点j之间的关联关系，若节点i与节点j相关，则定义$Y_{i,j}=1$，若两者不相关则定义$Y_{i,j}=0$。所以该社会网络矩阵为：
$$
Y=\begin{bmatrix}
			Y_{1,1}\quad \cdots \quad Y_{1,n} \\ \vdots \quad \ddots \quad \vdots \\ Y_{n,1}\quad \cdots \quad Y_{n,n}
		\end{bmatrix}
$$

$\qquad$矩阵应用例如下图：
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig3_网络示例3_霍崇德.png)

---

### 1.2 如何进行网络分析？
$\qquad$社会网络分析（
Social Network Analysis
）就是对社会关系结构及其属性加以分析的一套规范和方法。它主要分析的是不同社会单位（个体、群体或
社会）所构成的关系的结构及其属性。

$\qquad$网络分析大致包括如下几点：

    网络的简单描述/表征
    计算节点特征（例如中心性）
    组件、块、集团、等价物...
    网络可视化
    网络的统计建模，网络动力学

$\qquad$目前各种编程软件中都已经开发出可供网络分析的工具，例如python的networkx，R的igraph以及windows环境下的ucinet，pajek等，而本文介绍的是stata软件下的网络分析工具：`nwcommands`。

---

## 2. stata网络分析基本介绍
$\qquad$接下来将按照上文提到的网络分析内容介绍部分相关stata命令。
### 2.1 nwcommands下载

#### 方法1：本地安装

```stata
cd D:/
mkdir nwcmd 
cd nwcmd

// 下载原始文件
copy https://github.com/ThomasGrund/nwcommands/archive/refs/heads/master.zip  nwcommands.zip 
// 解压
unzipfile nwcommands.zip 

// 添加到 Stata 搜索路径中
adopath + D:/nwcmd/nwcommands-master

// 查看帮助
help nwcommands

//::: 更好的方法：放到 plus 文件夹中
cd `"`c(sysdir_plus)'"'
cap mkdir nwcommands

!copy "D:/nwcmd/nwcommands-master"  "D:/stata/plus/nwcommands" //需要酌情修改 plus 的路径
adppath + "D:/stata/plus/nwcommands" // 可以写入 Profile.do 
```

---

#### 方法2：在线安装 (受限于网络原因，不容易成功)


```stata
// A. 查看所有文档清单，依次点击下载
net from http://nwcommands.org
//或使用命令
. nwinstall, all
// B. 也可以用单独的命令下载
// B1. ado 文档部分
   // 安装程序文件
   net install nwcommands-ado.pkg, replace
   // 下载附件资料
   net get nwcommands-ado.pkg, replace
// B2. 帮助文档
   net install nwcommands-hlp.pkg, replace
// B3. 菜单模式支持文档
   net install nwcommands-dlg.pkg, replace
// B4. 扩展功能
   net install nwcommands-ext.pkg, replace
```

---

### 2.2 简单网络描述命令介绍

$\qquad$下面是对一些基本命令的介绍，更多的命令可以使用命令 `help nwcommands` 进行查询。

- 查看现在全部已有网络的网络名
```stata
    . nwds
```
---

- 引用外部网络
```stata
    //引用现成网络，其中name为文件名，type为文件类型
    . nwimport name, type(type)
    //例如引用安装时下载的gang_pajek.net，输入下面命令时需要确保该文件位于当前路径下
    . nwimport gang_pojek.net,type(pajek)
    . help nwexample
    //help文档中有详尽的网络实例，可以通过文档指引导入外部网络。
```
---

- 生成随机网络
```stata
     . nwrandom nodes 
    //示例：生成一个拥有15个节点的有向 Erdos-Renyi 网络
    . nwrandom 15,prob(.1)
    //查看网络
    . nwset,detail
(1 network)

--------------------------------------------------
 1) Current Network
--------------------------------------------------
   Network name: random
   Directed: true
   Nodes: 15
   Network id: 1
   Variables: net1 net2 net3 net4 net5 net6 net7 net8 net9 net10 net11 net12 net13 net14 net15
   Labels: net1 net2 net3 net4 net5 net6 net7 net8 net9 net10 net11 net12 net13 net14 net15
   Edgelabels: 

   //更多种类的随机网络可以用 help 查询 nwlattice,nwring,nwsmall等命令
   ```
---

- 创建网络:
    
```stata
    //使用neset创造网络，其中有两种创建方法对应上文介绍的两种网络表示类型。
    //1.用边与节点创造网络
    nwset varlist[, edgelist]
    //2.用矩阵创造网络
    nwset, mat(matamatrix)
```
---
$\qquad$示例：

$\qquad$方法一：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig4_nwset1_霍崇德.png)

$\qquad$图中变量代表六个有向线段即$\left\{\left(1,2\right),\left(1,3\right),\left(2,3\right),\left(3,1\right),\left(4,2\right),\left(5,5\right)\right\}$

$\qquad$所以按照边与节点的创造方法的stata代码为：

```stata 
    //清除原有网络
    . nwclear
    //设置网络
    . nwset ego alter,edgelist
    //使用nwset查看生成的网络
    . nwset
    (1 network)
--------------------
    network
    //用neset,detail查看详细网络数据
    . nwset,detail
    (1 network)

--------------------------------------------------
 1) Current Network
--------------------------------------------------
   Network name: network
   Directed: true
   Nodes: 5
   Network id: 1
   Variables: net1 net2 net3 net4 net5
   Labels: 1 2 3 4 5
   Edgelabels: 

 ```
---

$\qquad$方法二：


```stata
    //清除原有网络
    . nwclear
    //生成矩阵
    . mata: net = (0,1,0,0\1,0,0,1\1,1,0,0\1,1,1,0)
    //生成网络
    nwset, mat(net) name(network)
    //详细网络数据
    . nwset,detail
    (1 network)

--------------------------------------------------
 1) Current Network
--------------------------------------------------
   Network name: network
   Directed: true
   Nodes: 4
   Network id: 1
   Variables:  var1 var2 var3 var4
   Labels:  var1 var2 var3 var4
   Edgelabels: 
```
---

- 相似命令介绍

$\qquad$在`nwcommands`中包括非常多的命令，这些命令大多数与stata原本命令非常相似，如果你想使用某种在stata中也有类似功能的命令可以尝试输入 `nw + stata命令`。下面举几个例子。

```stata
    //清除网络
    nwclear

    //载入网络
    nwload

    //使用网络数据
    nwuse

    //在网页上寻找并使用网络
    webnwuse

    //储存网络
    nwsave

    //删除网络
    nwdrop

    //保存网络
    nwkeep

    //对网络进行描述
    nwsummarize
    nwtabulate
```
---

- 改变网络中数据

$\qquad$按照先后顺序介绍命令 `nwrecode` 和 `nwreplace`
```stata
    //引用外部网络
    webnwuse gang, clear
    //查看现有网络
    . nwset
    (3 networks)
--------------------
      network
      gang_valued
      gang
    //描述gang_valued网络
    . nwtabulate gang_valued
    Network:  gang_valued      Directed: false

gang_valued |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |      1,116       77.99       77.99
          1 |        182       12.72       90.71
          2 |         92        6.43       97.13
          3 |         25        1.75       98.88
          4 |         16        1.12      100.00
------------+-----------------------------------
      Total |      1,431      100.00
    //更改网络中2到4的值为99
    nwrecode gang_valued (2/4 = 99)
    //描述
    . nwtabulate gang_valued
    Network:  gang_valued      Directed: false

gang_valued |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |      1,116       77.99       77.99
          1 |        182       12.72       90.71
         99 |        133        9.29      100.00
------------+-----------------------------------
      Total |      1,431      100.00
```
---
```stata
    //引用新网络
    webnwuse florentine, nwclear
    //结果
    Loading successful
    (2 networks)
--------------------
      flobusiness
      flomarriage
    //描述marriage网络
    . nwtabulate flomarriage
    Network:  flomarriage      Directed: false

flomarriage |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |        100       83.33       83.33
          1 |         20       16.67      100.00
------------+-----------------------------------
      Total |        120      100.00
    //将marriage网络中flomarriage等于1中的部分替换为2
    nwreplace flomarriage = 2 if flobusiness == 1 & flomarriage ==1
    //查看改变后网络
    . nwtabulate flomarriage
    Network:  flomarriage      Directed: false

flomarriage |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |        100       83.33       83.33
          1 |         12       10.00       93.33
          2 |          8        6.67      100.00
------------+-----------------------------------
      Total |        120      100.00
```
---

### 2.3 计算网络节点特征

- 节点中心度

$\qquad$节点特征中最核心的是中心度(centrality)，中心度代表网络中一个节点与其他结点的关联程度，用数学公式来计算i点节点中心度为：
$C_{degree}\left(i\right)=\sum\limits_{j=1}^ny_{ij}$，
其中$y_{ij}$为上文介绍的网络表示矩阵的组成元素。

$\qquad$在stata中计算网络network节点中心度的命令为：
```stata
    nwdegree network
```
---


- 紧密中心度

$\qquad$节点特征中的紧密中心度(clossness centrality)是衡量个人（平均）与所有其他个人的距离有多近的指标。用数学公式来计算i点紧密中心度为：
$C_{clossness}\left(i\right)=\frac{N-1}{\sum\limits_{j=1}^nI_{ij}}$
，其中$I_{ij}$为i节点与j节点之间的最短距离。

$\qquad$在stata中计算网络network节点紧密中心度的命令为：
```stata
    nwclossness network
```
---

- 中介中心度

$\qquad$节点特征中的中介中心度(betweenness centrality)代表有多少最短路径经过该点。

$\qquad$在stata中计算网络network节点紧密中心度的命令为：
```stata
    nwbetween network
```
---

### 2.4 网络可视化

$\qquad$在 `nwcommands` 中可以运用命令 `nwplot` 进行网络可视化。
```stata
    //对网络gang作图，color代表颜色
    webnwuse gang
    nwplot gang, color(Birthplace)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig5_nwplot1_霍崇德.png)

---

```stata
    //网络图节点的区分由prison变量取值决定并且节点大小由变量arrests大小取值决定
    nwplot gang, color(Birthplace) symbol(Prison) size(Arrests)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig6_nwplot2_霍崇德.png)

---
```stata
    //lab表示在图中节点位置标注节点名
    webnwuse florentine
    nwplot flomarriage, lab
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig7_nwplot3_霍崇德.png)

---

```stata
    //nwplotmatrix作出矩阵图
    nwplotmatrix flomarriage, lab
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/网络分析_fig8_nwplot4_霍崇德.png)

---

$\qquad$更多可视化功能可以使用命令：
```stata
    help nwplot
```

nwcommands拥有非常具体的help文档，在实际使用中可以多使用`help nwcommands`进行查询。

## 3. 参考文献
- Grund, Thomas. “Social Network Analysis Using Stata.” Research Papers in Economics, 2015-更新版. [-PDF-](https://www.stata.com/meeting/italy15/abstracts/materials/it15_grund.pdf)