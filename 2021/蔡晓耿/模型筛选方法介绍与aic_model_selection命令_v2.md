&emsp; 

> **作者：** 蔡晓耿 (中山大学)   

> **邮箱：** <872149797@qq.com>  


&emsp;

---

**目录**
[[TOC]]

---

&emsp;


&emsp;

## 1. 模型筛选方法介绍

在检验假设或探索新想法时，我们可能会收集多个变量的数据，但并不确定哪些变量能够最好地解释因变量的变化，因此需要进行模型筛选工作。模型筛选是指针对给定的数据集，从一组候选模型中选出最佳拟合模型的过程，其三种常见方法包括：训练、验证、测试方法，重采样方法以及概率统计方法。

- 训练、验证、测试方法最为简单可靠，其过程包括在训练集上拟合候选模型，在验证集上调整模型，以及根据准确性或误差等指标选择在测试集上表现最好的模型，但这种方法需要大量的数据，且没有考虑过拟合问题。

- 重采样方法通过更小的数据集去实现前一方法的目标，如使用 K-Fold 交叉验证，将训练集划分为许多训练/测试组，每个模型将在各个组之间进行拟合和评估，最终选出平均表现最佳的模型。尽管重采样方法解决了前一方法需要大量数据的问题，但与前一方法类似，它们只评估模型性能，而忽略了模型太复杂带来的过拟合问题。

- 概率统计方法 (或信息准则) 同时考虑模型的简洁性和精确性，有效地缓解了过拟合问题。其中，模型性能通过对数似然评估，模型复杂性则通过模型的自由度或参数数量进行评估。概率统计方法的优点在于可以将所有数据用于拟合模型，不用划分为训练集、验证集和测试集，并可以直接对最终模型进行评分，但其也存在没有考虑模型的不确定性、最终选择的模型可能过于简单、必须自行设定每一模型的参数等缺点。

本推文将对概率统计方法中的常见方法即 AIC 和 BIC 规则进行详细介绍，并通过 `aic_model_selection` 命令在 Stata 中基于 AIC 和 BIC 规则进行模型筛选。


&emsp;

## 2. AIC、BIC 原理介绍

### 2.1 AIC 简介

AIC 规则即赤池信息准则 (Akaike Information Criterion)，于1973年由 "Hirotugu Akaike" 提出，它基于最大似然估计方法，以所选模型与真正模型之间的 Kullback-Leibler 距离最小为目标，选出最接近真实模型的模型。推导 AIC 准则我们需要先了解 FPE 准则以及 K-L 距离。

&emsp;

#### 2.1.1 FPE 准则

FPE 准则即最终预报误差准则 (Final Prediction Error)，用于自回归模型的定阶。在自回归模型 $AR(k)$ 中，我们用前 $k$ 期观测值的线性组合拟合当期序列值，如
$$
y_n=a_0+a_1y_{n-1}+\cdots+a_ky_{n-k}+\varepsilon_t, t\in Z \quad (1)
$$
然后通过选择合适的回归系数使得预报误差最小，即选择合适的 $a_i$，使得下式最小
$$
s_k^2=\frac{1}{N}\sum(y_n-a_0-a_1y_{n-1}-\cdots-a_ky_{n-k})^2 \quad (2)
$$
在确定 $a_i$ 后，我们用预报误差的平均值来评价模型拟合的优劣，并将其称为最终预报误差：
$$
FPE=E(y_{n}-\hat a_{k0}-\hat a_{k1}y_{n-1}-\cdots-\hat a_{kk}y_{n-k})^2 \quad (3)
$$
从而将模型的阶数 $k$ 的选择问题转化为 FPE 的极小化问题，这一思路被应用于 AIC 准则的推导。

&emsp;


#### 2.1.2 K-L 距离  
K-L 距离衡量相同事件空间里的两个概率分布的差异情况。设 $p$ 为实际概率分布，$q$ 为估计概率分布，则 K-L 距离为
$$
D(P||Q)=E(log\frac{P(X)}{Q(X)})=\int p(x)log\frac{P(X)}{Q(X)}dx \quad (4)
$$
两个概率分布之间的 K-L 距离越小，说明拟合效果越好。

&emsp;

#### 2.1.3 AIC 公式推导  

假设随机变量 $Y$ 的条件概率密度函数为 $f(y|\theta)$，其中 $\theta$ 为参数向量。现有 $Y$ 的一组观测值 $y_1$，$y_2$……$y_N$，则参数 $\theta$ 的似然函数为 
$$
L(\theta)=f(y_1|\theta)f(y_2|\theta)……f(y_N|\theta) \quad (5)
$$
使用极大似然法进行参数估计，即求出使似然函数 $L(\theta)$ 达到最大值的 $\hat\theta$。同时，参数 $\theta$ 的对数似然函数为
$$
l(\theta)=lnL(\theta)=\sum_{i=1}^{N}lnf(y_i|\theta) \quad (6)
$$
由于当 $N\rightarrow\infty$ 时，有
$$
\frac{1}{N}\sum_{i=1}^{N}lnf(y_i|\theta)\rightarrow Elnf(y_i|\theta) \quad (7)
$$
因此 $\hat\theta$ 也是使得 $Elnf(Y|\theta)$ 达到最大的 $\theta$ 的估计值。
&emsp;

现假设 $Y$ 的实际概率分布为 $g(y)=f(y|\theta_0)$，则 K-L 距离为：
$$
D(g(y)||f(y|\theta))=\int g(y)ln\frac{g(y)}{f(y|\theta)}dy=Elng(Y)-Elnf(Y|\theta) \quad (8)
$$
由于第一项 $Elng(Y)$ 为定值，因此使得 $Elnf(Y|\theta)$ 达到最大值的 $\hat\theta$ 同时也使得 K-L 距离达到最小值，这是极大似然法的本质。

&emsp;

类似于 FPE 准则，我们用 $E[D(g(y)||f(y|\hat\theta))]$ 衡量参数估计量的好坏，由于 $Elng(Y)$ 为常数，因此 K-L 距离最小化问题转化为：求 $\theta$ 的估计值 $\hat\theta$，使得 $EElnf(Y|\theta)$ 达到最大值。

假设从总体 $Y$ 中抽出的样本相互独立，当 $\lambda=\frac{maxl(\theta_0)}{maxl(\hat\theta)}$ 时，$-2lim_{N\rightarrow \infty}ln\lambda\sim\chi^2(k)$，$k$ 为 $\theta$ 的维数，则有：
$$
(2El(\theta))_{\theta=\hat\theta}-2El(\theta_0)=k \quad (9)
$$
同时，由 $E{(2El(\theta))_{\theta=\hat\theta}}=2NElnf(Y|\hat\theta)$ 的估计值进行无偏修正，最终得到 $2l(\hat\theta)-2k$。为了与 K-L 距离最小化保持一致，在前面取负号得到 AIC 准则的一般公式:
$$
AIC=-2l(\hat\theta)+2k \quad (10)
$$

其更为常见的形式为
$$
AIC= -2ln(L) + 2K \quad (11)
$$
其中， $K$ 是模型参数个数，$L$是似然函数。AIC 规则用 $-2ln(L)$ 反映模型的拟合情况，并引入参数个数的惩罚项 $2K$，当模型拟合情况越好、模型参数个数越少时，AIC 值越低，模型越接近于真实模型。如果一个模型的 AIC 值比另一个模型低 2 个单位以上，则认为它明显优于另一个模型。

&emsp;

### 2.2 BIC 简介

BIC 规则即贝叶斯信息准则 (Bayesian Information Criterion, or Schwarz Criterion)，1978 年由 Schwarz 提出，以其衍生研究领域 “贝叶斯概率和推断” 命名。BIC 规则适用于以最大似然估计的参数模型，并被用于选择最可能是真实模型的模型。

&emsp;

#### 2.2.1 贝叶斯公式
经典的贝叶斯公式如下：
$$
P(w|x)=\frac{p(x|w)p(w)}{p(x)} \quad (12)
$$
其中：$p(w)$ 为先验概率，指根据以往经验和分析得到的概率；$p(x|w)$ 为类条件概率，表示在某种类别的前提下某事发生的概率；而 $P(w|x)$ 为后验概率，指某件事已经发生，并且它属于某一类别的概率。

&emsp;

#### 2.2.2 BIC 公式推导
使用 BIC 准则筛选模型，即在给定数据 $\{y_j\}_{j=1}^{n}$ 下最大化模型 $(M_i)$ 的后验概率。
根据贝叶斯公式，有：
$$
P(M_i|y_1,\cdots,y_n)=\frac{P(y_1,\cdots,y_n|M_i)P(M_i)}{P(y_1,\cdots,y_n)} \quad (13)
$$
其中，$P(M_i|y_1,\cdots,y_n)$ 为模型后验概率；$P(y_1,\cdots,y_n|M_i)$ 为类条件概率，亦为模型的边缘概率；$P(M_i)$ 为先验概率。在给定数据 $\{y_j\}_{j=1}^{n}$ 时，$P(y_1,\cdots,y_2)$ 是定值，同时假设在数据未知时各个模型同样合理，即 $P(M_i)$ 也是定值。于是，最大化后验概率等价于最大化模型的边缘概率。

&emsp;

记 $f(y,\theta)$ 为参数模型密度函数，$\pi(\theta)$ 为 $\theta$ 的先验密度，$Y$ 与 $\theta$ 的联合密度为 $f(y,\theta)\pi (\theta)$，则 $Y$ 的边缘密度 (亦称为边缘似然) 为：
$$
\rho(y)=\int f(y,\theta)\pi(\theta)d\theta \quad (14)
$$
Schwarz 于1978年针对该式建立以下估计模型，如果模型 $f(y,\theta)$ 满足标准正则条件且先验密度函数 $\pi(\theta)$ 是扩散的 (即先验函数在参数空间上均匀分配权重)，则有：
$$
-2log\rho(Y)=-2ln(\hat\theta)+Klog(n)+\circ(1) \quad (15)
$$
其中，$\circ(1)$ 在 $n\rightarrow\infty$ 时有界。该式表明，边缘密度近似等于最大似然乘以一个基于估计参数数量和样本大小的调整项，而最大化边缘密度等价于最小化该式，其近似公式即为常见的 BIC 规则 ：
$$
BIC=-2ln(L) + Kln(N) \quad (16)
$$
其中， $K$ 是模型参数个数，$N$ 是样本数，$L$ 是似然函数。与 AIC 不同，BIC 更多地惩罚模型的复杂性，直观上体现为将 AIC 规则的参数个数惩罚项 $2K$ 替换为 $Kln(N)$，而当 $N$≥8 时，$ln(N)$>2，这意味着 BIC 规则更强调模型的简洁性，以防止追求模型高精度带来的模型复杂度过高。
&emsp;

### 2.3 小结

使用 AIC 和 BIC 规则，我们需要基于对研究系统的了解以及实验设计，创建一系列包含不同自变量组合的模型，然后通过计算、比较多个模型的 AIC 或 BIC 值，选出 AIC 值或 BIC 值最低的最佳模型，达到使用尽可能少的自变量解释尽可能大的变化量的目的。如果能够得到模型的对数似然值，则可以手动轻松计算 AIC 和 BIC 值，但计算似然函数相对复杂，其计算公式为：
$$
L=-\frac{N}{2} \times ln(2\pi )-\frac{N}{2} \times ln(\frac{SSE}{N})-\frac{N}{2} \quad (17)
$$
其中， $N$ 为样本量，$SSE$ 为残差平方和。通过 Stata 可以快速计算得到似然函数 (可在回归之后通过 `ereturn list` 命令查看)，也可以快速获取单个模型的 AIC 和 BIC 值 (可在回归后通过 `estimates stats` 命令查看)。而 `aic_model_selection` 命令能够方便快捷地计算出一系列模型的 AIC 值和 BIC 值，以下将对 `aic_model_selection` 命令进行详细介绍。注意：`aic_model_selection` 命令需要在 Stata 16.1 及以上版本使用。




&emsp;

## 3. aic_model_selection 命令介绍

在 `aic_model_selection` 命令之前， 常用的 Stata 模型筛选命令之一是全子集回归 (即最优子集回归)，即 `allpossible`。全子集回归通过对所有预测变量的可能组合模型进行拟合，并根据 Adjusted R<sup>2 </sup>，AIC 和 BIC 值等指标选出最佳模型。其优点是能够遍历所有可能的特征组合从而选出最优模型，但其缺点也十分明显，由于计算量过于庞大，我们无法对过多的变量进行全子集回归，`allpossible` 命令只允许用户输入最多 6 个自变量。


为了减少模型搜索空间，在全子集回归之后又出现了前向选择、后向选择等顺序算法，即以 p 值为停止标准，每次往模型中添加 (或删除) 一个变量，常用 Stata 命令为 `stepwise`。然而，顺序算法中计算得到的 p 值是错误的，因为它没有考虑选择过程，因此基于 p 值决定是否加入某一变量可能存在偏差。尽管如此，顺序算法中的前向选择生成的变量序列是可靠的，并被应用于 `aic_model_selection` 命令中。


`aic_model_selection` 基于前向选择生成的变量序列，按顺序每次添加一个自变量，对一系列模型进行回归并计算 AIC 或 BIC 值 。其基本用法为：

```stata
sw, pe(p_value): command_name varlist
aic_model_selection command_name new_varlist , [ bic ]
```

第一条命令是为了获取前向选择生成的新变量序列，其中 `sw` 是 `stepwise` 的缩写；`pe` 代表前向选择；`p_value` 指定前向选择中的停止标准，应尽可能大以避免错过最佳模型；`command_name` 可以是 `reg`，`logistic`，`ologit`，`probit`，`regress`，`streg` 等命令；`varlist` 包括因变量以及所有可能的预测变量。


第二条命令基于新变量序列计算 AIC 或 BIC 值，其中 `command_name` 需要与前一条命令对应；`new_varlist` 包括因变量以及经过第一条命令排序筛选后的预测变量序列；若指定选项 `bic`，则采用 BIC 规则进行计算，否则采用 AIC 规则进行计算。


&emsp;

## 4. 实例演示

为研究影响汽车价格的主要因素，我们从 Stata 自带数据集 auto.dta 中获取了 **price, rep78, headroom, trunk, weight, length, turn, displacement, gear_ratio, foreign** 10 个变量的数据。显而易见的是，如果将 9 个预测变量一起放入模型中，不仅模型的解释能力会下降，而且还可能因为参数过多而产生过拟合问题。以下采用 `aic_model_selection` 命令进行模型筛选。

导入数据集，进而对所有可能的控制变量进行前向选择：

```Stata
. sysuse auto, clear
local yx "price rep78 headroom trunk weight length turn displacement gear_ratio foreign"
. sw, pe(0.5): reg `yx'

Wald test, begin with empty model:
p = 0.0000 <  0.5000, adding displacement
p = 0.0000 <  0.5000, adding foreign
p = 0.0347 <  0.5000, adding weight
p = 0.0103 <  0.5000, adding length
p = 0.1561 <  0.5000, adding headroom
p = 0.2552 <  0.5000, adding turn
p = 0.3938 <  0.5000, adding trunk

      Source |       SS           df       MS      Number of obs   =        69
-------------+----------------------------------   F(7, 61)        =     12.83
       Model |   343444340         7  49063477.2   Prob > F        =    0.0000
    Residual |   233352619        61  3825452.77   R-squared       =    0.5954
-------------+----------------------------------   Adj R-squared   =    0.5490
       Total |   576796959        68  8482308.22   Root MSE        =    1955.9

------------------------------------------------------------------------------
       price | Coefficient  Std. err.      t    P>|t|     [95% conf. interval]
-------------+----------------------------------------------------------------
displacement |     12.439      7.296     1.70   0.093       -2.151      27.029
     foreign |   3451.008    714.976     4.83   0.000     2021.325    4880.691
      weight |      4.621      1.362     3.39   0.001        1.898       7.344
      length |    -73.522     38.779    -1.90   0.063     -151.065       4.021
    headroom |   -644.652    374.473    -1.72   0.090    -1393.457     104.153
        turn |   -129.616    116.658    -1.11   0.271     -362.889     103.656
       trunk |     79.236     92.253     0.86   0.394     -105.235     263.707
       _cons |   8454.464   4498.456     1.88   0.065     -540.754   17449.683
------------------------------------------------------------------------------
```

我们主要关注回归前的输出内容，此处各 p 值的计算方式如下：

- 第 3 行 $p\_value=0.0347$ 为模型 `reg price displacement foreign weight` 回归结果中 **weight** 变量的 p 值；
- 第 4 行 $p\_value=0.0103$ 为模型 `reg price displacement foreign weight length`  回归结果中 **length** 变量的 p 值；其他以此类推。
- 当 p 值小于命令规定的 0.5 时，前向选择将该变量纳入模型中。

基于这些 p 值，前向选择最终筛选出的最佳模型为 `reg displacement foreign weight length headroom turn trunk`，回归结果报告的正是这一模型。


而如上文所述，前向选择的筛选结果可能存在偏差，我们更关注其生成的变量序列，即 **displacement foreign weight length headroom turn trunk**，可以通过以下命令快速复制该变量序列：

```Stata
matrix list r(table)
```


获取变量序列后，我们执行以下核心命令计算 AIC 值：

```Stata
. global yx "price displacement foreign weight length headroom turn trunk"
. aic_model_selection reg 

AIC Model
1374.632 price displacement
1353.947 price displacement foreign
1345.396 price displacement foreign weight
1340.162 price displacement foreign weight length
1339.022 price displacement foreign weight length headroom
1338.922 price displacement foreign weight length headroom turn
1340.456 price displacement foreign weight length headroom turn trunk
```

由输出结果可以看出，`aic_model_selection` 命令按照变量序列每次添加一个控制变量，并根据公式 $AIC=2K-2ln(L)$ 计算得到 AIC 值，其中 AIC 值最小的模型为 `reg price displacement foreign weight length headroom turn`，即我们通过 AIC 规则筛选得到的最佳模型。


同理，我们可以采用 BIC 规则进行模型筛选，其命令如下：

```Stata
aic_model_selection reg $yx, bic

BIC Model
1379.240 price displacement
1360.859 price displacement foreign
1354.613 price displacement foreign weight
1351.683 price displacement foreign weight length
1352.846 price displacement foreign weight length headroom
1355.050 price displacement foreign weight length headroom turn
1358.888 price displacement foreign weight length headroom turn trunk
```

其中，BIC 值最小的模型为 `reg price displacement foreign weight length`，即我们通过 BIC 规则筛选得到的最佳模型。


三种筛选方法得到的最佳模型如下，可以看出，根据 AIC 和 BIC 规则筛选的最佳模型均不同于前向选择在 $p\_value<5$ 时筛选的模型，且 BIC 规则更倾向于选择简单的模型。

```
Wald test, begin with empty model:
p = 0.0000 <  0.8000, adding displacement
p = 0.0000 <  0.8000, adding foreign
p = 0.0347 <  0.8000, adding weight
p = 0.0103 <  0.8000, adding length      //BIC
p = 0.1561 <  0.8000, adding headroom
p = 0.2552 <  0.8000, adding turn        //AIC
p = 0.3938 <  0.8000, adding trunk       //前向选择
p = 0.5889 <  0.8000, adding rep78
p = 0.7193 <  0.8000, adding gear_ratio
```


&emsp;

## 5. 总结

本文简要介绍了 AIC、BIC 模型筛选方法以及 `aic_model_selection` 命令，并进行 Stata 实操。值得注意的是，尽管 AIC、BIC 规则在理论上十分漂亮，但在实际运用时仍可能存在一定的问题：我们只能筛选出现有组合中的最佳模型，但不能保证这个模型就能很好地刻画数据；此外，有时出于模型设定的需要，一部分变量在理论上必须控制，但在 AIC、BIC 规则下却可能将其剔除。因此，我们需要根据实际需要去判断是否使用 AIC、BIC 规则。


&emsp;

## 6. 参考资料

- M. Hashem Pesaran. Time Series and Panel Data Econometrics[C]. Oxford: Oxford University Press, 2015.
- Bruce E. Hansen. Econometrics[C]. Princeton: Princeton University Press, 2021.
- 陈晓锋. AIC准则及其在计量经济学中的应用研究[D].天津财经大学,2012: 19-21. [-Link-](https://t.cnki.net/kcms/detail?v=3uoqIhG8C475KOm_zrgu4lQARvep2SAkVR3-_UaYGQCi3Eil_xtLb7fUkIDP2fTFzIsRLm-wjNYxura9qF1zueMLz4QR2S7u)
- CSDN博客，作者名：白噪声序列，[AIC准则的理解](https://blog.csdn.net/weixin_43565540/article/details/106127755)
- CSDN博客，作者名：悟道修炼中，[理解赤池信息量（AIC）,贝叶斯信息量（BIC）](https://blog.csdn.net/chieryu/article/details/51746554)
- Stata帮助文件，作者名：Matthias Schonlau，[aic_model_selection: Model Selection with AIC](https://www.stata.com/meeting/canada21/slides/Canada21_Schonlau.pdf)
- 网文，作者名：Jason Brownlee，[Probabilistic Model Selection with AIC, BIC, and MDL](https://machinelearningmastery.com/probabilistic-model-selection-measures/)
- 网文，作者名：Rebecca Bevans，[An introduction to the Akaike information criterion](https://www.scribbr.com/statistics/akaike-information-criterion/)
- 网文，作者名：钱魏Way，[最优模型选择准则：AIC和BIC](https://www.biaodianfu.com/aic-bic.html)

## 7. 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
>
> &emsp; `lianxh AIC, m`
>
> &emsp; `lianxh 筛选, m`  
>
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace` 

- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [Stata检验：AIC-BIC-MSE-MAE-等信息准则的计算](https://www.lianxh.cn/news/fc6331df15b45.html)

- 专题：[回归分析](https://www.lianxh.cn/blogs/32.html)
  - [combinatorics：模型设定之自动筛选变量](https://www.lianxh.cn/news/a9daedde58729.html)
  - [gsreg：自动模型设定和变量筛选](https://www.lianxh.cn/news/61ae7a22439cf.html)

- 专题：[面板数据](https://www.lianxh.cn/blogs/20.html)
  - [ocmt：高维固定效应模型的变量筛选问题](https://www.lianxh.cn/news/5da27bc44470a.html)

- 专题：[PSM-Matching](https://www.lianxh.cn/blogs/41.html)
  - [Stata：psestimate-倾向得分匹配(PSM)中协变量的筛选](https://www.lianxh.cn/news/2da90377100cf.html)
  - [Stata：psestimate-倾向得分匹配(PSM)中匹配变量的筛选](https://www.lianxh.cn/news/af9a796f8d45c.html)

- 专题：[时间序列](https://www.lianxh.cn/blogs/28.html)
  - [vgets：VAR模型设定和筛选-T240](https://www.lianxh.cn/news/91c0b57b8e2fa.html)
