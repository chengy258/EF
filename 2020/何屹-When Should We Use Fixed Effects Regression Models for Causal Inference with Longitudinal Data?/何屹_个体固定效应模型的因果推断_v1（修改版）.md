&emsp;

> **作者**：何屹 (中山大学)    
> **E-Mail:** <heyi35@mail2.sysu.edu.cn> 

> **Source:** Imai, Kosuke, Kim, In Song. 2017. When Should We Use Fixed Effects Regression Models for Causal Inference with Longitudinal Data?. [-http://web.mit.edu/insong/www/pdf/FEmatch.pdf-](链接地址)

&emsp;

---

**目录**

[[TOC]]

---

&emsp; 

## 1. 简介

在大多数理论检验和政策评价中，推断变量间的因果关系是极为重要的。许多研究者在使用面板数据进行因果推断分析时，将个体固定效应模型作为他们的默认方法。那么，我们什么时候应该用个体固定效应回归模型来进行面板数据的因果推断呢？

实际上，这取决于我们更加关注不可观测的、不随时间变化的混淆变量还是结果变量与处理变量间的动态因果关系。当我们更关注前者时，个体固定效应模型是调整不可观测的、不随时间变化的混淆变量的有效工具；而当我们更关注后者时，基于边缘结构模型 (MSMs) 的“按可观测变量选择 (selection-on-observables) ”方法可能更有效地解释了动态因果关系。暂时没有方法可以在不增加额外假设的条件下，同时调整不可观测的、不随时间变化的混淆变量和动态因果关系。

Imai 和 Kim 的这篇文章首先对个体固定效应模型的基本因果假设进行分析，接着引入一种新的非参匹配框架，通过建立匹配估计量和加权个体固定效应估计量之间的等价关系，实现了多样的识别策略，在不存在动态因果关系的条件下调整不可观测的因素。

## 2. 个体固定效应模型的因果识别假设

### 2.1 线性个体固定效应模型

我们由基本的线性个体固定效应模型开始，进而拓展至非参的框架下对个体固定效应模型的因果假设进行分析。

假设一个均衡的、没有缺失的、包含 $N$ 个个体和 $T$ 期的面板数据集，个体均从一个总体中通过简单随机抽样得到。对于每个在时期 $t$ 的个体 $i$ ，我们观测到结果变量 $Y_{i t}$ 和二值处理变量 (也称“干预变量”) $X_{i t} \in\{0,1\}$ 。线性个体固定效应回归模型如下所示：

$$Y_{i t}=\alpha_{i}+\beta X_{i t}+\epsilon_{i t}  \quad (1)$$ 

在这个模型中，个体固定效应 $\alpha_{i}$ 捕获了一系列无法观测的、不随时间变化的混淆变量 (也称“混杂因素”) 。我们定义每个固定效应 $\alpha_{i}=h\left(\mathrm{U}_{i}\right)$ ，其中 $\mathrm{U}_{i}$ 代表了一系列无法观测的、不随时间变化的混淆变量， $h(\cdot)$ 是一个未知的函数。

为了得到 $\beta$ ，我们通常假设干扰项 $\epsilon_{i t}$ 符合严格的外生性，即：

$$\mathbb{E}\left(\epsilon_{i t} \mid \mathrm{X}_{i}, \alpha_{i}\right)=0  \quad (2)$$

因为 $\alpha_{i}$ 可以是 $\mathrm{U}_{i}$ 的任何函数，外生性假设也等同于 $\mathbb{E}\left(\epsilon_{i t} \mid \mathbf{X}_{i}, \mathbf{U}_{i}\right)=0$ 。

我们称基于 (1) 和 (2) 式的模型为 LIN-FE 。通过组内去心，我们得到 $\beta$ 的最小二乘估计量：

$$\begin{array}{r}
\hat{\beta}_{\text {LIN-FE }}=\underset{\beta}{\operatorname{argmin}} \sum_{i=1}^{N} \sum_{t=1}^{T}\left\{\left(Y_{i t}-\bar{Y}_{i}\right)\right.\left.-\beta\left(X_{i t}-\bar{X}_{i}\right)\right\}^{2}
\end{array}  \quad(3)$$

其中， $\bar{X}_{i}=\sum_{t=1}^{T} X_{i t} / T$ ， $\bar{Y}_{i}=\sum_{t=1}^{T} Y_{i t} / T$。如果面板数据符合 (1) 和 (2)，那么 $\hat{\beta}_{\text {LIN-FE }}$ 是 $\beta$ 的无偏估计量。

系数 $\beta$ 的含义是 $X_{i t}$ 对 $Y_{i t}$ 的平均同期效应。我们设定 $Y_{i t}(x)$ 代表个体 $i$ 在 $t$ 时期处理状态为 $X_{i t}=x$ ，$x=0,1$ 时对应的潜在结果。 (3) 式显示了处理变量没有变化的个体对 $\beta$ 的估计没有影响。因此，在 LIN-FE 下，因果效应的估计是处理状态有变化的个体间的平均处理效应：

$$\tau=\mathbb{E}\left(Y_{i t}(1)-Y_{i t}(0) \mid C_{i}=1\right)  \quad (4)$$

其中， $C_{i}=1\left\{0<\sum_{t=1}^{T} X_{i t}<T\right\}$ 。在潜在结果的线性假设下， $\beta=\tau$。

### 2.2 非参框架下的因果识别分析

本部分中，我们在非参固定效应模型 (NP-FE) 的框架下利用有向无环图 (DAGs) 分析个体固定效应模型的基本因果假设。

我们放宽 (1) 中的线性假设，并将 (2) 中的均值独立扩大至统计独立，得到如下的非参固定效应模型 ( NP-FE ) ：

**假设一 (非参固定效应模型)** ：_对于每个 $i=1,2, \ldots, N$ 和 $t=1,2, \ldots, T$ ，_

$$Y_{i t}=g\left(X_{i t}, \mathbf{U}_{i}, \boldsymbol{\epsilon}_{i t}\right)  \quad (5)$$

$$\epsilon_{i t} \Perp\left\{\mathrm{X}_{i}, \mathrm{U}_{i}\right\},  \quad (6)$$

_其中 $g(\cdot)$ 可以是任何函数。_

我们使用有向无环图 (DAGs) 来考察 NP-FE 的因果假设。 DAG 可以用来表示相应的非参结构方程模型，不需要函数形式和变量分布的假设，并且允许个体影响的异质性 [<sup>1,2</sup>](#refer-anchor)。简便起见，图 1 的 DAG 展示了三期的因果关系，但我们假设所有时期均存在着相同的因果关系。

<div  align="center">   
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/何屹_固定效应模型因果推断_图1.png" width = "220" height = "200" alt="图1" align=center />
</div>

<center>图 1 三期的个体固定效应模型有向无环图</center>

在图一的 DAG 中，黑色实线箭头表示可能存在的直接因果效应，没有箭头表示不存在直接因果效应的假设。此外，我们假设 DAGs 已包含了所有相关的、能被观测或不可观测的变量。因此，图 1 的 DAG 也假设不存在不可观测的、随时间变化的混淆变量。

通过图 1 的 DAG ，我们可以将 NP-FE 的假设一理解为以下四条假设：

    假设 (a) ：不存在不可观测的、随时间变化的混淆变量。
    假设 (b) ：过去的结果变量不直接影响现在的结果变量。
    假设 (c) ：过去的结果变量不直接影响现在的处理变量。
    假设 (d) ：过去的处理变量不直接影响现在的结果变量。

接着，我们采用潜在结果框架对分配机制的假设进行说明 [<sup>3,4</sup>](#refer-anchor)。我们将假设 (d) 称为不存在延滞效应，用数学公式表达如下：

**假设二 (无延滞效应)** ：_对于每个 $i=1,2, \ldots, N$ 和 $t=1,2, \ldots, T$ ，潜在结果为：_

$$Y_{i t}\left(X_{i 1}, X_{i 2}, \ldots, X_{i, t-1}, X_{i t}\right)=Y_{i t}\left(X_{i t}\right)$$

我们将假设的随机化实验分配机制表达如下：
 
**假设三 (序列可忽略性 (非混杂性) )** ：_对于每个 $i=1,2, \ldots, N$ 和 $t=1,2, \ldots, T$ ，_

$$\begin{array}{l}
\left\{Y_{i t}(1), Y_{i t}(0)\right\}_{t=1}^{T} \Perp X_{i 1} \mid \mathrm{U}_{i} \\
\vdots \\
\left\{Y_{i t}(1), Y_{i t}(0)\right\}_{t=1}^{T} \Perp X_{i t^{\prime}} \mid X_{i 1}, \ldots, X_{i, t^{\prime}-1}, \mathrm{U}_{i}, \\
\vdots \\
\left\{Y_{i t}(1), Y_{i t}(0)\right\}_{t=1}^{T} \Perp X_{i t} \mid X_{i 1}, \ldots, X_{i, T-1}, \mathrm{U}_{i}
\end{array}$$

假设三意味着给定历史处理变量和 $\mathrm{U}_{i}$ 的条件下，当期的处理变量独立于任意时期的潜在结果，对应着 NP-FE 的假设 (a) 与 (c) 和 (2) 式所示的 LIN-FE 的严格外生性假设。举例来说 [<sup>4</sup>](#refer-anchor)，个体不会根据自己任意时期的潜在结果 (在捕鱼或狩猎上的生产率) 决定当期处理变量的取值 (是进行捕鱼还是狩猎) ，即假设 (c)。另一方面，例如在考察教育对个人收入的影响时，个体的处理变量 (教育水平) 取值是随机化的，与其他未观测因素 $\epsilon_{i t}$ (其他影响收入的因素) 不相关，满足外生性假设。由大数定律可知，随机化的关键作用是可以平衡个体间其他因素的分布，从而消除选择性偏差，使得个体间具有可比性。

### 2.3 哪些因果识别假设可以被放宽？

在固定效应模型中，不存在不可观测的、随时间变化的混淆变量的假设 (假设 (a) ) 是较难放宽的。因此，我们对其他三项识别假设 (假设 (b) 、 (c) 和 (d) ) 进行探讨。

首先，假设 (b) 是可以被放宽的。假设过去的结果变量可以直接影响现在的结果变量 (如图 2 (a) 所示) ，在这种情形下，过去的结果变量通常不会混淆现在的处理变量和现在的结果变量之间的因果关系，因为过去的结果变量没有直接影响现在的处理变量。

接着，我们设想一下过去的处理变量可以直接影响现在的结果变量的情景，即放宽假设 (d) 。通常来说，研究者通过将处理变量的滞后项加入模型来中解决这个问题。图 2 (b) 的 DAG 概括了上述模型：

$$Y_{i t}=g\left(X_{i 1}, \ldots, X_{i t}, \mathbf{U}_{i}, \boldsymbol{\epsilon}_{i t}\right)  \quad (7)$$

在这个模型下，假设三依旧成立。图 1 和图 2 (b) 中的 DAG 唯一的差别在于，在后者中，我们必须调整过去的处理变量，因为它们混淆了现在的处理变量和结果变量间的因果关系。

但是，我们无法同时非参调整所有过去的处理变量和不可观测的、不随时间变化的混淆变量 $\mathbf{U}_{i}$ 。非参调整即根据混淆变量进行精确匹配，对于处理组个体找到相同特征的控制组个体，匹配后，利用控制组结果作为处理组反事实结果的估计。

为了调整 $\mathbf{U}_{i}$ ，我们需要根据 $\mathbf{U}_{i}$ 进行匹配，这要求我们比较**同一组内**不同时期的处理组和控制组观测。然而，如 (8) 式所示，同一组内不同时期的观测不具有相同的处理历史。

因此，在实际操作中，研究者通常加入几期滞后项到模型中。然而，加入模型的处理变量滞后项的数量通常是随意选取且很少有实际证据支撑的。

<div  align="center">   
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/何屹_固定效应模型因果推断_图2.jpg" width = "300" height = "300" alt="图2" align=center />
</div>

<center>图 2 个体固定效应模型的因果识别假设</center>

最后，我们考虑放宽假设 (c) 的情形，即过去的结果变量可以直接影响现在的处理变量。如图 2 (c) 所示，这违背了假设三，因为过去的干扰项和现在的处理变量间存在着相关关系，导致内生性。

为了解决这个问题，通常会在线性个体固定效应模型中加入结果变量的滞后项：

$$Y_{i t}=\alpha_{i}+\beta X_{i t}+\rho Y_{i, t-1}+\epsilon_{i t}  \quad (8)$$

图 2 (d) 中的 DAG 对应着 (8) 式所示的模型。这个模型的识别策略建立在工具变量的基础上。然而，每个工具变量的有效性依赖于其对结果变量没有直接因果效应的假设。在实际操作中，这些假设并没有实际证据的支撑。

**总的来说， LIN-FE 和其非参数拓展形式 NP-FE 需要三项核心的因果识别假设：**

    ①不存在不可观测的、随时间变化的混淆变量。
    ②过去的处理变量不影响现在的结果变量。
    ③过去的结果变量不影响现在的处理变量。

### 2.4 加入可观测的、随时间变化的混淆变量

由于固定效应模型只能调整不随时间变化的、不可观测的混淆变量，研究者通常在模型中加入一系列可观测的、随时间变化的混淆变量 $\mathrm{Z}_{i t}$ 作为协变量来增强假设的可信度。即使在这种情况下，上述因果识别分析仍保持不变。然而，我们必须额外假设结果变量和可观测的、随时间变化的混淆变量之间没有动态因果关系。

**假设四 (加入 $\mathrm{Z}_{i t}$ 的非参固定效应模型)**：_对于每个 $i=1,2, \ldots, N$ 和 $t=1,2, \ldots, T$ ，_

$$Y_{i t}=g\left(X_{i 1}, \ldots, X_{i t}, \mathbf{U}_{i}, Z_{i 1}, \ldots, Z_{i t}, \epsilon_{i t}\right)，  \quad (9)$$

$$\boldsymbol{\epsilon}_{i t} \Perp\left\{\mathbf{X}_{i}, \mathbf{Z}_{i}, \mathbf{U}_{i}\right\}  \quad (10)$$

_其中， $\mathbf{Z}_{i}=\left(\mathrm{Z}_{i 1} \mathrm{Z}_{i 2} \ldots \mathrm{Z}_{i t}\right)$ 。_

在这个模型中，只有同期的 $\mathrm{Z}_{i t}$ 和 $\mathbf{U}_{i}$ 会混淆 $X_{i t}$ 和 $Y_{i t}$ 间的同期因果关系。过去的处理变量和过去的随时间变化的混淆变量不需要被调整，因为它们不直接影响现在的结果变量 $Y_{i t}$ 。

<div  align="center">   
<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/何屹_固定效应模型因果推断_图3.jpg" width = "220" height = "200" alt="图3" align=center />
</div>

<center>图 3 (对应假设四) 三期的个体固定效应模型有向无环图</center>

现在，假设 $\mathrm{Z}_{i t}$ 可以直接影响未来和现在的结果变量 $Y_{i t^{\prime}}$ ，其中 $t^{\prime} \geq t$ 。在这种情形下，我们需要通过加入相关的混淆变量滞后项 ($\mathrm{Z}_{i t^{\prime}}$ ， $t^{\prime}<t$) 来调整过去和同期的 $\mathrm{Z}_{i t}$ 。然而，我们无法同时非参地调整过去随时间变化的混淆变量的整个序列和 $\mathbf{U}_{i}$ 。

进一步地，与不存在 $\mathrm{Z}_{i t}$ 的 NP-FE 类似，如果结果变量 $Y_{i t}$ 直接或间接通过 $\mathrm{Z}_{i t^{\prime}}$ 影响未来的处理变量 $X_{i t^{\prime}}$ ($t^{\prime}>t$) ，则 $\epsilon_{i t}$ 和 $\mathrm{Z}_{i t^{\prime}}$ 间存在相关关系，因此违背了假设四。

上述的讨论表明，无论 $\mathrm{Z}_{i t}$ 是否存在，研究者都面临着相同的权衡：选择调整不可观测的、不随时间变化的混淆变量还是动态因果关系。

## 3. 新的非参匹配框架

**因果推断的重点在于如何通过比较处理组和控制组的观测值，可信地估计反事实结果。** 对于一个处理组观测，我们实际观测到处理状况下的结果，但是我们需要使用观测到的控制组观测的结果来推断处理组观测的反事实结果。匹配是一种非参数方法，通过找到一组与每个处理组观测相似的控制组观测来估计反事实结果。

本部分中，我们提出了一种组内匹配估计量，放宽了固定效应估计量的线性假设。尽管动态因果关系和不可观测的、不随时间变化的混淆变量间的权衡是不可避免的，放宽线性个体固定效应模型的函数形式假设可以在识别假设满足时得到更稳健的推断。

### 3.1 组内匹配估计量的一致性

**命题 1 (线性固定效应估计量的不一致性)** [<sup>5</sup>](#refer-anchor)：_假设 $\mathbb{E}\left(Y_{i t}^{2}\right)<\infty$ 且 $\mathbb{E}\left(C_{i} S_{i}^{2}\right)>0$ ，其中 $S_{i}^{2}=\sum_{t=1}^{T}\left(X_{i t}-\bar{X}_{i}\right)^{2} /(T-1)$ 。在假设二、三和简单随机抽样的条件下，(3) 式中的线性固定效应估计量不是 (4) 式定义的平均处理效应的一致估计量：_

$$\hat{\beta}_{\mathrm{LIN}-\mathrm{FE}} \stackrel{p}{\longrightarrow} \frac{\mathbb{E}\left\{C_{i}\left(\frac{\sum_{i=1}^{T} X_{i t} Y_{i t}}{\sum_{i=1}^{T} X_{i t}}-\frac{\sum_{i=1}^{T}\left(1-X_{i t}\right) Y_{i t}}{\sum_{i=1}^{T} 1-X_{i t}}\right) S_{i}^{2}\right\}}{\mathbb{E}\left(C_{i} S_{i}^{2}\right)} \neq \tau$$

之前的讨论发现，在假设二、三下，即使 $\mathbf{U}_{i}$ 是不可观测的，但我们可以通过比较同一组内不同时期的处理组和控制组观测来非参数地调整它们。这种组内比较的方法促使了以下匹配估计量的产生：

$$\begin{aligned}
\hat{\tau}=\frac{1}{\sum_{i=1}^{N} C_{i}} & \sum_{i=1}^{N} C_{i}\left(\frac{\sum_{t=1}^{T} X_{i t} Y_{i t}}{\sum_{t=1}^{T} X_{i t}}\right.\left.-\frac{\sum_{t=1}^{T}\left(1-X_{i t}\right) Y_{i t}}{\sum_{t=1}^{T}\left(1-X_{i t}\right)}\right)
\end{aligned}  \quad (11)$$

其中， $C_{i}=1\left\{0<\sum_{t=1}^{T} X_{i t}<T\right\}$ 。这个匹配估计量不需要线性假设，在假设二和三的条件下，其是 (4) 式定义的 ATE (Average Treatment Effect) 的一致估计量。

进一步地，我们定义匹配集合 $\mathcal{M}_{i t}$ ，包含与观测 $(i, t)$ 匹配的观测。例如，在 (11) 式的估计量中，每个处理组 (控制组) 观测与同一组内所有的控制组 (处理组) 观测相匹配。此时，匹配集合为：

$$\mathcal{M}_{i t}=\left\{\left(i^{\prime}, t^{\prime}\right): i^{\prime}=i, X_{i^{\prime} t^{\prime}}=1-X_{i t}\right\}  \quad (12)$$

我们的匹配框架能够通过使用不同的匹配集合来实现多种识别策略。对于任意给定的匹配集合 $\mathcal{M}_{i t}$  ，我们可以定义对应的组内匹配估计量 $\hat{\tau}$ ：

$$\hat{\tau}=\frac{1}{\sum_{i=1}^{N} \sum_{t=1}^{T} D_{i t}} \sum_{i=1}^{N} \sum_{t=1}^{T} D_{i t}\left(\widehat{Y_{i t}(1)}-\widehat{Y_{i t}(0)}\right)  \quad (13)$$

其中，当 $X_{i t}=x$ 时， $Y_{i t}(x)$ 可以被观测；当 $X_{i t}=1-x$ 时，我们使用 $(i, t)$ 匹配集合的平均结果来估计 $Y_{i t}(x)$ ：

$$\widehat{Y_{i t}(x)}=\left\{\begin{array}{ll}
Y_{i t} & \text { if } X_{i t}=x \\
\frac{1}{\left|\mathcal{M}_{i t}\right|} \sum_{\left(i^{\prime}, t^{\prime}\right) \in \mathcal{M}_{i t}} Y_{i^{\prime} t^{\prime}} & \text { if } X_{i t}=1-x
\end{array}\right.  \quad (14)$$

其中， ${\left|\mathcal{M}_{i t}\right|}$ 代表了匹配集合中观测的个数， $D_{i t}=1\left\{\left|\mathcal{M}_{i t}\right|>0\right\}$ 。在 (12) 式定义的匹配集合中，可以发现对任何 $t$ ， $D_{i t}=C_{i}$ 。

### 3.2 基于组内比较的识别策略

#### 3.2.1 根据协变量 $\mathrm{Z}_{i t}$ 进行匹配

正如在回归模型中加入混淆变量作为控制变量可以消除模型的混杂偏差 (confounding bias) ，当根据 $\mathrm{Z}_{i t}$ 进行匹配可以消除混杂偏差时，组内最近邻匹配估计量是 ATE 的一致估计量。

此时，组内最近邻匹配对应的匹配集合为：

$$\begin{aligned}
\mathcal{M}_{i t}=\left\{\left(i^{\prime}, t^{\prime}\right)\right.&: i^{\prime}=i,X_{i^{\prime} t^{\prime}} \left.=1-X_{i t}, \mathcal{D}\left(\mathrm{Z}_{i t}, \mathrm{Z}_{i^{\prime} t^{\prime}}\right)=J_{i t}\right\}
\end{aligned}$$

其中，$\mathcal{D}(\cdot, \cdot)$ 是一种距离测度 (如马氏距离) ，并且

$$J_{i t}=\min _{\left(i, t^{\prime}\right) \in \mathcal{M}_{i t}} \mathcal{D}\left(Z_{i t}, Z_{i t^{\prime}}\right)$$

有了匹配集合后，组内最近邻匹配估计量可以通过 (13) 式进行计算。 

#### 3.2.2 "Before-and-After Design"

我们设计事前-事后 (Before-and-After, BA ) 比较方法，其中，我们假设平均潜在结果在短时间内没有时间趋势。 由于 BA 还需要无延滞效应的假设，对于一个给定的个体，处理状态只变化一次时， BA 可能最为有用。在 BA 下，我们比较处理状态变化前后紧接着的两个结果。无时间趋势的假设表述如下：

**假设五 (事前-事后设计)：**_对于 $i=1,2, \ldots, N$ 和 $t=1,2, \ldots, T$ ，_

$$\begin{aligned}
& \mathbb{E}\left(Y_{i t}(x)-Y_{i, t-1}(x) \mid X_{i t} \neq X_{i, t-1}\right)=0 \end{aligned}$$ 

_其中，$x \in\{0,1\}$ 。_

在假设二和假设五下，处理状态变化前后结果的平均差异是局部 ATE 的有效估计量，即 $\mathbb{E}\left(Y_{i t}(1)-Y_{i t}(0) \mid X_{i t} \neq X_{i, t-1}\right)$ 。

为了在我们的匹配框架下实施 BA 设计，我们比较两段紧挨着的有着相反处理状态的时期内的观测。此时，匹配集合如下：

$$\mathcal{M}_{i t}=\left\{\left(i^{\prime}, t^{\prime}\right): i^{\prime}=i, t^{\prime}=t-1, X_{i^{\prime} t^{\prime}}=1-X_{i t}\right\}  \quad (15)$$

很明显，这个匹配估计量等价于一阶差分 (FD) 估计量：

$$
\begin{aligned}
\hat{\beta}_{\mathrm{FD}}=\underset{\beta}{\operatorname{argmin}} \sum_{i=1}^{N} \sum_{t=2}^{T}\{&\left(Y_{i t}-Y_{i, t-1}\right) \left.-\beta\left(X_{i t}-X_{i, t-1}\right)\right\}^{2}
\end{aligned}
\quad (16)$$

我们可以将 BA 一般化以便使用较多的滞后项来估计反事实结果。 $L$ 代表滞后项的数量，则匹配集合为：

$$
\begin{aligned}
\mathcal{M}_{i t}=\left\{\left(i^{\prime}, t^{\prime}\right): i^{\prime}\right.&=i, t^{\prime} \in\{t-1, \ldots, t-L\} ，X_{i^{\prime} t^{\prime}} \left.=1-X_{i t}\right\}
\end{aligned}
\quad (17)$$

进一步地，我们可以估计长期 ATE ，即：

$$
\mathbb{E}\left\{Y_{i, t+F}(1)-Y_{i, t+F}(0) \mid X_{i t} \neq X_{i, t-1}\right\}
\quad (18)$$

其中， $F$ 为非负整数。我们采用以下的匹配估计量估计在 $X_{i t}=x$ 时第 $t+F$ 期的潜在结果：

$$\hat{\tau}=\frac{1}{\sum_{i=1}^{N} \sum_{t=L+1}^{T-F} D_{i t}} \sum_{i=1}^{N} \sum_{t=L+1}^{T-F} D_{i t}\left(Y_{i, t+F}(1)-Y_{i, t+F}(0)\right)  \quad (19)$$ 

其中，

$$\widehat{Y_{i, t+F}(x)}=\left\{\begin{array}{ll}
Y_{i, t+F} & \text { if } X_{i t}=1-X_{i, t-1}=x \\
\frac{1}{\left|\mathcal{M}_{i t}\right|} \sum_{\left(i^{\prime}, t^{\prime}\right) \in \mathcal{M}_{i t}} Y_{i^{\prime} t^{\prime}} & \text { if } X_{i t}=1-x
\end{array}\right.
$$

$$
D_{i t}=\left\{\begin{array}{ll}
1 & \text { if } X_{i t}=1-X_{i, t-1} \\
0 & \text { otherwise }
\end{array}\right.
\quad (20)$$

## 4. 加权个体固定效应估计量

### 4.1 加权个体固定效应估计量与组内匹配估计量的等价关系

**定理一 (组内匹配估计量与加权个体固定效应估计量)：** _匹配集合 $\mathcal{M}_{i t}$ 定义的任意组内匹配估计量 $\hat{\tau}$ 等于加权线性固定效应估计量：_

$$
\begin{aligned}
\hat{\beta}_{\text {WFE }}=\underset{\beta}{\operatorname{argmin}} \sum_{i=1}^{N} \sum_{t=1}^{T} W_{i t} &\left\{\left(Y_{i t}-\bar{Y}_{i}^{*}\right)\right.\left.-\beta\left(X_{i t}-\bar{X}_{i}^{*}\right)\right\}^{2}
\end{aligned}
\quad (21)$$

_其中， $\bar{X}_{i}^{*}=\sum_{t=1}^{T} W_{i t} X_{i t} / \sum_{t=1}^{T} W_{i t}$ , $\bar{Y}_{i}^{*}=\sum_{t=1}^{T} W_{i t} Y_{i t} / \sum_{t=1}^{T} W_{i t}$ ，权重为：_

$$
W_{i t}=D_{i t} \sum_{i^{\prime}=1}^{N} \sum_{t^{\prime}=1}^{T} w_{i t}^{i^{\prime} t^{\prime}}
$$

其中，

$$
w_{i t}^{i^{\prime} t^{\prime}}=\left\{\begin{array}{ll}
1 & \text { if }(i, t)=\left(i^{\prime}, t^{\prime}\right) \\
1 /\left|\mathcal{M}_{i^{\prime} t^{\prime}}\right| & \text { if }(i, t) \in \mathcal{M}_{i^{\prime} t^{\prime}} \\
0 & \text { otherwise. }
\end{array}\right.
\quad (22)$$

由定理一可以得到几个实用的启示：

1. **即使个体数量很多，我们也可以有效率地计算组内匹配估计量。** 我们可以先让每个变量减去其组内加权平均 (即 (21) 式中的 $\bar{Y}_{i}^{*}$ 和 $\bar{X}_{i}^{*}$ ) ，然后对这些“去权”的变量进行加权回归，从而得到加权线性固定效应估计量。

2. **利用定理一中的等价关系，我们可以使用多种基于模型的稳健标准误对组内匹配估计量进行统计推断。**

3. **即使线性假设成立，组内匹配估计量仍是 ATE 的一致估计量。** 这使得我们可以基于加权最小二乘和不加权最小二乘估计量间的差异进行模型设定检验，其中，原假设是线性个体固定效应模型是正确的。

### 4.2 权重如何确定？

在定理一中，我们说明了组内匹配估计量与加权个体固定效应估计量间的等价关系，那么 WFE 中的匹配权重 $w_{i t}^{i^{\prime} t^{\prime}}$ 和回归权重 $W_{i t}$ 该如何确定呢？

首先，我们从公式上进行分析：对于给定的观测 $(i,t)$ ，我们将它和所有的观测进行比较 ( $(i^{\prime}, t^{\prime})$ ， $i^{\prime}=1,2, \ldots, N$ 和 $t^{\prime}=1,2, \ldots, T$ ) 。如果 $(i,t)$ 恰好等于 $(i^{\prime}, t^{\prime})$ ，即当 $(i,t)$ 与自身比较时，那么匹配权重等于 1 ；如果 $(i,t)$ 属于 $(i^{\prime}, t^{\prime})$ 对应的匹配集合时，那么匹配权重等于 1 除以匹配集合中元素的个数；如果 $(i,t)$ 不符合上述两种情况的话，匹配权重为 0 。而回归权重就等于 $(i,t)$ 与所有观测进行比较的匹配权重之和。

我们举个例子进行更加具体的说明：

假设有一面板数据包含 100 家公司 ( $i = 1,2, \ldots, 100$ ) 和每家公司 10 年 ( $t = 1,2, \ldots, 10$ ) 的财务状况 (结果变量 $Y$ ) 和是否进行数字化转型升级 (处理变量 $X$ ) 。

$$
X_{i t}=\left\{\begin{array}{ll}
1 & \text { 公司进行了数字化转型升级 } \\
0 & \text { 公司未进行数字化转型升级 }
\end{array}\right.
$$

同时，简便起见，我们假设其符合 BA 设计 (无时间趋势) 且公司的数字化转型升级是一个不可逆的过程，即处理变量只会从 0 变为 1 且最多只变化一次。

我们采用 (11) 式的估计量来估计公司数字化转型升级对财务状况的平均当期影响：

$$\begin{aligned}
\hat{\tau}=\frac{1}{\sum_{i=1}^{N} C_{i}} & \sum_{i=1}^{N} C_{i}\left(\frac{\sum_{t=1}^{T} X_{i t} Y_{i t}}{\sum_{t=1}^{T} X_{i t}}\right.\left.-\frac{\sum_{t=1}^{T}\left(1-X_{i t}\right) Y_{i t}}{\sum_{t=1}^{T}\left(1-X_{i t}\right)}\right)
\end{aligned}$$

其中， $C_{i}=1\left\{0<\sum_{t=1}^{T} X_{i t}<T\right\}$ 。(11) 式对应的匹配集合如下：

$$\mathcal{M}_{i t}=\left\{\left(i^{\prime}, t^{\prime}\right): i^{\prime}=i, X_{i^{\prime} t^{\prime}}=1-X_{i t}\right\}$$

我们假设第 1 家公司在第 5 年进行了数字化转型升级，即 $X_{1 1}=X_{1 2}=X_{1 3}=X_{1 4}=0$ ， $X_{1 5}=X_{1 6}= \ldots =X_{1, 10}=1$ 。对于观测 $(i, t)=( 1 , 1 )$ ，当 $i^{\prime} \not = 1$ 时，显然 $(i, t)\not =(i^{\prime}, t^{\prime})$ ，并且 $(i, t)$ 不属于 $\mathcal{M}_{i^{\prime} t^{\prime}}$ ，因为 $(i, t)$ 和 $(i^{\prime}, t^{\prime})$ 不属于同一组。因此，匹配权重为 0 。当 $i^{\prime}=1$ 时，我们考虑 $t$ 的取值： $t^{\prime}=1$ 时， $(i, t)=(i^{\prime}, t^{\prime})$ ，因此匹配权重为 1 ；当 $t^{\prime}=2,3,4$ 时， $\mathcal{M}_{i^{\prime} t^{\prime}}$ 包含了组内所有处理组观测，即 $(1,5),(1,6),\ldots,(1,10)$ ，因此此时匹配权重为 0 ； 当 $t^{\prime}=5,6,\ldots,10$ 时， $\mathcal{M}_{i^{\prime} t^{\prime}}$ 包含了组内所有控制组观测，此时匹配权重为 $1 /|\mathcal{M}_{i^{\prime} t^{\prime}}|$ ，即 $\frac{1}{4}$ 。

接着，我们对 $(1,1)$ 的回归权重进行分析。由于第 1 家公司处理状况有变化，所以 $(1,1)$ 对应的匹配集合不为空集，即 $D_{i t} = 1$ 。回归权重等于匹配权重之和，因此在本例中， $(1,1)$ 的回归权重等于 $1+\frac{1} {4}\times 6 = \frac{5} {2}$ ，对于其他的观测，可以采用同样的方法计算其匹配权重与回归权重。同时，我们注意到由于 $W_{i t}=D_{i t} \sum_{i^{\prime}=1}^{N} \sum_{t^{\prime}=1}^{T} w_{i t}^{i^{\prime} t^{\prime}}$ ，因此如果某家公司的处理状况没有变化 (即其在观测窗口间没有进行数字化转型升级或在观测窗口前已进行过数字化转型升级) ，那么对于这家公司的所有观测， $D_{i t}=0$ , $t=1,2,\ldots,10$ ，进而 $W_{i t}=0$ 。我们可以认为在计算估计量时这家公司的所有观测先被自动删掉了，因此非参匹配估计量使用的观测数量远少于线性固定效应模型。

通过上述说明，我们了解了如何计算匹配权重和回归权重。实际上，匹配权重就是在估计反事实结果时使用的观测数量的倒数，即 $(i,t)$ 对于估计 $(i^{\prime}, t^{\prime})$ 的处理效应的贡献。我们考虑组内匹配估计量的一般形式 (如 (13) 式所示)：

$$\hat{\tau}=\frac{1}{\sum_{i=1}^{N} \sum_{t=1}^{T} D_{i t}} \sum_{i=1}^{N} \sum_{t=1}^{T} D_{i t}\left(\widehat{Y_{i t}(1)}-\widehat{Y_{i t}(0)}\right)$$

其中，当 $X_{i t}=x$ 时， $Y_{i t}(x)$ 可以被观测；当 $X_{i t}=1-x$ 时，我们使用 $(i, t)$ 匹配集合的平均结果来估计 $Y_{i t}(x)$ ：

$$\widehat{Y_{i t}(x)}=\left\{\begin{array}{ll}
Y_{i t} & \text { if } X_{i t}=x \\
\frac{1}{\left|\mathcal{M}_{i t}\right|} \sum_{\left(i^{\prime}, t^{\prime}\right) \in \mathcal{M}_{i t}} Y_{i^{\prime} t^{\prime}} & \text { if } X_{i t}=1-x
\end{array}\right.$$

还是使用上述举例的面板数据：对于第 1 家公司第 5-10 年的数据，我们只能观测到数字化转型升级后公司的财务状况，我们需要通过其对应的匹配集合中观测结果的平均数来估计反事实结果。更为具体的，对于观测 $(1,5)$ ， $X_{1 5}=1$ ， $\widehat{Y_{1 5}(1)} = Y_{1 5}$ ， $\widehat{Y_{1 5}(0)} = \frac{1} {4}(Y_{1 1}+Y_{1 2}+Y_{1 3}+Y_{1 4})$ ，而 $w_{1 1}^{1 5}$ 恰好等于 $\frac{1} {4}$ 。

### 4.3 模拟时间趋势

定理一也可以用来增强 BA 设计的可信度。上述可知， BA 设计需要无时间趋势的假设 (假设五) ，从而之前时期的结果可以被用来估计之后处理状态变化时的平均反事实结果。然而，在实际中，这个识别假设可能是不成立的，尤其是当我们估计如 (19) 式定义的长期平均处理效应。为了解决这个问题，一个可行的方法是利用匹配估计量和加权最小二乘估计量之间的等价关系，使用实施处理前的观测来构造时间趋势。

例如，研究者可以使用如下的预先 $F$ 期的加权最小二乘估计量：

$$
\begin{array}{r}
\hat{\beta}=\underset{\beta}{\operatorname{argmin}} \sum_{i=1}^{N} \sum_{t=1}^{T} W_{i t}\left\{\left(Y_{i t}-\bar{Y}_{i}\right)\right.\left.-\beta\left(X_{i t}-\bar{X}_{i}\right)-f_{\gamma}(t)\right\}^{2}
\end{array}
\quad (23)$$

其中，$f_{\gamma}(t)$ 是含参的时间趋势模型 (例如， $f_{\gamma}(t)=\gamma_{1}(t-\bar{t})+\gamma_{2}\left(t^{2}-\bar{t}^{2}\right)$) ， $\bar{t}$ 代表平均年限， $W_{i t}$ 是基于 (17) 式定义的匹配集合通过定理一计算出的权重：

$$
W_{i t}=D_{i t} \sum_{i^{\prime}=1}^{N} \sum_{t^{\prime}=1}^{T} w_{i t}^{i^{\prime} t^{\prime}}
$$

其中，

$$
w_{i t}^{i^{\prime} t^{\prime}}=\left\{\begin{array}{ll}
1 & \text { if }(i, t)=\left(i^{\prime}, t^{\prime}+F\right) \\
1 /\left|\mathcal{M}_{i^{\prime} t^{\prime}}\right| & \text { if }(i, t) \in \mathcal{M}_{i^{\prime} t^{\prime}} \\
0 & \text { otherwise }
\end{array}\right.
\quad (24)$$

 $\mathcal{M}_{i t}$ 如 (17) 式所示。尽管上述模型假设所有个体的时间趋势是相同的，如果时期的数量足够，我们也可以对每个个体构造不同的时间趋势：将 $f_{\gamma}(t)$ 替换为 $f_{\gamma i}(t)$ (例如， $f_{\gamma i}(t)=\gamma_{i 1}(t-\bar{t})+\gamma_{i 2}\left(t^{2}-\bar{t}^{2}\right)$)。

## 5. WFE 与组内匹配估计量的应用

### 5.1 估计加入 GATT 对双边贸易量的影响

我们通过估计 GATT (关税及贸易总协定) 成员对双边贸易量的影响并比较多种固定效应模型的结果来阐述上述提出的 WFE 估计量。结果表明，不同的因果假设会导致格外不同的结果，BA 设计表明：双方都为 GATT 成员平均上轻微增加了双边贸易量。

我们使用的数据集包含 1948 - 1994 年间的 10,289 对双边组合，共有 196,207 条观测。处理变量定义如下：

$$
X_{i t}=\left\{\begin{array}{ll}
1 & \text { 双边组合中的两个国家均为 GATT 的成员 } \\
0 & \text { otherwise }
\end{array}\right.
$$

对于任意一对双边组合，处理状态最多变化一次，且变化方向仅为控制组到处理组。接下来，我们考虑不同的识别策略。

我们首先考虑 Rose (2007) [<sup>6</sup>](#refer-anchor) 使用的线性固定效应模型：

$$
\log Y_{i t}=\alpha_{i}+\beta X_{i t}+\delta^{\top} Z_{i t}+\epsilon_{i t}
\quad (25)$$

其中， $X_{i t}$ 是第 $t$ 年第 $i$ 对双边组合的二值处理变量， $Y_{i t}$ 是双边贸易量， $Z_{i t}$ 代表了一系列随时间变化的混淆变量：普遍优惠制 (GSP)、实际 GDP 的对数、实际人均 GDP 的对数、地区自由贸易协定、货币联盟和是否为殖民地。**正如上文所讨论的，这个模型的优点是能够调整不可观测的、不随时间变化的混淆变量。** 模型回归结果如下：

```r
## Standard: dyad fixed effect
F2A.U.std <- wfe(ltrade ~ gattff + gsp + lrgdp + lrgdppc +
                 regional+ custrict + curcol,
                 unit.index = "dyad", time.index="year",
                 treat="gattff", method = "unit", data = Data,
                 unweighted =TRUE,
                 hetero.se = TRUE, auto.se = TRUE,
                 verbose = TRUE, White = TRUE)
options(digits = 3)
print(summary(F2A.U.std))
```

```r
Coefficients:
         Estimate Std.Error t.value p.value    
gattff    -0.0477    0.0242   -1.97   0.049 *  
gsp        0.1145    0.0236    4.86 1.2e-06 ***
lrgdp     -0.0587    0.0302   -1.94   0.052 .  
lrgdppc    0.9215    0.0436   21.13 < 2e-16 ***
regional   0.6705    0.0919    7.30 2.9e-13 ***
custrict   0.6394    0.1192    5.36 8.2e-08 ***
curcol     0.3635    0.1544    2.35   0.019 *  
---
Signif. codes:  
0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

如 gattff 变量估计结果所示，双方都为正式成员平均上显著减少了双边贸易量。

接下来，我们逐步改进 LIN-FE 。首先，我们放宽线性假设。**正如命题一所说的，当不同双边组合的处理效应和处理分配概率不同时，线性假设会导致偏差。** 因此，我们采用 (11) 式所示的非参匹配估计量。此时，回归结果如下：

```r
## ATE: dyad fixed effect
F2A.U.ate <- wfe(ltrade ~ gattff + gsp + lrgdp + lrgdppc +
                 regional+ custrict + curcol,
                 unit.index = "dyad", time.index="year",
                 treat="gattff", method = "unit", data = Data,
                 qoi = "ate",
                 hetero.se = TRUE, auto.se = TRUE,
                 verbose = TRUE, White = TRUE)
options(digits = 3)
print(summary(F2A.U.ate))
```

```r
Coefficients:
         Estimate Std.Error t.value p.value    
gattff    -0.0686    0.0219   -3.13  0.0017 ** 
gsp        0.0425    0.0354    1.20  0.2292    
lrgdp      0.0293    0.0415    0.71  0.4793    
lrgdppc    0.8372    0.0593   14.11 < 2e-16 ***
regional   0.9695    0.0979    9.90 < 2e-16 ***
custrict   0.4186    0.1522    2.75  0.0059 ** 
curcol     0.6159    0.1162    5.30 1.2e-07 ***
---
Signif. codes:  
0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

非参匹配估计量结果显示，双方都为正式成员对贸易量有着显著的负向影响。

由于任何双边组合的处理状态最多只变化一次，非参匹配估计量意味着组内较前时期的控制组观测和后期处理组观测的比较。然而，由于数据集跨越了一段相对较长的时期，因此有些处理组观测可能在时间上与其匹配的控制组观测离得太远。例如，使用观测到的 1950 年的贸易量来估计 1994 年的反事实贸易量是不可信的。

为了增加处理组和控制组观测之间的可比性，我们采用一阶差分估计量，比较处理状态变化前后的两期间的观测 (如 (16) 式所示) 。回归结果如下：

```r
## FD: dyad fixed effect
## regional should be excluded from the full model with no variation
F2A.U.fd <- wfe(ltrade ~ gattff + gsp + lrgdp + lrgdppc +
                custrict + curcol,
                unit.index = "dyad", time.index="year",
                treat="gattff", method = "unit", data = Data,
                qoi = "ate", estimator = "fd",
                hetero.se= TRUE, auto.se= TRUE,
                verbose = TRUE, White = TRUE)
options(digits = 3)
print(summary(F2A.U.fd))
```

```r
Coefficients:
         Estimate Std.Error t.value p.value    
gattff     0.0749    0.0526    1.42 0.15435    
gsp       -0.1286    0.1368   -0.94 0.34709    
lrgdp     -0.3245    1.2669   -0.26 0.79785    
lrgdppc    1.2214    1.2701    0.96 0.33622    
custrict   0.3654    0.1071    3.41 0.00064 ***
curcol     0.0362    0.0377    0.96 0.33608    
---
Signif. codes:  
0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
White statistics for functional misspecification: 19.1 with Pvalue= 0.00399
Reject the null of NO misspecification: TRUE
```

结果显示，双方都为正式成员对贸易量的影响在统计上不显著。怀特 p 值代表了模型识别检验的 p 值，原假设为线性固定效应模型是正确的，而 0.004 的 p 值拒绝了原假设。

在使用 FD 估计量时，我们需要无时间趋势的假设，因为我们使用处理状态变化前一期的控制组观测来估计处理状态下的反事实结果。尽管这个识别策略更加可信，但是无时间趋势的假设可能过于严格：在现实中贸易量总体上随时间增加。当研究者希望估计长期影响 (如 (18) 式所示) 而不是同期影响时，这个问题尤其地突出。

因此，我们通过在匹配集合中加入更多处理状态变化之前的控制组观测 (如 (17) 式所示) 来使一阶差分估计量更加一般化。通过匹配和加权固定效应估计量间的等价关系，我们可以调整双边贸易量的时间趋势。如下，我们在加权固定效应估计量中加入时间趋势的二次项：

$$
\begin{aligned}
\hat{\beta}_{\mathrm{BA}}=& \underset{\beta}{\operatorname{argmin}} \sum_{i=1}^{N} \sum_{t=1}^{T} W_{i t}\left\{\left(Y_{i t}-\bar{Y}_{i}\right)\right.\left.-\beta\left(X_{i t}-\bar{X}_{i}\right)-\gamma_{1}\left(t-\bar{t}_{i}\right)-\gamma_{2}\left(t^{2}-\bar{t}_{i}^{2}\right)\right\}^{2}
\end{aligned}
\quad (26)$$

其中， $W_{i t}$ 如 (24) 式所示。回归结果显示，无论加入多少阶滞后项时，双方都为正式成员对贸易量的同期影响均较小且不显著。

需要注意的是，上述的线性固定效应模型和 BA 设计都做出了如下假设：

1. 不存在不可观测的、随时间变化的混淆变量；
2. 过去的结果变量不会混淆当期处理变量和结果变量间的因果关系；
3. 个体间相互独立。

这些假设可能不大符合实际。研究发现，过去对双边贸易的参与度会影响国家参与 GATT 的动机，即过去的结果变量 (双边贸易量) 影响现在的处理变量 (是否加入 GATT ) 。我们假定这些假设成立并专注于上文提到的对线性固定效应估计量的改进。

### 5.2 总结

Imai 和 Kim 的这篇文章主要贡献在于两点：1、对个体固定效应模型所需要的因果识别假设做了充分的分析和说明；2、提出新的非参匹配框架，改进线性固定效应模型，并通过匹配估计量和加权固定效应估计量间的等价关系，实现多种识别策略和模型推断等。

明确固定效应模型的因果识别假设对于研究者得到稳健的因果推断有着重要意义。例如， Sutherland (2020) [<sup>7</sup>](#refer-anchor) 利用双向固定效应模型估计前瞻性指引 (forward guidance) 改变对预测者利率预测变化的影响时，需要假设不存在其他不可观测的、随时间变化的混淆变量，私营部门利率预测的变化不会影响未来央行前瞻性指引的修订 (过去的结果不影响未来的处理变量) ，过去前瞻性指引的变化不会直接影响当期利率预测的变化 (过去的处理变量不影响现在的结果) 。

<div id="refer-anchor"></div>

## 参考文献

- Pearl, J. (2009). Introduction to Probabilities, Graphs, and Causal Models. In Causality (pp. 1-40). Cambridge: Cambridge University Press. doi:10.1017/CBO9780511803161.003

- Pearl, J. (2009). Causality and Structural Models in Social Science and Economics. In Causality (pp. 133-172). Cambridge: Cambridge University Press. doi:10.1017/CBO9780511803161.007

- 张星健，Dev, Labor, Economy 知乎专栏，[因果推断，选择偏误与随机试验](https://zhuanlan.zhihu.com/p/33299957)

- 赵西亮. 基本有用的计量经济学. 北京大学出版社, 2017.

- Chernozhukov, Victor, Iván Fernández‐Val, Jinyong Hahn, and Whitney Newey. 2013. “Average and Quantile Effects in Nonseparable Panel Models.” Econometrica 82(2): 535–80.

- Rose, Andrew K. 2007. “Do We Really Know That the WTO Increases Trade? Reply.” American Economic Review 97(5): 2019–25.

- Sutherland, Christopher, Forward Guidance and Expectation Formation: A Narrative Approach (January 19, 2020). Available at SSRN: https://ssrn.com/abstract=3522229 or http://dx.doi.org/10.2139/ssrn.3522229

