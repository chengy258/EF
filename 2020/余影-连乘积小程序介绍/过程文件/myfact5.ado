*在保留自定义结果显示格式的基础上，产生新变量
*------------------------begin---myfact5.ado----------------------*
capture program drop myfact5
program define myfact5
version 15

syntax anything(name=k) [, GENerate(string) Format(string)]

  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  
  if "`generate'" != ""{
    gen `generate' = `a'
  }
  else{
    dis "`k'! = " `format' `a'
  }
  
end 
*------------------------end-----myfact5.ado----------------------*
