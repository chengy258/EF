



# JFE最高引文献-EF

**任务：** 写一篇推文列示这个论文清单，提供 [微软学术](https://academic.microsoft.com) 链接和 PDF 原文链接。

- [JFE-Top Cited Papers: 1974-2012](http://jfe.rochester.edu/allstar.htm)

> 任务：整理出一份参考文献清单即可。以 「Ownership Structure and the Cost of Corporate Borrowing JFE」 为例，可以通过 [微软学术](https://academic.microsoft.com)  搜索获得这篇文章的 DOI 和 链接地址，最终得到论文的引文格式如下：


最终的推文类似于下面这篇推文，

- [金融领域引用率最高的50篇论文](https://www.lianxh.cn/news/1b0b0a25b5133.html)


但是，这篇推文的参考文献格式不太规范。你要用「微软学术」( https://academic.microsoft.com/home ) 搜索文章的标题，然后把网址贴入如下链接中：


- Lin, Chen, Yue Ma, Paul H. Malatesta, and Yuhai Xuan. 2011. “Ownership Structure and the Cost of Corporate Borrowing.” Journal of Financial Economics 100 (1): 1–23. [-PDF-](http://sci-hub.ren/10.1016/J.JFINECO.2010.10.012)，[-Link-](https://academic.microsoft.com/paper/2125728726)
- Myers, Stewart C. 1977. “Determinants of Corporate Borrowing.” Journal of Financial Economics 5 (2): 147–75. [-PDF-](http://sci-hub.ren/10.1016/0304-405X(77)90015-0)，[-Link-](https://academic.microsoft.com/paper/2000578291)
- xxx. [-PDF-]()，[-Link-]()







&emsp;

